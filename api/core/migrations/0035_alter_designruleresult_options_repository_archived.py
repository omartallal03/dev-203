# Generated by Django 4.1.4 on 2022-12-07 15:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0034_remove_apidesignruletestsuite_api_endpoint_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='designruleresult',
            options={'ordering': ('rule_type_name',)},
        ),
        migrations.AddField(
            model_name='repository',
            name='archived',
            field=models.BooleanField(default=False),
        ),
    ]
