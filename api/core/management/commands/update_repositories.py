import sys

from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_duration

from api import settings
from core.avatar import download_gitlab_avatar, cleanup_avatars
from core.models.repository import Repository
from core.repository import (
    repositories_needing_update, get_repository_updater, SourceException, RateLimitException
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        interval = parse_duration(settings.REPOSITORY_UPDATE_INTERVAL)
        repositories = repositories_needing_update(interval)

        if repositories.count() == 0:
            self.stdout.write('No repositories need updates')

        has_failures = False
        rate_limit_hit = {}

        for repository in repositories:
            if rate_limit_hit.setdefault(repository.source, False):
                self.stdout.write(f'Skipping {repository.source}: {repository} due to rate limit')
                continue

            self.stdout.write(f'Updating {repository.source}: {repository}...', ending='')

            updater = get_repository_updater(repository.source)

            try:
                updater.update_repository(repository)
                self.stdout.write(' done')
                if repository.source == Repository.Source.GITLAB and repository.avatar_url:
                    self.stdout.write('  Updating avatar...')
                    download_gitlab_avatar(repository)
            except RateLimitException:
                rate_limit_hit[repository.source] = True
                self.stdout.write(' rate limit hit')
            except SourceException as exception:
                self.stdout.write()
                self.stdout.write(f'  Error: {exception}')
            except Exception as exception:
                has_failures = True
                self.stdout.write()
                self.stdout.write(f'  Unexpected error: {exception}')

        self.stdout.write('Cleaning up avatars...', ending='')
        deleted_count = cleanup_avatars()
        self.stdout.write(f' {deleted_count} deleted')

        if has_failures:
            sys.exit(1)
