import sys

from django.conf import settings
from django.core.management.base import BaseCommand

from core.adr import get_apis_to_validate, validate_api, ValidationError


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--adr-validator-path", default=settings.ADR_VALIDATOR_PATH)

    def handle(self, *args, **options):
        apis = get_apis_to_validate()

        has_failures = False

        for api, env in apis:
            self.stdout.write(f"Validating API: {api} ({env.api_url})")
            try:
                validate_api(api, env, options["adr_validator_path"])
            except ValidationError as e:
                self.stdout.write(f"Validation failed: {e}", self.style.ERROR)
            except Exception as e:
                has_failures = True
                self.stderr.write(f"Unexptected error: {e}", self.style.ERROR)

        if has_failures:
            sys.exit(1)
