import os
from pathlib import Path
import time

import requests

from api.settings import AVATARS_DIR, AVATAR_TTL
from core.models.repository import Repository


def download_gitlab_avatar(repository: Repository):
    if repository.source != Repository.Source.GITLAB or repository.avatar_url is None:
        return

    directory = (Path(AVATARS_DIR) / "gitlab" / str(repository.owner_name)).resolve()
    file = directory / str(repository.name)

    if file.exists():
        stat = file.stat()
        if stat.st_mtime > (time.time() - AVATAR_TTL):
            return
    else:
        directory.mkdir(mode=0o755, parents=True, exist_ok=True)

    response = requests.get(repository.avatar_url)
    if response.ok:
        with file.open("wb+") as f:
            f.write(response.content)


def cleanup_avatars() -> int:
    cutoff = (time.time() - AVATAR_TTL) + 60
    root = Path(AVATARS_DIR).resolve()

    delete_count = 0

    for root, _, files in os.walk(root, topdown=False):
        for file in files:
            path = os.path.join(root, file)
            if os.stat(path).st_mtime < cutoff:
                os.unlink(path)
                delete_count += 1

    return delete_count
