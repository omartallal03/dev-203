from django.db.models import Count, F, Q
from django.db.models import QuerySet
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework.viewsets import GenericViewSet

from core.pagination import StandardResultsSetPagination


class FacetMixin(GenericViewSet):
    pagination_class = StandardResultsSetPagination

    text_search_fields = ()
    supported_facets = {}

    def list(self, request: Request, *args, **kwargs):
        queryset: QuerySet = self.filter_queryset(self.get_queryset())

        search_filter = Q()
        search_text = request.query_params.get('q')

        if search_text:
            for search_item in search_text.split(' '):
                if not search_item:
                    continue
                item_filter = Q()
                for field_name in self.text_search_fields:
                    item_filter |= Q(**{f'{field_name}__icontains': search_item})
                search_filter &= item_filter

        facet_inputs = {
            f: request.query_params.getlist(f.replace("__", "_")) for f in self.supported_facets}
        facet_filters = build_facet_filters(facet_inputs)

        facets = self._build_facets(queryset, facet_filters, search_filter)

        search_filter &= Q(*facet_filters.values())
        results = queryset.filter(search_filter)

        return self._create_response(results, facets)

    def _create_response(self, results_queryset: QuerySet, facets: dict):
        page = self.paginate_queryset(results_queryset)
        serializer: Serializer = self.get_serializer(page, many=True)
        paginated_response = self.get_paginated_response(serializer.data)

        response_data = paginated_response.data.copy()
        response_data['facets'] = facets

        return Response(response_data)

    def _build_facets(self, queryset: QuerySet, facet_filters: dict, search_filter: Q):
        facets = {}

        for facet, display_name in self.supported_facets.items():
            other_facet_filters = [
                v for k, v in facet_filters.items()
                if k != facet and v is not None]

            combined_filter = Q(search_filter, *other_facet_filters)

            field_vals = {"term": F(facet)}

            if display_name:
                field_vals["display_name"] = F(display_name)

            order_by = 'display_name' if display_name else 'term'

            term_counts = queryset \
                .values(**field_vals) \
                .annotate(count=Count('id', filter=combined_filter or None)) \
                .exclude(term__isnull=True) \
                .order_by(order_by)

            facets[facet.replace("__", "_")] = {'terms': list(term_counts)}

        return facets


def build_facet_filters(facet_inputs: dict) -> dict:
    return {
        facet: Q(**{f'{facet}__in': selected_values})
        for facet, selected_values in facet_inputs.items()
        if selected_values
    }
