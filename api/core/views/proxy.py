from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponse, QueryDict
from django.views.generic import TemplateView
import requests


class ProxyView(TemplateView):
    """ Generic view to proxy a request. Only supports GET for now """

    remote_url = None
    format_param_name = "format"
    format = None
    template_name = 'core/proxy.html'
    docs = ""

    def get_remote_url(self):
        if self.remote_url is None:
            raise ImproperlyConfigured(
                "ProxyView requires either a definition of "
                "'remote_url' or an implementation of 'get_remote_url()'")
        return self.remote_url

    def get_query_params(self):
        params = self.request.GET.copy()
        format_vals = params.pop(self.format_param_name, None)
        if format_vals and format_vals[0] in ['json', 'html']:
            self.format = format_vals[0]
        return params

    def get_format(self):
        if self.format:
            return self.format
        accept_val = self.request.headers.get('Accept')
        if accept_val:
            for accept_type in accept_val.split(","):
                if accept_type.startswith("text/html"):
                    return 'html'
                if accept_type.startswith("application/json"):
                    return 'json'
        return 'json'

    def get_request_headers(self):
        return {}

    def transform_content(self, content, pretty=False):  # noqa
        return content

    def get_content_type(self):
        return self.remote_response.headers.get('content-type')

    def get_context_data(self, **kwargs):
        # prepare request and send to remote url
        self.remote_url = self.get_remote_url()
        headers = self.get_request_headers()
        query_params = self.get_query_params()
        if query_params:
            rq = QueryDict(mutable=True)
            rq.update(query_params)
            self.remote_query_string = rq.urlencode()
        else:
            self.remote_query_string = ""
        self.remote_response = requests.get(self.remote_url, params=query_params, headers=headers)
        return super().get_context_data(**kwargs)

    def render_to_response(self, context, **response_kwargs):
        fmt = self.get_format()
        self.content = self.transform_content(
            self.remote_response.content, pretty=(fmt == 'html'))
        status_code = self.remote_response.status_code

        if fmt == 'html':
            self.content_type = 'text/html'
            response_kwargs["status"] = status_code
            return super().render_to_response(context, **response_kwargs)

        return HttpResponse(
            content=self.content, content_type=self.content_type, status=status_code)
