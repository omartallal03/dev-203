import logging
import datetime

import requests
from django.utils import timezone
from django.urls import reverse
from django.template.loader import render_to_string
from django.core.exceptions import ObjectDoesNotExist, ImproperlyConfigured
from django.db.models import F, Prefetch
from django.http import HttpResponse
from requests import RequestException, Timeout
from rest_framework import status
from rest_framework.exceptions import NotFound, APIException
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from core.exceptions import APIStatusCodeException
from core.models.api import API, Relation, Environment
from core.models.event import Event
from core.models.repository import Repository, RepositoryProgrammingLanguage
from core.pagination import StandardResultsSetPagination
from core.serializers import APISerializer, EventSerializer, RepositorySerializer
from core.gitlab import create_issue
from core.views.mixins import FacetMixin

__all__ = [
    'APIViewSet', 'APIForumPostsView', 'APIImplementedByView', 'APISpecificationView',
    'EventViewSet', 'RepositoryViewSet', 'proxy_url']

REQUEST_TIMEOUT_SECONDS = 10

logger = logging.getLogger(__name__)


class APIViewSet(RetrieveModelMixin, FacetMixin):
    """ Provides a list of APIs and corresponding totals for facets

    Results can be filtered as follows

    * q=[text fragment]
    * organization_name=[organization name]
    * api_type=[api_type]
    """
    queryset = (
        API.objects
           .prefetch_related("badges", "environments", "referenced_apis")
           .select_related("organization")
           .order_by('api_id')
           .with_last_session()
    )
    serializer_class = APISerializer
    lookup_field = 'api_id'
    text_search_fields = (
        'service_name',
        'description',
        'organization__name',
        'api_type',
    )
    supported_facets = {
        'api_type': None,
        'organization__ooid': 'organization__name',
    }


class APIImplementedByView(APIView):
    def get(self, request, api_id):
        apis = API.objects \
            .filter(relations_from__to_api=api_id,
                    relations_from__name=Relation.TYPE_REFERENCE_IMPLEMENTATION) \
            .order_by('api_id')

        serializer = APISerializer(apis, many=True)
        return Response(serializer.data)


class APIForumPostsView(APIView):
    def get(self, request, api_id):
        try:
            api = API.objects.get(api_id=api_id)
        except ObjectDoesNotExist as e:
            raise NotFound(detail='API not found') from e

        if not api.forum_url:
            raise NotFound(detail=f'API {api_id} does not have forum integration configured')
        return proxy_url(api.forum_url + '.json', 'forum integration')


class APISpecificationView(APIView):
    def get(self, request, api_id, environment):
        try:
            env_type = Environment.EnvironmentType(environment)
            env = Environment.objects.filter(name=env_type.value, api_id=api_id).get()
        except ValueError as e:
            raise NotFound(detail='Invalid environment type: ' + environment) from e
        except ObjectDoesNotExist as e:
            raise NotFound(detail=f'No environment "{environment}" for api {api_id}') from e

        if not env.specification_url:
            raise NotFound(detail=f'API {api_id} does not have a {environment} specification')
        return proxy_url(env.specification_url, 'specification')


def proxy_url(url, name):
    # pylint complains about the empty `except ValueError`
    # pylint:disable=try-except-raise
    try:
        response = requests.get(url, timeout=REQUEST_TIMEOUT_SECONDS)
        if response.status_code != status.HTTP_200_OK:
            raise APIStatusCodeException(
                detail=f'Failed to retrieve {name} URL at {url} (response code is not 200 OK): '
                       f'{response.status_code}: {response.text}',
                status_code=status.HTTP_502_BAD_GATEWAY)
    except Timeout as e:
        raise APIStatusCodeException(
            detail=f'Failed to retrieve {name} URL at {url} due to timeout',
            status_code=status.HTTP_504_GATEWAY_TIMEOUT) from e
    except ValueError:
        # ValueError indicates an invalid url or invalid arguments, that's a local/server error
        raise
    except RequestException as e:
        # A different RequestException indicates an error at the remote end
        raise APIStatusCodeException(detail=f'Failed to retrieve {name} URL: {e}',
                                     status_code=status.HTTP_502_BAD_GATEWAY) from e

    # Note: Our security middleware inserts X-Content-Type-Options=nosniff here.
    # Our current uses don't depend on the content type header, so sending nosniff unconditionally
    # is the simplest/safest option.
    return HttpResponse(response.content, content_type=response.headers.get('Content-Type'))


class EventViewSet(GenericViewSet, ListModelMixin, CreateModelMixin):
    serializer_class = EventSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        twentyfour_hours_ago = timezone.now() - datetime.timedelta(hours=24)
        return Event.objects.filter(start_date__gt=twentyfour_hours_ago, is_published=True)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)
        event = serializer.data

        context = {
            'event': event,
            'manage_url': self.request.build_absolute_uri(
                reverse('admin:core_event_change', args=[event.get('id')])
            )
        }

        title = render_to_string('issues/event_title.txt', context)
        content = render_to_string('issues/event_content.txt', context)

        try:
            result = create_issue(title, content, 'New Event')
        except ImproperlyConfigured as e:
            logger.error(e)
            raise APIException(detail='The Gitlab API is not properly configured') from e
        except Exception as e:
            logger.error(e)
            raise APIException(detail='Something went wrong while posting to the GitLab API') \
                from e

        return Response(result, status=status.HTTP_201_CREATED, headers=headers)


class RepositoryViewSet(FacetMixin):
    queryset = (
        Repository.objects.prefetch_related(
            Prefetch(
                'repositoryprogramminglanguage_set',
                queryset=RepositoryProgrammingLanguage.objects.order_by('-usage'),
            ),
            'related_apis',
            'topics'
        )
        .order_by(F('last_change').desc(nulls_last=True))
    )
    serializer_class = RepositorySerializer
    text_search_fields = (
        'owner_name',
        'name',
    )
    supported_facets = {
        'programming_languages': 'programming_languages__name',
        'topics': 'topics__name',
    }
