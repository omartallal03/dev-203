from collections import defaultdict
from datetime import timedelta
import logging

from influxdb_client.client.flux_table import FluxRecord, TableList
from rest_framework import status
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework.views import APIView

from core.metrics import execute_query, new_influxdb_client


logger = logging.getLogger(__name__)

TIME_RANGES = {
    "last-month": (-30, timedelta(days=1)),
    "last-year": (-365, timedelta(days=30)),
}

DEFAULT_TIME_RANGE = "last-month"


class MetricsView(APIView):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self._client = new_influxdb_client()
        self._query_api = self._client.query_api()

    def get(self, request: Request):
        time_range = request.query_params.get("time-range", DEFAULT_TIME_RANGE)
        time_range_params = TIME_RANGES.get(time_range, None)

        if time_range_params is None:
            raise ParseError("Incorrect value for time-range parameter")

        count_tables = execute_query("total_counts", self._query_api, time_range_params[0], time_range_params[1])
        if count_tables is None:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

        design_rule_tables = execute_query(
            "design_rules_last", self._query_api, time_range_params[0], time_range_params[1])
        if design_rule_tables is None:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

        design_rule_api_successes_tables = execute_query(
            "design_rules_api_successes_last", self._query_api, time_range_params[0], time_range_params[1])
        if design_rule_api_successes_tables is None:
            return Response(status=status.HTTP_503_SERVICE_UNAVAILABLE)

        data = serialize_total_counts(count_tables)
        data["design_rules_last"] = serialize_design_rules(design_rule_tables)
        data["design_rules_api_successes_last"] = serialize_design_rules_api_successes(design_rule_api_successes_tables)

        return Response(data)


def serialize_total_counts(tables: TableList) -> dict:
    output = {
        "organization": {
            "total_counts": {},
        },
        "api": {
            "total_counts": {},
        },
        "repository": {
            "total_counts": {},
        },
    }

    for table in tables:
        record: FluxRecord
        for record in table.records:
            # In the frond-end the counts are displayed per day only.
            # So we're shifting the time of the metric with with 1 day and use only the date part.
            # This is tied to the Influx query because of the grouping per day.
            #
            # 2022-07-04 00:00:00+00:00 -> 2022-07-03
            date = (record.get_time() - timedelta(days=1)).date()

            output[record.get_measurement()]["total_counts"][date.isoformat()] = int(record.get_value())

    return output


def serialize_design_rules(tables: TableList) -> dict:
    output = defaultdict(lambda: defaultdict(dict))

    for table in tables:
        record: FluxRecord
        for record in table.records:
            field = record.get_field()
            output[record["version"]][record["rule"]][field] = int(record.get_value())

    return output


def serialize_design_rules_api_successes(tables: TableList) -> dict:
    successes_last = defaultdict(dict)
    output = defaultdict(dict)

    for table in tables:
        record: FluxRecord
        for record in table.records:
            successes_last[record["version"]][record["successes"]] = int(record.get_value())

    for version, counts in successes_last.items():
        design_rule_count = len(counts) - 1
        for successes, count in counts.items():
            label = f"{successes}/{design_rule_count}"
            output[version][label] = count

    return output
