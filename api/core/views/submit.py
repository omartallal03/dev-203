import logging

from django.core.exceptions import ImproperlyConfigured
from django.template.loader import render_to_string
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView
import yaml

from core import gitlab
from core.serializers import APISerializer, SubmitRepositoryForm


logger = logging.getLogger(__name__)


class SubmitAPIView(APIView):
    def post(self, request):

        # The input has no id, which it needs to be a valid API
        data_to_validate = dict(**request.data, id='temporary-id')
        serializer = APISerializer(data=data_to_validate)
        serializer.is_valid(raise_exception=True)

        context = {
            'data': request.data,
            'api_file_contents': yaml.dump(request.data, sort_keys=False),
        }

        title = render_to_string('issues/api_title.txt', context)
        content = render_to_string('issues/api_content.txt', context)

        try:
            result = gitlab.create_issue(title, content, 'New API')
        except ImproperlyConfigured as e:
            logger.error(e)
            raise APIException(detail='The Gitlab API is not properly configured') from e
        except Exception as e:
            logger.error(e)
            raise APIException(detail='Something went wrong while posting to the GitLab API') \
                from e

        return Response(result)


class SubmitRepositoryView(APIView):
    def post(self, request):
        serializer = SubmitRepositoryForm(data=request.data)
        serializer.is_valid(raise_exception=True)

        try:
            result = serializer.save()
        except Exception as e:
            raise APIException(detail='Error saving the repository') from e

        return Response(result)
