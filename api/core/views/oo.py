import json

from django.conf import settings

from .proxy import ProxyView


class OOAPIView(ProxyView):
    remote_url = f"{settings.OO_API_URL}/organisaties"

    def transform_content(self, content, pretty=False):
        if self.remote_response.status_code == 200:
            content = json.loads(content)
            dump_kwargs = {"indent": 2} if pretty else {}
            content = json.dumps(content, **dump_kwargs)

        return content
