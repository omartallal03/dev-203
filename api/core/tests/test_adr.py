import unittest
from unittest.mock import patch, MagicMock

from subprocess import CompletedProcess

from django.test import TestCase

from core.adr import validate_api, run_validator, ValidationError
from core.models.api import API, Environment, Organization
from core.models.design_rule import DesignRuleSession, DesignRuleResult


class ValidateAPI(TestCase):
    @classmethod
    def setUpTestData(cls):
        org = Organization.objects.create(name="Test", ooid=1)
        cls.api = API.objects.create(api_id="test-api", api_type=API.APIType.REST_JSON, organization=org)
        cls.env = Environment.objects.create(name="production", api_url="http://example.com", api=cls.api)

    @patch("core.adr.run_validator")
    def test_first_test(self, run_validator: MagicMock):
        run_validator.return_value = [
            {"rule": "TEST-01", "passed": False, "message": "Did not pass"},
        ]

        validate_api(self.api, self.env, "path")

        run_validator.assert_called_once_with("path", "http://example.com")

        session: DesignRuleSession = self.api.last_design_rule_session()
        self.assertIsNotNone(session)

        results = session.results.all()
        self.assertEqual(results.count(), 1)

        result: DesignRuleResult = results[0]
        self.assertEqual(result.rule_type_name, "TEST-01")
        self.assertEqual(result.success, False)
        self.assertEqual(result.errors, ["Did not pass"])

    @patch("core.adr.run_validator")
    def test_percentage_score(self, run_validator: MagicMock):
        run_validator.return_value = [
            {"rule": "TEST-01", "passed": False, "message": ""},
            {"rule": "TEST-02", "passed": True, "message": ""},
            {"rule": "TEST-03", "passed": True, "message": ""},
            {"rule": "TEST-04", "passed": True, "message": ""},
        ]

        validate_api(self.api, self.env, "")

        session: DesignRuleSession = self.api.last_design_rule_session()
        self.assertEqual(session.percentage_score, 75.0)

        run_validator.return_value = [
            {"rule": "TEST-01", "passed": True, "message": ""},
            {"rule": "TEST-02", "passed": True, "message": ""},
        ]

        validate_api(self.api, self.env, "")

        session: DesignRuleSession = self.api.last_design_rule_session()
        self.assertEqual(session.percentage_score, 100.0)

        run_validator.return_value = [
            {"rule": "TEST-01", "passed": False, "message": ""},
            {"rule": "TEST-02", "passed": False, "message": ""},
        ]

        validate_api(self.api, self.env, "")

        session: DesignRuleSession = self.api.last_design_rule_session()
        self.assertEqual(session.percentage_score, 0.0)

    @patch("core.adr.run_validator")
    def test_other_session(self, run_validator: MagicMock):
        run_validator.return_value = [
            {"rule": "TEST-01", "passed": False, "message": ""},
        ]

        validate_api(self.api, self.env, "")
        session_a: DesignRuleSession = self.api.last_design_rule_session()

        validate_api(self.api, self.env, "")
        session_b: DesignRuleSession = self.api.last_design_rule_session()

        self.assertNotEqual(session_a.pk, session_b.pk)


@patch("subprocess.run")
class RunValidator(unittest.TestCase):
    def test_ok(self, run: MagicMock):
        run.return_value = CompletedProcess("", 0, stdout='{"key":123}', stderr="")

        output = run_validator("", "")

        self.assertDictEqual(output, {"key": 123})

    def test_exit(self, run: MagicMock):
        run.return_value = CompletedProcess("", 1, stdout='{"key":123}', stderr="")

        output = run_validator("", "")

        self.assertDictEqual(output, {"key": 123})

    def test_error(self, run: MagicMock):
        run.return_value = CompletedProcess("", 1, stdout='{"key":123}', stderr="an error")

        with self.assertRaises(ValidationError) as ctx:
            run_validator("", "")

        self.assertEqual(str(ctx.exception), "adr-validator: an error")
