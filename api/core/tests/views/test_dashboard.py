from datetime import datetime
from unittest import TestCase

from influxdb_client.client.flux_table import FluxRecord, FluxTable, TableList

from core.views.dashboard import serialize_total_counts, serialize_design_rules, serialize_design_rules_api_successes


class Dashboard(TestCase):
    def test_total_counts(self):
        t1 = FluxTable()
        t1.records = [
            FluxRecord(table=t1, values={
                "_measurement": "organization", "_value": 1, "_time": datetime(2022, 11, 29, 11, 55, 14),
            }),
            FluxRecord(table=t1, values={
                "_measurement": "api", "_value": 2, "_time": datetime(2022, 11, 28, 0, 0, 0),
            }),
            FluxRecord(table=t1, values={
                "_measurement": "repository", "_value": 3, "_time": datetime(2022, 11, 27, 23, 59, 59),
            }),
        ]

        tables = TableList([t1])
        data = serialize_total_counts(tables)

        self.assertEqual(3, len(data))
        self.assertDictEqual(data["organization"], {"total_counts": {"2022-11-28": 1}})
        self.assertDictEqual(data["api"], {"total_counts": {"2022-11-27": 2}})
        self.assertDictEqual(data["repository"], {"total_counts": {"2022-11-26": 3}})

    def test_design_rules(self):
        t1 = FluxTable()
        t1.records = [
            FluxRecord(table=t1, values={"_field": "f1", "version": "v1", "rule": "r1", "_value": 1}),
            FluxRecord(table=t1, values={"_field": "f1", "version": "v1", "rule": "r2", "_value": 2}),
            FluxRecord(table=t1, values={"_field": "f1", "version": "v2", "rule": "r3", "_value": 3}),
            FluxRecord(table=t1, values={"_field": "f1", "version": "v3", "rule": "r4", "_value": 4}),
            FluxRecord(table=t1, values={"_field": "f2", "version": "v3", "rule": "r4", "_value": 5}),
        ]

        tables = TableList([t1])
        data = serialize_design_rules(tables)

        self.assertEqual(3, len(data))
        self.assertDictEqual(data["v1"], {"r1": {"f1": 1}, "r2": {"f1": 2}})
        self.assertDictEqual(data["v2"], {"r3": {"f1": 3}})
        self.assertDictEqual(data["v3"], {"r4": {"f1": 4, "f2": 5}})

    def test_design_rules_api_successes(self):
        t1 = FluxTable()
        t1.records = [
            FluxRecord(table=t1, values={"version": "t1", "successes": 0, "_value": 12}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 1, "_value": 34}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 2, "_value": 56}),
            FluxRecord(table=t1, values={"version": "t1", "successes": 3, "_value": 78}),
        ]

        t2 = FluxTable()
        t2.records = [
            FluxRecord(table=t2, values={"version": "t2", "successes": 0, "_value": 12}),
            FluxRecord(table=t2, values={"version": "t2", "successes": 1, "_value": 42}),
            FluxRecord(table=t2, values={"version": "t2", "successes": 2, "_value": 0}),
        ]

        tables = TableList([t1, t2])
        data = serialize_design_rules_api_successes(tables)

        self.assertEqual(2, len(data))
        self.assertDictEqual(data["t1"], {"0/3": 12, "1/3": 34, "2/3": 56, "3/3": 78})
        self.assertDictEqual(data["t2"], {"0/2": 12, "1/2": 42, "2/2": 0})
