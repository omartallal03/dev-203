import json
from unittest.mock import patch
import textwrap

from django.test import SimpleTestCase


class SubmitRepositoryViewTest(SimpleTestCase):
    def setUp(self):
        # Display whole JSON diffs
        self.maxDiff = None

    @patch('core.gitlab.create_issue')
    def test_submit_valid(self, mock_create_issue):
        mock_create_issue.return_value = {}

        request_data = {
            'organization_name': 'Test Organization',
            'organization_oin': '00001234567890123456',
            'repository_type': 'gitlab',
            'repository_full_path': 'test-organization/test-project',
        }

        self.client.post(
            '/api/submit-repository',
            data=json.dumps(request_data),
            content_type='application/json')

        create_issue_args = mock_create_issue.call_args[0]
        title = create_issue_args[0]
        description = create_issue_args[1]
        labels = create_issue_args[2]

        expected_yaml = textwrap.dedent('''\
            ```yaml
            organization:
              name: Test Organization
              oin: "00001234567890123456"
            repositories:
              - gitlab: test-organization/test-project
            ```
            ''')

        self.assertEqual('Add a new repository: gitlab:test-organization/test-project', title)
        self.assertIn(expected_yaml, description)
        self.assertEqual('New repository', labels)
