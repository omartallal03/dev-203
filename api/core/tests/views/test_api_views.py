import json

from django.utils import timezone
from django.test import TestCase

from core.models.api import API
from core.models.design_rule import APIDesignRuleTestSuite, DesignRuleSession, DesignRuleResult
from core.models.organization import Organization
from core.serializers import APISerializer
from core.tests.utils import prevent_logging

API_PATH = '/api/apis'


class APIViewTest(TestCase):
    def setUp(self):
        org = Organization.objects.create(name='Test Organization', ooid=1)
        self.api1 = API.objects.create(
            api_id='api1',
            description='First API',
            organization=org,
            service_name='First Service',
            api_type=API.APIType.REST_JSON,
            api_authentication='none',
            is_reference_implementation=False,
            contact_email='contact@api1.com',
        )
        self.api2 = API.objects.create(
            api_id='api2',
            description='Second API',
            organization=org,
            service_name='Better Service',
            api_type=API.APIType.GRAPHQL,
            api_authentication='unknown',
            is_reference_implementation=True,
            contact_email='contact@api2.com',
        )

        session_1 = DesignRuleSession.objects.create(
            test_suite=APIDesignRuleTestSuite.objects.create(api=self.api1),
            started_at=timezone.now())
        DesignRuleResult.objects.create(session=session_1, rule_type_name="TEST-01", success=True)

        session_2 = DesignRuleSession.objects.create(
            test_suite=APIDesignRuleTestSuite.objects.create(api=self.api2),
            started_at=timezone.now())
        DesignRuleResult.objects.create(session=session_2, rule_type_name="TEST-01", success=True)

        # Display whole JSON diffs
        self.maxDiff = None

    def test_get_single(self):
        response = self.client.get(API_PATH + '/api2')

        self.assertEqual(response.status_code, 200)

        expected = json.dumps(APISerializer(self.api2).data)

        self.assertJSONEqual(response.content, expected)

    @prevent_logging
    def test_post_not_allowed(self):
        response = self.client.post(API_PATH)

        self.assertEqual(response.status_code, 405)

    @prevent_logging
    def test_put_not_allowed(self):
        response = self.client.put(API_PATH + '/api3')

        self.assertEqual(response.status_code, 405)

    @prevent_logging
    def test_patch_not_allowed(self):
        response = self.client.patch(API_PATH + '/api3')

        self.assertEqual(response.status_code, 405)

    @prevent_logging
    def test_delete_not_allowed(self):
        response = self.client.delete(API_PATH + '/api3')

        self.assertEqual(response.status_code, 405)

    @prevent_logging
    def test_adr_testable(self):
        response = self.client.get(API_PATH + '/api1')
        self.assertTrue("design_rule_scores" in response.data)
        self.assertEqual(1, len(response.data["design_rule_scores"]["results"]))

    @prevent_logging
    def test_not_adr_testable(self):
        response = self.client.get(API_PATH + '/api2')
        self.assertTrue("design_rule_scores" not in response.data)
