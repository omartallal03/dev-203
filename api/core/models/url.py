from django.db import models
from django.db.models.functions import Concat
from django.utils import timezone
from django.utils.html import format_html

from . import MAX_ENUM_LENGTH, MAX_URL_LENGTH
from .api import API


class URLQuerySet(models.QuerySet):
    """ Custom queryset to provide handy shortcuts """

    def with_last_probe(self):
        """ Adds the most recent design rule session efficiently to the queryset """

        # Using postgres specific "DISTINCT ON". In combination with the order_by clause this
        # queries for the most recent probe per test url
        probe_qs = URLProbe.objects.order_by("url", "-timestamp").distinct("url")

        # Add the most recent probe to the original queryset. It will be available as
        # '_last_probe' attribute on the url object
        pf = models.Prefetch('urlprobe_set', queryset=probe_qs, to_attr='_prefetched_last_probe')
        return self.prefetch_related(pf)


class URL(models.Model):
    _last_probe = None

    objects = URLQuerySet.as_manager()

    url = models.CharField(max_length=MAX_URL_LENGTH, unique=True)
    # Todo: I expect that the number of uptime probes will become large, so we may want to cache
    #  some statistics about each url so we don't have to scan all uptime probes for a page load.
    #  However that can only be implemented once we know how we want to use the uptimes.

    class _DummyProbe:
        timestamp = None
        def ok(self): return None  # noqa
        def errmsg(self): return None  # noqa

    def last_probe(self):
        if hasattr(self, "_prefetched_last_probe"):
            if self._prefetched_last_probe:
                return self._prefetched_last_probe[0]
            return None
        if self._last_probe is None:
            self._last_probe = self.urlprobe_set.order_by('-timestamp').first() \
                or self._DummyProbe()
        return self._last_probe

    def last_timestamp(self):
        return self.last_probe().timestamp
    last_timestamp.short_description = "Last probe timestamp"

    def last_ok(self):
        return self.last_probe().ok()
    last_ok.boolean = True

    def last_errmsg(self):
        return self.last_probe().errmsg()
    last_errmsg.short_description = "Last error message"

    def used_in_api(self):
        return format_html(';<br>\n'.join(
            format_html('{} url for {}', link.field, link.api.api_id)
            for link in self.api_links.all()
        ))
    used_in_api.admin_order_field = 'api_links__api__api_id'

    def __str__(self):
        return self.url

    # Called from apps.CoreConfig.ready()
    # We cannot create querysets while models are being initialized, so these need to be set lazily
    @staticmethod
    def ready_hook():
        last_probe_query = URLProbe.objects.filter(
            url=models.OuterRef('id')).order_by('-timestamp')[:1]

        URL.last_timestamp.admin_order_field = models.Subquery(
            last_probe_query.values('timestamp'))

        URL.last_ok.admin_order_field = models.Subquery(
            last_probe_query.annotate(ok=URLProbe.ok.admin_order_field).values('ok'))

        URL.last_errmsg.admin_order_field = models.Subquery(
            last_probe_query.annotate(errmsg=URLProbe.errmsg.admin_order_field).values('errmsg'))


class URLProbe(models.Model):
    # Factor the url to a separate table to save space
    url = models.ForeignKey(URL, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(default=timezone.now)
    status_code = models.SmallIntegerField(null=True, blank=True)
    error = models.TextField(blank=True)

    def ok(self):
        return self.status_code == 200
    ok.boolean = True
    ok.admin_order_field = models.Case(
        models.When(status_code__exact=200, then=True),
        default=False,
        output_field=models.BooleanField()
    )

    def errmsg(self):
        if self.ok():
            return ''
        if self.status_code is not None:
            return f'HTTP status {self.status_code}'
        return self.error
    errmsg.admin_order_field = models.Case(
        models.When(status_code__exact=200, then=models.Value('')),
        models.When(
            status_code__isnull=False,
            then=Concat(models.Value('HTTP status '), 'status_code')),
        default='error',
        output_field=models.CharField()
    )

    def used_in_api(self):
        return format_html(';<br>\n'.join(
            format_html('{} url for {}', link.field, link.api.api_id)
            for link in self.url.api_links.all()
        ))
    used_in_api.admin_order_field = 'url__api_links__api__api_id'

    def __str__(self):
        status = 'ok' if self.ok() else self.errmsg()
        return f'{self.url.url}: {status}'


class URLApiLink(models.Model):
    class FieldReference(models.TextChoices):
        FORUM = 'forum'
        CONTACT = 'contact'
        PRODUCTION_API = 'production_api'
        PRODUCTION_SPEC = 'production_spec'
        PRODUCTION_DOC = 'production_doc'
        ACCEPTANCE_API = 'acceptance_api'
        ACCEPTANCE_SPEC = 'acceptance_spec'
        ACCEPTANCE_DOC = 'acceptance_doc'
        DEMO_API = 'demo_api'
        DEMO_SPEC = 'demo_spec'
        DEMO_DOC = 'demo_doc'

    url = models.ForeignKey(URL, on_delete=models.CASCADE, related_name='api_links')
    api = models.ForeignKey(API, on_delete=models.CASCADE, related_name='url_links')
    field = models.CharField(max_length=MAX_ENUM_LENGTH, choices=FieldReference.choices)

    def __str__(self):
        return f'{self.field} url for {self.api}'
