from django.db import models
from django.utils.translation import gettext_lazy as _

from . import MAX_TEXT_LENGTH


class OrganizationQuerySet(models.QuerySet):
    def get_by_natural_key(self, ooid):
        return self.get(ooid=ooid)


class Organization(models.Model):
    name = models.CharField(_("name"), max_length=MAX_TEXT_LENGTH)
    ooid = models.PositiveIntegerField(_("ooid"), unique=True, help_text=_("Organisaties overheid ID"))
    active = models.BooleanField(_("active"), default=True)

    objects = OrganizationQuerySet.as_manager()

    def natural_key(self):
        return (self.ooid,)

    class Meta:
        verbose_name = _("organization")
