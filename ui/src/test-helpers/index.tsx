// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { ThemeProvider } from 'styled-components'
import { Queries, render, RenderResult } from '@testing-library/react'
import { BrowserRouter as Router } from 'react-router-dom'
import '@testing-library/jest-dom/extend-expect'

import { mount } from 'enzyme'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Formik } from 'formik'
import theme from '../theme'

export function flushPromises() {
  return new Promise((resolve) => setTimeout(resolve, 0))
}

// based on https://testing-library.com/docs/react-testing-library/setup#custom-render
const AllTheProviders: React.FunctionComponent = ({ children }) => {
  const queryClient = new QueryClient()

  return (
    <Router>
      <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <Formik initialValues={{}} onSubmit={jest.fn()}>
            {children}
          </Formik>
        </QueryClientProvider>
      </ThemeProvider>
    </Router>
  )
}

const renderWithProviders = (
  ui: JSX.Element,
  options: Record<string, string> = {},
): RenderResult<Queries, HTMLElement> =>
  render(ui, { wrapper: AllTheProviders, ...options })

export const renderWithRouter = (
  ui: JSX.Element,
  { route = '/' } = {},
): RenderResult<Queries, HTMLElement> => {
  window.history.pushState({}, 'Test page', route)

  return render(<ThemeProvider theme={theme}>{ui}</ThemeProvider>, {
    wrapper: Router,
  })
}

export const mountWithProviders = (Component: JSX.Element): void => {
  mount(<AllTheProviders>{Component}</AllTheProviders>)
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { renderWithProviders }
