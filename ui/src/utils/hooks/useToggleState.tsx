// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { useState } from 'react'

export default function useToggleState(
  initialState: boolean,
): [boolean, (forcedState: boolean) => void] {
  const [state, setState] = useState(initialState)

  const toggleState = (forcedState: boolean) => {
    setState(forcedState)
  }

  return [state, toggleState]
}
