// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import queryString from 'query-string'

export function generateQueryParams<T extends Record<string, any>>(params: T) {
  return queryString.stringify(params, {
    skipEmptyString: true,
  })
}
