// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { camelCase } from 'change-case'

function objectKeysToCamelCase<T>(
  obj: string | string[] | Record<string, string>,
): T {
  if (obj instanceof Array) {
    return obj.map((value) => {
      return objectKeysToCamelCase(value)
    }) as unknown as T
  }

  if (typeof obj === 'object' && obj !== null) {
    const newObj: Record<string, string> = {}

    Object.entries(obj).forEach(([key, value]) => {
      newObj[camelCase(key)] = objectKeysToCamelCase(value) as string
    })

    return newObj as unknown as T
  }

  return obj as unknown as T
}

export default objectKeysToCamelCase
