// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { snakeCase } from 'change-case'

function objectKeysToSnakeCase<T>(
  obj: string | string[] | Record<string, string>,
): T {
  if (Array.isArray(obj)) {
    return obj.map((value) => {
      return objectKeysToSnakeCase(value)
    }) as unknown as T
  } else if (typeof obj === 'object' && obj !== null) {
    const newObj: Record<string, string> = {}

    Object.entries(obj).forEach(([key, value]) => {
      newObj[snakeCase(key)] = objectKeysToSnakeCase(value) as string
    })

    return newObj as unknown as T
  }

  return obj as unknown as T
}

export default objectKeysToSnakeCase
