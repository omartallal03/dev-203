// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { configure } from 'enzyme'
import '@testing-library/jest-dom'
import { configureCharts } from './containers/App/helpers'

configure({ adapter: new Adapter() })

configureCharts()
