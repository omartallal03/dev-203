// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const GITLAB_REPO_URL =
  'https://gitlab.com/commonground/don/developer.overheid.nl'
