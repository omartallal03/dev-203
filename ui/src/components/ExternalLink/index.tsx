// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import External from '../../assets/icons/External'

interface Props extends React.ComponentProps<'a'> {
  href: string
  title?: string
}

const ExternalLink: React.FunctionComponent<Props> = ({
  href,
  title,
  ...props
}) => {
  return (
    <a href={href} target="_blank" rel="noreferrer" {...props}>
      {title || href}
      <External />
    </a>
  )
}

export default ExternalLink
