// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import * as React from 'react'
import { shallow } from 'enzyme'
import External from '../../assets/icons/External'
import ExternalLink from '.'

const props: React.ComponentProps<typeof ExternalLink> = {
  href: 'https://developer.overheid.nl',
  title: 'Developer Overheid',
}

describe('<ExternalLink />', () => {
  it('renders correctly', () => {
    const component = shallow(<ExternalLink {...props} />)
    expect(component.find('a')).toHaveLength(1)
  })

  it('has target _blank', () => {
    const component = shallow(<ExternalLink {...props} />)
    expect(component.find('a').prop('target')).toEqual('_blank')
  })

  it('has rel noreferrer', () => {
    const component = shallow(<ExternalLink {...props} />)
    expect(component.find('a').prop('rel')).toEqual('noreferrer')
  })

  it('renders the title', () => {
    const component = shallow<typeof ExternalLink>(<ExternalLink {...props} />)
    const text = component.findWhere((node) => {
      return node.text() === props.title
    })

    expect(text).toHaveLength(1)
    expect(text.text()).toEqual('Developer Overheid')
  })

  it('renders the external icon', () => {
    const component = shallow(<ExternalLink {...props} />)
    expect(component.find(External)).toHaveLength(1)
  })
})
