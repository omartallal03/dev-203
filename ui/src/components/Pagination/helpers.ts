// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { isNaN } from 'formik'

export const ELLIPSIS = '...'

export const calculatePages = (
  currentPage: number,
  totalPageCount: number,
): string[] => {
  if (isNaN(currentPage) || isNaN(totalPageCount)) {
    return []
  }

  const allPages = [...Array(totalPageCount).keys()].map((i) => i + 1)
  const pages: string[] = []
  allPages.forEach((page) => {
    if (
      page === 1 ||
      page === totalPageCount ||
      Math.abs(page - currentPage) <= 1
    ) {
      pages.push(page.toString())
    } else {
      pages.push(ELLIPSIS)
    }
  })
  return pages.filter(
    (page, index) => !(page === ELLIPSIS && pages[index + 1] === ELLIPSIS),
  )
}

export const scrollToTop = (): void => {
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth',
  })
}

export const onPreviousPageButtonClickedHandler = (
  currentPage: number,
  onPageChangedHandler: (page: string) => void,
): void => {
  return onPageChangedHandler((currentPage - 1).toString())
}

export const onNextPageButtonClickedHandler = (
  currentPage: number,
  onPageChangedHandler: (page: string) => void,
): void => {
  return onPageChangedHandler((currentPage + 1).toString())
}
