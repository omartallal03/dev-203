// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
export const PAGINATION_OPTIONS = [10, 25, 50, 100, 250, 500]
export const DEFAULT_ROWS_PER_PAGE = '10'
export const DEFAULT_PAGE = '1'
