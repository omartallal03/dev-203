// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import {
  StyledPagination,
  StyledPageButton,
  StyledArrowButton,
  StyledDottedButton,
  SelectField,
  SelectFieldContainer,
  StyledPages,
} from './styles'
import {
  calculatePages,
  ELLIPSIS,
  onNextPageButtonClickedHandler,
  onPreviousPageButtonClickedHandler,
  scrollToTop,
} from './helpers'
import { PAGINATION_OPTIONS } from './constants'

interface PaginationProps {
  currentPage: string
  totalRows: number
  rowsPerPage: number
  onPageChangedHandler: (page: string) => void
  onResultsPerPageChange: (value: string) => void
}

type ResultsPerPageProps = {
  onResultsPerPageChange: (value: string) => void
  totalRows: number
  rowsPerPage: number
}

interface Props {
  resultsPerPage: number[]
  totalRows: number
}

export const RenderOptions: React.FunctionComponent<Props> = ({
  resultsPerPage,
  totalRows,
}) => {
  const options = resultsPerPage.filter((option) => {
    return totalRows % option !== totalRows
  })

  if (options.length === 1 && totalRows > options[0]) {
    options.push(resultsPerPage[1])
  }

  if (!options.length) {
    const option = resultsPerPage[0]
    return <option value={option}>{option}</option>
  }

  return (
    <>
      {options.map((option) => {
        return (
          <option key={option} value={option}>
            {option}
          </option>
        )
      })}
    </>
  )
}

const Pagination: React.FunctionComponent<PaginationProps> & {
  ResultsPerPage: React.FunctionComponent<ResultsPerPageProps>
} = ({
  totalRows,
  rowsPerPage,
  onPageChangedHandler,
  onResultsPerPageChange,
  ...props
}) => {
  const currentPage = parseInt(props.currentPage, 10)
  const totalPageCount = Math.ceil(totalRows / rowsPerPage)
  const pages = calculatePages(currentPage, totalPageCount)

  const isPreviousButtonDisabled = totalPageCount === 0 || currentPage === 1
  const isNextButtonDisabled =
    totalPageCount === 0 || currentPage === totalPageCount

  const handlePageChangedHandler = (page: string) => {
    onPageChangedHandler(page)
    scrollToTop()
  }

  const handleClickBack = () => {
    onPreviousPageButtonClickedHandler(currentPage, handlePageChangedHandler)
  }

  const handleClickNext = () => {
    onNextPageButtonClickedHandler(currentPage, handlePageChangedHandler)
  }

  const renderPages = () => {
    return pages.map((page, index) => {
      if (page === ELLIPSIS) {
        return <StyledDottedButton key={index}>{ELLIPSIS}</StyledDottedButton>
      }

      const isCurrentPage = page === currentPage.toString()

      return (
        <StyledPageButton
          onClick={() => handlePageChangedHandler(page)}
          current={isCurrentPage}
          disabled={isCurrentPage}
          key={`Pagination${page}`}
          aria-label={`Pagina ${page}`}
        >
          {page}
        </StyledPageButton>
      )
    })
  }

  if (totalRows === 0) {
    return null
  }

  return (
    <StyledPagination data-testid="pagination">
      <StyledPages>
        <StyledArrowButton
          disabled={isPreviousButtonDisabled}
          onClick={handleClickBack}
          aria-label="Vorige pagina"
        >
          «
        </StyledArrowButton>
        {renderPages()}
        <StyledArrowButton
          disabled={isNextButtonDisabled}
          onClick={handleClickNext}
          aria-label="Volgende pagina"
        >
          »
        </StyledArrowButton>
      </StyledPages>

      <Pagination.ResultsPerPage
        onResultsPerPageChange={onResultsPerPageChange}
        totalRows={totalRows}
        rowsPerPage={rowsPerPage}
      />
    </StyledPagination>
  )
}

const ResultsPerPage: React.FunctionComponent<ResultsPerPageProps> = ({
  rowsPerPage,
  onResultsPerPageChange,
}) => {
  const renderOptions = () => {
    return PAGINATION_OPTIONS.map((option) => {
      return (
        <option key={option} value={option}>
          {option}
        </option>
      )
    })
  }

  return (
    <SelectFieldContainer>
      <label htmlFor="resultsPerPage">Aantal resultaten per pagina</label>
      <SelectField
        id="resultsPerPage"
        onChange={(e) => onResultsPerPageChange(e.currentTarget.value)}
        value={rowsPerPage}
      >
        {renderOptions()}
      </SelectField>
    </SelectFieldContainer>
  )
}

Pagination.ResultsPerPage = ResultsPerPage

export default Pagination
