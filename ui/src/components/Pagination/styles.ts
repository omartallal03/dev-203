// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../theme/mediaQueries'
import arrowDownSolidIcon from '../../assets/svg/arrow-down-solid-icon.svg'

export const StyledPagination = styled.div`
  display: flex;
  margin-top: ${(p) => p.theme.tokens.spacing10};
  ${mq.xs`
    flex-direction: column;
    align-items: center;
    padding: 0 1rem;
    margin-top: 2rem;
  `}
`

export const StyledPages = styled.div`
  align-items: center;
  display: flex;
  min-height: ${(p) => p.theme.tokens.spacing08};
`

export const StyledPageButton = styled.button<{ current?: boolean }>`
  background-color: ${(p) =>
    p.current ? p.theme.tokens.colorBrand3 : p.theme.tokens.colorBrand2};
  border: none;
  color: ${(p) => (p.current ? p.theme.colorTextInverse : p.theme.colorText)};
  cursor: ${(p) => (p.current ? 'default' : 'pointer')};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  height: 40px;
  margin-right: ${(p) => p.theme.tokens.spacing01};
  user-select: none;
  width: 40px;

  &:hover {
    filter: brightness(90%);
  }

  ${mq.xs`
    width: 48px;
    height: 48px;
  `}
`

export const StyledArrowButton = styled(StyledPageButton)`
  &:disabled {
    background-color: ${(p) => p.theme.tokens.colorPaletteGray200};
    color: ${(p) => p.theme.tokens.colorPaletteGray500};
    cursor: default;
    filter: brightness(100%);
  }

  ${mq.xs`
    display: inline-block};
    margin-right: ${(p) => p.theme.tokens.spacing02};
  `}

  ${mq.smUp`
    margin-right: ${(p) => p.theme.tokens.spacing01};
  `}
`

export const StyledDottedButton = styled.div`
  align-items: center;
  background-color: transparent;
  border: none;
  display: inline-flex;
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  height: 40px;
  justify-content: center;
  margin-right: ${(p) => p.theme.tokens.spacing01};
  text-align: center;
  user-select: none;
  width: 40px;
`

export const StyledPageCount = styled.span`
  margin-left: ${(p) => p.theme.tokens.spacing03};
  ${mq.mdDown`
    display: none;
  `}
`

export const SelectFieldContainer = styled.div`
  bottom: ${(p) => p.theme.tokens.spacing07};
  display: flex;
  flex-direction: column;
  margin-left: auto;
  margin-top: ${(p) => p.theme.tokens.spacing08};
  position: relative;
  ${mq.xs`
    margin-left: unset;
    max-width: 214px;
  `}
`

export const SelectField = styled.select`
  -webkit-appearance: none;
  appearance: none;
  background: url(${arrowDownSolidIcon}) right center no-repeat;
  background-color: ${(p) => p.theme.tokens.colorBackground};
  background-origin: content-box;
  border-radius: 0;
  height: 48px;
  margin-top: ${(p) => p.theme.tokens.spacing03};
  padding: 0 ${(p) => p.theme.tokens.spacing04};
`
