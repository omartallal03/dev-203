// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import userEvent from '@testing-library/user-event'
import { renderWithProviders, screen } from '../../test-helpers'
import { calculatePages } from './helpers'
import Pagination from './index'

const props: React.ComponentProps<typeof Pagination> = {
  currentPage: 1,
  totalRows: 25,
  rowsPerPage: 10,
  onPageChangedHandler: jest.fn(),
  onResultsPerPageChange: jest.fn(),
}

describe('<Pagination />', () => {
  const scrollTo = window.scrollTo

  beforeEach(() => {
    window.scrollTo = jest.fn()
  })
  afterEach(() => {
    window.scrollTo = scrollTo
  })

  it.each([
    {
      currentPage: 1,
      totalPageCount: 2,
      expected: ['1', '2'],
    },
    {
      currentPage: 2,
      totalPageCount: 2,
      expected: ['1', '2'],
    },
    {
      currentPage: 1,
      totalPageCount: 10,
      expected: ['1', '2', '...', '10'],
    },
    {
      currentPage: 2,
      totalPageCount: 10,
      expected: ['1', '2', '3', '...', '10'],
    },
    {
      currentPage: 3,
      totalPageCount: 10,
      expected: ['1', '2', '3', '4', '...', '10'],
    },
    {
      currentPage: 4,
      totalPageCount: 10,
      expected: ['1', '...', '3', '4', '5', '...', '10'],
    },
    {
      currentPage: 8,
      totalPageCount: 10,
      expected: ['1', '...', '7', '8', '9', '10'],
    },
    {
      currentPage: 9,
      totalPageCount: 10,
      expected: ['1', '...', '8', '9', '10'],
    },
    {
      currentPage: 10,
      totalPageCount: 10,
      expected: ['1', '...', '9', '10'],
    },
  ])(
    'returns $expected when currentPage: $currentPage and totalPageCount: $totalPageCount',
    ({ currentPage, totalPageCount, expected }) => {
      expect(calculatePages(currentPage, totalPageCount)).toEqual(expected)
    },
  )

  describe('Previous Page', () => {
    it('triggers the onPageChangedHandler function', () => {
      renderWithProviders(<Pagination {...props} currentPage={2} />)

      screen.getByRole('button', { name: 'Vorige pagina' }).click()

      expect(props.onPageChangedHandler).toHaveBeenCalledWith('1')
    })

    it('is disabled on the first page', () => {
      renderWithProviders(<Pagination {...props} currentPage={'1'} />)

      expect(
        screen.getByRole('button', { name: 'Vorige pagina' }),
      ).toBeDisabled()
    })

    it('is hidden when there are no results', () => {
      renderWithProviders(
        <Pagination {...props} totalRows={0} currentPage={'2'} />,
      )

      expect(
        screen.queryByRole('button', { name: 'Vorige pagina' }),
      ).not.toBeInTheDocument()
    })
  })

  describe('Next Page', () => {
    it('triggers the onPageChangedHandler function', () => {
      renderWithProviders(<Pagination {...props} />)

      screen.getByRole('button', { name: 'Volgende pagina' }).click()

      expect(props.onPageChangedHandler).toHaveBeenCalledWith('2')
    })

    it('is disabled on the last page', () => {
      renderWithProviders(<Pagination {...props} currentPage={'3'} />)

      expect(
        screen.getByRole('button', { name: 'Volgende pagina' }),
      ).toBeDisabled()
    })

    it('is hidden when there are no results', () => {
      renderWithProviders(
        <Pagination {...props} totalRows={0} currentPage={'3'} />,
      )

      expect(
        screen.queryByRole('button', { name: 'Volgende pagina' }),
      ).not.toBeInTheDocument()
    })
  })

  it('should scroll to the top of the pages after changing pages', () => {
    renderWithProviders(<Pagination {...props} />)

    screen.getByRole('button', { name: 'Volgende pagina' }).click()

    expect(window.scrollTo).toHaveBeenCalledWith({
      behavior: 'smooth',
      left: 0,
      top: 0,
    })
  })

  describe('Results per page', () => {
    it('should show results per page', async () => {
      renderWithProviders(<Pagination {...props} />)

      expect(
        screen.getByRole('combobox', {
          name: /Aantal resultaten per pagina/,
        }),
      ).toBeInTheDocument()
    })

    it('should handle results per page', async () => {
      renderWithProviders(<Pagination {...props} />)

      const resultsPerPage = screen.getByRole('combobox', {
        name: /Aantal resultaten per pagina/,
      })

      await userEvent.selectOptions(resultsPerPage, ['25'])

      expect(props.onResultsPerPageChange).toHaveBeenCalledWith('25')
    })
  })
})
