// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledErrorMessage = styled.span<{ level: string }>`
  color: ${(p) => {
    switch (p.level) {
      case 'warning':
        return p.theme.colorAlertWarning
      case 'error':
      default:
        return p.theme.colorAlertError
    }
  }};
`
