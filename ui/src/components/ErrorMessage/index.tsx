// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StyledErrorMessage } from './styles'

interface Props {
  level: string
}

const ErrorMessage: React.FunctionComponent<Props> = ({
  level = 'error',
  ...props
}) => {
  if (!props.children) {
    return null
  }

  return <StyledErrorMessage level={level} {...props} />
}

export default ErrorMessage
