// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'
import theme from '../../theme'
import { DONSmall } from '../CustomDON'
import { StyledHeader, StyledAddLinkMobile, StyledAddIcon } from './styles'

interface Props {
  totalResults: number | null
  objectName: string
  objectNamePlural: string
  addLinkTarget?: string
}

export const ResultsHeader: React.FunctionComponent<Props> = ({
  children,
  totalResults,
  objectName,
  objectNamePlural,
  addLinkTarget,
}) => (
  <StyledHeader>
    {!!totalResults && (
      <DONSmall data-test="total">
        {totalResults} {totalResults > 1 ? objectNamePlural : objectName}
      </DONSmall>
    )}
    {!!addLinkTarget && (
      <StyledAddLinkMobile as={Link} to={addLinkTarget} variant="link">
        <StyledAddIcon color={theme.colorTextLink} />
        {objectName} toevoegen
      </StyledAddLinkMobile>
    )}
    {children}
  </StyledHeader>
)
