// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Button } from '@commonground/design-system'
import mq from '../../theme/mediaQueries'

import AddIcon from '../../assets/icons/AddIcon'

export const StyledHeader = styled.header`
  align-items: center;
  display: flex;
  justify-content: space-between;

  ${mq.mdUp`
    padding: 0 0 1rem 0;
  `}

  ${mq.smDown`
    width: 100%;
  `}
`

export const StyledAddLinkMobile = styled(Button)`
  ${mq.mdUp`
    display: none;
  `}
`

export const StyledAddIcon = styled(AddIcon)`
  height: 20px;
  margin-right: ${(p) => p.theme.tokens.spacing01};
  width: 20px;
`
