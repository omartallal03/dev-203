// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle<{
  theme: {
    colorBackgroundSite: string
  }
}>`
  body {
    background-color: ${(p) => p.theme.colorBackgroundSite};
    word-wrap: break-word;
    word-break: break-word;
    font-family: 'Source Sans Pro', sans-serif !important;
  }
`
