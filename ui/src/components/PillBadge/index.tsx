// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export default styled.span`
  background-color: ${(p) => p.theme.colorBackgroundButtonSecondary};
  border-radius: 25px;
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  padding: 4px 12px;
  text-align: center;
  user-select: none;
`
