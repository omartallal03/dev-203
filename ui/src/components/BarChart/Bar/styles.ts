// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import theme from '../../../theme'

export const Container = styled.div`
  background: ${theme.tokens.colorPaletteGray300};
  position: relative;
  width: 100%;
`

export const Percentage = styled.div<{ value: number }>`
  background: ${theme.tokens.colorPaletteBlue800};
  height: 1.5rem;
  transition: all 0.3s;
  width: ${(p) => p.value}%;

  :hover {
    background: rgb(36, 138, 186);
  }

  :after {
    background: white;
    content: ${(p) => `'${p.value}%'`};
    font-weight: bold;
    left: ${(p) => `calc(${p.value}% - 14px)`};
    position: absolute;
    top: 100%;
    transition: all 0.3s;
    white-space: nowrap;
    z-index: 1;
  }
`

export const StaticTitle = styled.div`
  align-items: center;
  bottom: 2rem;
  cursor: default;
  display: block;
  font-size: 1rem;
  font-weight: 600;
  height: 1.5rem;
  overflow: hidden;
  position: absolute;
  text-overflow: ellipsis;
  white-space: nowrap;

  width: 100%;
`

export const StaticPercentages = styled.div`
  align-items: center;
  display: flex;
  font-size: 0.875rem;
  height: 1.5rem;
  justify-content: space-between;
  position: absolute;

  width: 100%;
`
