// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../test-helpers'
import { Bar } from './index'

const props: React.ComponentProps<typeof Bar> = {
  title: 'Title',
  percentage: 10,
}

describe('<Bar />', () => {
  it('renders correctly', () => {
    renderWithProviders(<Bar {...props} />)

    expect(screen.getByText(/Title/)).toBeInTheDocument()
    expect(screen.getByText('0%')).toBeInTheDocument()
    expect(screen.getByTestId('percentage')).toHaveAttribute('value', '10')
    expect(screen.getByText('100%')).toBeInTheDocument()
  })
})
