// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Container, Percentage, StaticPercentages, StaticTitle } from './styles'

interface Props {
  title: string
  percentage: number
}

const MIN_VALUE = 5
const MAX_VALUE = 90

export const Bar: React.FunctionComponent<Props> = ({ title, percentage }) => {
  const renderStaticPercentages = () => {
    if (percentage < MIN_VALUE) {
      return <div>100%</div>
    }
    if (percentage > MAX_VALUE) {
      return <div>0%</div>
    }
    return (
      <React.Fragment>
        <div>0%</div>
        <div>100%</div>
      </React.Fragment>
    )
  }

  return (
    <Container>
      <StaticTitle title={title}>{title}</StaticTitle>
      <Percentage value={percentage} data-testid="percentage" />
      <StaticPercentages>{renderStaticPercentages()}</StaticPercentages>
    </Container>
  )
}
