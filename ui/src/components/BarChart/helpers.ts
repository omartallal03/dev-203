// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import { Metrics } from '../../domain/models/types'

const getPercentage = (value: number, total: number) => {
  return Math.round((value / total) * 100)
}

export const getOptionsFromData = (
  data?: Metrics['designRulesLast'],
  ruleset?: string,
) => {
  if (typeof data === 'undefined' || typeof ruleset === 'undefined') {
    return []
  }

  const designRules = Object.entries(data[ruleset.toString()])

  return designRules.map(([rule, { successCount, totalCount }]) => {
    return {
      rule,
      percentage: getPercentage(successCount, totalCount),
    }
  })
}
