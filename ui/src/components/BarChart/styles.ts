// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../theme/mediaQueries'

export const Container = styled.div`
  background: #fff;
  border-radius: 4px;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24);
  min-height: 400px;
  padding: ${(p) => p.theme.tokens.spacing06};
  position: relative;
  transition: all 0.3s;

  ${mq.mdUp`
    min-width: calc(50% - ${(p) => p.theme.tokens.spacing06});
  `}
`

export const Content = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  gap: ${(p) => p.theme.tokens.spacing10};
  height: 100%;
  justify-content: center;
  padding-bottom: ${(p) => p.theme.tokens.spacing12};
  position: relative;
`

export const Title = styled.h3`
  color: rgb(51, 51, 51);
  font-size: 1.125rem;
  margin: 0;
  padding: 0;
`
export const Subtitle = styled.h4`
  color: rgb(102, 102, 102);
  font-size: 0.875rem;
  font-weight: 500;
  margin: 0;
  padding: 0;
`

export const Divider = styled.hr`
  background-color: #ccd6eb;
  border: none;
  bottom: 4.5rem;
  color: #ccd6eb;
  height: 1px;
  position: absolute;
  width: 100%;

  ${mq.mdUp`
    bottom: ${(p) => p.theme.tokens.spacing09};
  `}
`

export const RulesetContainer = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing04};
  width: 100%;
`
