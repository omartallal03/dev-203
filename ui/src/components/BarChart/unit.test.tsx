// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen, waitFor } from '../../test-helpers'
import { metricsMock } from '../../../.jest/mocks/metrics.mock'
import { modelFromMetricsResponse } from '../../domain/models/metrics'
import { getOptionsFromData } from './helpers'
import BarChart from './index'

const props: React.ComponentProps<typeof BarChart> = {
  title: 'Test title',
  subtitle: 'Test subtitle',
  data: modelFromMetricsResponse(metricsMock).designRulesLast,
}

describe('<BarChart />', () => {
  it('renders the title correctly', async () => {
    renderWithProviders(<BarChart {...props} />)

    await waitFor(() => {
      expect(screen.getByText('Test title')).toBeInTheDocument()
    })
  })

  it('renders the subtitle correctly', async () => {
    renderWithProviders(<BarChart {...props} />)

    await waitFor(() => {
      expect(screen.getByText('Test subtitle')).toBeInTheDocument()
    })
  })

  it('implements a ruleset selection', async () => {
    renderWithProviders(<BarChart {...props} />)

    await waitFor(() => {
      expect(
        screen.getByRole('combobox', { name: /API ruleset versie/ }),
      ).toBeInTheDocument()
    })
  })
})

describe('getOptionsFromData()', () => {
  it.each([
    {
      data: undefined,
      suite: undefined,
      expected: [],
    },
    {
      data: props.data,
      suite: '09_juli_2020',
      expected: [
        { percentage: 20, rule: 'api_03' },
        { percentage: 20, rule: 'api_16' },
        { percentage: 60, rule: 'api_20' },
        { percentage: 20, rule: 'api_48' },
        { percentage: 10, rule: 'api_51' },
        { percentage: 20, rule: 'api_56' },
        { percentage: 20, rule: 'api_57' },
      ],
    },
  ])(
    'returns $expected when data $data and suite $suite',
    ({ data, suite, expected }) => {
      expect(getOptionsFromData(data, suite)).toEqual(expected)
    },
  )
})
