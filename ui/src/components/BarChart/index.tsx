// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { useState } from 'react'
import GenericChart from '../GenericChart'
import { Metrics } from '../../domain/models/types'
import RulesetSelector from '../RulesetSelector'
import { getRulesetsFromData } from '../../helpers'
import { Bar } from './Bar'
import { Container, Content, Subtitle, Title, RulesetContainer } from './styles'
import { getOptionsFromData } from './helpers'
import NoDataAvailable from './NoDataAvailable'

interface Props
  extends Omit<React.ComponentProps<typeof GenericChart>, 'options'> {
  data?: Metrics['designRulesLast']
}

const BarChart: React.FunctionComponent<Props> = ({
  data,
  title,
  subtitle,
}) => {
  const rulesets = getRulesetsFromData(data)
  const [selectedRuleset, setSelectedRuleset] = useState(rulesets[0])
  const options = getOptionsFromData(data, selectedRuleset?.value)
  const hasData = !!options.length

  const renderBars = () => {
    if (!hasData) {
      return <NoDataAvailable />
    }

    return options.map(({ percentage, rule }) => {
      return <Bar key={rule} title={rule} percentage={percentage} />
    })
  }

  return (
    <Container>
      <Title>{title}</Title>
      <Subtitle>{subtitle}</Subtitle>
      <RulesetContainer>
        <RulesetSelector
          onChange={setSelectedRuleset}
          options={rulesets}
          value={selectedRuleset}
        />
      </RulesetContainer>
      <Content>{renderBars()}</Content>
    </Container>
  )
}

export default BarChart
