// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Container = styled.p`
  color: rgb(103, 109, 128);
  font-weight: bold;
  margin-top: ${(p) => p.theme.tokens.spacing10};
  max-width: 300px;
`
