// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Divider } from '../styles'
import { Container } from './styles'

const NoDataAvailable: React.FunctionComponent = () => {
  return (
    <React.Fragment>
      <Container>
        Er is (nog) geen data beschikbaar om deze grafiek te kunnen laten tonen.
      </Container>
      <Divider />
    </React.Fragment>
  )
}

export default NoDataAvailable
