// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../test-helpers'
import NoDataAvailable from './index'

describe('<NoDataAvailable />', () => {
  it('renders the correct message', () => {
    renderWithProviders(<NoDataAvailable />)

    expect(
      screen.getByText(
        /Er is \(nog\) geen data beschikbaar om deze grafiek te kunnen laten tonen./,
      ),
    ).toBeInTheDocument()
  })
})
