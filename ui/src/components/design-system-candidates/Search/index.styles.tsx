// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import styled from 'styled-components'
import { Spinner } from '@commonground/design-system'

export const StyledSearch = styled.div`
  position: relative;
`

const SearchIcon = ({ ...props }) => (
  <svg viewBox="0 0 12 12" {...props}>
    <path
      d="M8.576 7.547h-.542l-.192-.185A4.44 4.44 0 0 0 8.92 4.46a4.46 4.46 0 1 0-4.46 4.46 4.44 4.44 0 0 0 2.903-1.078l.185.192v.542L10.977 12 12 10.978l-3.424-3.43zm-4.116 0A3.083 3.083 0 0 1 1.372 4.46 3.083 3.083 0 0 1 4.46 1.372 3.083 3.083 0 0 1 7.547 4.46 3.083 3.083 0 0 1 4.46 7.547z"
      fill="#757575"
      fillRule="nonzero"
    />
  </svg>
)

export const StyledSearchIcon = styled(SearchIcon)`
  height: ${(p) => p.theme.tokens.spacing05};
  left: ${(p) => p.theme.tokens.spacing05};
  position: absolute;
  top: calc(50% - ${(p) => p.theme.tokens.spacing03});
  width: ${(p) => p.theme.tokens.spacing05};
`

export const StyledInput = styled.input`
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray500};
  color: ${(p) => p.theme.colorText};
  font-family: 'Source Sans Pro', sans-serif;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  outline: none;
  padding-bottom: ${(p) => p.theme.tokens.spacing04};
  padding-left: ${(p) => p.theme.tokens.spacing08};
  padding-right: ${(p) => p.theme.tokens.spacing04};
  padding-top: ${(p) => p.theme.tokens.spacing04};
  width: 100%;

  &:focus {
    border: 2px solid ${(p) => p.theme.tokens.colorFocus};
    padding-bottom: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    padding-left: calc(${(p) => p.theme.tokens.spacing08} - 1px);
    padding-right: calc(${(p) => p.theme.tokens.spacing04} - 1px);
    padding-top: calc(${(p) => p.theme.tokens.spacing04} - 1px);
  }

  &::placeholder {
    color: ${(p) => p.theme.tokens.colorPaletteGray600};
  }
`

export const StyledSpinner = styled(Spinner)`
  position: absolute;
  right: 0;
  top: 0.75rem;
`
