// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { ThemeProvider } from 'styled-components'
import theme from '../../../theme'
import {
  StyledSearch,
  StyledInput,
  StyledSearchIcon,
  StyledSpinner,
} from './index.styles'

interface Props {
  onQueryChanged?: (value: string) => void
  inputProps: {
    placeholder: string
    name?: string
    id?: string
    value?: string
    defaultValue?: string
  }
  searching?: boolean
}

const Search: React.FunctionComponent<Props> = ({
  inputProps,
  onQueryChanged,
  searching,
  children,
  ...props
}) => (
  <ThemeProvider theme={theme}>
    <StyledSearch {...props}>
      <StyledInput
        onChange={(event) => {
          return onQueryChanged && onQueryChanged(event.target.value)
        }}
        {...inputProps}
      />
      <StyledSearchIcon />
      {children}
      {searching && <StyledSpinner />}
    </StyledSearch>
  </ThemeProvider>
)

export default Search
