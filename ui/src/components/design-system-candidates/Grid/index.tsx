// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const Container = styled.div`
  margin: 0 auto;
  max-width: ${(p) => p.theme.containerWidth};
  padding: 0 ${(p) => p.theme.containerPadding};
  width: 100%;
`

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin: 0 -${(p) => p.theme.tokens.spacing05};
`

export const Col = styled.div`
  display: flex;
  flex: 1;
  flex-basis: ${(p: { width: string }) => p.width};
  flex-direction: column;
  padding: 0 ${(p) => p.theme.tokens.spacing05};
`
