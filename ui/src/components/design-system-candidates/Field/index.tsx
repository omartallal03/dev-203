// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, oneOf } from 'prop-types'
import styled from 'styled-components'
import { Field as FormikField } from 'formik'
import { WIDTH } from './const'

const getMaxWidth = (maxWidth) => {
  switch (maxWidth) {
    case WIDTH.EXTRA_SMALL:
      return '7.5rem'
    case WIDTH.SMALL:
      return '15rem'
    case WIDTH.MEDIUM:
      return '30rem'
    case WIDTH.LARGE:
      return '45rem'
    default:
      return '100%'
  }
}

// This filters the maxWidth prop from the passed props and is a workaround until
// htps://github.com/styled-components/styled-components/issues/2878 is implemented.
const FormikFieldWithFilteredProp = (props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { maxWidth, ...rest } = props
  return <FormikField {...rest}>{props.children}</FormikField>
}

FormikFieldWithFilteredProp.propTypes = {
  maxWidth: oneOf([WIDTH.EXTRA_SMALL, WIDTH.SMALL, WIDTH.MEDIUM, WIDTH.LARGE]),
  children: node,
}

const Field = styled(FormikFieldWithFilteredProp)`
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray500};
  color: ${(p) => p.theme.colorText};
  display: block;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  line-height: ${(p) => p.theme.tokens.lineHeightText};
  max-width: ${(p) => getMaxWidth(p.maxWidth)};
  outline: none;
  padding: ${(p) => p.theme.tokens.spacing04};
  width: 100%;

  ${(p) =>
    p.component === 'textarea'
      ? `
        min-height: 10rem;
      `
      : null}

  &:focus {
    border: 2px solid ${(p) => p.theme.tokens.colorPaletteBlue700};
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
  }

  &::placeholder {
    color: ${(p) => p.theme.tokens.colorPaletteGray600};
  }

  &.invalid {
    border: 2px solid ${(p) => p.theme.tokens.colorAlertError};
    padding: calc(${(p) => p.theme.tokens.spacing04} - 1px);
  }
`

export default Field
