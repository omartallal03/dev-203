// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { useRef } from 'react'
import HighchartsReact from 'highcharts-react-official'
import Highcharts from 'highcharts'
import { merge } from 'lodash'
import { TimeRange } from '../../containers/Apis/ApiStatistics/constants'
import { StyledChartsContainer } from './styles'

interface Props extends React.ComponentProps<typeof HighchartsReact> {
  showOptions?: boolean
  useBaseOptions?: boolean
  chartOptions?: JSX.Element | null
  title?: string
  subtitle?: string
  titleMargin?: number
  timeRange?: TimeRange
}

const GenericChart: React.FunctionComponent<Props> = ({
  children,
  options,
  ChartOptions,
  showOptions,
  useBaseOptions = true,
  title = '',
  titleMargin = 50,
  subtitle = '',
  timeRange,
  ...props
}) => {
  const chartComponentRef = useRef<HighchartsReact.RefObject>(null)

  const baseOptions: Highcharts.Options = {
    chart: {
      style: {
        fontFamily: 'Source Sans Pro',
      },
      zooming: {
        type: 'x',
      },
      spacing: [24, 24, 24, 24],
      height: 400,
    },
    plotOptions: {
      area: {
        fillColor: 'rgba(11, 113, 161, 0.2)',
        lineColor: '#0B71A1',
        marker: {
          fillColor: '#0B71A1',
          symbol: 'square',
        },
      },
      line: {
        color: '#0B71A1',
        marker: {
          color: '#0B71A1',
          symbol: 'square',
        },
      },
    },
    credits: {
      enabled: false,
    },
    title: {
      text: title,
      margin: titleMargin,
      align: 'left',
      style: {
        fontWeight: 'bold',
      },
    },
    subtitle: {
      text: subtitle,
      align: 'left',
      style: {
        fontSize: '0.875rem',
      },
    },
    xAxis: {
      type: 'datetime',
      dateTimeLabelFormats: {
        month: '%b %Y',
      },
      minPadding: 0.05,
      maxPadding: 0.05,
      labels: {
        style: {
          color: '#212121',
          fontSize: '0.875rem',
        },
      },
    },
    yAxis: {
      title: {
        text: '',
      },
      labels: {
        style: {
          color: '#212121',
          fontSize: '0.875rem',
        },
      },
    },
    legend: {
      enabled: false,
    },
  }

  const chartOptions = useBaseOptions ? merge(baseOptions, options) : options

  return (
    <StyledChartsContainer>
      <HighchartsReact
        highcharts={Highcharts}
        options={chartOptions}
        ref={chartComponentRef}
        {...props}
      />
      {children}
    </StyledChartsContainer>
  )
}

export default GenericChart
