// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledChartsContainer = styled.div`
  border-radius: 4px;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.24);
  position: relative;
  width: auto;
`
