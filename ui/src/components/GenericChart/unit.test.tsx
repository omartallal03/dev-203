// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import GenericChart from './index'

describe('<GenericChart />', () => {
  it('renders the title correctly', async () => {
    render(<GenericChart title="Test title" subtitle="Test subtitle" />)

    await waitFor(() => {
      expect(screen.getAllByText('Test title')).toHaveLength(2)
      expect(screen.getAllByText('Test subtitle')).toHaveLength(2)
    })
  })

  it('renders the subtitle correctly', async () => {
    render(<GenericChart title="Test title" subtitle="Test subtitle" />)

    await waitFor(() => {
      expect(screen.getAllByText('Test subtitle')).toHaveLength(2)
    })
  })
})
