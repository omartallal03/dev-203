// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen, waitFor } from '../../test-helpers'
import AreaChart from './index'

describe('<AreaChart />', () => {
  it('renders the title correctly', async () => {
    renderWithProviders(
      <AreaChart title="Test title" subtitle="Test subtitle" />,
    )

    await waitFor(() => {
      expect(screen.getAllByText('Test title')).toHaveLength(2)
    })
  })

  it('renders the subtitle correctly', async () => {
    renderWithProviders(
      <AreaChart title="Test title" subtitle="Test subtitle" />,
    )

    await waitFor(() => {
      expect(screen.getAllByText('Test subtitle')).toHaveLength(2)
    })
  })
})
