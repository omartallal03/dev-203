// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import Highcharts from 'highcharts'
import GenericChart from '../GenericChart'
import { TimeRange } from '../../containers/Apis/ApiStatistics/constants'

interface Props
  extends Omit<React.ComponentProps<typeof GenericChart>, 'options'> {
  data?: {
    totalCounts: [number, number][]
  }
  timeRange: TimeRange
}

const AreaChart: React.FunctionComponent<Props> = ({
  data,
  timeRange,
  ...props
}) => {
  const options: Highcharts.Options = {
    chart: {
      zooming: {
        type: 'x',
      },
    },
    series: [
      {
        name: props.title,
        type: 'area',
        data: data?.totalCounts,
      },
    ],
    xAxis: {
      type: 'datetime',
      crosshair: true,
    },
  }
  return (
    <GenericChart
      {...props}
      timeRange={timeRange}
      options={options}
      showOptions={true}
    />
  )
}

export default AreaChart
