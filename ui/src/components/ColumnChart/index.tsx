// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { useState } from 'react'
import Highcharts from 'highcharts'
import GenericChart from '../GenericChart'
import { Metrics } from '../../domain/models/types'
import theme from '../../theme'
import { getRulesetsFromData } from '../../helpers'
import RulesetSelector from '../RulesetSelector'
import { Container, RulesetContainer } from './styles'

const RULESET_SELECTOR_MARGIN = 145
const RULESET_SELECTOR_MARGIN_NONE = 0

interface Props
  extends Omit<React.ComponentProps<typeof GenericChart>, 'options'> {
  data?: Metrics['designRulesApiSuccessesLast']
}

const ColumnChart: React.FunctionComponent<Props> = ({
  data = {},
  ...props
}) => {
  const rulesets = getRulesetsFromData(data)
  const [selectedRuleset, setSelectedRuleset] = useState(rulesets[0] || {})
  const { categories, values } = data[selectedRuleset.value?.toString()] || {}
  const hasData = !!categories?.length && !!values?.length

  const options: Highcharts.Options = {
    xAxis: {
      categories,
    },
    series: [
      {
        name: props.title,
        type: 'column',
        data: values,
        color: theme.tokens.colorPaletteBlue800,
      },
    ],
  }

  return (
    <Container>
      <GenericChart
        {...props}
        options={options}
        showOptions={true}
        titleMargin={
          hasData ? RULESET_SELECTOR_MARGIN : RULESET_SELECTOR_MARGIN_NONE
        }
      >
        <RulesetContainer>
          <RulesetSelector
            onChange={setSelectedRuleset}
            options={rulesets}
            value={selectedRuleset}
          />
        </RulesetContainer>
      </GenericChart>
    </Container>
  )
}

export default ColumnChart
