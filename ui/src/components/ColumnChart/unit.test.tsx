// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../test-helpers'
import { metricsMock } from '../../../.jest/mocks/metrics.mock'
import ColumnChart from './index'

const props: React.ComponentProps<typeof ColumnChart> = {
  title: 'ColumnChart title',
  subtitle: 'ColumnChart subtitle',
  data: metricsMock,
}

describe('<ColumnChart />', () => {
  it('renders correctly', () => {
    renderWithProviders(<ColumnChart {...props} />)

    expect(screen.getAllByText('ColumnChart title')).toHaveLength(2)
    expect(screen.getAllByText('ColumnChart subtitle')).toHaveLength(2)
  })

  it('renders a ruleset selector', () => {
    renderWithProviders(<ColumnChart {...props} />)

    expect(screen.getAllByText('API ruleset versie')).toHaveLength(1)
  })
})
