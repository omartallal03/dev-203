// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../theme/mediaQueries'

export const Container = styled.div`
  ${mq.mdUp`
    height: 50%;
    width: 50%;
  `}
`

export const RulesetContainer = styled.div`
  left: ${(p) => p.theme.tokens.spacing06};
  position: absolute;
  top: 83px;
  width: calc(100% - ${(p) => p.theme.tokens.spacing09});
  z-index: 1;
`
