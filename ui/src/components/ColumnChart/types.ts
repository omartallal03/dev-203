// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
export type Data = Record<string, [string, number]>

export interface ReturnValue {
  categories: string[]
  values: number[]
}
