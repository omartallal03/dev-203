// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../theme/mediaQueries'
import { Container } from '../design-system-candidates/Grid'
import { ReactComponent as GitlabIcon } from '../../assets/svg/gitlab-black-icon.svg'
import ExternalIcon from '../../assets/icons/External'

export const FeedbackArea = styled.div`
  background-color: ${(p) => p.theme.tokens.colorBackground};
  margin-top: 100px;
`

export const FeedbackContainer = styled(Container)`
  padding-bottom: ${(p) => p.theme.tokens.spacing09};
  padding-top: ${(p) => p.theme.tokens.spacing09};

  ${mq.smUp`
    padding-top: ${(p) => p.theme.tokens.spacing11};
    padding-bottom: ${(p) => p.theme.tokens.spacing11};
  `}
`

export const StyledTitle = styled.h2`
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  margin-top: 0;
`

export const StyledText = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  margin-top: 15px;
`

export const StyledGitlabIcon = styled(GitlabIcon)`
  height: 1rem;
  margin-right: ${(p) => p.theme.tokens.spacing06};
`

export const StyledExternalIcon = styled(ExternalIcon)`
  height: 1rem;
  margin-left: ${(p) => p.theme.tokens.spacing06};
`
