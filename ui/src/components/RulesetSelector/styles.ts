// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import { SelectComponent } from '@commonground/design-system'

export const Label = styled.label`
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const Container = styled.div`
  width: 100%;
`

export const StyledSelectComponent = styled(SelectComponent)`
  width: 100% !important;
`
