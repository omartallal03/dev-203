// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../test-helpers'
import RulesetSelector from './index'

const props: React.ComponentProps<typeof RulesetSelector> = {
  options: [{ label: 'label', value: 'value' }],
}

describe('<RulesetSelector />', () => {
  it('renders correctly', () => {
    renderWithProviders(<RulesetSelector {...props} />)

    expect(screen.getByText('API ruleset versie')).toBeInTheDocument()
    expect(screen.getByText('Kies een API ruleset versie')).toBeInTheDocument()
  })
})
