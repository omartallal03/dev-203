// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Container, Label, StyledSelectComponent } from './styles'

interface Props
  extends Omit<
    React.ComponentProps<typeof StyledSelectComponent>,
    'name' | ' | aria-labelledby' | 'placeholder' | 'size'
  > {}

const RulesetSelector: React.FunctionComponent<Props> = ({
  options,
  onChange,
  value,
}) => {
  if (!options.length) {
    return null
  }

  return (
    <Container>
      <Label id="apiRulesetVersion">API ruleset versie</Label>
      <StyledSelectComponent
        onChange={onChange}
        options={options}
        name="apiRulesetVersion"
        aria-labelledby="apiRulesetVersion"
        value={value}
        placeholder="Kies een API ruleset versie"
        size="m"
      />
    </Container>
  )
}

export default RulesetSelector
