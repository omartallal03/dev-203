// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../theme/mediaQueries'
import { Container } from '../design-system-candidates/Grid'
import { ReactComponent as GitlabIcon } from '../../assets/svg/gitlab-white-icon.svg'

export const HeaderArea = styled.header`
  background-color: ${(p) => p.theme.tokens.colorBrand3};
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.24), 0 5px 10px 0 rgba(0, 0, 0, 0.24);
  margin-bottom: ${(p) => p.theme.tokens.spacing09};
  overflow: hidden;

  ${mq.smUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing11};
  `}
`

export const TopNavigationArea = styled.nav`
  background-color: rgba(0, 0, 0, 0.3);
`

export const TopNavigationContainer = styled(Container)`
  height: ${(p) => p.theme.headerTopNavigationHeight};
`

export const HeaderContainer = styled(Container)`
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
  margin-top: ${(p) => p.theme.tokens.spacing07};

  ${mq.smUp`
    margin-top: ${(p) => p.theme.tokens.spacing09};
    margin-bottom: ${(p) => p.theme.tokens.spacing09};
  `}
`

export const StyledNavigationList = styled.ul`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: flex-end;
  list-style: none;
  margin: 0;
  padding: 0;
`

export const StyledNavigationListItem = styled.li`
  height: 100%;

  :not(:last-child) {
    margin-right: 8px;
  }
`

export const StyledTopNavigationAnchor = styled.a`
  align-items: center;
  color: ${(p) => p.theme.colorTextInverse};
  display: flex;
  height: 100%;
  padding: 0 20px;
  text-decoration: none;

  &:hover {
    background-color: rgba(0, 0, 0, 0.2);
    color: ${(p) => p.theme.colorTextInverse};
    text-decoration: underline;
  }
`

export const StyledTopNavigationAnchorActive = styled(
  StyledTopNavigationAnchor,
)`
  border-bottom: 1px solid white;
  font-weight: 600;
`

export const StyledGitlabIcon = styled(GitlabIcon)`
  height: 16px;
  margin-right: 8px;
  width: 16px;
`

export const StyledTitle = styled.h2`
  color: ${(p) => p.theme.colorTextInverse};
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin-bottom: ${(p) => p.theme.tokens.spacing03};
  margin-top: 0;

  ${mq.smUp`
    font-size: ${(p) => p.theme.tokens.fontSizeXXLarge};
  `}
`

export const StyledText = styled.p`
  color: ${(p) => p.theme.colorTextInverse};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
`

export const NavigationWrapper = styled.div`
  div:first-child {
    background-color: ${(p) => p.theme.tokens.colorBrand3};

    margin: 0 auto;
    max-width: ${(p) => p.theme.containerWidth};
    width: 100%;
  }

  li {
    font-size: ${(p) => p.theme.tokens.fontSizeLarge};

    a {
      color: ${(p) => p.theme.colorTextInverse};

      ${mq.smDown`
        color: ${(p) => p.theme.colorTextLight};
      `}

      &:active,
      &.active {
        background: unset !important;
        border-bottom: 4px solid white;
        color: ${(p) => p.theme.colorTextInverse};
        padding-bottom: calc(${(p) => p.theme.tokens.spacing05} - 4px);

        ${mq.smDown`
          color: ${(p) => p.theme.colorTextLight};
        `}
      }

      &:hover,
      &:focus {
        background: rgba(255, 255, 255, 0.2);
        color: ${(p) => p.theme.colorTextInverse};
        text-decoration: underline;

        ${mq.smDown`
          color: ${(p) => p.theme.colorTextLight};
        `}
      }
    }
  }
`

export const SkipLink = styled.a`
  clip: rect(1px, 1px, 1px, 1px);
  color: white;
  height: 1px;
  margin-right: auto;
  overflow: hidden;
  position: absolute;
  text-decoration: none;
  white-space: nowrap;
  width: 1px;

  &:focus {
    clip: auto;
    height: auto;
    position: static;
    width: auto;
  }
`
