// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { ReactNode } from 'react'

export interface NavItem {
  name: string
  Icon: ReactNode
  to: string
}
