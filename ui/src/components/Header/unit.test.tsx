// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { screen } from '@testing-library/react'
import { renderWithProviders } from '../../test-helpers'
import Header from './index'

describe('Header', () => {
  it('should have a forum link', () => {
    renderWithProviders(<Header />)

    expect(
      screen.getByRole('link', { name: /Forum/, current: false }),
    ).toHaveAttribute('href', 'https://forum.developer.overheid.nl')
  })

  it('should have a developer link', () => {
    renderWithProviders(<Header />)

    expect(
      screen.getByRole('link', { name: /Developer/, current: 'page' }),
    ).toHaveAttribute('href', 'https://developer.overheid.nl')
  })

  describe('a11y', () => {
    it('contains a skip link to the main content block', () => {
      renderWithProviders(<Header />)

      expect(
        screen.getByRole('link', { name: /Naar hoofdinhoud/, current: false }),
      ).toHaveAttribute('href', '#content')
    })
  })
})
