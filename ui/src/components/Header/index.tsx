// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import PrimaryNavigation from '@commonground/design-system/dist/components/PrimaryNavigation'
import { useLocation } from 'react-router-dom'

// Constants
import { navItems } from '../../containers/App/constants'
import { GITLAB_REPO_URL } from '../../constants'

// Components
import NavLink from './NavLink'

// Styles
import {
  HeaderArea,
  HeaderContainer,
  TopNavigationArea,
  TopNavigationContainer,
  StyledNavigationList,
  StyledNavigationListItem,
  StyledTopNavigationAnchor,
  StyledGitlabIcon,
  StyledTitle,
  StyledText,
  NavigationWrapper,
  StyledTopNavigationAnchorActive,
  SkipLink,
} from './styles'

const Header: React.FunctionComponent = () => {
  const { pathname } = useLocation()

  return (
    <HeaderArea>
      <TopNavigationArea aria-label="Gerelateerde websites">
        <TopNavigationContainer>
          <StyledNavigationList>
            <SkipLink href="#content">Naar hoofdinhoud</SkipLink>
            <StyledNavigationListItem>
              <StyledTopNavigationAnchorActive
                href="https://developer.overheid.nl"
                aria-current="page"
              >
                Developer
              </StyledTopNavigationAnchorActive>
            </StyledNavigationListItem>
            <StyledNavigationListItem>
              <StyledTopNavigationAnchor href="https://forum.developer.overheid.nl">
                Forum
              </StyledTopNavigationAnchor>
            </StyledNavigationListItem>
            <StyledNavigationListItem>
              <StyledTopNavigationAnchor href={GITLAB_REPO_URL}>
                <StyledGitlabIcon />
                GitLab
              </StyledTopNavigationAnchor>
            </StyledNavigationListItem>
          </StyledNavigationList>
        </TopNavigationContainer>
      </TopNavigationArea>

      <HeaderContainer>
        <StyledTitle>Developer Overheid</StyledTitle>
        <StyledText>Ontwikkelen voor de overheid doen we samen</StyledText>
      </HeaderContainer>

      <NavigationWrapper id="wrapper">
        <PrimaryNavigation
          aria-label="Website navigatie"
          items={navItems}
          LinkComponent={NavLink}
          mobileMoreText="Meer"
          pathname={pathname}
        />
      </NavigationWrapper>
    </HeaderArea>
  )
}

export default Header
