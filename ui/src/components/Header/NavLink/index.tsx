// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { ReactNode } from 'react'
import { Link, useLocation } from 'react-router-dom'

interface Props extends React.ComponentProps<typeof Link> {
  Icon?: ReactNode
}

const NavLink: React.FunctionComponent<Props> = ({
  to,
  className,
  children,
  Icon,
  ...props
}) => {
  const { pathname } = useLocation()
  const finalClassName = pathname === to ? `${className} active` : className

  return (
    <Link className={finalClassName} to={to} {...props}>
      {children}
    </Link>
  )
}

export default NavLink
