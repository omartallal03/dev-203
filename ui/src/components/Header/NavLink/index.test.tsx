// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { renderWithRouter } from '../../../test-helpers'
import NavLink from './index'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useLocation: () => ({
    pathname: '/page',
  }),
}))

describe('<NavLink />', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('receives active classname', () => {
    const { getByText, rerender } = renderWithRouter(
      <NavLink to="/">home</NavLink>,
    )
    expect(getByText('home')).not.toHaveClass('active')

    rerender(<NavLink to="/page">page</NavLink>)
    expect(getByText('page')).toHaveClass('active')
  })
})
