// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../theme/mediaQueries'

export const H1 = styled.h1`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: ${(p) => `${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing05}`};

  ${mq.smUp`
    font-size: ${(p) => p.theme.tokens.fontSizeXXLarge};
  `}
`

export const H2 = styled.h2`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: ${(p) => p.theme.tokens.spacing05} 0;

  ${mq.smUp`
    margin: ${(p) =>
      `${p.theme.tokens.spacing05} 0 ${p.theme.tokens.spacing07}`};
  `}
`

export const H3 = styled.h3`
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
`
