// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { useEffect } from 'react'
import { useFormikContext } from 'formik'

interface Props {
  handle: (values: any) => void // eslint-disable-line @typescript-eslint/no-explicit-any
}

/**
 * Use this as a global Formik onChange function
 * Use the values READ ONLY -Don't create an infinite loop by SETTING any Formik values
 */
export const OnFormikValueChange: React.FunctionComponent<Props> = ({
  handle,
}): null => {
  const { values } = useFormikContext()

  useEffect(() => {
    handle(values)
  }, [values, handle])

  return null
}

/**
 * For instance useful when having a number field that can be submitted empty:
 * eg: Yup.number().transform(convertEmptyValueTo(0))
 */
export const convertEmptyValueTo =
  (n: unknown = null) =>
  (value: unknown, originalValue: unknown): unknown => {
    if (typeof originalValue === 'string' && originalValue === '') {
      return n
    }
    return value
  }
