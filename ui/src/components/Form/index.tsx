// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Field as FormikField } from 'formik'
import mq from '../../theme/mediaQueries'
import ExtendedFormikField from '../design-system-candidates/Field'
import checkboxIcon from '../../assets/svg/checkbox-icon.svg'
import arrowDownSolidIcon from '../../assets/svg/arrow-down-solid-icon.svg'

export const Fieldset = styled.fieldset`
  border: none;
  margin: 0 0 ${(p) => p.theme.tokens.spacing08} 0;
  padding: 0;
`
// To be used as a fieldset nested in another fieldset
export const SubFieldset = styled(Fieldset)`
  margin-bottom: ${(p) => p.theme.tokens.spacing06};

  legend {
    font-size: ${(p) => p.theme.tokens.fontSizeLarge};
    font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  }
`

export const Legend = styled.legend`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin-bottom: ${(p) => p.theme.tokens.spacing06};
  padding: 0;
`

export const Label = styled.label`
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const FieldSetLabel = styled.fieldset`
  border: none;
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
  padding: 0;
`

export const Field = styled(ExtendedFormikField)`
  background-color: ${(p) => p.theme.tokens.colorBackground};
  border-radius: 0;
  height: 48px;

  ${mq.xs`
    max-width: 100%;
  `}
`

export const SelectField = styled(ExtendedFormikField)`
  -webkit-appearance: none;
  appearance: none;
  background: url(${arrowDownSolidIcon}) right center no-repeat;
  background-color: ${(p) => p.theme.tokens.colorBackground};
  border-radius: 0;
  height: 48px;

  ${mq.xs`
    max-width: 100%;
  `}
`

export const CheckboxField = styled(FormikField)`
  -webkit-appearance: none;
  appearance: none;
  background: #ffffff;
  border: 1px solid ${(p) => p.theme.colorBorderInput};
  border-radius: 2px;
  height: 18px;
  margin-right: ${(p) => p.theme.tokens.spacing05};
  min-width: 18px;
  padding: 0;
  position: relative;

  &:focus {
    padding: 0;
  }

  &:checked {
    background: ${(p) => p.theme.tokens.colorBrand3};
    border: 0 none;

    &::after {
      background: url(${checkboxIcon}) no-repeat;
      background-position: center;
      content: '';
      height: 18px;
      position: absolute;
      width: 18px;
    }
  }
`

export const RadioOptionGroup = styled.div`
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray400};
  display: flex;
  flex-wrap: wrap;
  width: fit-content;

  ${mq.xs`
    width: 100%;
  `}
`

export const RadioOptionWrapper = styled.div`
  color: ${(p) => p.theme.colorText};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  position: relative;

  &:not(:last-child) {
    border-right: 1px solid ${(p) => p.theme.tokens.colorPaletteGray400};
  }

  ${mq.xs`
    flex: 1 1 auto;
    text-align: center;
  `}

  label {
    background-color: ${(p) => p.theme.tokens.colorPaletteGray300};
    box-shadow: inset 0 -3px 0 rgba(0, 0, 0, 0.16);
    font-weight: ${(p) => p.theme.tokens.fontWeightBold};
    margin: 0;
    padding: ${(p) =>
      `${p.theme.tokens.spacing04} ${p.theme.tokens.spacing06}`};
    white-space: nowrap;
  }

  input {
    cursor: pointer;
    opacity: 0;
    position: absolute;

    &:checked + label {
      background-color: ${(p) => p.theme.tokens.colorBrand3};
      box-shadow: inset 0 3px 0 rgba(0, 0, 0, 0.16);
      color: white;
    }

    &:focus + label {
      background-color: ${(p) => p.theme.tokens.colorBrand3};
      color: white;
    }
  }
`
