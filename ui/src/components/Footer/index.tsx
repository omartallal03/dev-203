// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { Button } from '@commonground/design-system'

// Helpers
import { renderImages } from './helpers'

// Styles
import styles from './styles.module.scss'

interface Props extends React.ComponentProps<'footer'> {}

const Footer: React.FunctionComponent<Props> = ({ className, ...props }) => {
  return (
    <footer className={clsx(styles.footer, className)} {...props}>
      <div className={clsx(styles['footer__container'])}>
        <div className={clsx(styles['footer__text'])}>
          Een initiatief van
          <Button
            variant="link"
            as={Link}
            to="/privacy"
            className={clsx(styles['footer__link'])}
          >
            Privacyverklaring
          </Button>
        </div>
        <div className={clsx(styles['footer__container--image'])}>
          {renderImages()}
        </div>
      </div>
    </footer>
  )
}

export default Footer
