// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import LogoBZK from '../../assets/images/logo-bzk.png'
import LogoVNG from '../../assets/svg/logo-vng.svg'

export const images = [
  {
    className: 'footer__logo--bzk',
    src: LogoBZK,
    title: 'Ministerie van Binnenlandse Zaken en Koninkrijksrelaties',
  },
  {
    className: 'footer__logo--vng',
    src: LogoVNG,
    title: 'VNG Realisatie',
  },
]
