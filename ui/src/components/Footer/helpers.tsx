// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import clsx from 'clsx'

// Constants
import { images } from './constants'

// Styles
import styles from './styles.module.scss'

export const renderImages = () => {
  return images.map(({ className, ...image }) => {
    return (
      <img
        key={image.title}
        className={clsx(styles[className])}
        alt={`Logo ${image.title}`}
        {...image}
      />
    )
  })
}
