// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'

// Test helpers
import { renderWithProviders, screen } from '../../test-helpers'

// Component
import Footer from '.'

describe('<Footer />', () => {
  it('has footer text', () => {
    renderWithProviders(<Footer />)

    expect(screen.getByText(/Een initiatief van/)).toBeInTheDocument()
  })

  it('has a privacy statement link', () => {
    renderWithProviders(<Footer />)

    const link = screen.getByRole('link', { name: 'Privacyverklaring' })

    expect(link).toHaveAttribute('href', '/privacy')
  })

  it.each([
    {
      name: 'Logo Ministerie van Binnenlandse Zaken en Koninkrijksrelaties',
    },
    {
      name: 'Logo VNG Realisatie',
    },
  ])('has an img: $name', ({ name }) => {
    renderWithProviders(<Footer />)

    expect(
      screen.getByRole('img', {
        name,
      }),
    ).toBeInTheDocument()
  })
})
