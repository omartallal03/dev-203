// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { Formik } from 'formik'
import { breakpoints } from '../../theme'
import { ReactComponent as FilterIcon } from '../../assets/svg/filter-icon.svg'
import { APIQueryParams } from '../../containers/Apis/types'
import { RepositoryQueryParams } from '../../containers/Repositories/types'
import Filter from './Filter'
import { StyledFilterButton, StyledAPIFilters } from './styles'
import { facetsContainTermsForFilterByKey, formatOptions } from './helpers'
import { Facets, Filter as IFilter } from './types'

interface Props {
  facets: Facets
  filters: IFilter[]
  initialValues?: APIQueryParams | RepositoryQueryParams
  onExpand?: (isExpanded: boolean) => void
  onSubmit: (newFilters) => void
  shouldExpand?: boolean
  sortOnCount?: boolean
}

const Filters: React.FunctionComponent<Props> = ({
  facets,
  filters,
  initialValues = {},
  onExpand: handleExpand,
  onSubmit,
  shouldExpand,
  sortOnCount,
  ...props
}) => {
  const isMobile = window.innerWidth < breakpoints.md
  const [showFilters, setShowFilters] = useState(!isMobile)

  return (
    <>
      <StyledFilterButton
        variant="secondary"
        onClick={() => setShowFilters(!showFilters)}
        showFilters={showFilters}
      >
        <FilterIcon />
        Filters
      </StyledFilterButton>
      <StyledAPIFilters showFilters={showFilters} {...props}>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          enableReinitialize
        >
          {({ handleSubmit, values }) => (
            <form onSubmit={handleSubmit}>
              {filters
                .filter((filter) => {
                  return facetsContainTermsForFilterByKey(facets, filter.key)
                })
                .map((filter, i) => {
                  const options = formatOptions(facets, filter)

                  return (
                    <Filter
                      id={filter.key}
                      key={i}
                      name={filter.key}
                      onChangeHandler={handleSubmit}
                      onExpand={handleExpand}
                      options={options}
                      shouldExpand={shouldExpand}
                      sortOnCount={sortOnCount}
                      skipTo={filter.skipTo}
                      title={filter.label}
                      value={values[filter.key.toString()] || []}
                    />
                  )
                })}
            </form>
          )}
        </Formik>
      </StyledAPIFilters>
    </>
  )
}

export default Filters
