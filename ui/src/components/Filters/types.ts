// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export type FacetKey =
  | 'organization_ooid'
  | 'api_type'
  | 'programming_languages'

export interface Term {
  term: string
  display_name: string
  count: number
}

export interface Terms {
  terms: Term[]
}

export type Facets = Record<FacetKey, Terms>

export interface Filter {
  key: FacetKey
  label: string
  display_name?: string
  getLabel: (term: string) => string
  skipTo: string
}

export interface FilterOption {
  count: number
  disabled: boolean
  label: string
  value: string
}
