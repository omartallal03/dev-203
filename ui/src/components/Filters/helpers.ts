// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { Facets, Filter, FilterOption } from './types'

const shouldShowDisplayName = ['organization_ooid', 'programming_languages']

export const formatOptions = (
  facets: Facets,
  filter: Filter,
): FilterOption[] => {
  return facets[filter.key.toString()].terms.map((termData) => {
    return {
      value: termData.term.toString(),
      label: (() => {
        if (shouldShowDisplayName.includes(filter.key)) {
          return filter.getLabel(termData.display_name)
        }
        return filter.getLabel(termData.term)
      })(),
      count: termData.count,
      disabled: termData.count === 0,
    }
  })
}

export const facetsContainTermsForFilterByKey = (
  facets: Facets | undefined,
  filterKey: string,
): boolean => !!facets?.[filterKey.toString()]?.terms.length
