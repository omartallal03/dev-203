// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Button } from '@commonground/design-system'

import mq from '../../theme/mediaQueries'

interface StyledAPIFiltersProps {
  showFilters: boolean
}

export const StyledFilterButton = styled(Button)`
  justify-content: center;
  margin-bottom: ${(p) => (p.showFilters ? '0' : p.theme.tokens.spacing08)};
  width: 100%;

  ${mq.mdUp`
    display: none;
  `}

  & svg {
    height: 16px;
    margin-right: ${(p) => p.theme.tokens.spacing04};
    width: 16px;
  }
`

export const StyledAPIFilters = styled.div`
  display: ${(p: StyledAPIFiltersProps) => (p.showFilters ? 'block' : 'none')};
  margin-bottom: ${(p) => p.theme.tokens.spacing09};
  width: 215px;
`
