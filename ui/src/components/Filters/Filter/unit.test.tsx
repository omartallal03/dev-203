// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { Filter } from '../types'
import { APIType } from '../../../domain/models/enums'
import { renderWithProviders, screen } from '../../../test-helpers'
import { MAX_ITEMS_SHOWN } from './constants'
import { sortOptionsByCount } from './helpers'
import GenericFilter from './index'

export const FILTERS: Filter[] = [
  {
    key: 'api_type',
    label: 'API type',
    getLabel: (term: string) => APIType.valueOf(term).label,
    skipTo: 'organization_ooid',
  },
  {
    key: 'organization_ooid',
    label: 'Organisatie',
    getLabel: (term: string) => term,
    skipTo: 'apis',
  },
]

const generateOptions = (total: number = MAX_ITEMS_SHOWN) => {
  return Array(total)
    .fill(total)
    .map((value, index) => ({
      value: value.toString(),
      label: value.toString(),
      count: index * 10,
      disabled: false,
    }))
}

const props: React.ComponentProps<typeof GenericFilter> = {
  title: 'the title',
  name: 'MyFilter',
  options: [],
  value: [],
  onChangeHandler: jest.fn(),
}

describe('<Filter />', () => {
  it('shows  heading', () => {
    renderWithProviders(<GenericFilter {...props} />)

    expect(
      screen.getByRole('heading', { level: 2, name: 'the title' }),
    ).toBeInTheDocument()
  })

  describe(`${MAX_ITEMS_SHOWN} or less options in the filter`, () => {
    it('shows filters without an Expandable component', () => {
      renderWithProviders(
        <GenericFilter
          {...props}
          options={generateOptions()}
          shouldExpand={true}
        />,
      )
      expect(screen.queryAllByText('+ Alle opties')).toHaveLength(0)
    })
  })

  describe(`more than ${MAX_ITEMS_SHOWN} options in the filter`, () => {
    it('shows filters with an Expandable component', () => {
      renderWithProviders(
        <GenericFilter
          {...props}
          options={generateOptions(MAX_ITEMS_SHOWN + 1)}
          shouldExpand={true}
        />,
      )
      expect(screen.getByText('+ Alle opties')).toBeInTheDocument()
    })
  })

  describe('a11y', () => {
    it('has a skip link', () => {
      renderWithProviders(<GenericFilter {...props} skipTo="acme" />)

      expect(screen.getByRole('link')).toBeInTheDocument()
      expect(screen.getByRole('link')).toHaveAttribute('href', '#acme')
    })
  })
})

describe('sortOptionsByCount()', () => {
  it('sorts by count and limits the number of items returned', () => {
    const options = generateOptions(20)

    expect(sortOptionsByCount(options, 5)).toEqual([
      { count: 190, disabled: false, label: '20', value: '20' },
      { count: 180, disabled: false, label: '20', value: '20' },
      { count: 170, disabled: false, label: '20', value: '20' },
      { count: 160, disabled: false, label: '20', value: '20' },
      { count: 150, disabled: false, label: '20', value: '20' },
    ])
  })
})
