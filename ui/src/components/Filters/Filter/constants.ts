// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
export const MAX_ITEMS_SHOWN = 10
export const ITEM_HEIGHT = '2.25rem'
