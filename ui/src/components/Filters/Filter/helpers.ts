// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { take } from 'lodash'
import { FilterOption } from '../types'
import { MAX_ITEMS_SHOWN } from './constants'

export const sortOptionsByCount = (
  options: FilterOption[],
  maxLength = MAX_ITEMS_SHOWN,
) => {
  const sortedOptions = [...options]
  const sortedByCount = sortedOptions.sort((optionA, optionB) => {
    return optionB.count - optionA.count
  })

  return take(sortedByCount, maxLength).sort((optionA, optionB) => {
    return optionA.label.localeCompare(optionB.label)
  })
}
