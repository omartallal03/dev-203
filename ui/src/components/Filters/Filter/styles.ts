// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledAPIFilter = styled.div`
  &:not(:last-child) {
    margin-bottom: ${(p) => p.theme.tokens.spacing09};
  }

  h2 {
    color: ${(p) => p.theme.tokens.colorPaletteGray900};
    font-size: ${(p) => p.theme.tokens.fontSizeMedium};
    font-weight: ${(p) => p.theme.tokens.fontWeightSemiBold};
    line-height: ${(p) => p.theme.tokens.lineHeightText};
    margin: 0 0 ${(p) => p.theme.tokens.spacing05};
  }
`

export const StyledHeaderContainer = styled.div.attrs(({ ref }) => ({
  ref: ref,
}))`
  display: flex;

  a {
    clip: rect(1px, 1px, 1px, 1px);
    color: ${(p) => p.theme.tokens.colorPaletteGray900};
    height: 1px;
    margin-left: auto;
    overflow: hidden;
    position: absolute;
    text-decoration: none;
    white-space: nowrap;
    width: 1px;

    &:focus {
      clip: auto;
      height: auto;
      position: static;
      width: auto;
    }
  }
`
