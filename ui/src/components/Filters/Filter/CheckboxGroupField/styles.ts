// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledCheckboxGroupField = styled.div`
  display: flex;
  margin-top: ${(p) => p.theme.tokens.spacing04};
  position: relative;
  user-select: none;

  input {
    flex: 0 0 auto;
    margin: 3px 8px 0 0;

    &[disabled] {
      opacity: 0.5;

      + label {
        opacity: 0.5;
        text-decoration: line-through;
      }
    }

    &:not([disabled]) {
      cursor: pointer;

      + label {
        cursor: pointer;
      }
    }
  }

  label {
    color: ${(p) => p.theme.tokens.colorPaletteGray900};
    font-size: ${(p) => p.theme.tokens.fontSizeMedium};
    line-height: ${(p) => p.theme.tokens.lineHeightText};
  }

  .count {
    color: ${(p) => p.theme.colorTextLight};
    font-size: ${(p) => p.theme.tokens.fontSizeSmall};
    line-height: ${(p) => p.theme.tokens.lineHeightText};
    margin-left: ${(p) => p.theme.tokens.spacing03};
  }
`
