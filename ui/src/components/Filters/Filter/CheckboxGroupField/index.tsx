// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { FieldArray } from 'formik'
import { CheckboxField } from '../../../Form'
import { DONSmall } from '../../../CustomDON'
import { StyledCheckboxGroupField } from './styles'

interface Props {
  name: string
  options: {
    value: string
    count: number
    label: string
    disabled?: boolean
  }[]
  onChange: () => void
  value: string[]
}

const CheckboxGroupField: React.FunctionComponent<Props> = ({
  name,
  options,
  value,
  onChange,
}) => (
  <FieldArray name={name}>
    {(arrayHelpers) => (
      <>
        {options.map((option, index) => (
          <StyledCheckboxGroupField key={index}>
            <CheckboxField
              type="checkbox"
              id={`${name}.${index}`}
              name={`${name}.${index}`}
              value={option.value}
              checked={value.indexOf(option.value) !== -1}
              disabled={option.disabled}
              onChange={() => {
                value.indexOf(option.value) === -1
                  ? arrayHelpers.insert(index, option.value)
                  : arrayHelpers.remove(value.indexOf(option.value))
                onChange && setTimeout(() => onChange(), 0)
              }}
            />
            <label key={index} htmlFor={`${name}.${index}`}>
              {option.label}
              <DONSmall className="count">({option.count})</DONSmall>
            </label>
          </StyledCheckboxGroupField>
        ))}
      </>
    )}
  </FieldArray>
)

export default CheckboxGroupField
