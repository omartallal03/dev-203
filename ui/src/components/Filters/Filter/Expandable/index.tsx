// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { StyledToggleButton } from './styles'

interface Props {
  onExpand?: (isExpanded: boolean) => void
  showExpandButton?: boolean
}

const Expandable: React.FunctionComponent<Props> = ({
  children,
  onExpand,
  showExpandButton,
}) => {
  const [isExpanded, setIsExpanded] = useState(false)

  const handleToggleExpand = () => {
    const expanded = !isExpanded
    setIsExpanded(expanded)

    if (typeof onExpand === 'function') {
      onExpand(expanded)
    }
  }

  return (
    <>
      <div data-test="content">{children}</div>
      {showExpandButton && (
        <StyledToggleButton onClick={handleToggleExpand} type="button">
          {isExpanded ? '- Minder opties' : '+ Alle opties'}
        </StyledToggleButton>
      )}
    </>
  )
}

export default Expandable
