// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import userEvent from '@testing-library/user-event'
import { renderWithProviders, screen } from '../../../../test-helpers'
import Expandable from './index'

describe('<Expandable />', () => {
  it('renders its children', () => {
    renderWithProviders(
      <Expandable>
        <p>Content</p>
      </Expandable>,
    )

    expect(screen.getByText(/Content/)).toBeInTheDocument()
  })

  it('hides the expand button by default', () => {
    renderWithProviders(
      <Expandable>
        <p>Content</p>
      </Expandable>,
    )

    expect(screen.queryAllByText('+ Alle opties')).toHaveLength(0)
    expect(screen.queryAllByText('- Minder opties')).toHaveLength(0)
  })

  it('renders an expand button when enabled', () => {
    renderWithProviders(
      <Expandable showExpandButton={true}>
        <p>Content</p>
      </Expandable>,
    )

    expect(screen.getByText('+ Alle opties')).toBeInTheDocument()
  })

  it('expands when clicked and fires onExpand', async () => {
    const onExpand = jest.fn()
    renderWithProviders(
      <Expandable showExpandButton={true} onExpand={onExpand}>
        <p>Content</p>
      </Expandable>,
    )
    await userEvent.click(screen.getByText('+ Alle opties'))

    expect(screen.getByText('- Minder opties')).toBeInTheDocument()
    expect(onExpand).toHaveBeenCalledWith(true)
  })
})
