// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledContent = styled.div``

export const StyledToggleButton = styled.button`
  background-color: transparent;
  border: none;
  color: ${(p) => p.theme.colorTextLink};
  cursor: pointer;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  padding: 12px 10px;
  text-align: left;
`
