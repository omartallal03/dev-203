// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useRef } from 'react'
import { FilterOption } from '../types'
import Expandable from './Expandable'
import CheckboxGroupField from './CheckboxGroupField'
import { StyledAPIFilter, StyledHeaderContainer } from './styles'
import { MAX_ITEMS_SHOWN } from './constants'
import { sortOptionsByCount } from './helpers'

interface Props {
  title: string
  name: string
  options: FilterOption[]
  value: string[]
  onChangeHandler: () => void
  skipTo?: string
  id?: string
  shouldExpand?: boolean
  sortOnCount?: boolean
  onExpand?: (isExpanded: boolean) => void
}

const Filter: React.FunctionComponent<Props> = ({
  id,
  name,
  onChangeHandler,
  onExpand,
  options,
  shouldExpand,
  skipTo,
  sortOnCount,
  title,
  value,
}) => {
  const titleRef: React.MutableRefObject<HTMLDivElement | null> = useRef(null)

  const handleExpand = (isExpanded: boolean) => {
    if (typeof onExpand === 'function') {
      onExpand(isExpanded)
    }

    if (!isExpanded) {
      titleRef.current?.scrollIntoView({
        behavior: 'smooth',
      })
    }
  }

  const showExpandButton = options.length > MAX_ITEMS_SHOWN && shouldExpand
  const filterOptions = sortOnCount ? sortOptionsByCount(options) : options

  return (
    <StyledAPIFilter id={id}>
      <StyledHeaderContainer ref={titleRef}>
        <h2>{title}</h2>
        <a href={`#${skipTo}`}>overslaan</a>
      </StyledHeaderContainer>
      <Expandable onExpand={handleExpand} showExpandButton={showExpandButton}>
        <CheckboxGroupField
          name={name}
          options={filterOptions}
          value={value}
          onChange={onChangeHandler}
        />
      </Expandable>
    </StyledAPIFilter>
  )
}

export default Filter
