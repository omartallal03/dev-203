// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { FILTERS } from '../../containers/Apis/ApiFilters/constants'
import { renderWithProviders, screen } from '../../test-helpers'
import { facetsContainTermsForFilterByKey, formatOptions } from './helpers'
import { Facets } from './types'
import Filters from './index'

const props: React.ComponentProps<typeof Filters> = {
  facets: {
    programming_languages: {
      terms: [
        {
          term: 27,
          display_name: 'C',
          count: 1,
        },
        {
          term: 3,
          display_name: 'C#',
          count: 4,
        },
        {
          term: 5,
          display_name: 'CSS',
          count: 27,
        },
        {
          term: 24,
          display_name: 'Dart',
          count: 4,
        },
        {
          term: 8,
          display_name: 'Dockerfile',
          count: 43,
        },
        {
          term: 16,
          display_name: 'Gherkin',
          count: 1,
        },
        {
          term: 13,
          display_name: 'Go',
          count: 29,
        },
        {
          term: 26,
          display_name: 'HCL',
          count: 4,
        },
        {
          term: 4,
          display_name: 'HTML',
          count: 56,
        },
        {
          term: 14,
          display_name: 'Java',
          count: 1,
        },
        {
          term: 6,
          display_name: 'JavaScript',
          count: 45,
        },
        {
          term: 22,
          display_name: 'Jsonnet',
          count: 1,
        },
        {
          term: 10,
          display_name: 'Jupyter Notebook',
          count: 2,
        },
        {
          term: 19,
          display_name: 'Makefile',
          count: 3,
        },
        {
          term: 15,
          display_name: 'PLpgSQL',
          count: 3,
        },
        {
          term: 28,
          display_name: 'PowerShell',
          count: 1,
        },
        {
          term: 2,
          display_name: 'Python',
          count: 19,
        },
        {
          term: 21,
          display_name: 'QML',
          count: 1,
        },
        {
          term: 1,
          display_name: 'R',
          count: 2,
        },
        {
          term: 12,
          display_name: 'Ruby',
          count: 7,
        },
        {
          term: 23,
          display_name: 'SCSS',
          count: 4,
        },
        {
          term: 20,
          display_name: 'Scheme',
          count: 1,
        },
        {
          term: 7,
          display_name: 'Shell',
          count: 42,
        },
        {
          term: 18,
          display_name: 'Smarty',
          count: 8,
        },
        {
          term: 25,
          display_name: 'Swift',
          count: 3,
        },
        {
          term: 9,
          display_name: 'TSQL',
          count: 1,
        },
        {
          term: 17,
          display_name: 'TypeScript',
          count: 9,
        },
      ],
    },
    topics: {
      terms: [
        {
          term: 10,
          display_name: 'CDF main web map tool.',
          count: 1,
        },
        {
          term: 1,
          display_name: 'adr',
          count: 2,
        },
        {
          term: 7,
          display_name: 'aggregation',
          count: 1,
        },
        {
          term: 2,
          display_name: 'api',
          count: 3,
        },
        {
          term: 3,
          display_name: 'beheermodel',
          count: 1,
        },
        {
          term: 4,
          display_name: 'bomos',
          count: 1,
        },
        {
          term: 8,
          display_name: 'db',
          count: 1,
        },
        {
          term: 5,
          display_name: 'logius',
          count: 2,
        },
        {
          term: 9,
          display_name: 'rest',
          count: 1,
        },
        {
          term: 6,
          display_name: 'standaarden',
          count: 2,
        },
      ],
    },
  },
  filters: [
    {
      key: 'programming_languages',
      label: 'Technologie',
      skipTo: 'repositories',
      getLabel: (term: string) => term,
    },
  ],
}

describe('<Filters />', () => {
  it('renders correctly', () => {
    renderWithProviders(<Filters {...props} />)

    expect(screen.getByText('Filters')).toBeInTheDocument()
    expect(screen.getByText('Technologie')).toBeInTheDocument()
  })
})

describe('formatOptions()', () => {
  const apiTypeFilter = FILTERS.find((filter) => filter.key === 'api_type')
  const organizationFilter = FILTERS.find(
    (filter) => filter.key === 'organization_ooid',
  )

  it.each([
    {
      facets: {
        organization_ooid: {
          terms: [],
        },
      },
      filter: organizationFilter,
      expected: [],
    },
    {
      facets: {
        organization_ooid: {
          terms: [
            {
              term: '00000001001589623000',
              display_name: 'MijnBV',
              count: 5,
            },
          ],
        },
      },
      filter: organizationFilter,
      expected: [
        {
          value: '00000001001589623000',
          label: 'MijnBV',
          count: 5,
          disabled: false,
        },
      ],
    },
    {
      facets: {
        api_type: {
          terms: [
            { term: 'grpc', count: 5 },
            { term: 'graphql', count: 0 },
          ],
        },
      },
      filter: apiTypeFilter,
      expected: [
        { value: 'grpc', label: 'gRPC', count: 5, disabled: false },
        { value: 'graphql', label: 'GraphQL', count: 0, disabled: true },
      ],
    },
  ])(
    'returns $expected when facets $facets',
    ({ facets, filter, expected }) => {
      expect(formatOptions(facets as unknown as Facets, filter)).toEqual(
        expected,
      )
    },
  )
})

describe('facetsContainTermsForFilterByKey()', () => {
  it.each([
    {
      facets: { organization_ooid: { terms: [] } },
      filter: 'organization_ooid',
      expected: false,
    },
    {
      facets: { organization_ooid: { terms: [{ term: 'foo', count: 1 }] } },
      filter: 'organization_ooid',
      expected: true,
    },
  ])(
    'returns $expected when facets $facets',
    ({ facets, filter, expected }) => {
      expect(
        facetsContainTermsForFilterByKey(facets as unknown as Facets, filter),
      ).toEqual(expected)
    },
  )
})
