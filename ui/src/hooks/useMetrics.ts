// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useQuery } from 'react-query'
import { UseQueryResult } from 'react-query/types/react/types'
import MetricsRepository from '../domain/metrics-repository'
import { Metrics } from '../domain/models/types'

export const useMetrics = (
  timeRange: string | null,
): UseQueryResult<Metrics> => {
  return useQuery(['metrics', timeRange], () =>
    MetricsRepository.getMetrics(timeRange),
  )
}
