// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useEffect, useState } from 'react'
import {
  createSearchParams,
  useLocation,
  useNavigate,
  useSearchParams,
} from 'react-router-dom'
import queryString from 'query-string'

const serialize = <T>(state: T): string => queryString.stringify(state as any)
const deserialize = <T>(state: string): T =>
  queryString.parse(state, {
    parseBooleans: true,
    parseNumbers: true,
  }) as unknown as T

export function useStateParams<T>(initialState: T): [T, (state: T) => void] {
  const [, setSearchParams] = useSearchParams()
  const location = useLocation()
  const navigate = useNavigate()
  const search = new URLSearchParams(location.search)

  const existingValue = search.toString()

  const [state, setState] = useState<T>(
    existingValue ? deserialize(existingValue) : initialState,
  )

  useEffect(() => {
    if (existingValue && deserialize(existingValue) !== state) {
      setState(deserialize(existingValue))
    }
  }, [existingValue]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (!existingValue && typeof initialState !== 'undefined') {
      setSearchParams(serialize(initialState))
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const onChange = (s: T) => {
    setState(s)
    const value = serialize(s)
    const searchParams = new URLSearchParams(value)
    const pathname = location.pathname

    navigate({
      pathname,
      search: `${createSearchParams(searchParams.toString())}`,
    })
  }

  return [state, onChange]
}
