// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import * as Router from 'react-router-dom'
import { act, renderHook } from '@testing-library/react-hooks'
import { useStateParams } from './useStateParams'

const mockedUsedNavigate = jest.fn()

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: () => mockedUsedNavigate,
  useLocation: () => jest.fn(),
}))

describe('useStateParams()', () => {
  it.each([
    {
      initialState: false,
      paramsName: 'boolean',
      expected: 'boolean=false',
    },
    {
      initialState: 10,
      paramsName: 'number',
      expected: 'number=10',
    },
    {
      initialState: 'value',
      paramsName: 'value',
      expected: 'value=value',
    },
    {
      initialState: { foo: 'bar' },
      paramsName: 'object',
      expected: 'foo=bar',
    },
  ])(
    'handles $paramsName correctly',
    ({ initialState, paramsName, expected }) => {
      const wrapper = ({ children }) => (
        <Router.MemoryRouter>{children}</Router.MemoryRouter>
      )
      const { result } = renderHook(() => useStateParams(initialState), {
        wrapper,
      })

      const [state, onChange] = result.current

      expect(state).toEqual(initialState)
      expect(typeof onChange).toEqual('function')
    },
  )

  it('correctly navigates to the new value after onChange()', () => {
    const wrapper = ({ children }) => (
      <Router.MemoryRouter>{children}</Router.MemoryRouter>
    )
    const { result } = renderHook(() => useStateParams({}), {
      wrapper,
    })

    const [, onChange] = result.current

    act(() => {
      onChange({ foo: 'bar' })
    })

    expect(mockedUsedNavigate).toHaveBeenCalledWith({
      pathname: undefined,
      search: 'foo=bar',
    })
  })
})
