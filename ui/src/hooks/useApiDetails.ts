// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useQuery } from 'react-query'
import { UseQueryResult } from 'react-query/types/react/types'
import APIDetailsRepository from '../domain/api-details-repository'
import { Api } from '../domain/models/types'

export const useApiDetails = (id: string): UseQueryResult<Api> => {
  return useQuery([id], () => APIDetailsRepository.getById(id))
}
