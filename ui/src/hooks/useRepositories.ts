// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useQuery } from 'react-query'
import { UseQueryResult } from 'react-query/types/react/types'
import { IncomingApiResponse, IncomingRepository } from '../domain/models/types'
import { generateQueryParams } from '../utils/uriHelpers'
import { RepositoryQueryParams } from '../containers/Repositories/types'
import CodeRepository from '../domain/code-repository'

export const useRepositories = (
  queryParams: RepositoryQueryParams,
): UseQueryResult<IncomingApiResponse<IncomingRepository[]>> => {
  return useQuery(
    ['apis', queryParams],
    () => CodeRepository.getAll(generateQueryParams(queryParams)),
    { keepPreviousData: true },
  )
}
