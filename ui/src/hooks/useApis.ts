// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { useQuery } from 'react-query'
import { UseQueryResult } from 'react-query/types/react/types'
import { IncomingApi, IncomingApiResponse } from '../domain/models/types'
import APIRepository from '../domain/api-repository'
import { APIQueryParams } from '../containers/Apis/types'
import { generateQueryParams } from '../utils/uriHelpers'

export const useApis = (
  queryParams: APIQueryParams,
): UseQueryResult<IncomingApiResponse<IncomingApi[]>> => {
  return useQuery(
    ['apis', queryParams],
    () => APIRepository.getAll(generateQueryParams(queryParams)),
    { keepPreviousData: true },
  )
}
