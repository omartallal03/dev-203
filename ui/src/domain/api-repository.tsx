// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import Cookies from 'js-cookie'
import { modelFromAPIResponse } from './models/api'
import { IncomingApi, IncomingApiResponse } from './models/types'

class APIRepository {
  static async getAll(
    queryParams?: URLSearchParams | string,
  ): Promise<IncomingApiResponse<IncomingApi[]>> {
    const params = queryParams || `rowsPerPage=${Number.MAX_SAFE_INTEGER}`
    const result = await fetch(`/api/apis?${params}`)

    if (!result.ok) {
      throw new Error(
        `Er ging iets fout tijdens het ophalen van de beschikbare API's`,
      )
    }

    return result.json()
  }

  static async getById(apiId: string): Promise<IncomingApi> {
    const result = await fetch(`/api/apis/${apiId}`)

    if (!result.ok || result.status !== 200) {
      throw new Error(
        `Er ging iets fout bij het ophalen voor de API met ID '${apiId}'`,
      )
    }

    return result.json()
  }

  static async fetchImplementedByInfo(id: string): Promise<IncomingApi[]> {
    const response = await fetch(`/api/apis/${id}/implemented-by`)
    if (response.ok) {
      const json = await response.json()
      return json.map((api) => modelFromAPIResponse(api))
    } else {
      throw new Error(
        `Er ging iets fout bij het ophalen van de referentie implementaties voor API met ID '${id}'`,
      )
    }
  }

  static async create(data: Record<string, string>): Promise<{ json: never }> {
    const response = await fetch('/api/submit-api', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
    })
    if (response.ok) {
      return response.json()
    } else {
      throw new Error('Er ging iets fout tijdens het toevoegen van de API.')
    }
  }
}

export default APIRepository
