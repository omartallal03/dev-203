// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { IncomingOoResponse } from './models/types'
import { modelFromApiResponse } from './models/oo'

export default class OoRepository {
  static async search(
    value?: string,
  ): Promise<{ label: string; value: string }[]> {
    const result = await fetch(value ? `/api/oo/?zoek=${value}` : `/api/oo/`)

    if (!result.ok) {
      throw new Error('Kan de lijst met organisaties niet ophalen.')
    }

    const oo: IncomingOoResponse = await result.json()

    return modelFromApiResponse(oo)
  }
}
