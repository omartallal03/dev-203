// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { IncomingOoResponse } from './types'

export const modelFromApiResponse = (
  data?: IncomingOoResponse,
): { label: string; value: string }[] => {
  if (typeof data === 'undefined') {
    return []
  }

  return data.map((organisatie) => ({
    label: organisatie.naam,
    value: organisatie.systeem_id.toString(),
  }))
}
