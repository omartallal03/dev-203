// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { ApiEnvironment } from '../../containers/Apis/ApiDetail/APIDetails/ApiEnvironments/types'
import { Forum } from '../../containers/Apis/ApiDetail/APIDetails/ForumPosts/types'
import { Facets } from '../../components/Filters/types'
import { APIAuthentication, APIType, EnvironmentType } from './enums'

export interface IncomingApiResponse<T> {
  facets: Facets
  page: number
  rowsPerPage: number
  totalResults: number
  results: T
  programmingLanguages: IncomingProgrammingLanguages
}

export interface IncomingScores {
  has_documentation: boolean
  has_specification: boolean
  has_contact_details: boolean
  provides_sla: boolean
}

export interface IncomingDesignRuleScores {
  results: {
    rule_type_name: string
    rule_type_description: string
    rule_type_url: string
    success: boolean
    errors: string[]
  }[]
}

export interface IncomingEnvironment {
  name: EnvironmentType
  api_url: string
  specification_url: string
  documentation_url: string
}

interface IncomingForum {
  vendor: string
  url: string
}

export interface IncomingTermsOfUse {
  government_only: boolean
  pay_per_use: boolean
  support_response_time: number
  uptime_guarantee: string
}

type ProgrammingLanguageName = string
type ProgrammingLanguagePercentage = number

export type IncomingProgrammingLanguages = Record<
  ProgrammingLanguageName,
  ProgrammingLanguagePercentage
>

export interface IncomingRelatedCode {
  id: string
  last_change: string
  name: string
  owner_name: string
  programming_languages: IncomingProgrammingLanguages
  source: string
  stars: number
  url: string
}

export interface IncomingApi {
  api_authentication: APIAuthentication
  api_type: string
  badges: string[]
  description: string
  design_rule_scores: IncomingDesignRuleScores
  environments: IncomingEnvironment[]
  forum: IncomingForum
  id: string
  is_reference_implementation: boolean
  organization: {
    name: string
    ooid: number
  }
  related_repositories: IncomingRelatedCode[]
  relations: Record<string, string>
  scores: IncomingScores
  service_name: string
  terms_of_use: IncomingTermsOfUse
}

export interface TermsOfUse {
  governmentOnly: boolean
  payPerUse: boolean
  supportResponseTime?: number
  uptimeGuarantee: number
}

export interface Scores {
  hasContactDetails?: boolean
  hasDocumentation?: boolean
  providesSla?: boolean
  hasSpecification?: boolean
}

export interface TotalScore {
  points: number
  maxPoints: number
}

export interface Api {
  apiAuthentication: APIAuthentication
  apiType: APIType
  badges: string[]
  description: string
  designRuleScores: {
    results: {
      name: string
      description: string
      url: string
      success: boolean
      errors: string[]
    }[]
  } | null
  environments: ApiEnvironment[]
  forum: Forum
  id: string
  isReferenceImplementation: boolean
  organization: {
    name: string
    ooid: number
  }
  relatedCode: IncomingRelatedCode[]
  relations: Record<string, string>
  scores: Scores
  serviceName: string
  termsOfUse: TermsOfUse
  totalScore: TotalScore
}

export interface IncomingRelatedApi {
  service_name: string
  organization_name: string
  api_id: string
}

export interface IncomingRepository {
  id: number
  owner_name: string
  description: string
  name: string
  url: string
  last_change: string
  stars: number | null
  source: string
  programming_languages: IncomingProgrammingLanguages
  related_apis: {
    api_id: string
    service_name: string
    organization_name: string
  }[]
  avatar_url: string
  fork_count: number | null
  issue_open_count: number | null
  merge_request_open_count: number | null
}

export interface Pagination {
  page: number
  totalResults: number
  rowsPerPage: number
}

export interface Repository {
  id: number
  ownerName: string
  description: string
  name: string
  url: string
  lastChange: Date
  stars: number | null
  source: string
  relatedApis: {
    apiId: string
    serviceName: string
    organizationName: string
  }[]
  programmingLanguages: Record<string, number>
  avatarUrl: string
  forkCount: number | null
  issueOpenCount: number | null
  mergeRequestOpenCount: number | null
}

export interface IncomingEvent {
  id: number
  title: string
  start_date: string
  location: string
  registration_url: string
}

export interface Event {
  id: number
  title: string
  startDate: Date
  location: string
  registrationUrl: string
}

export interface IncomingSubmitApiResponse {
  id: number
  iid: number
  project_id: number
  title: string
  description: string
  state: string
  created_at: string
  updated_at: string
  closed_at: string | null
  closed_by: string | null
  labels: string[]
  milestone: string | null
  assignees: string[]
  author: {
    id: number
    username: string
    name: string
    state: string
    avatar_url: string
    web_url: string
  }
  type: string
  assignee: string | null
  user_notes_count: number
  merge_requests_count: number
  upvotes: number
  downvotes: number
  due_date: string | null
  confidential: boolean
  discussion_locked: string | null
  issue_type: string
  web_url: string
  time_stats: {
    time_estimate: number
    total_time_spent: number
    human_time_estimate: string | null
    human_total_time_spent: string | null
  }
  task_completion_status: { count: number; completed_count: number }
  weight: string | null
  blocking_issues_count: number
  has_tasks: boolean
  _links: {
    self: string
    notes: string
    award_emoji: string
    project: string
  }
  references: {
    short: string
    relative: string
    full: string
  }
  subscribed: boolean
  moved_to_id: string | null
  service_desk_reply_to: string | null
  epic_iid: string | null
  epic: string | null
  health_status: string | null
}

export interface IncomingSubmitCodeRequest {
  url: string
  relatedApis: string[]
}

export type IncomingSubmitCodeResponse = 'Code toegevoegd' | { detail: string }

type IncomingTotalCounts = Record<string, Record<string, number>>
type IncomingDesignRules = Record<
  string,
  Record<string, { success_count: number; total_count: number }>
>
type IncomingDesignRulesScore = Record<string, Record<string, number>>

type DesignRules = Record<
  string,
  Record<string, { successCount: number; totalCount: number }>
>
type DesignRulesScore = Record<
  string,
  {
    categories: []
    values: []
  }
>

export interface IncomingMetrics {
  api: IncomingTotalCounts
  organization: IncomingTotalCounts
  repository: IncomingTotalCounts
  design_rules_last: IncomingDesignRules
  design_rules_api_successes_last: IncomingDesignRulesScore
}

type TotalCount = { totalCounts: [number, number][] } | undefined
export interface Metrics {
  api: TotalCount
  organization: TotalCount
  repository: TotalCount
  designRulesLast: DesignRules
  designRulesApiSuccessesLast: DesignRulesScore
}

export interface IncomingOoObject {
  systeem_id: number
  naam: string
}

export type IncomingOoResponse = IncomingOoObject[]
