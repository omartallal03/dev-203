// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { Repository, IncomingRepository } from './types'

export const modelFromCodeResponse = (
  code: IncomingRepository,
): Repository => ({
  id: code.id,
  ownerName: code.owner_name,
  description: code.description,
  name: code.name,
  url: code.url,
  lastChange: new Date(code.last_change),
  stars: code.stars,
  source: code.source,
  relatedApis: code.related_apis.map((relatedApi) => ({
    apiId: relatedApi.api_id,
    organizationName: relatedApi.organization_name,
    serviceName: relatedApi.service_name,
  })),
  programmingLanguages: code.programming_languages || [],
  avatarUrl: code.avatar_url,
  forkCount: code.fork_count,
  issueOpenCount: code.issue_open_count,
  mergeRequestOpenCount: code.merge_request_open_count,
})
