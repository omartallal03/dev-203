// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import moment from 'moment'
import { camelCase } from 'change-case'
import { ReturnValue } from '../../components/ColumnChart/types'
import { TimeRange } from '../../containers/Apis/ApiStatistics/constants'
import { IncomingMetrics, Metrics } from './types'

const isSuiteValue = (value: unknown): value is Record<string, number> => {
  return typeof value === 'object' && value !== null
}

export const modelFromMetricsResponse = (
  metrics: IncomingMetrics,
  timeRange: string | null,
): Metrics => {
  return Object.keys(metrics).reduce((result, metric) => {
    if (
      metric === 'api' ||
      metric === 'organization' ||
      metric === 'repository'
    ) {
      return {
        ...result,
        [camelCase(metric.toString())]: Object.keys(
          metrics[metric.toString()],
        ).reduce((metricValue, value) => {
          return {
            ...metricValue,
            [camelCase(value).toString()]: Object.entries(
              metrics[metric.toString()][value.toString()],
            ).map(([date, value]) => {
              if (timeRange === TimeRange.LastYear) {
                return [moment.utc(date).startOf('month').valueOf(), value]
              }
              return [moment.utc(date).valueOf(), value]
            }),
          }
        }, {}),
      }
    }

    if (metric === 'design_rules_last') {
      return {
        ...result,
        [camelCase(metric.toString())]: Object.entries(
          metrics[metric.toString()],
        ).reduce((metrics, [suiteKey, suiteValue]) => {
          if (!isSuiteValue(suiteValue)) {
            return metrics
          }

          return {
            ...metrics,
            [suiteKey]: Object.entries(suiteValue).reduce(
              (ruleResult, [ruleKey, ruleValue]) => {
                return {
                  ...ruleResult,
                  [camelCase(ruleKey)]: Object.entries(ruleValue).reduce(
                    (ruleValue, [ruleValueKey, ruleValueValue]) => {
                      return {
                        ...ruleValue,
                        [camelCase(ruleValueKey)]: ruleValueValue,
                      }
                    },
                    {},
                  ),
                }
              },
              {},
            ),
          }
        }, {}),
      }
    }

    if (metric === 'design_rules_api_successes_last') {
      return {
        ...result,
        [camelCase(metric.toString())]: Object.entries(
          metrics[metric.toString()],
        ).reduce((metrics, [suiteKey, suiteValue]) => {
          if (!isSuiteValue(suiteValue)) {
            return metrics
          }

          return {
            ...metrics,
            [suiteKey]: Object.entries(suiteValue).reduce(
              (rules: ReturnValue, [category, value]) => {
                return {
                  ...rules,
                  categories: [...rules.categories, category],
                  values: [...rules.values, value],
                }
              },
              {
                categories: [],
                values: [],
              },
            ),
          }
        }, {}),
      }
    }

    console.warn('ignoring unrecognized metric key: ', metric)

    return result
  }, {} as Metrics)
}
