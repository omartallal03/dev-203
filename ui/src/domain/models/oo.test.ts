// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { modelFromApiResponse } from './oo'

describe('Oo', () => {
  describe('modelFromApiResponse()', () => {
    it.each([
      {
        data: undefined,
        expected: [],
      },
      {
        data: [
          {
            naam: 'Stichting Geonovum',
            systeem_id: 1,
          },
        ],
        expected: [
          {
            label: 'Stichting Geonovum',
            value: '1',
          },
        ],
      },
    ])('correctly models from the API response', ({ data, expected }) => {
      expect(modelFromApiResponse(data)).toEqual(expected)
    })
  })
})
