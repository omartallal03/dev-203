// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { metricsMock } from '../../../.jest/mocks/metrics.mock'
import { modelFromMetricsResponse } from './metrics'

describe('modelFromMetricsResponse()', () => {
  it('correctly models a response without data', () => {
    expect(
      modelFromMetricsResponse({
        api: { total_counts: [] },
        organization: { total_counts: [] },
        repository: { total_counts: [] },
        design_rules_last: {},
        design_rules_api_successes_last: {},
      }),
    ).toEqual({
      api: { totalCounts: [] },
      organization: { totalCounts: [] },
      repository: { totalCounts: [] },
      designRulesLast: {},
      designRulesApiSuccessesLast: {},
    })
  })

  it('correctly models the api metric', () => {
    expect(modelFromMetricsResponse(metricsMock).api).toEqual({
      totalCounts: [
        [1659312000000, 101],
        [1659398400000, 101],
      ],
    })
  })

  it('correctly models the organization metric', () => {
    expect(modelFromMetricsResponse(metricsMock).organization).toEqual({
      totalCounts: [
        [1659312000000, 18],
        [1659398400000, 18],
      ],
    })
  })

  it('correctly models the organization metric', () => {
    expect(modelFromMetricsResponse(metricsMock).repository).toEqual({
      totalCounts: [
        [1659312000000, 114],
        [1659398400000, 114],
      ],
    })
  })

  it('correctly models the design_rules_last metric', () => {
    expect(modelFromMetricsResponse(metricsMock).designRulesLast).toEqual({
      '09_juli_2020': {
        api_03: { successCount: 2, totalCount: 10 },
        api_16: { successCount: 2, totalCount: 10 },
        api_20: { successCount: 6, totalCount: 10 },
        api_48: { successCount: 2, totalCount: 10 },
        api_51: { successCount: 1, totalCount: 10 },
        api_56: { successCount: 2, totalCount: 10 },
        api_57: { successCount: 2, totalCount: 10 },
      },
    })
  })

  it('correctly models the design_rules_successes_last metric', () => {
    expect(
      modelFromMetricsResponse(metricsMock).designRulesApiSuccessesLast,
    ).toEqual({
      '09_juli_2020': {
        categories: ['0/7', '1/7', '2/7', '3/7', '4/7', '5/7', '6/7', '7/7'],
        values: [4, 4, 0, 0, 0, 0, 1, 1],
      },
    })
  })
})
