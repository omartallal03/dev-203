// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import Cookies from 'js-cookie'
import {
  IncomingApiResponse,
  IncomingRepository,
  IncomingSubmitCodeRequest,
  IncomingSubmitCodeResponse,
} from './models/types'

export default class CodeRepository {
  static async getAll(
    queryParams: URLSearchParams | string,
  ): Promise<IncomingApiResponse<IncomingRepository[]>> {
    const result = await fetch(`/api/repositories?${queryParams}`)

    if (!result.ok) {
      throw new Error('Er ging iets fout tijdens het ophalen van de code')
    }

    return result.json()
  }

  static async create(
    code: IncomingSubmitCodeRequest,
  ): Promise<IncomingSubmitCodeResponse> {
    const result = await fetch(`/api/repositories`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken'),
      },
      body: JSON.stringify(code),
    })

    const resultJSON = await result.json()

    if (!result.ok) {
      throw new Error(
        'Er ging iets fout tijdens het toevoegen van de code' +
        resultJSON?.detail
          ? `: ${resultJSON?.detail}`
          : '',
      )
    }

    return resultJSON
  }
}
