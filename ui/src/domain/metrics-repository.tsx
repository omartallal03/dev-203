// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { TIME_RANGE_KEY } from '../containers/Apis/ApiStatistics/constants'
import { IncomingMetrics, Metrics } from './models/types'
import { modelFromMetricsResponse } from './models/metrics'

const METRICS_API_URL = `/api/dashboard/metrics`

export default class MetricsRepository {
  static async getMetrics(timeRange: string | null): Promise<Metrics> {
    const result = await fetch(
      (() => {
        if (typeof timeRange === 'string') {
          return `${METRICS_API_URL}?${TIME_RANGE_KEY}=${timeRange}`
        }
        return METRICS_API_URL
      })(),
    )

    if (!result.ok || result.status !== 200) {
      throw new Error(`Er ging iets fout bij het ophalen van de statistieken`)
    }

    const metrics: IncomingMetrics = await result.json()

    return modelFromMetricsResponse(metrics, timeRange)
  }
}
