// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { metricsMock } from '../../.jest/mocks/metrics.mock'
import MetricsRepository from './metrics-repository'

describe('MetricsRepository()', () => {
  describe('getMetrics()', () => {
    it('returns metrics', async () => {
      jest.spyOn(global, 'fetch').mockImplementation(
        jest.fn(() =>
          Promise.resolve({
            ok: true,
            status: 200,
            json: () => Promise.resolve(metricsMock),
          }),
        ) as jest.Mock,
      )

      const result = await MetricsRepository.getMetrics()

      expect(result).toEqual({
        api: {
          totalCounts: [
            [1659312000000, 101],
            [1659398400000, 101],
          ],
        },
        designRulesApiSuccessesLast: {
          '09_juli_2020': {
            categories: [
              '0/7',
              '1/7',
              '2/7',
              '3/7',
              '4/7',
              '5/7',
              '6/7',
              '7/7',
            ],
            values: [4, 4, 0, 0, 0, 0, 1, 1],
          },
        },
        designRulesLast: {
          '09_juli_2020': {
            api_03: { successCount: 2, totalCount: 10 },
            api_16: { successCount: 2, totalCount: 10 },
            api_20: { successCount: 6, totalCount: 10 },
            api_48: { successCount: 2, totalCount: 10 },
            api_51: { successCount: 1, totalCount: 10 },
            api_56: { successCount: 2, totalCount: 10 },
            api_57: { successCount: 2, totalCount: 10 },
          },
        },
        organization: {
          totalCounts: [
            [1659312000000, 18],
            [1659398400000, 18],
          ],
        },
        repository: {
          totalCounts: [
            [1659312000000, 114],
            [1659398400000, 114],
          ],
        },
      })

      expect(global.fetch).toHaveBeenCalledWith('/api/dashboard/metrics')
    })

    it('handles errors', async () => {
      jest.spyOn(global, 'fetch').mockImplementation(
        jest.fn(() =>
          Promise.resolve({
            ok: false,
            status: 400,
            json: () => Promise.reject(new Error('invalid json')),
          }),
        ) as jest.Mock,
      )

      try {
        await MetricsRepository.getMetrics()
      } catch (e) {
        expect(e).toEqual(
          new Error('Er ging iets fout bij het ophalen van de statistieken'),
        )
      }
    })
  })
})
