// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { render, waitFor } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import { Formik } from 'formik'
import {
  act,
  screen,
  fireEvent,
  renderWithProviders,
} from '../../../../test-helpers'
import theme from '../../../../theme'
import APIRepository from '../../../../domain/api-repository'
import SubmitCodeForm from './index'

describe('SubmitCodeForm', () => {
  jest.spyOn(APIRepository, 'getAll').mockResolvedValue({
    results: [],
    facets: {},
    totalResults: 0,
    rowsPerPage: 10,
    page: 1,
    programmingLanguages: [],
  })
  it('should exist', async () => {
    jest.spyOn(console, 'error').mockImplementationOnce(jest.fn())
    renderWithProviders(<SubmitCodeForm />)

    await waitFor(() => {
      expect(
        screen.getByRole('button', { name: 'Project toevoegen' }),
      ).toBeInTheDocument()
    })
  })

  describe('Form', () => {
    it('should have an alert about new project requirements', async () => {
      renderWithProviders(<SubmitCodeForm />)

      await waitFor(() => {
        expect(screen.getByRole('alert')).toBeInTheDocument()
      })
    })

    it('should have a button to create an issue on GitLab', async () => {
      renderWithProviders(<SubmitCodeForm />)

      await waitFor(() => {
        expect(
          screen.getByRole('link', { name: /Melding maken op GitLab/ }),
        ).toBeInTheDocument()
      })
    })

    it('should have a project URL input field', async () => {
      renderWithProviders(<SubmitCodeForm />)

      await waitFor(() => {
        expect(screen.getByRole('textbox', { name: /URL/ })).toBeInTheDocument()
      })
    })

    it("should have a used API's select field", async () => {
      renderWithProviders(<SubmitCodeForm />)

      await waitFor(() => {
        expect(
          screen.getByRole('combobox', {
            name: "Gebruikte API's",
          }),
        ).toBeInTheDocument()
      })
    })

    it('should have a submit button', async () => {
      renderWithProviders(<SubmitCodeForm />)

      await waitFor(() => {
        expect(
          screen.getByRole('button', { name: /Project toevoegen/ }),
        ).toBeInTheDocument()
      })
    })

    describe('Errors', () => {
      it('shows an error when the API is down', async () => {
        jest
          .spyOn(APIRepository, 'getAll')
          .mockRejectedValueOnce(
            new Error(
              `Er ging iets fout tijdens het ophalen van de beschikbare API's`,
            ),
          )

        render(
          <ThemeProvider theme={theme}>
            <SubmitCodeForm />
          </ThemeProvider>,
        )

        await waitFor(() => {
          expect(
            screen.getByText(
              /Er ging iets fout tijdens het ophalen van de beschikbare API's/,
            ),
          ).toBeInTheDocument()
        })
      })
    })

    describe('Validations', () => {
      it('show an error when the project url is missing', async () => {
        render(
          <ThemeProvider theme={theme}>
            <SubmitCodeForm />
          </ThemeProvider>,
        )

        fireEvent.change(screen.getByRole('textbox', { name: /URL/ }), {
          target: { value: '' },
        })
        fireEvent.click(
          screen.getByRole('button', { name: /Project toevoegen/ }),
        )

        await waitFor(() => {
          expect(
            screen.getByText(/URL is a required field/),
          ).toBeInTheDocument()
        })
      })

      it('show an error when the project url is invalid', async () => {
        renderWithProviders(<SubmitCodeForm />)
        act(() => {
          fireEvent.change(screen.getByRole('textbox', { name: /URL/ }), {
            target: { value: '24/05/2020' },
          })
        })
        fireEvent.click(
          screen.getByRole('button', { name: /Project toevoegen/ }),
        )

        await waitFor(() => {
          expect(
            screen.getByText(/URL must be a valid URL/),
          ).toBeInTheDocument()
        })
      })

      it('show an error when the project url is not GitHub/GitLab', async () => {
        renderWithProviders(<SubmitCodeForm />)
        act(() => {
          fireEvent.change(screen.getByRole('textbox', { name: /URL/ }), {
            target: { value: 'http://www.test.com' },
          })
        })
        fireEvent.click(
          screen.getByRole('button', { name: /Project toevoegen/ }),
        )

        await waitFor(() => {
          expect(
            screen.getByText(/URL moet een GitLab of GitHub repository zijn/),
          ).toBeInTheDocument()
        })
      })

      it('shows an error when used API is missing', async () => {
        renderWithProviders(<SubmitCodeForm />)
        fireEvent.click(
          screen.getByRole('button', { name: /Project toevoegen/ }),
        )

        await waitFor(() => {
          expect(
            screen.getByText(
              'Het verplichte veld, Gebruikte API(’s), is niet ingevuld.',
            ),
          ).toBeInTheDocument()
        })
      })
    })
  })

  describe('a11y', () => {
    it('has a fieldset with legend', () => {
      const handleSubmit = jest.fn()
      renderWithProviders(
        <Formik initialValues={{}} onSubmit={handleSubmit}>
          <SubmitCodeForm />
        </Formik>,
      )
      expect(
        screen
          .getByText(/Project details/i, { selector: 'legend' })
          .closest('fieldset'),
      ).toBeInTheDocument()
    })
  })
})
