// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect } from 'react'
import { Formik, Field } from 'formik'
import {
  Alert,
  Label,
  TextInput,
  Button,
  ErrorMessage,
  SelectFormik,
} from '@commonground/design-system'
import { FormikValues } from 'formik/dist/types'
import CodeRepository from '../../../../domain/code-repository'
import { modelFromAPIResponse } from '../../../../domain/models/api'
import APIRepository from '../../../../domain/api-repository'
import { Api } from '../../../../domain/models/types'
import objectKeysToSnakeCase from '../../../../utils/objectKeysToSnakeCase'
import { GITLAB_REPO_URL } from '../../../../constants'
import { StyledFieldset, HelperMessage, Spacing, Legend } from './styles'
import validationSchema from './validationSchema'

const SubmitCodeForm: React.FunctionComponent = () => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<string | null>(null)
  const [responseData, setResponseData] = useState<
    string | Record<string, string> | null
  >(null)
  const [apis, setApis] = useState<Api[]>([])

  async function loadApis() {
    try {
      const response = await APIRepository.getAll()
      const apis = response.results.map((api) => modelFromAPIResponse(api))
      setApis(apis)
    } catch {
      setError("Er ging iets fout tijdens het ophalen van de beschikbare API's")
    }
  }

  useEffect(() => {
    ;(async () => {
      await loadApis()
    })()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const submitCode = async (code) => {
    setError(null)
    setLoading(true)

    const submitValues = {
      url: code.url,
      relatedApis: code.relatedApis,
    }

    try {
      const responseData = await CodeRepository.create(
        objectKeysToSnakeCase(submitValues),
      )
      setResponseData(responseData)
    } catch {
      setError(
        'Er ging iets fout tijdens het toevoegen van de code. Gelieve opnieuw te proberen.',
      )
    }

    setLoading(false)
  }

  if (responseData) {
    if (typeof responseData !== 'string' && 'web_url' in responseData) {
      return (
        <p data-test="code-submitted-message">
          De code is toegevoegd.{' '}
          <a
            href={responseData.web_url}
            target="_blank"
            rel="noopener noreferrer"
          >
            {responseData.web_url}
          </a>
        </p>
      )
    }
    return <p data-test="code-submitted-message">De code is toegevoegd. </p>
  }

  const initialValues: FormikValues = {
    url: '',
    relatedApis: [],
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submitCode}
      data-test="form"
    >
      {({ errors, touched, handleSubmit }) => (
        <form onSubmit={handleSubmit} data-testid="form">
          <StyledFieldset disabled={loading}>
            <Alert variant="info">
              Momenteel kunnen alleen projecten toegevoegd worden die een API
              gebruiken uit het API overzicht van Developer Overheid.
              <br />
              <br />
              Ook graag een ander project toevoegen? Laat het ons weten via
              GitLab.
              <br />
              <br />
              <a
                href={`${GITLAB_REPO_URL}/issues`}
                target="_blank"
                rel="noopener noreferrer"
              >
                Melding maken op GitLab
              </a>
            </Alert>

            <Legend>Project details</Legend>
            <TextInput name="url" size="l">
              URL
              <HelperMessage>
                We ondersteunen URLs van GitLab repositories/snippets en GitHub
                repositories/gists.
              </HelperMessage>
            </TextInput>

            <Spacing>
              <Label htmlFor="relatedApis">Gebruikte API(’s)</Label>
              <HelperMessage>
                We ondersteunen URLs van GitLab repositories/snippets en GitHub
                repositories/gists.
              </HelperMessage>
              <Field
                aria-label="Gebruikte API's"
                component={SelectFormik}
                name="relatedApis"
                size="l"
                isMulti={true}
                placeholder=""
                options={apis.map((api) => ({
                  value: api.id,
                  label: `${api.organization.name} - ${api.serviceName}`,
                }))}
              />
            </Spacing>
            {errors.relatedApis && touched.relatedApis && (
              <ErrorMessage data-test="error-message">
                {errors.relatedApis}
              </ErrorMessage>
            )}
            {error && <ErrorMessage>{error}</ErrorMessage>}
          </StyledFieldset>
          <Button type="submit" disabled={loading}>
            Project toevoegen
          </Button>
        </form>
      )}
    </Formik>
  )
}

export default SubmitCodeForm
