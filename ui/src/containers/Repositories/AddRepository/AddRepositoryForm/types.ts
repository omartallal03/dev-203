// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export interface SubmitCode {
  url: string
  relatedApis: string[]
}
