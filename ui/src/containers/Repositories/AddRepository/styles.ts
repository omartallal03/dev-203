// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import PageContentCard from '../../Apis/AddApi/PageContentCard'

export const StyledSubmitCode = styled.div`
  margin: 0 auto 100px;
  max-width: ${(p) => p.theme.containerWidth};
  padding: 0 ${(p) => p.theme.containerPadding};
`

export const StyledPageContentCard = styled(PageContentCard)`
  margin-top: 40px;
  padding: 0 !important;
`
