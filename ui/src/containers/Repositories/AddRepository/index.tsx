// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Helmet } from 'react-helmet'
import SubmitCodeForm from './AddRepositoryForm'
import { StyledSubmitCode, StyledPageContentCard } from './styles'

const SubmitCode: React.FunctionComponent = () => (
  <StyledSubmitCode>
    <Helmet>
      <title>Developer Overheid: project toevoegen</title>
      <meta
        name="description"
        content="Voeg code toe door onderstaand formulier in te vullen."
      />
    </Helmet>
    <h1>Project toevoegen</h1>
    <p>Voeg code toe door onderstaand formulier in te vullen.</p>
    <StyledPageContentCard>
      <StyledPageContentCard.Body>
        <SubmitCodeForm />
      </StyledPageContentCard.Body>
    </StyledPageContentCard>
  </StyledSubmitCode>
)

export default SubmitCode
