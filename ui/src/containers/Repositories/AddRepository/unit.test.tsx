// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shallow } from 'enzyme'
import { Helmet } from 'react-helmet'
import { mountWithProviders } from '../../../test-helpers'
import SubmitCode from './index'

describe('<SubmitCode />', () => {
  it('contains the page title', () => {
    jest.spyOn(console, 'error').mockImplementationOnce(jest.fn())
    const wrapper = shallow(<SubmitCode />)
    expect(wrapper.find('h1').exists()).toBe(true)
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<SubmitCode />)
      expect(Helmet.peek().title).toEqual(
        'Developer Overheid: project toevoegen',
      )
    })
  })
})
