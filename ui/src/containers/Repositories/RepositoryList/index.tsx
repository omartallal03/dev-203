// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Repository } from '../../../domain/models/types'
import RepositoryCard from './RepositoryCard'
import { StyledList, StyledListItem } from './styles'

interface Props extends React.ComponentProps<'div'> {
  code: Repository[]
}

const RepositoryList: React.FunctionComponent<Props> = ({ code }) => (
  <StyledList id="repositories">
    {code.map((code, index) => (
      <StyledListItem
        key={`repository-${code.name}-${index}`}
        data-test="link"
        id={`repository-${code.name}`}
      >
        <RepositoryCard repository={code} />
      </StyledListItem>
    ))}
  </StyledList>
)

export default RepositoryList
