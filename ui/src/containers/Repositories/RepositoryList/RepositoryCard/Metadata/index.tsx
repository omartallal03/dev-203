// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import moment from 'moment'
import { ReactComponent as Stars } from '../../../../../assets/svg/star.svg'
import { ReactComponent as Forks } from '../../../../../assets/svg/fork.svg'
import { ReactComponent as Issues } from '../../../../../assets/svg/issues.svg'
import { ReactComponent as PullRequests } from '../../../../../assets/svg/pull-request.svg'
import { ReactComponent as LatestCommit } from '../../../../../assets/svg/latest-commit.svg'
import { Repository } from '../../../../../domain/models/types'
import { MetadataCircle, MetadataContainer, MetadataLink } from './styles'
import { getRepositoryMetadata } from './helpers'

interface Props {
  repository: Repository
}

const Metadata: React.FunctionComponent<Props> = ({ repository, ...props }) => {
  const {
    forkCount,
    forksURL,
    isGitHubRepository,
    issueOpenCount,
    issuesURL,
    lastChange,
    mergeRequestOpenCount,
    mergeRequestsURL,
    programmingLanguage,
    stars,
    starsURL,
    url,
  } = getRepositoryMetadata(repository)

  const renderProgrammingLanguage = () => {
    if (typeof programmingLanguage === 'undefined') {
      return null
    }

    return (
      <>
        <MetadataCircle
          color={programmingLanguage?.color}
          role="figure"
          aria-labelledby="programmingLanguage"
          title={`${programmingLanguage.percentage}%`}
        />
        <div
          id="programmingLanguage"
          title={`${programmingLanguage.percentage}%`}
        >
          {programmingLanguage?.name}
        </div>
      </>
    )
  }

  return (
    <MetadataContainer {...props}>
      <div>
        <MetadataLink
          disabled={!isGitHubRepository}
          href={`${url}/search?l=${programmingLanguage?.name}`}
          target="_blank"
          rel="noreferrer"
        >
          {renderProgrammingLanguage()}
        </MetadataLink>
        {typeof stars === 'number' && (
          <MetadataLink href={starsURL} target="_blank" rel="noreferrer">
            {stars} <Stars />
          </MetadataLink>
        )}
        {typeof forkCount === 'number' && (
          <MetadataLink href={forksURL} target="_blank" rel="noreferrer">
            {forkCount} <Forks />
          </MetadataLink>
        )}
        {typeof issueOpenCount === 'number' && (
          <MetadataLink href={issuesURL} target="_blank" rel="noreferrer">
            {issueOpenCount} <Issues />
          </MetadataLink>
        )}
        {typeof mergeRequestOpenCount === 'number' && (
          <MetadataLink
            href={mergeRequestsURL}
            target="_blank"
            rel="noreferrer"
          >
            {mergeRequestOpenCount} <PullRequests />
          </MetadataLink>
        )}
      </div>
      <div>
        <span>
          <LatestCommit />
          <span>{moment(lastChange).fromNow()} bijgewerkt</span>
        </span>
      </div>
    </MetadataContainer>
  )
}

export default Metadata
