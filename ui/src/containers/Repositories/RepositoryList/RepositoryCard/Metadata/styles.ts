// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../../../../theme/mediaQueries'

export const MetadataContainer = styled.div`
  align-items: center;
  color: ${(p) => p.theme.tokens.colorPaletteGray600};
  display: flex;
  flex-wrap: wrap;
  margin-top: 18px;

  a {
    color: ${(p) => p.theme.tokens.colorPaletteGray600};
    text-decoration: none;
  }

  div {
    display: flex;
    a:hover,
    span:not(:last-child):hover {
      color: ${(p) => p.theme.tokens.colorPaletteBlue800};
      cursor: pointer;
      svg {
        fill: ${(p) => p.theme.tokens.colorPaletteBlue800};
      }
    }
  }

  div:first-child {
    ${mq.mdDown`
      flex-basis: 100%;
    `}

    a:first-child {
      span:first-child {
        flex: 1;
      }
      span:nth-child(2) {
        flex: 1;
      }
    }
  }

  div:last-child {
    ${mq.mdDown`
      flex-basis: 100%;
      margin-top: ${(p) => p.theme.tokens.spacing02};
    `}
  }

  a,
  span {
    align-items: center;
    display: flex;
    margin-right: ${(p) => p.theme.tokens.spacing03};
  }

  svg {
    fill: ${(p) => p.theme.tokens.colorPaletteGray600};
    height: ${(p) => p.theme.tokens.spacing05};
  }
`

export const MetadataLink = styled.a<{ disabled?: boolean }>`
  pointer-events: ${(p) => (p.disabled ? 'none' : 'auto')};
`

export const MetadataCircle = styled.span<{ color?: string }>`
  background-color: ${(p) => p.color || 'gray'};
  border-radius: 50%;
  display: inline-block;
  margin-right: ${(p) => p.theme.tokens.spacing02};
  max-height: 1rem;
  max-width: 1rem;
  min-height: 1rem;
  min-width: 1rem;
`
