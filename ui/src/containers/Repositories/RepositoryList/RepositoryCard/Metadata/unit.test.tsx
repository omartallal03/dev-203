// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import * as moment from 'moment'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import { getRepositoryMetadata } from './helpers'
import Metadata from './index'

const props: React.ComponentProps<typeof Metadata> = {
  repository: {
    programmingLanguages: { TypeScript: 0.1, Python: 0.2 },
    source: 'gitlab',
    relatedApis: [],
    mergeRequestOpenCount: 1,
    issueOpenCount: 2,
    forkCount: 3,
    name: 'developer.overheid.nl',
    id: 1,
    ownerName: 'VNG Realisatie BV',
    url: 'https://gitlab.com/commonground/don/developer.overheid.nl',
    stars: 4,
    lastChange: new Date(1650862800000),
    avatarUrl:
      'https://gitlab.com/uploads/-/system/project/avatar/10525408/DON_favicon_Gitlab.png?width=64',
  },
}

describe('<Metadata />', () => {
  beforeAll(() => {
    moment.locale('nl')
  })

  it('shows main programming language and color', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByRole('figure')).toBeInTheDocument()
    expect(screen.getByRole('figure')).toHaveAttribute('color', '#2b7489')
    expect(screen.getByText(/TypeScript/)).toBeInTheDocument()
  })

  it('hides the color when empty', () => {
    renderWithProviders(
      <Metadata
        repository={{ ...props.repository, programmingLanguages: [] }}
      />,
    )

    expect(screen.queryByRole('figure')).not.toBeInTheDocument()
  })

  it('shows the number of stars', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByText('4')).toBeInTheDocument()
  })

  it('shows the number of forks', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByText('3')).toBeInTheDocument()
  })

  it('shows the number of open issues', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByText('2')).toBeInTheDocument()
  })

  it('shows the number of open merge requests', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByText('1')).toBeInTheDocument()
  })

  it('shows the last change data', () => {
    renderWithProviders(<Metadata {...props} />)

    expect(screen.getByText(/geleden bijgewerkt/)).toBeInTheDocument()
  })

  describe('Links', () => {
    it('has a link to the language overview when repo is GitHub', () => {
      renderWithProviders(
        <Metadata
          repository={{
            ...props.repository,
            url: 'https://github.com/commonground/developer.overheid.nl',
            source: 'github',
          }}
        />,
      )

      const link = screen.getByRole('link', { name: '0.1%' })
      expect(link.getAttribute('href')).toEqual(
        'https://github.com/commonground/developer.overheid.nl/search?l=TypeScript',
      )
      expect(link.getAttribute('disabled')).toBeFalsy()
    })

    it('has a disabled link to the language overview when repo is not GitHub', () => {
      renderWithProviders(<Metadata {...props} />)

      expect(screen.getByRole('link', { name: '0.1%' })).toHaveStyle(
        'pointer-events: none',
      )
    })

    it('has a link for the number of stars', () => {
      renderWithProviders(<Metadata {...props} />)

      expect(screen.getByText('4').getAttribute('href')).toEqual(
        'https://gitlab.com/commonground/don/developer.overheid.nl/-/starrers',
      )
    })

    it('has a link for the number of forks', () => {
      renderWithProviders(<Metadata {...props} />)

      expect(screen.getByText('3').getAttribute('href')).toEqual(
        'https://gitlab.com/commonground/don/developer.overheid.nl/-/forks',
      )
    })

    it('has a link for the number of issues', () => {
      renderWithProviders(<Metadata {...props} />)

      expect(screen.getByText('2').getAttribute('href')).toEqual(
        'https://gitlab.com/commonground/don/developer.overheid.nl/-/issues',
      )
    })

    it('has a link for the number of merge requests', () => {
      renderWithProviders(<Metadata {...props} />)

      expect(screen.getByText('1').getAttribute('href')).toEqual(
        'https://gitlab.com/commonground/don/developer.overheid.nl/-/merge_requests',
      )
    })
  })
})

describe('getRepositoryMetadata()', () => {
  it('returns the correct data for gitlab', () => {
    const repository = {
      ...props.repository,
    }

    expect(getRepositoryMetadata(repository)).toMatchInlineSnapshot(`
      Object {
        "forkCount": 3,
        "forksURL": "https://gitlab.com/commonground/don/developer.overheid.nl/-/forks",
        "isGitHubRepository": false,
        "issueOpenCount": 2,
        "issuesURL": "https://gitlab.com/commonground/don/developer.overheid.nl/-/issues",
        "lastChange": 2022-04-25T05:00:00.000Z,
        "mergeRequestOpenCount": 1,
        "mergeRequestsURL": "https://gitlab.com/commonground/don/developer.overheid.nl/-/merge_requests",
        "programmingLanguage": Object {
          "color": "#2b7489",
          "name": "TypeScript",
          "percentage": 0.1,
        },
        "stars": 4,
        "starsURL": "https://gitlab.com/commonground/don/developer.overheid.nl/-/starrers",
        "url": "https://gitlab.com/commonground/don/developer.overheid.nl",
      }
    `)
  })

  it('returns the correct data for github', () => {
    const repository = {
      ...props.repository,
      source: 'github',
      url: 'https://github.com/commonground/developer.overheid.nl',
    }

    expect(getRepositoryMetadata(repository)).toMatchInlineSnapshot(`
      Object {
        "forkCount": 3,
        "forksURL": "https://github.com/commonground/developer.overheid.nl/fork",
        "isGitHubRepository": true,
        "issueOpenCount": 2,
        "issuesURL": "https://github.com/commonground/developer.overheid.nl/issues",
        "lastChange": 2022-04-25T05:00:00.000Z,
        "mergeRequestOpenCount": 1,
        "mergeRequestsURL": "https://github.com/commonground/developer.overheid.nl/pulls",
        "programmingLanguage": Object {
          "color": "#2b7489",
          "name": "TypeScript",
          "percentage": 0.1,
        },
        "stars": 4,
        "starsURL": "https://github.com/commonground/developer.overheid.nl/stargazers",
        "url": "https://github.com/commonground/developer.overheid.nl",
      }
    `)
  })
})
