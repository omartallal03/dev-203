// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { Repository } from '../../../../../domain/models/types'

interface GetRepositoryMetadata
  extends Pick<
    Repository,
    | 'forkCount'
    | 'issueOpenCount'
    | 'lastChange'
    | 'mergeRequestOpenCount'
    | 'stars'
    | 'url'
  > {
  forksURL: string
  isGitHubRepository: boolean
  issuesURL: string
  mergeRequestsURL: string
  programmingLanguage?: {
    name: string
    color?: string | undefined
    percentage?: number
  }
  starsURL: string
}

export const getRepositoryMetadata = ({
  forkCount,
  issueOpenCount,
  lastChange,
  mergeRequestOpenCount,
  programmingLanguages,
  source,
  stars,
  url,
}: Repository): GetRepositoryMetadata => {
  const isGitHubRepository = source.toLowerCase().includes('github')
  const [programmingLanguage, programmingLanguagePercentage] =
    Object.entries(programmingLanguages).at(0) || []
  const starsURL = isGitHubRepository
    ? `${url}/stargazers`
    : `${url}/-/starrers`
  const forksURL = isGitHubRepository ? `${url}/fork` : `${url}/-/forks`
  const issuesURL = isGitHubRepository ? `${url}/issues` : `${url}/-/issues`
  const mergeRequestsURL = isGitHubRepository
    ? `${url}/pulls`
    : `${url}/-/merge_requests`

  return {
    forkCount,
    forksURL,
    isGitHubRepository,
    issueOpenCount,
    issuesURL,
    lastChange,
    mergeRequestOpenCount,
    mergeRequestsURL,
    programmingLanguage: programmingLanguage
      ? {
          name: programmingLanguage,
          color:
            programmingLanguage &&
            programmingLanguageColors[programmingLanguage.toString()],
          percentage: programmingLanguagePercentage,
        }
      : undefined,
    stars,
    starsURL,
    url,
  }
}

export const programmingLanguageColors = {
  ABAP: '#E8274B',
  ActionScript: '#882B0F',
  Ada: '#02f88c',
  Agda: '#315665',
  'AGS Script': '#B9D9FF',
  Alloy: '#64C800',
  AMPL: '#E6EFBB',
  AngelScript: '#C7D7DC',
  ANTLR: '#9DC3FF',
  Apex: '#1797c0',
  'API Blueprint': '#2ACCA8',
  APL: '#5A8164',
  'Apollo Guidance Computer': '#0B3D91',
  AppleScript: '#101F1F',
  Arc: '#aa2afe',
  AspectJ: '#a957b0',
  Assembly: '#6E4C13',
  ATS: '#1ac620',
  AutoHotkey: '#6594b9',
  AutoIt: '#1C3552',
  Ballerina: '#FF5000',
  Batchfile: '#C1F12E',
  Bison: '#6A463F',
  Blade: '#f7523f',
  BlitzMax: '#cd6400',
  Boo: '#d4bec1',
  Brainfuck: '#2F2530',
  C: '#555555',
  'C#': '#178600',
  'C++': '#f34b7d',
  Ceylon: '#dfa535',
  Chapel: '#8dc63f',
  Cirru: '#ccccff',
  Clarion: '#db901e',
  Clean: '#3F85AF',
  Click: '#E4E6F3',
  Clojure: '#db5855',
  CoffeeScript: '#244776',
  ColdFusion: '#ed2cd6',
  'ColdFusion CFC': '#ed2cd6',
  'Common Lisp': '#3fb68b',
  'Common Workflow Language': '#B5314C',
  'Component Pascal': '#B0CE4E',
  Crystal: '#000100',
  CSON: '#244776',
  CSS: '#563d7c',
  Cuda: '#3A4E3A',
  D: '#ba595e',
  Dart: '#00B4AB',
  DataWeave: '#003a52',
  DM: '#447265',
  Dockerfile: '#384d54',
  Dogescript: '#cca760',
  Dylan: '#6c616e',
  E: '#ccce35',
  eC: '#913960',
  ECL: '#8a1267',
  Eiffel: '#4d6977',
  EJS: '#a91e50',
  Elixir: '#6e4a7e',
  Elm: '#60B5CC',
  'Emacs Lisp': '#c065db',
  EmberScript: '#FFF4F3',
  EQ: '#a78649',
  Erlang: '#B83998',
  'F#': '#b845fc',
  Factor: '#636746',
  Fancy: '#7b9db4',
  Fantom: '#14253c',
  FLUX: '#88ccff',
  Forth: '#341708',
  Fortran: '#4d41b1',
  FreeMarker: '#0050b2',
  Frege: '#00cafe',
  'G-code': '#D08CF2',
  'Game Maker Language': '#71b417',
  GDScript: '#355570',
  Genie: '#fb855d',
  Gherkin: '#5B2063',
  Glyph: '#c1ac7f',
  Gnuplot: '#f0a9f0',
  Go: '#00ADD8',
  Golo: '#88562A',
  Gosu: '#82937f',
  'Grammatical Framework': '#ff0000',
  GraphQL: '#e10098',
  Groovy: '#e69f56',
  Hack: '#878787',
  Haml: '#ece2a9',
  Handlebars: '#f7931e',
  Harbour: '#0e60e3',
  Haskell: '#5e5086',
  Haxe: '#df7900',
  HTML: '#e34c26',
  Hy: '#7790B2',
  IDL: '#a3522f',
  Idris: '#b30000',
  'IGOR Pro': '#0000cc',
  Io: '#a9188d',
  Ioke: '#078193',
  Isabelle: '#FEFE00',
  J: '#9EEDFF',
  Java: '#b07219',
  JavaScript: '#f1e05a',
  JFlex: '#DBCA00',
  Jolie: '#843179',
  JSONiq: '#40d47e',
  Julia: '#a270ba',
  'Jupyter Notebook': '#DA5B0B',
  Kotlin: '#F18E33',
  KRL: '#28430A',
  Lasso: '#999999',
  Latte: '#f2a542',
  Less: '#1d365d',
  Lex: '#DBCA00',
  LFE: '#4C3023',
  LiveScript: '#499886',
  LLVM: '#185619',
  LOLCODE: '#cc9900',
  LookML: '#652B81',
  LSL: '#3d9970',
  Lua: '#000080',
  Makefile: '#427819',
  Markdown: '#083fa1',
  Marko: '#42bff2',
  Mask: '#f97732',
  Max: '#c4a79c',
  MAXScript: '#00a6a6',
  Mercury: '#ff2b2b',
  Meson: '#007800',
  Metal: '#8f14e9',
  Mirah: '#c7a938',
  MQL4: '#62A8D6',
  MQL5: '#4A76B8',
  MTML: '#b7e1f4',
  NCL: '#28431f',
  Nearley: '#990000',
  Nemerle: '#3d3c6e',
  nesC: '#94B0C7',
  NetLinx: '#0aa0ff',
  'NetLinx+ERB': '#747faa',
  NetLogo: '#ff6375',
  NewLisp: '#87AED7',
  Nextflow: '#3ac486',
  Nim: '#ffc200',
  Nit: '#009917',
  Nix: '#7e7eff',
  Nu: '#c9df40',
  NumPy: '#9C8AF9',
  'Objective-C': '#438eff',
  'Objective-C++': '#6866fb',
  'Objective-J': '#ff0c5a',
  OCaml: '#3be133',
  Omgrofl: '#cabbff',
  ooc: '#b0b77e',
  Opal: '#f7ede0',
  Oxygene: '#cdd0e3',
  Oz: '#fab738',
  P4: '#7055b5',
  Pan: '#cc0000',
  Papyrus: '#6600cc',
  Parrot: '#f3ca0a',
  Pascal: '#E3F171',
  Pep8: '#C76F5B',
  Perl: '#0298c3',
  PHP: '#4F5D95',
  PigLatin: '#fcd7de',
  Pike: '#005390',
  PLSQL: '#dad8d8',
  PogoScript: '#d80074',
  PostScript: '#da291c',
  PowerBuilder: '#8f0f8d',
  PowerShell: '#012456',
  Processing: '#0096D8',
  Prolog: '#74283c',
  'Propeller Spin': '#7fa2a7',
  Pug: '#a86454',
  Puppet: '#302B6D',
  PureBasic: '#5a6986',
  PureScript: '#1D222D',
  Python: '#3572A5',
  QML: '#44a51c',
  R: '#198CE7',
  Racket: '#3c5caa',
  Ragel: '#9d5200',
  RAML: '#77d9fb',
  Rascal: '#fffaa0',
  Reason: '#ff5847',
  Rebol: '#358a5b',
  Red: '#f50000',
  "Ren'Py": '#ff7f7f',
  Ring: '#2D54CB',
  Roff: '#ecdebe',
  Rouge: '#cc0088',
  Ruby: '#701516',
  RUNOFF: '#665a4e',
  Rust: '#dea584',
  SaltStack: '#646464',
  SAS: '#B34936',
  Sass: '#a53b70',
  Scala: '#c22d40',
  Scheme: '#1e4aec',
  SCSS: '#c6538c',
  sed: '#64b970',
  Self: '#0579aa',
  Shell: '#89e051',
  Shen: '#120F14',
  Slash: '#007eff',
  Slim: '#2b2b2b',
  Smalltalk: '#596706',
  Solidity: '#AA6746',
  SourcePawn: '#f69e1d',
  SQF: '#3F3F3F',
  Squirrel: '#800000',
  'SRecode Template': '#348a34',
  Stan: '#b2011d',
  'Standard ML': '#dc566d',
  Stylus: '#ff6347',
  SuperCollider: '#46390b',
  SVG: '#ff9900',
  Swift: '#ffac45',
  SystemVerilog: '#DAE1C2',
  Tcl: '#e4cc98',
  Terra: '#00004c',
  TeX: '#3D6117',
  'TI Program': '#A0AA87',
  Turing: '#cf142b',
  Twig: '#c1d026',
  TypeScript: '#2b7489',
  'Unified Parallel C': '#4e3617',
  Uno: '#9933cc',
  UnrealScript: '#a54c4d',
  Vala: '#fbe5cd',
  VCL: '#148AA8',
  Verilog: '#b2b7f8',
  VHDL: '#adb2cb',
  'Vim script': '#199f4b',
  Volt: '#1F1F1F',
  Vue: '#2c3e50',
  wdl: '#42f1f4',
  WebAssembly: '#04133b',
  wisp: '#7582D1',
  X10: '#4B6BEF',
  xBase: '#403a40',
  XC: '#99DA07',
  XQuery: '#5232e7',
  XSLT: '#EB8CEB',
  Yacc: '#4B6C4B',
  YAML: '#cb171e',
  YARA: '#220000',
  Zephir: '#118f9e',
  '1C Enterprise': '#814CCC',
}
