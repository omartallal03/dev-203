// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'

export const PillContainer = styled.div`
  border-radius: 12.5px;
  display: flex;
  flex-wrap: wrap;
  margin-top: ${(p) => p.theme.tokens.spacing04};
  row-gap: ${(p) => p.theme.tokens.spacing03};

  div {
    margin-right: ${(p) => p.theme.tokens.spacing03};
  }
`

export const StyledPillBadge = styled.div`
  align-items: center;
  border: ${(p) => `1px solid ${p.theme.tokens.colorBrandSecondary}`};
  border-radius: 12.5px;
  color: ${(p) => p.theme.tokens.colorPaletteBlue800};
  cursor: default;
  display: flex;
  justify-content: center;
  padding: 0 1rem;

  &:hover {
    background: ${(p) => p.theme.tokens.colorBrandSecondary};
    color: ${(p) => p.theme.tokens.colorPaletteBlue900};
  }
`
