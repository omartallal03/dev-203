// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { StyledComponentPropsWithRef } from 'styled-components'
import { IncomingProgrammingLanguages } from '../../../../../domain/models/types'
import { PillContainer, StyledPillBadge } from './styles'

interface Props extends StyledComponentPropsWithRef<typeof PillContainer> {
  programmingLanguages: IncomingProgrammingLanguages
}

const ProgrammingLanguages: React.FunctionComponent<Props> = ({
  programmingLanguages,
  ...props
}) => {
  return (
    <PillContainer {...props}>
      {Object.entries(programmingLanguages).map(
        ([programmingLanguage, percentage]) => {
          return (
            <StyledPillBadge key={programmingLanguage} title={`${percentage}%`}>
              {programmingLanguage}
            </StyledPillBadge>
          )
        },
      )}
    </PillContainer>
  )
}

export default ProgrammingLanguages
