// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import ProgrammingLanguages from './index'

const props: React.ComponentProps<typeof ProgrammingLanguages> = {
  programmingLanguages: { TypeScript: 0.1, Python: 0.2 },
}

describe('<ProgrammingLanguages />', () => {
  it('renders correctly', () => {
    renderWithProviders(<ProgrammingLanguages {...props} />)

    expect(screen.getByText(/TypeScript/)).toBeInTheDocument()
    expect(screen.getByText(/Python/)).toBeInTheDocument()
  })
})
