// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import Card from '../../../Apis/ApiDetail/APIDetails/Card'
import { Image } from '../../../Home/Image'
import mq from '../../../../theme/mediaQueries'

export const StyledCard = styled(Card)`
  display: flex;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  padding: ${(p) => p.theme.tokens.spacing06};

  img {
    max-height: ${(p) => p.theme.tokens.spacing09};
  }
  ${mq.lgUp`
    max-width: 704px;
  `}
  ${mq.mdDown`
    max-width: 553px;
  `}
  ${mq.smDown`
    max-width: 405px;
  `}
  ${mq.xsDown`
    max-width: 320px;
    padding: ${(p) => p.theme.tokens.spacing05};
  `}
`

export const StyledImage = styled(Image)`
  border-radius: ${(p) => p.theme.tokens.spacing01};

  ${mq.xsDown`
    display: unset;
  `}
`

export const ContainerLeft = styled.div`
  margin-right: ${(p) => p.theme.tokens.spacing03};
  img {
    width: 3rem;
  }
  ${mq.xsDown`
    display: none;
  `}
`

export const ContainerRight = styled.div`
  overflow: hidden;
  
  ${StyledImage} {
    ${mq.smUp`
        display: none;
    `}
  }
}`
