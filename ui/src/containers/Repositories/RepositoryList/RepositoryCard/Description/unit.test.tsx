// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import Description from './index'

const props: React.ComponentProps<typeof Description> = {
  repository: {
    url: '',
  },
}

describe('<Description />', () => {
  it('renders correctly', () => {
    renderWithProviders(<Description {...props} />)
    expect(
      screen.getByText(
        /Dit component verrijkt de huisnummer en postcodes bevragingen met gegevens uit de BAG./,
      ),
    ).toBeInTheDocument()
  })
})
