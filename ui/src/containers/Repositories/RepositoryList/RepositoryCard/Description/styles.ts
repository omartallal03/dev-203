// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'

export const DescriptionContainer = styled.p`
  color: ${(p) => p.theme.tokens.colorPaletteGray900};
  margin: 0;
  span:first-child {
    color: ${(p) => p.theme.tokens.colorPaletteGray600};
    font-size: 14px;
    font-weight: 600;
    margin-right: ${(p) => p.theme.tokens.spacing02};
    text-transform: uppercase;
  }

  a {
    color: ${(p) => p.theme.tokens.colorPaletteGray900};

    &:hover {
      color: ${(p) => p.theme.tokens.colorPaletteBlue800};
    }
  }
`
