// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Repository } from '../../../../../domain/models/types'
import { DescriptionContainer } from './styles'

interface Props {
  repository: Repository
}

const Description: React.FunctionComponent<Props> = ({
  repository,
  ...props
}) => {
  return (
    <DescriptionContainer {...props}>
      <span>STABLE |</span> Dit component verrijkt de huisnummer en postcodes
      bevragingen met gegevens uit de BAG.{' '}
      <a href="https://github.com">Bekijk README</a>
    </DescriptionContainer>
  )
}

export default Description
