// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { StyledComponentPropsWithRef } from 'styled-components'
import { Repository } from '../../../../domain/models/types'
import {
  ContainerLeft,
  ContainerRight,
  StyledCard,
  StyledImage,
} from './styles'
import ProgrammingLanguages from './ProgrammingLanguages'
import RelatedAPIs from './RelatedAPIs'
import Metadata from './Metadata'
import Title from './Title'
import Description from './Description'

interface Props extends StyledComponentPropsWithRef<typeof StyledCard> {
  repository: Repository
}

const RepositoryCard: React.FunctionComponent<Props> = ({
  repository,
  ...props
}) => {
  return (
    <StyledCard {...props}>
      <ContainerLeft>
        <StyledImage src={repository.avatarUrl} alt={repository.name} />
      </ContainerLeft>
      <ContainerRight>
        <Title repository={repository} />
        {!!repository?.description && <Description repository={repository} />}
        <ProgrammingLanguages
          programmingLanguages={repository.programmingLanguages}
        />
        <Metadata repository={repository} />
        <RelatedAPIs relatedApis={repository.relatedApis} />
      </ContainerRight>
    </StyledCard>
  )
}

export default RepositoryCard
