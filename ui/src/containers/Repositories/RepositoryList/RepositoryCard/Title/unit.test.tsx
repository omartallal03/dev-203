// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import Title from './index'

const props: React.ComponentProps<typeof Title> = {
  repository: {
    url: 'https://github.com/ownerName/name',
    name: 'name',
    ownerName: 'ownerName',
  },
}

describe('<Title />', () => {
  it('renders the title correctly', () => {
    renderWithProviders(<Title {...props} />)

    expect(screen.getByText(/ownerName/)).toBeInTheDocument()
    expect(screen.getByText(/name/)).toBeInTheDocument()
  })

  it('has the correct external links', () => {
    renderWithProviders(<Title {...props} />)

    expect(
      screen.getByRole('link', { name: /ownerName/ }).getAttribute('href'),
    ).toEqual('https://github.com/ownerName')
    expect(
      screen.getByRole('link', { name: /name/ }).getAttribute('href'),
    ).toEqual('https://github.com/ownerName/name')
  })
})
