// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../../../../theme/mediaQueries'

export const TitleContainer = styled.div`
  display: flex;
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  margin-top: ${(p) => p.theme.tokens.spacing02};

  ${mq.xsDown`
    flex-wrap: wrap;
  `}

  div {
    flex: 1;
    margin-left: auto;
  }

  a {
    align-items: center;
    color: ${(p) => p.theme.tokens.colorPaletteBlue800};
    margin-right: ${(p) => p.theme.tokens.spacing02};
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }

    ${mq.xsDown`
        display: flex;
    `}

    &:nth-child(1):after {
      color: ${(p) => p.theme.tokens.colorPaletteGray600};
      content: '/';
      display: inline-block;
      padding-left: ${(p) => p.theme.tokens.spacing02};
    }
  }
`
