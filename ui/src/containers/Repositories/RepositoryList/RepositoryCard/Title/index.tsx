// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import ExternalLink from '../../../../../components/ExternalLink'
import RepositoryPlaceholder from '../../../../../assets/svg/repository-placeholder.svg'
import { Repository } from '../../../../../domain/models/types'
import { StyledImage } from '../styles'
import { TitleContainer } from './styles'

interface Props {
  repository: Repository
}

const Title: React.FunctionComponent<Props> = ({ repository, ...props }) => {
  return (
    <TitleContainer {...props}>
      <div>
        <ExternalLink
          href={repository.url.replace(`/${repository.name}`, '')}
          title={repository.ownerName}
        />
        <ExternalLink href={repository.url} title={repository.name} />
      </div>
      <StyledImage src={RepositoryPlaceholder} alt={repository.name} />
    </TitleContainer>
  )
}

export default Title
