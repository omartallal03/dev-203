// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../../../../theme/mediaQueries'

export const RelatedApisContainer = styled.div`
  margin-top: ${(p) => p.theme.tokens.spacing05};
  max-width: 100%;
  overflow: hidden;

  svg:first-child {
    visibility: hidden;
  }
`

export const StyledCollapsibleTitle = styled.div`
  align-items: center;
  color: ${(p) => p.theme.tokens.colorPaletteGray600};
  display: flex;
  text-transform: uppercase;

  h3 {
    font-weight: normal;
    margin: 0;
    padding: 0;
  }

  div {
    border-top: ${(p) => `1px solid ${p.theme.tokens.colorBrandSecondary}`};
    flex: 1;
    height: 1px;
    margin: 0 -1rem 0 0.5rem;
    width: 100%;
  }
`

export const StyledCollapsibleBody = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;

  a {
    align-items: center;
    color: ${(p) => p.theme.tokens.colorPaletteGray900};
    display: flex;
    margin-top: ${(p) => p.theme.tokens.spacing02};
    text-decoration: none;

    &:hover {
      color: ${(p) => p.theme.tokens.colorPaletteBlue800};
      div:first-child {
        text-decoration: underline;
      }
    }

    ${mq.xsDown`
        flex-wrap: wrap;
    `}

    div:first-child {
      flex-grow: 0;
      overflow: hidden;
      white-space: nowrap;

      ${mq.xsDown`
        flex-grow: 1;
        white-space: unset;
        flex-basis: 100%;
      `}
    }
    div:nth-child(2) {
      color: ${(p) => p.theme.tokens.colorPaletteGray700};
      margin-left: ${(p) => p.theme.tokens.spacing02};
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;

      ${mq.xsDown`
        margin-left: unset;
      `}
    }

    svg {
      fill: ${(p) => p.theme.tokens.colorPaletteGray700};
      flex-shrink: 0;

      ${mq.xsDown`
        position: absolute;
        right: 1.75rem;
      `}
    }
  }
`
