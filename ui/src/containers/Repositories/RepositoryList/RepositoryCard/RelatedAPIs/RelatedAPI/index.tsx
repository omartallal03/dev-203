// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as ChevronRight } from '../../../../../../assets/svg/chevron-right.svg'
import { Repository } from '../../../../../../domain/models/types'

interface Props {
  relatedAPI: Repository['relatedApis'][number]
}

const RelatedAPI: React.FunctionComponent<Props> = ({
  relatedAPI,
  ...props
}) => {
  return (
    <Link to={`/apis/${relatedAPI.apiId}`} {...props}>
      <div title={relatedAPI.serviceName}>{relatedAPI.serviceName}</div>
      <div title={relatedAPI.organizationName}>
        {relatedAPI.organizationName}
      </div>
      <ChevronRight />
    </Link>
  )
}

export default RelatedAPI
