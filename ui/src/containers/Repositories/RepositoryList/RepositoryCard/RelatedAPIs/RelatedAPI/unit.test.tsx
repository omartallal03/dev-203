// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../../../test-helpers'
import RelatedAPI from './index'

const props: React.ComponentProps<typeof RelatedAPI> = {
  relatedAPI: {
    apiId: 'apiId',
    organizationName: 'organizationName',
    serviceName: 'serviceName',
  },
}

describe('<RelatedAPI />', () => {
  beforeEach(() => {
    renderWithProviders(<RelatedAPI {...props} />)
  })

  it('shows the service name ', () => {
    expect(screen.getByText(/serviceName/)).toBeInTheDocument()
  })

  it('shows the organization name ', () => {
    expect(screen.getByText(/organizationName/)).toBeInTheDocument()
  })

  it('has a link to the API', () => {
    expect(screen.getByRole('link').getAttribute('href')).toEqual('/apis/apiId')
  })
})
