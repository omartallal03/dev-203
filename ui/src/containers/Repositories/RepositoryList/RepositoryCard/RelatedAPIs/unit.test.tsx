// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import RelatedAPIs from './index'

const props: React.ComponentProps<typeof RelatedAPIs> = {
  relatedApis: [
    {
      apiId: 'apiId',
      organizationName: 'organizationName',
      serviceName: 'serviceName',
    },
  ],
}

describe('<RelatedAPIs />', () => {
  it('shows the number of APIs used', () => {
    renderWithProviders(<RelatedAPIs {...props} />)

    expect(screen.getByText(/Gebruikt 1 API's/i)).toBeInTheDocument()
  })

  it('shows the organization and service name', () => {
    renderWithProviders(<RelatedAPIs {...props} />)

    expect(screen.getByText(/organizationName/i)).toBeInTheDocument()
    expect(screen.getByText(/serviceName/i)).toBeInTheDocument()
  })
})
