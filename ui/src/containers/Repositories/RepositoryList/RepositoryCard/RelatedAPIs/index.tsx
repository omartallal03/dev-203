// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Collapsible } from '@commonground/design-system'
import { Repository } from '../../../../../domain/models/types'
import {
  RelatedApisContainer,
  StyledCollapsibleBody,
  StyledCollapsibleTitle,
} from './styles'
import RelatedAPI from './RelatedAPI'

interface Props {
  relatedApis: Repository['relatedApis']
}

const RelatedAPIs: React.FunctionComponent<Props> = ({
  relatedApis,
  ...props
}) => {
  if (!relatedApis.length) {
    return null
  }

  return (
    <RelatedApisContainer {...props}>
      <Collapsible
        initiallyOpen={true}
        title={
          <StyledCollapsibleTitle>
            <h3>Gebruikt {relatedApis.length} API&apos;s</h3>
            <div />
          </StyledCollapsibleTitle>
        }
      >
        <StyledCollapsibleBody>
          {relatedApis.map((relatedAPI) => (
            <RelatedAPI key={relatedAPI.apiId} relatedAPI={relatedAPI} />
          ))}
        </StyledCollapsibleBody>
      </Collapsible>
    </RelatedApisContainer>
  )
}

export default RelatedAPIs
