// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../../test-helpers'
import RepositoryCard from './index'

const props: React.ComponentProps<typeof RepositoryCard> = {
  repository: {
    url: 'https://github.com/ownerName/name',
    name: 'name',
    ownerName: 'ownerName',
    programmingLanguages: { TypeScript: 0.1, Python: 0.1 },
    source: 'GitHub repository',
    relatedApis: [
      {
        apiId: 'apiId',
        serviceName: 'serviceName',
        organizationName: 'organizationName',
      },
    ],
  },
}

describe('<RepositoryCard />', () => {
  it('renders correctly', () => {
    renderWithProviders(<RepositoryCard {...props} />)

    expect(screen.getAllByRole('img')).toHaveLength(2)
    expect(screen.getByText(/name/)).toBeInTheDocument()
    expect(screen.getByText(/ownerName/)).toBeInTheDocument()
    expect(screen.getAllByText(/TypeScript/)).toHaveLength(2)
    expect(screen.getByText(/Python/)).toBeInTheDocument()
    expect(screen.getByText(/serviceName/)).toBeInTheDocument()
    expect(screen.getByText(/organizationName/)).toBeInTheDocument()
  })
})
