// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

export interface RepositoryQueryParams {
  q: string
  programming_languages: string[]
  page: string
  rowsPerPage: string
}
