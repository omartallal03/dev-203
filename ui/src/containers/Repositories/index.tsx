// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import Spinner from '@commonground/design-system/dist/components/Spinner'
import { Helmet } from 'react-helmet'
import Pagination from '../../components/Pagination'
import { modelFromCodeResponse } from '../../domain/models/code'
import { generateQueryParams } from '../../utils/uriHelpers'
import { ResultsHeader } from '../../components/ResultsHeader'
import { DONSmall } from '../../components/CustomDON'
import { useDebounce } from '../../utils/hooks/useDebounce'
import { useRepositories } from '../../hooks/useRepositories'
import RepositoryList from './RepositoryList'
import {
  StyledOverviewPage,
  StyledOverviewHeader,
  StyledOverviewBody,
  StyledResultsContainer,
  StyledSubtitle,
  StyledForm,
  StyledSearch,
  SearchDiv,
  Label,
  StyledRepositoryFilters,
} from './styles'
import { getQueryParams } from './helpers'

const RepositoryOverview: React.FunctionComponent = () => {
  const [searchParams] = useSearchParams()
  const navigate = useNavigate()

  const queryParams = getQueryParams(searchParams)
  const {
    data: response,
    error,
    isLoading,
    isFetching,
  } = useRepositories(queryParams)
  const result = Object.assign({}, response, {
    repositories: response?.results?.map((repository) =>
      modelFromCodeResponse(repository),
    ),
  })

  const debounced = useDebounce(searchParams.get('q'), 300)

  const isSearching = debounced !== searchParams.get('q')

  const handleSearchHandler = (query: string) => {
    searchParams.delete('pagina')
    searchParams.set('q', query)

    navigate(`/repositories?${searchParams}`)
  }

  const formSubmitHandler = (event) => {
    event.preventDefault()

    const input = event.target.query
    handleSearchHandler(input.value)
  }

  const formChangeHandler = (event) => {
    event.preventDefault()

    const input = event.target.value
    handleSearchHandler(input)
  }

  const handleFilterChange = (newFilters) => {
    const currentFilters = getQueryParams(searchParams)

    if (newFilters.q !== currentFilters.q) {
      newFilters.programming_languages = []
    }

    const translatedFilters = {
      q: newFilters.q,
      technologie: newFilters.programming_languages || [],
    }

    navigate(`?${generateQueryParams(translatedFilters)}`)
  }

  const handlePageChange = (page) => {
    searchParams.set('pagina', page.toString())

    navigate(`?${searchParams}`)
  }

  const handleResultsPerPageChange = (resultsPerPage: string) => {
    searchParams.set('aantalPerPagina', resultsPerPage)
    searchParams.set('pagina', '1')

    navigate(`?${searchParams}`)
  }

  const { page } = queryParams
  const totalResults =
    !isLoading && !error && result ? result.totalResults : null

  const renderContent = () => {
    if (isLoading || isFetching) {
      return <Spinner data-testid="loading" />
    }

    if (error) {
      return (
        <DONSmall>
          Er ging iets fout tijdens het ophalen van de repositories.
        </DONSmall>
      )
    }

    if (!result.repositories?.length) {
      return <DONSmall>Er zijn (nog) geen repositories beschikbaar.</DONSmall>
    }

    return (
      <React.Fragment>
        <ResultsHeader
          totalResults={totalResults}
          objectName="repository"
          objectNamePlural="repositories"
        />
        <RepositoryList code={result.repositories} />
        <Pagination
          currentPage={page}
          totalRows={result.totalResults}
          rowsPerPage={result.rowsPerPage}
          onPageChangedHandler={handlePageChange}
          onResultsPerPageChange={handleResultsPerPageChange}
        />
      </React.Fragment>
    )
  }

  return (
    <StyledOverviewPage>
      <Helmet>
        <title>Developer Overheid: open source repositories</title>
        <meta
          name="description"
          content="Overzicht van open source projecten van (semi-)overheidsorganisaties, of die hun API's gebruiken."
        />
      </Helmet>

      <StyledOverviewHeader>
        <div>
          <h1>Open source repositories</h1>
          <StyledSubtitle>
            Overzicht van open source projecten van
            (semi-)overheidsorganisaties, of die hun API&apos;s gebruiken.
          </StyledSubtitle>
          <StyledForm onSubmit={(event) => formSubmitHandler(event)}>
            <SearchDiv onChange={(event) => formChangeHandler(event)}>
              <Label htmlFor="search-repositories">Vind repositories</Label>
              <StyledSearch
                inputProps={{
                  placeholder: 'Vul een zoekterm in',
                  name: 'query',
                  id: 'search-repositories',
                  value: queryParams.q,
                }}
                searching={isSearching}
              />
            </SearchDiv>
          </StyledForm>
        </div>
      </StyledOverviewHeader>

      <StyledOverviewBody>
        <StyledRepositoryFilters
          initialValues={queryParams}
          facets={result.facets}
          onSubmit={handleFilterChange}
        />
        <StyledResultsContainer>{renderContent()}</StyledResultsContainer>
      </StyledOverviewBody>
    </StyledOverviewPage>
  )
}

export default RepositoryOverview
