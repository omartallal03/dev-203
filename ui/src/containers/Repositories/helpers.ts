// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { RepositoryQueryParams } from './types'

export const getQueryParams = (
  searchParams: URLSearchParams,
): RepositoryQueryParams => {
  return {
    q: searchParams.get('q') || '',
    programming_languages: searchParams.getAll('technologie') || [],
    page: searchParams.get('pagina') || '1',
    rowsPerPage: searchParams.get('aantalPerPagina') || '10',
  }
}
