// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import Search from '../../components/design-system-candidates/Search'
import mq from '../../theme/mediaQueries'
import AddIcon from '../../assets/icons/AddIcon'
import RepositoryFilters from './RepositoryFilters'

export const StyledOverviewPage = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  max-width: ${(p) => p.theme.containerWidth};
  padding: 0 ${(p) => p.theme.containerPadding};
`

export const StyledOverviewHeader = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const StyledSubtitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`

export const StyledForm = styled.form`
  display: flex;

  ${mq.mdDown`
    display: block;
  `}
`

export const StyledSearch = styled(Search)`
  margin-bottom: ${(p) => p.theme.tokens.spacing09};
  max-width: 495px;

  ${mq.mdDown`
    margin-bottom: ${(p) => p.theme.tokens.spacing05};
  `}
`

export const StyledAddIcon = styled(AddIcon)`
  height: ${(p) => p.theme.tokens.spacing05};
  margin-right: ${(p) => p.theme.tokens.spacing04};
`

export const StyledOverviewBody = styled.div`
  display: flex;
  width: 100%;

  ${mq.smDown`
    flex-direction: column;
  `}
`

export const StyledResultsContainer = styled.div`
  flex: 1;
  margin-bottom: ${(p) => p.theme.tokens.spacing12};

  ${mq.smDown`
    margin-left: 0;
  `}
`

export const SearchDiv = styled.div`
  display: inline-block;
  margin-right: 19px;
  width: 486px;

  ${mq.smDown`
    display: block;
    margin-right: 0;
    width: auto;
  `}
`

export const Label = styled.label`
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const StyledRepositoryFilters = styled(RepositoryFilters)`
  flex: 0 0 250px;
  margin-right: ${(p) => p.theme.tokens.spacing10};
  margin-top: ${(p) => p.theme.tokens.spacing08};
`
