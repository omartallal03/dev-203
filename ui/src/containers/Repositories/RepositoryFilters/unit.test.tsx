// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { renderWithProviders, screen } from '../../../test-helpers'
import RepositoryFilters from './index'

describe('<RepositoryFilters />', () => {
  it('renders correctly', () => {
    renderWithProviders(<RepositoryFilters />)

    expect(screen.getByText('Filters')).toBeInTheDocument()
  })
})
