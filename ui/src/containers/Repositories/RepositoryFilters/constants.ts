// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import { Filter } from '../../../components/Filters/types'

export const FILTERS: Filter[] = [
  {
    key: 'programming_languages',
    label: 'Technologie',
    getLabel: (term: string) => term,
    skipTo: 'repositories',
  },
]
