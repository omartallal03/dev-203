// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import userEvent from '@testing-library/user-event'
import { fireEvent } from '@testing-library/dom'
import { Helmet } from 'react-helmet'
import { waitFor } from '@testing-library/react'
import { UseQueryResult } from 'react-query/types/react/types'
import {
  screen,
  act,
  renderWithProviders,
  mountWithProviders,
} from '../../test-helpers'
import CodeRepository from '../../domain/code-repository'
import {
  IncomingApiResponse,
  IncomingRepository,
} from '../../domain/models/types'
import { repositoriesMock } from '../../../.jest/mocks/repositories.mock'
import * as hooks from '../../hooks/useRepositories'
import RepositoryOverview from './index'

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as Record<string, string>),
  useNavigate: () => mockedUsedNavigate,
}))

const mockedUsedNavigate = jest.fn()

const mockBackendSuccess = () => {
  const result = {
    ...repositoriesMock,
  } as unknown as IncomingApiResponse<IncomingRepository[]>

  jest.spyOn(global, 'fetch').mockImplementation(
    () =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(result),
      }) as Promise<Response>,
  )
  jest.spyOn(CodeRepository, 'getAll').mockReturnValue(Promise.resolve(result))
  jest.spyOn(hooks, 'useRepositories').mockReturnValueOnce({
    error: false,
    isLoading: false,
    isFetching: false,
    data: result,
  } as UseQueryResult<IncomingApiResponse<IncomingRepository[]>>)
}

const mockBackendError = () => {
  jest.spyOn(CodeRepository, 'getAll').mockRejectedValue(new Error('rejected'))
  jest.spyOn(hooks, 'useRepositories').mockReturnValueOnce({
    error: new Error('rejected'),
    isLoading: false,
    isFetching: false,
    data: [],
  } as UseQueryResult<IncomingApiResponse<IncomingRepository[]>>)
}

const mockBackendEmptyState = () => {
  const result = {
    ...repositoriesMock,
    results: [],
  } as unknown as IncomingApiResponse<IncomingRepository[]>

  jest.spyOn(global, 'fetch').mockImplementation(
    () =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(result),
      }) as Promise<Response>,
  )
  jest.spyOn(CodeRepository, 'getAll').mockReturnValue(Promise.resolve(result))
  jest.spyOn(hooks, 'useRepositories').mockReturnValueOnce({
    error: false,
    isLoading: false,
    isFetching: false,
    data: [],
  } as UseQueryResult<IncomingApiResponse<IncomingRepository[]>>)
}

describe('RepositoryOverview', () => {
  it('shows the heading', async () => {
    mockBackendSuccess()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(screen.getByRole('heading', { level: 1 }).textContent).toEqual(
        'Open source repositories',
      )
    })
  })

  it('handles backend errors gracefully', async () => {
    mockBackendError()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(
        screen.getByText(
          /Er ging iets fout tijdens het ophalen van de repositories./,
        ),
      ).toBeInTheDocument()
    })
  })

  it('shows a message when there are no projects available', async () => {
    mockBackendEmptyState()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(
        screen.getByText(/Er zijn \(nog\) geen repositories beschikbaar./),
      ).toBeInTheDocument()
    })
  })

  it('has a search input field', async () => {
    mockBackendSuccess()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(screen.getByLabelText('Vind repositories')).toBeInTheDocument()
    })
  })

  it('shows the total number of projects', async () => {
    mockBackendSuccess()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(screen.getByText(/11 repositories/i)).toBeInTheDocument()
    })
  })

  it('shows a list of projects', async () => {
    mockBackendSuccess()
    renderWithProviders(<RepositoryOverview />)

    await waitFor(() => {
      expect(
        screen.getAllByRole('link', { name: /statistiekcbs/ }),
      ).toHaveLength(2)
    })
  })

  describe('Pagination', () => {
    const scrollTo = window.scrollTo
    beforeEach(() => {
      window.scrollTo = jest.fn()
    })
    afterEach(() => {
      window.scrollTo = scrollTo
    })

    it('shows pagination', async () => {
      mockBackendSuccess()
      renderWithProviders(<RepositoryOverview />)

      await waitFor(() => {
        expect(screen.getByLabelText('Pagina 1')).toBeInTheDocument()
        expect(screen.getByLabelText('Pagina 2')).toBeInTheDocument()
      })
    })

    it('handles pagination', async () => {
      mockBackendSuccess()
      renderWithProviders(<RepositoryOverview />)

      const button = await screen.findByLabelText('Pagina 2')
      await userEvent.click(button)
      await waitFor(() => {
        expect(mockedUsedNavigate).toHaveBeenLastCalledWith('?pagina=2')
      })
    })

    it('shows results per page', async () => {
      mockBackendSuccess()
      renderWithProviders(<RepositoryOverview />)

      const resultsPerPage = await screen.findByLabelText(
        'Aantal resultaten per pagina',
      )
      await userEvent.selectOptions(resultsPerPage, ['10'])
      await waitFor(() => {
        expect(mockedUsedNavigate).toHaveBeenLastCalledWith(
          '?aantalPerPagina=10&pagina=1',
        )
      })
    })
  })

  describe('Filter', () => {
    it('filters by text', async () => {
      mockBackendSuccess()
      renderWithProviders(<RepositoryOverview />)

      const input = screen.getByLabelText('Vind repositories')
      act(() => {
        fireEvent.change(input, { target: { value: 'owner_name/1' } })
      })

      await waitFor(() => {
        expect(mockedUsedNavigate).toHaveBeenLastCalledWith(
          expect.stringContaining('/repositories?q=owner_name'),
        )
      })
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', async () => {
      mockBackendSuccess()
      mountWithProviders(<RepositoryOverview />)
      await waitFor(() => {
        expect(Helmet.peek().title).toEqual(
          'Developer Overheid: open source repositories',
        )
      })
    })
  })
})
