// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import 'moment/locale/nl'
import React, { Suspense } from 'react'
import moment from 'moment'
import Moment from 'moment-timezone'
import { Routes, Route } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles as DSGlobalStyles } from '@commonground/design-system'
import { QueryClient, QueryClientProvider } from 'react-query'
import Header from '../../components/Header'
import Feedback from '../../components/Feedback'
import Footer from '../../components/Footer'
import GlobalStyles from '../../components/GlobalStyles'
import theme from '../../theme'
import CustomRouter from './CustomRouter'
import { configureCharts } from './helpers'
import { AppContainer, ContentWrap } from './styles'
import { routes } from './constants'
import '@fontsource/source-sans-pro/latin.css'

moment.locale('nl')

window.moment = Moment

const twentyFourHoursInMs = 1000 * 60 * 60 * 24
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
      refetchOnReconnect: false,
      retry: false,
      staleTime: twentyFourHoursInMs,
    },
  },
})

configureCharts()

const history = createBrowserHistory()

const App: React.FunctionComponent = () => {
  return (
    <CustomRouter history={history}>
      <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <AppContainer className="App">
            <DSGlobalStyles />
            <GlobalStyles />

            <ContentWrap>
              <Header />
              <Suspense fallback={<div />}>
                <main role="main" id="content">
                  <Routes>
                    {routes.map(({ path, element: Element }) => (
                      <Route key={path} path={path} element={<Element />} />
                    ))}
                  </Routes>
                  <Feedback />
                </main>
              </Suspense>
            </ContentWrap>
            <Footer />
          </AppContainer>
        </QueryClientProvider>
      </ThemeProvider>
    </CustomRouter>
  )
}

export default App
