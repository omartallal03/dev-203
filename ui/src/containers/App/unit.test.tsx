// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { shallow } from 'enzyme'
import * as moment from 'moment'
import App from './index'

describe('<App />', () => {
  it('renders correctly', () => {
    const component = shallow(<App />)
    expect(component.exists()).toBe(true)
  })

  it('sets the correct locale for times and dates', () => {
    shallow(<App />)

    expect(moment.locale()).toEqual('nl')
  })

  describe('a11y', () => {
    it('has a main block for skip links to target', () => {
      const component = shallow(<App />)
      expect(component.find('#content')).toHaveLength(1)
    })
  })
})
