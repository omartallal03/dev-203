// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { createMemoryHistory } from 'history'
import { act } from '@testing-library/react'
import { render } from '../../../test-helpers'
import CustomRouter from './index'

describe('<CustomRouter />', () => {
  it('calls goat counter on route change', () => {
    const setState = jest.fn()
    jest.spyOn(React, 'useState').mockReturnValue([{}, setState])
    ;(window as any).goatcounter = jest.fn()
    ;(window as any).goatcounter.count = jest.fn()

    const history = createMemoryHistory()

    render(<CustomRouter history={history} />)

    act(() => {
      history.push('/apis')
    })

    expect((window as any).goatcounter.count).toHaveBeenCalledWith({
      path: expect.any(Function),
    })
  })
})
