// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { BrowserRouterProps, Router } from 'react-router-dom'
import { BrowserHistory } from 'history'
import React, { useCallback, useLayoutEffect, useState } from 'react'

interface Props extends BrowserRouterProps {
  history: BrowserHistory
}

const CustomRouter: React.FunctionComponent<Props> = ({
  history,
  ...props
}) => {
  const [state, setState] = useState({
    action: history.action,
    location: history.location,
  })

  const updateStatistics = useCallback(() => {
    window.goatcounter?.count({
      path: (path) => path,
    })
  }, [history.location.pathname]) // eslint-disable-line react-hooks/exhaustive-deps

  useLayoutEffect(() => {
    history.listen((update) => {
      setState(update)

      updateStatistics()
    })
  }, [history]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Router
      {...props}
      location={state.location}
      navigationType={state.action}
      navigator={history}
    />
  )
}

export default CustomRouter
