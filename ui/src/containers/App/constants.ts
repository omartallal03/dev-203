// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import IconHome from '../../assets/icons/IconHome'
import IconPresent from '../../assets/icons/IconPresent'
import IconCode from '../../assets/icons/IconCode'
import IconInfo from '../../assets/icons/IconInfo'
import IconRules from '../../assets/icons/list-check-2.svg'

// Pages
import Home from '../Home'
import About from '../About'
import APIRules from '../ApiRules'
import APIOverview from '../Apis'
import SubmitAPI from '../Apis/AddApi'
import APIDetail from '../Apis/ApiDetail'
import APISpecification from '../Apis/ApiDetail/APISpecification'
import APIStatistics from '../Apis/ApiStatistics'
import RepositoryOverview from '../Repositories'
import SubmitCode from '../Repositories/AddRepository'
import PrivacyStatement from '../PrivacyStatement'

// Types
import { NavItem } from '../../components/Header/types'

export const routes = [
  {
    path: '/',
    element: Home,
  },
  {
    path: 'about',
    element: About,
  },
  {
    path: 'privacy',
    element: PrivacyStatement,
  },
  {
    path: 'api-rules',
    element: APIRules,
  },
  {
    path: 'apis',
    element: APIOverview,
  },
  {
    path: 'apis/add/*',
    element: SubmitAPI,
  },
  {
    path: 'apis/:id/*',
    element: APIDetail,
  },
  {
    path: 'statistics/*',
    element: APIStatistics,
  },
  {
    path: 'repositories',
    element: RepositoryOverview,
  },
  {
    path: 'repositories/add',
    element: SubmitCode,
  },
  {
    path: 'detail/:id/*',
    element: APIDetail,
  },
  {
    path: 'detail/:id/:environment/specification',
    element: APISpecification,
  },
]

export const navItems: NavItem[] = [
  {
    name: 'Home',
    Icon: IconHome,
    to: '/',
  },
  {
    name: "API's",
    Icon: IconPresent,
    to: '/apis',
  },
  {
    name: 'Repositories',
    Icon: IconCode,
    to: '/repositories',
  },
  {
    name: 'API Rules',
    Icon: IconRules,
    to: '/api-rules',
  },
  {
    name: 'Over',
    Icon: IconInfo,
    to: '/about',
  },
]
