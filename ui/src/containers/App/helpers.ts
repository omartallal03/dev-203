// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import Accessibility from 'highcharts/modules/accessibility'
import Highcharts from 'highcharts'
import Bullet from 'highcharts/modules/bullet'
import NoDataToDisplay from 'highcharts/modules/no-data-to-display'
import theme from '../../theme'

export const configureCharts = () => {
  Accessibility(Highcharts) // https://www.highcharts.com/docs/accessibility/accessibility-module
  Bullet(Highcharts)
  NoDataToDisplay(Highcharts)

  Highcharts.setOptions({
    plotOptions: {
      series: {
        animation: false,
      },
    },
    noData: {
      style: {
        width: 300,
        fontWeight: 'bold',
        fontSize: '1rem',
        color: theme.colorTextLight,
      },
    },
    lang: {
      noData:
        'Er is (nog) geen data beschikbaar om deze grafiek te kunnen laten tonen.',
      months: [
        'januari',
        'februari',
        'maart',
        'april',
        'mei',
        'juni',
        'juli',
        'augustus',
        'september',
        'oktober',
        'november',
        'december',
      ],
      shortMonths: [
        'jan',
        'feb',
        'mrt',
        'apr',
        'mei',
        'jun',
        'jul',
        'aug',
        'sep',
        'okt',
        'nov',
        'dec',
      ],
      weekdays: [
        'zondag',
        'maandag',
        'dinsdag',
        'woensdag',
        'donderdag',
        'vrijdag',
        'zaterdag',
      ],
      shortWeekdays: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    },
  })
}
