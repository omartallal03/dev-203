// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import {
  NavLink,
  Route,
  Routes,
  Navigate,
  useLocation,
  useNavigate,
} from 'react-router-dom'

import { Helmet } from 'react-helmet'
import { useEventListener } from 'usehooks-ts'
import PageContentCard from './PageContentCard'
import SubmitAPIMergeRequest from './SubmitAPIMergeRequest'
import SubmitAPIForm from './SubmitAPIForm'

import { StyledSubmitAPI, StyledTabs } from './styles'

const SubmitAPI: React.FunctionComponent = () => {
  const location = useLocation()
  const navigate = useNavigate()

  const isForm = location.pathname.includes('form')
  const isMergeRequest = location.pathname.includes('merge-request')

  useEventListener('keydown', (event) => {
    // https://www.w3.org/TR/wai-aria-practices/examples/tabs/tabs-2/tabs.html
    // https://codepen.io/pen
    if (isForm && event.key === 'ArrowRight') {
      navigate('merge-request')
    }

    if (isMergeRequest && event.key === 'ArrowLeft') {
      navigate('form')
    }
  })

  return (
    <StyledSubmitAPI>
      <Helmet>
        <title>Developer Overheid: API toevoegen</title>
        <meta
          name="description"
          content="Voeg je API toe door onderstaand formulier in te vullen of een Merge Request aan te maken."
        />
      </Helmet>

      <h1>API toevoegen</h1>
      <p>
        Voeg je API toe door onderstaand formulier in te vullen of een Merge
        Request aan te maken.
      </p>

      <StyledTabs role="tablist" aria-label="API toevoegen">
        <NavLink to="form" role="tab" aria-selected={isForm} tabIndex={0}>
          Toevoegen via formulier
        </NavLink>
        <NavLink
          to="merge-request"
          role="tab"
          aria-selected={isMergeRequest}
          tabIndex={-1}
        >
          Via Merge Request
        </NavLink>
      </StyledTabs>

      <PageContentCard>
        <PageContentCard.Body>
          <Routes>
            <Route path="/" element={<Navigate to="form" />} />
            <Route path="form" element={<SubmitAPIForm />} />
            <Route path="merge-request" element={<SubmitAPIMergeRequest />} />
          </Routes>
        </PageContentCard.Body>
      </PageContentCard>
    </StyledSubmitAPI>
  )
}

export default SubmitAPI
