// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
export const BRANCH_NAME = `data/{organization}-{api}`
export const FILE_NAME = `content/api/{organization}-{api}.yaml`
export const EXAMPLE_FILE = `service_name: Voorbeeld van een API naam
description: Voorbeeld van een omschrijving
organization:
  name: Voorbeeld van een organisatie naam
  ooid: 12345678
api_type: rest_json
api_authentication: api_key
environments:
  - name: production
    api_url: https://api.example.com/service/
    specification_url: https://api.example.com/service/swagger/?format=openapi
    documentation_url: https://api.example.com/service/
  - name: acceptance
    api_url: https://acpt.api.example.com/service/
    specification_url: https://acpt.api.example.com/service/swagger/?format=openapi
    documentation_url: https://acpt.api.example.com/service/
  - name: demo
    api_url: https://demo.api.example.com/service/
    specification_url: https://demo.api.example.com/service/swagger/?format=openapi
    documentation_url: https://demo.api.example.com/service/
contact:
  email: helpdesk@voorbeeld.nl
  phone: "0031612345678"
  url: https://github.com/VNG-Realisatie/nlx
is_reference_implementation: false
relations:
  example-api-id:
    - reference-implementation
terms_of_use:
  government_only: true
  pay_per_use: false
  uptime_guarantee: 99.9
`
export const GITLAB_CONTENT_REPO_URL =
  'https://gitlab.com/commonground/don/don-content'
