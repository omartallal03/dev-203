// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

import CodeBlock from './CodeBlock'
import {
  EXAMPLE_FILE,
  FILE_NAME,
  BRANCH_NAME,
  GITLAB_CONTENT_REPO_URL,
} from './constants'

const SubmitAPIMergeRequest: React.FunctionComponent = () => (
  <div>
    <p>
      Maak op <a href={GITLAB_CONTENT_REPO_URL}>GitLab</a> een fork van de
      repository onder een eigen account en maak een nieuwe branch aan met de
      naam:
    </p>
    <CodeBlock>{BRANCH_NAME}</CodeBlock>

    <p>Voeg een bestand toe aan de branch met de volgende naam:</p>
    <CodeBlock>{FILE_NAME}</CodeBlock>

    <p>En voeg de inhoud toe aan de hand van de onderstaande structuur:</p>
    <CodeBlock>{EXAMPLE_FILE}</CodeBlock>

    <p>Ten minste één omgeving is verplicht.</p>
    <p>
      De organisatienaam kan{' '}
      <a href="src/pages/SubmitAPI/SubmitAPIMergeRequest/SubmitAPIMergeRequest">
        hier
      </a>{' '}
      worden opgezocht.
    </p>
    <p>
      Maak vervolgens een Merge Request aan van{' '}
      <a href={`${GITLAB_CONTENT_REPO_URL}/-/forks`}>
        jouw geforkte repository
      </a>{' '}
      naar de officiële repository om jouw toevoeging in te dienen.
    </p>
  </div>
)

export default SubmitAPIMergeRequest
