// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

export const StyledPre = styled.pre`
  background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  border: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
  border-radius: ${(p) => p.theme.tokens.spacing02};
  display: flex;
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
  padding: 9px 48px 9px 14px;
  position: relative;
  white-space: pre-wrap;
`

export const StyledCopyButton = styled.button`
  align-items: center;
  background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  border: none;
  cursor: pointer;
  display: flex;
  padding: ${(p) => p.theme.tokens.spacing03} ${(p) => p.theme.tokens.spacing04};
  position: absolute;
  right: 0;
  top: 0;
`

export const StyledFeedback = styled.div`
  background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  font-family: 'Source Sans Pro', sans-serif;
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  margin-right: ${(p) => p.theme.tokens.spacing02};
  padding: 0 ${(p) => p.theme.tokens.spacing02};
`
