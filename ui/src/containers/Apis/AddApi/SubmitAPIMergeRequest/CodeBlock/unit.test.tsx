// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { shallow } from 'enzyme'
import * as helpers from './helpers'
import CodeBlock from './index'

describe('showFeedback function', () => {
  jest.useFakeTimers('modern')

  it('fires feedback function', () => {
    const feedback = 'hi'
    const feedbackFn = jest.fn()

    helpers.showFeedback(feedback, feedbackFn)
    expect(feedbackFn).toHaveBeenCalledWith('hi')
  })

  it('fires feedback function with `null` after 2500ms timeout', async () => {
    const feedback = 'hi'
    const feedbackFn = jest.fn()

    helpers.showFeedback(feedback, feedbackFn)

    jest.advanceTimersByTime(2500)

    expect(feedbackFn).toHaveBeenLastCalledWith(null)
  })
})

describe('copy function', () => {
  it('shows an error when we can not copy (JSDOM LIMITATION)', () => {
    const errorMock = jest.fn()
    console.error = errorMock

    const copyString = 'copied!'
    const feedbackFn = jest.fn()

    // Default export needed because of spy in other test
    helpers.copy(copyString, feedbackFn)

    expect(errorMock).toHaveBeenCalled()
    expect(feedbackFn).toHaveBeenCalledWith('Niet gelukt')
  })
})

describe('CodeBlock', () => {
  it('renders', () => {
    const text = 'copy me'
    const wrapper = shallow(<CodeBlock>{text}</CodeBlock>)

    expect(wrapper.text()).toContain(text)
    expect(wrapper.find('[data-test="copy-button"]')).toHaveLength(1)
  })

  it('fires copy function on click button', () => {
    const text = 'copy me'
    const wrapper = shallow(<CodeBlock>{text}</CodeBlock>)
    const copySpy = jest.spyOn(helpers, 'copy')

    console.error = jest.fn()
    wrapper.find('[data-test="copy-button"]').simulate('click')
    expect(copySpy).toHaveBeenCalledWith(text, expect.any(Function))
  })
})
