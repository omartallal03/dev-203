// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { ReactNode } from 'react'

export function showFeedback(
  feedback: ReactNode,
  setFeedback: React.SetStateAction<unknown>,
): void {
  if (typeof setFeedback === 'function') {
    setFeedback(feedback)
  }
  setTimeout(() => {
    if (typeof setFeedback === 'function') {
      setFeedback(null)
    }
  }, 2500)
}

export async function copy(
  content: ReactNode,
  setFeedback: React.SetStateAction<unknown>,
): Promise<void> {
  if (navigator.clipboard) {
    try {
      if (typeof content === 'string') {
        await navigator.clipboard.writeText(content)
      }
      showFeedback('Gekopieerd', setFeedback)
    } catch (error) {
      showFeedback('Niet gelukt', setFeedback)
      console.error('Unable to copy using clipboard', error)
    }
  } else {
    const textField = document.createElement('textarea')
    if (typeof content === 'string') {
      textField.innerText = content
    }
    document.body.appendChild(textField)
    textField.select()
    try {
      const success = document.execCommand('copy')
      if (success) {
        showFeedback('Gekopieerd', setFeedback)
      } else {
        showFeedback('Niet gelukt', setFeedback)
      }
    } catch (error) {
      showFeedback('Niet gelukt', setFeedback)
      console.error('Unable to copy using execCommand', error)
    }
    textField.remove()
  }
}
