// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { ReactComponent as ClipboardIcon } from '../../../../../assets/svg/clipboard-icon.svg'
import { copy } from './helpers'
import { StyledPre, StyledCopyButton, StyledFeedback } from './styles'

const CodeBlock: React.FunctionComponent = ({ children, ...props }) => {
  const [feedback, setFeedback] = useState(null)

  return (
    <StyledPre {...props}>
      {children}
      <StyledCopyButton
        onClick={() => copy(children, setFeedback)}
        data-test="copy-button"
        aria-label="Kopiëren"
      >
        {feedback && <StyledFeedback>{feedback}</StyledFeedback>}
        <ClipboardIcon />
      </StyledCopyButton>
    </StyledPre>
  )
}

export default CodeBlock
