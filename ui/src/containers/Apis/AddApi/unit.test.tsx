// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { fireEvent } from '@testing-library/dom'
import {
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../../test-helpers'
import SubmitAPI from './index'

const mockUseNavigate = jest.fn()
jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as Record<string, string>),
  useLocation: () => ({
    pathname: '/form',
  }),
  useNavigate: () => mockUseNavigate,
}))

describe('<SubmitApi />', () => {
  it('contains the page title', async () => {
    renderWithProviders(<SubmitAPI />)

    expect(screen.getByRole('heading', { level: 1 }).textContent).toEqual(
      'API toevoegen',
    )
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<SubmitAPI />)

      expect(Helmet.peek().title).toEqual('Developer Overheid: API toevoegen')
    })

    it('responds to arrow left/right to switch tabs', () => {
      renderWithProviders(<SubmitAPI />)

      fireEvent.keyDown(screen.getByRole('tablist'), { key: 'ArrowRight' })

      expect(mockUseNavigate).toHaveBeenCalledWith('merge-request')
    })
  })
})
