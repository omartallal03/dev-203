// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import { Formik } from 'formik'
import { FormikHelpers, FormikValues } from 'formik/dist/types'
import APIRepository from '../../../../domain/api-repository'
import { OnFormikValueChange } from '../../../../components/Form/helpers'
import { modelFromAPIResponse } from '../../../../domain/models/api'
import {
  Api,
  IncomingApi,
  IncomingApiResponse,
} from '../../../../domain/models/types'
import SubmitAPIForm from './SubmitAPIForm'
import { schema } from './validationSchema'
import { convertFormDataToRequestBody } from './helpers'
import { INITIAL_VALUES } from './constants'

type Props = Record<string, never>

interface State {
  submitted: boolean
  responseData: any // eslint-disable-line @typescript-eslint/no-explicit-any
  apisLoaded: boolean
  apisError: boolean
  storedFormValues: any | null // eslint-disable-line @typescript-eslint/no-explicit-any
  result: {
    apis: Api[]
  }
}

class SubmitAPIFormPage extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    let storedFormValues = null
    try {
      storedFormValues = JSON.parse(
        sessionStorage.getItem('storedFormValues') || '',
      )
    } catch (e) {}

    this.state = {
      submitted: false,
      responseData: {},
      result: {
        apis: [],
      },
      apisLoaded: false,
      apisError: false,
      storedFormValues, // Only use this to save values when unmounting
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitToApi = this.submitToApi.bind(this)
  }

  async componentDidMount(): Promise<void> {
    window && window.addEventListener('beforeunload', this.handleReset)
  }

  componentWillUnmount(): void {
    sessionStorage.setItem(
      'storedFormValues',
      JSON.stringify(this.state.storedFormValues),
    )

    window && window.removeEventListener('beforeunload', this.handleReset)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async submitToApi(data: FormikValues): Promise<any> {
    return APIRepository.create(data)
  }

  async handleSubmit(
    values: FormikValues,
    helpers: FormikHelpers<FormData>,
  ): Promise<void> {
    // The form has already passed validation,
    // this call serves only to apply Yup type coercion and transforms.
    const parsedFormData = schema.validateSync(values)
    const submitData = convertFormDataToRequestBody(parsedFormData)

    try {
      const responseData = await this.submitToApi(submitData)
      helpers.setSubmitting(false)
      this.setState({
        submitted: true,
        responseData,
        storedFormValues: null,
      })
      this.handleReset()
    } catch (error) {
      helpers.setSubmitting(false)
      helpers.setStatus({
        msg: 'Er ging iets fout tijdens het toevoegen van de API. Gelieve opnieuw te proberen.',
      })
      console.error(error)
    }
  }

  formikValueChange = (values: FormikValues): void => {
    const { isBasedOnReferenceImplementation } = values

    if (isBasedOnReferenceImplementation === 'true') {
      this.callAPIAndModelResponse()
    }

    this.setState({ storedFormValues: values })
  }

  handleReset = (): void => {
    sessionStorage.removeItem('storedFormValues')
    this.setState({ storedFormValues: null })
  }

  async fetchApiList(): Promise<IncomingApiResponse<IncomingApi[]>> {
    return APIRepository.getAll(
      `rowsPerPage=${Number.MAX_SAFE_INTEGER}&isReferenceImplementation=true`,
    )
  }

  async callAPIAndModelResponse(): Promise<void> {
    try {
      const response = await this.fetchApiList()
      const result = {
        apis: response.results.map((api) => modelFromAPIResponse(api)),
      }
      this.setState({ result, apisLoaded: true })
    } catch (error) {
      this.setState({ apisError: true, apisLoaded: true })
      console.error(error)
    }
  }

  render(): JSX.Element {
    const {
      storedFormValues,
      result: { apis },
      apisLoaded,
      apisError,
    } = this.state

    return (
      <div>
        {this.state.submitted ? (
          <p data-test="api-submitted-message">
            De API is toegevoegd. Wij zullen deze zo snel mogelijk nakijken.
          </p>
        ) : (
          <Formik
            initialValues={storedFormValues || INITIAL_VALUES}
            onSubmit={this.handleSubmit}
            onReset={this.handleReset}
            validationSchema={schema}
          >
            {(props) => (
              <>
                <OnFormikValueChange handle={this.formikValueChange} />
                <SubmitAPIForm
                  {...props}
                  apis={{ data: apis, loaded: apisLoaded, error: apisError }}
                />
              </>
            )}
          </Formik>
        )}
      </div>
    )
  }
}

export default SubmitAPIFormPage
