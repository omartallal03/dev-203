// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { FormData } from './types'

export const INITIAL_VALUES: FormData = {
  description: '',
  organization: {
    name: '',
    ooid: '',
  },
  serviceName: '',
  apiType: 'unknown',
  apiAuthentication: '',

  productionApiUrl: '',
  productionSpecificationUrl: '',
  productionDocumentationUrl: '',

  hasAcceptanceEnvironment: false,
  acceptanceApiUrl: '',
  acceptanceDocumentationUrl: '',
  acceptanceSpecificationUrl: '',

  hasDemoEnvironment: false,
  demoApiUrl: '',
  demoDocumentationUrl: '',
  demoSpecificationUrl: '',

  contact: {
    email: '',
    phone: '',
    url: '',
  },
  termsOfUse: {
    governmentOnly: false,
    payPerUse: false,
    uptimeGuarantee: 99.5,
    supportResponseTime: undefined,
  },
  isBasedOnReferenceImplementation: 'false',
  referenceImplementation: '',
}
