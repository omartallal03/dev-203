// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { FormikValues } from 'formik/dist/types'
import { EnvironmentType } from '../../../../domain/models/enums'
import { RELATION_TYPE_REFERENCE_IMPLEMENTATION } from '../../constants'
import { RequestBody } from './types'

export const createRelation = (
  isBasedOnReferenceImplementation: string,
  referenceImplementation: string,
): Record<string, string[]> | undefined => {
  return isBasedOnReferenceImplementation
    ? {
        [referenceImplementation]: [RELATION_TYPE_REFERENCE_IMPLEMENTATION],
      }
    : undefined
}

export const convertFormDataToRequestBody = (
  formData: FormikValues,
): RequestBody => {
  const requestBody: RequestBody = {} as RequestBody

  requestBody.description = formData.description
  requestBody.organization = formData.organization
  requestBody.service_name = formData.serviceName
  requestBody.api_type = formData.apiType
  requestBody.api_authentication = formData.apiAuthentication

  requestBody.relations = createRelation(
    formData.isBasedOnReferenceImplementation,
    formData.referenceImplementation,
  )

  requestBody.environments = [
    {
      name: EnvironmentType.PRODUCTION.value,
      api_url: formData.productionApiUrl,
      specification_url: formData.productionSpecificationUrl,
      documentation_url: formData.productionDocumentationUrl,
    },
  ]

  if (formData.hasAcceptanceEnvironment) {
    requestBody.environments.push({
      name: EnvironmentType.ACCEPTANCE.value,
      api_url: formData.acceptanceApiUrl,
      specification_url: formData.acceptanceSpecificationUrl,
      documentation_url: formData.acceptanceDocumentationUrl,
    })
  }

  if (formData.hasDemoEnvironment) {
    requestBody.environments.push({
      name: EnvironmentType.DEMO.value,
      api_url: formData.demoApiUrl,
      specification_url: formData.demoSpecificationUrl,
      documentation_url: formData.demoDocumentationUrl,
    })
  }

  formData.contact = formData.contact || {}
  requestBody.contact = {
    email: formData.contact.email,
    phone: formData.contact.phone,
    url: formData.contact.url,
  }

  formData.termsOfUse = formData.termsOfUse || {}
  requestBody.terms_of_use = {
    government_only: formData.termsOfUse.governmentOnly,
    pay_per_use: formData.termsOfUse.payPerUse,
    uptime_guarantee: formData.termsOfUse.uptimeGuarantee,
    support_response_time: formData.termsOfUse.supportResponseTime,
  }

  return requestBody
}
