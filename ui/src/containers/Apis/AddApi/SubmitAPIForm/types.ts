// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export interface RequestBody {
  description: string
  organization: {
    name: string
    ooid: number
  }
  service_name: string
  api_type: string
  api_authentication: string
  relations: Record<string, string[]> | undefined
  environments: {
    name: string
    api_url: string
    specification_url: string
    documentation_url: string
  }[]
  contact: {
    email: string
    phone: string
    url: string
  }
  terms_of_use: {
    government_only: boolean
    pay_per_use: boolean
    uptime_guarantee: number
    support_response_time: number | undefined
  }
}

export interface FormData {
  description: string
  organization: {
    name: string
    ooid: string
  }
  serviceName: string
  apiType: string
  apiAuthentication: string
  isBasedOnReferenceImplementation: string
  referenceImplementation: string
  productionApiUrl: string
  productionSpecificationUrl: string
  productionDocumentationUrl: string
  hasAcceptanceEnvironment: boolean
  acceptanceApiUrl: string
  acceptanceSpecificationUrl: string
  acceptanceDocumentationUrl: string
  hasDemoEnvironment: boolean
  demoApiUrl: string
  demoSpecificationUrl: string
  demoDocumentationUrl: string
  contact: {
    email: string
    phone: string
    url: string
  }
  termsOfUse: {
    governmentOnly: boolean
    payPerUse: boolean
    uptimeGuarantee: number
    supportResponseTime: number | undefined
  }
}
