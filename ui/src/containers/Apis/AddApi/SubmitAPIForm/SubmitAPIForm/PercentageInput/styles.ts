// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Field } from '../../../../../../components/Form'

export const Box = styled.div`
  display: flex;
  width: 138px;
`

export const Input = styled(Field)`
  border-right: 0;
  flex-grow: 1;
`

export const Pct = styled.div`
  background-color: ${(p) => p.theme.tokens.colorPaletteGray200};
  border: 1px solid ${(p) => p.theme.colorBorderInput};
  border-left: none;
  height: 48px;
  line-height: 45px;
  min-width: 3rem;
  text-align: center;
`
