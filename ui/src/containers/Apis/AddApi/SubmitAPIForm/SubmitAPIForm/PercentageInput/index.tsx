// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { FieldProps } from 'formik'
import { Pct, Input, Box } from './styles'

const PercentageInput: React.FunctionComponent<FieldProps> = ({
  field,
  ...inputProps
}) => {
  return (
    <Box>
      <Input {...field} {...inputProps} />
      <Pct>%</Pct>
    </Box>
  )
}

export default PercentageInput
