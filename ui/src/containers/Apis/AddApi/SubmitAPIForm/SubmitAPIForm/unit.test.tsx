// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { Formik } from 'formik'
import { APIType } from '../../../../../domain/models/enums'
import { renderWithProviders, screen } from '../../../../../test-helpers'
import { Api } from '../../../../../domain/models/types'
import SubmitAPIForm from './index'

const data: Api[] = [
  {
    id: 'test-api.json',
    organizationName: 'Organization Name',
    serviceName: 'Service Name',
    apiType: APIType.REST_JSON,
    totalScore: { points: 10, maxPoints: 10 },
  },
] as Api[]

const props: React.ComponentProps<typeof SubmitAPIForm> = {
  apis: {
    error: false,
    loaded: true,
    data,
  },
  errors: {},
  status: {},
  touched: {},
  handleReset: jest.fn(),
  values: {},
  handleSubmit: jest.fn(),
  isSubmitting: false,
}

describe('SubmitAPIForm', () => {
  it('should exist', () => {
    renderWithProviders(<SubmitAPIForm {...props} />)

    expect(screen.getByText('API toevoegen')).toBeInTheDocument()
  })

  describe('a11y', () => {
    it.each([
      {
        name: 'API details',
      },
      {
        name: 'Omgevingen',
      },
      {
        name: 'Productie',
      },
      {
        name: 'Acceptatie',
      },
      {
        name: 'Demo',
      },
      {
        name: 'Contact',
      },
      {
        name: 'Referentie-implementatie',
      },
      {
        name: 'Gebruiksvoorwaarden',
      },
    ])('has a fieldset with legend for $name', ({ name }) => {
      const handleSubmit = jest.fn()
      renderWithProviders(
        <Formik initialValues={{}} onSubmit={handleSubmit}>
          <SubmitAPIForm {...props} />
        </Formik>,
      )

      expect(
        screen.getByText(name, { selector: 'legend' }).closest('fieldset'),
      ).toBeInTheDocument()
    })
  })
})
