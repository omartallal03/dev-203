// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { ErrorMessage } from '@commonground/design-system'
import { FieldProps } from 'formik'
import { AsyncPaginate, LoadOptions } from 'react-select-async-paginate'
import { GroupBase } from 'react-select'
import OoRepository from '../../../../../../domain/oo-repository'
import { StyledLink, AsyncPaginateContainer } from './styles'
import { loadingMessage, noOptionsMessage, LoadingIndicator } from './helpers'
import { OptionType, Additional } from './types'

const OrganizationSelector: React.FunctionComponent<FieldProps> = ({
  field,
  form: { setFieldValue },
  ...props
}) => {
  const [hasError, setError] = useState(false)

  const handleChange = (option) => {
    setFieldValue('organization.name', option?.label)
    setFieldValue('organization.ooid', parseInt(option?.value))
  }

  const loadOptionsAsync: LoadOptions<
    OptionType,
    GroupBase<OptionType>,
    Additional
  > = async (search, _group, additional) => {
    setError(false)
    const page = additional?.page || 1

    try {
      const options = await OoRepository.search(search)
      const hasMore = false

      return {
        options,
        hasMore,
        additional: {
          page: page + 1,
        },
      }
    } catch (err) {
      setError(true)
      return {
        options: [],
        hasMore: false,
        additional: {
          page: 1,
        },
      }
    }
  }

  return (
    <AsyncPaginateContainer>
      <AsyncPaginate
        {...field}
        {...props}
        classNamePrefix="ReactSelect"
        loadOptions={loadOptionsAsync}
        loadingMessage={loadingMessage}
        noOptionsMessage={() => noOptionsMessage(hasError)}
        defaultOptions
        value={field.value?.value}
        onChange={handleChange}
        debounceTimeout={300}
        components={{ LoadingIndicator }}
        additional={{
          page: 1,
        }}
      />
      {hasError && (
        <ErrorMessage data-testid="error">
          Kan organisaties niet ophalen. <br />
          Probeer het later nog eens of voeg de API{' '}
          <StyledLink to="/apis/add/merge-request">via een MR toe</StyledLink>.
        </ErrorMessage>
      )}
    </AsyncPaginateContainer>
  )
}

export default OrganizationSelector
