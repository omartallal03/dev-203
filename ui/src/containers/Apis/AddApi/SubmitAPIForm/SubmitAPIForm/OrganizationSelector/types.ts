// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
export type OptionType = {
  value: number | string
  label: string
}

export type Additional = {
  page: number
}
