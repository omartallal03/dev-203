// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import React from 'react'
import { Spinner } from '@commonground/design-system'
import { StyledMessage } from './styles'

export const loadingMessage = () => {
  return <StyledMessage>Organisaties aan het ophalen ...</StyledMessage>
}

export const noOptionsMessage = (hasError: boolean) => {
  return (
    <StyledMessage>
      {hasError ? 'Kan organisaties niet ophalen.' : 'Niets gevonden!'}
    </StyledMessage>
  )
}

export const LoadingIndicator = () => {
  return <Spinner />
}
