// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { waitFor, waitForElementToBeRemoved } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { renderWithProviders, screen } from '../../../../../../test-helpers'
import { default as Repository } from '../../../../../../domain/oo-repository'
import { loadingMessage, noOptionsMessage } from './helpers'
import OrganizationSelector from './index'

const props: React.ComponentProps<typeof OrganizationSelector> = {
  field: {},
  form: {
    setFieldValue: jest.fn(),
  },
}

describe('<OrganizationSelector />', () => {
  it('implements search', async () => {
    jest.spyOn(Repository, 'search').mockResolvedValue([
      {
        label: 'organisatie',
        value: '1',
      },
    ])

    await waitFor(() => {
      renderWithProviders(<OrganizationSelector {...props} />)
    })

    await userEvent.type(screen.getByRole('combobox'), 'organisatie')
    expect(
      screen.getByText('Organisaties aan het ophalen ...'),
    ).toBeInTheDocument()

    await waitForElementToBeRemoved(
      screen.getByText('Organisaties aan het ophalen ...'),
    )
    expect(Repository.search).toHaveBeenLastCalledWith('organisatie')
    expect(screen.getByText('organisatie')).toBeInTheDocument()
  })

  it('returns an error message with link on error', async () => {
    jest.spyOn(React, 'useState').mockReturnValue([true, jest.fn()])
    jest.spyOn(Repository, 'search').mockResolvedValue([])

    renderWithProviders(<OrganizationSelector {...props} />)

    await waitFor(() => {
      expect(screen.getByTestId('error')).toBeInTheDocument()
      expect(
        screen.getByRole('link', { name: 'via een MR toe' }),
      ).toBeInTheDocument()
    })
  })
})

describe('loadingMessage()', () => {
  it('renders correctly', () => {
    renderWithProviders(loadingMessage())

    expect(
      screen.getByText('Organisaties aan het ophalen ...'),
    ).toBeInTheDocument()
  })
})

describe('noOptionsMessage()', () => {
  it.each([
    {
      hasError: false,
      expected: 'Niets gevonden!',
    },
    {
      hasError: true,
      expected: 'Kan organisaties niet ophalen.',
    },
  ])(
    'renders "$expected" when "hasError=$hasError"',
    ({ hasError, expected }) => {
      renderWithProviders(noOptionsMessage(hasError))

      expect(screen.getByText(expected)).toBeInTheDocument()
    },
  )
})
