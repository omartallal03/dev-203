// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const capitalize = (value: string): string => {
  return value.charAt(0).toUpperCase() + value.slice(1)
}

export const showEnvironment = (
  environment: string,
  values: Record<string, unknown>,
  optional?: boolean,
): boolean => {
  return (
    !optional || values[`has${capitalize(environment)}Environment`] === 'true'
  )
}
