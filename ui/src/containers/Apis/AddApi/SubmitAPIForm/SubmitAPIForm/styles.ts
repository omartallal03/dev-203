// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Field } from 'formik'
import { Legend } from '../../../../../components/Form'

export const StyledFormGroupColumnContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  @media screen and (min-width: 688px) {
    margin: 0 -28px;
  }
`

export const StyledFormGroupColumn = styled.div`
  flex: 1 0 100%;
  padding: 0;

  @media screen and (min-width: 688px) {
    padding: 0 28px;
    flex: 1 1 50%;
  }
`

export const StyledFormGroup = styled.div`
  border: none;
  margin-bottom: 1rem;
`

export const StyledLegend = styled(Legend)`
  font-size: 1rem;
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const StyledFormSetting = styled.div`
  display: flex;

  label {
    cursor: pointer;
    flex: 0 1 auto;
    margin-bottom: 0;
    order: 2;
    user-select: none;
  }
`

export const HelperMessage = styled.small`
  color: ${(p) => p.theme.colorTextLabel};
  display: block;
  font-size: ${(p) => p.theme.tokens.fontSizeSmall};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const SelectFieldLoading = styled.div`
  background: url(/static/media/arrow-down-solid-icon.cc2fbbbb.svg) right center
    no-repeat;
  background-color: ${(p) => `1px solid ${p.theme.colorBackgroundInput}`};
  border: ${(p) => `1px solid ${p.theme.colorBorderChoice}`};
  border-radius: 0;
  display: block;
  height: 48px;
  max-width: 45rem;
  outline: none;
  padding: ${(p) => p.theme.tokens.spacing04};
`
export const StyledField = styled(Field)`
  border: none;
  display: flex;
  max-width: 45rem;
  padding: 0;
  width: 100% !important;
`
