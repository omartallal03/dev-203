// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as i18n from './yup-translations'

describe('i18n', () => {
  it('returns the correct Dutch translations', () => {
    expect(i18n.array).toMatchInlineSnapshot(`
      Object {
        "max": "Het veld \${path} moet uit maximaal \${max} items bestaan",
        "min": "Het veld \${path} moet uit ten minste \${min} items bestaan",
      }
    `)
    expect(i18n.boolean).toMatchInlineSnapshot(`Object {}`)
    expect(i18n.date).toMatchInlineSnapshot(`
      Object {
        "max": "Het veld \${path} veld moet eerder zijn dan \${max}",
        "min": "Het veld \${path} veld moet later zijn dan \${min}",
      }
    `)
    expect(i18n.mixed).toMatchInlineSnapshot(`
      Object {
        "default": "Het veld \${path} is ongeldig",
        "notOneOf": "Het veld \${path} mag niet een van de deze waarden zijn: \${values}",
        "notType": [Function],
        "oneOf": "Het veld \${path} moet een van deze waarden zijn: \${values}",
        "required": "Het verplichte veld, \${path}, is niet ingevuld.",
      }
    `)
    expect(i18n.number).toMatchInlineSnapshot(`
      Object {
        "integer": "Het veld \${path} moet een geheel getal zijn",
        "lessThan": "Het veld \${path} moet kleiner zijn dan \${less}",
        "max": "Het veld \${path} moet kleiner dan of gelijk aan \${max} zijn",
        "min": "Het veld \${path} moet groter dan of gelijk aan \${min} zijn",
        "moreThan": "Het veld \${path} moet groter zijn dan \${more}",
        "negative": "Het veld \${path} moet een negatief getal zijn",
        "positive": "Het veld \${path} moet een natuurlijk getal zijn",
      }
    `)
    expect(i18n.object).toMatchInlineSnapshot(`
      Object {
        "noUnknown": "Het veld \${path} mag geen eigenschappen bevatten die niet eerder zijn gedefinieerd",
      }
    `)
    expect(i18n.string).toMatchInlineSnapshot(`
      Object {
        "email": "Het veld \${path} moet een geldig e-mailadres zijn",
        "length": "Het veld \${path} moet uit exact \${length} tekens bestaan",
        "lowercase": "Het veld \${path} moet uit kleine letters bestaan",
        "matches": "Het veld \${path} moet overeenkomen met: \\"\${regex}\\"",
        "max": "Het veld \${path} mag uit maximaal \${max} karakters bestaan",
        "min": "Het veld \${path} moet tenminste uit {min} tekens bestaan",
        "trim": "Het veld \${path} moet een getrimde tekst zijn",
        "uppercase": "Het veld \${path} moet uit hoofdletters bestaan",
        "url": "Het veld \${path} moet een geldige URL zijn",
      }
    `)
  })
})
