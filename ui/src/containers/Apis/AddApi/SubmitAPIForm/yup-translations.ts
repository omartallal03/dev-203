// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

// based on https://github.com/jquense/yup/blob/master/src/locale.ts

/* eslint-disable no-template-curly-in-string */

import {
  ArrayLocale,
  BooleanLocale,
  DateLocale,
  MixedLocale,
  NumberLocale,
  ObjectLocale,
  StringLocale,
} from 'yup/lib/locale'

export const mixed: MixedLocale = {
  default: 'Het veld ${path} is ongeldig',
  required: 'Het verplichte veld, ${path}, is niet ingevuld.',
  oneOf: 'Het veld ${path} moet een van deze waarden zijn: ${values}',
  notOneOf: 'Het veld ${path} mag niet een van de deze waarden zijn: ${values}',
  notType: ({ path, type }: { path: string; type: string }): string =>
    `Het veld, ${path}, moet van het type "${type}" zijn`,
}

export const string: StringLocale = {
  length: 'Het veld ${path} moet uit exact ${length} tekens bestaan',
  min: 'Het veld ${path} moet tenminste uit {min} tekens bestaan',
  max: 'Het veld ${path} mag uit maximaal ${max} karakters bestaan',
  matches: 'Het veld ${path} moet overeenkomen met: "${regex}"',
  email: 'Het veld ${path} moet een geldig e-mailadres zijn',
  url: 'Het veld ${path} moet een geldige URL zijn',
  trim: 'Het veld ${path} moet een getrimde tekst zijn',
  lowercase: 'Het veld ${path} moet uit kleine letters bestaan',
  uppercase: 'Het veld ${path} moet uit hoofdletters bestaan',
}

export const number: NumberLocale = {
  min: 'Het veld ${path} moet groter dan of gelijk aan ${min} zijn',
  max: 'Het veld ${path} moet kleiner dan of gelijk aan ${max} zijn',
  lessThan: 'Het veld ${path} moet kleiner zijn dan ${less}',
  moreThan: 'Het veld ${path} moet groter zijn dan ${more}',
  positive: 'Het veld ${path} moet een natuurlijk getal zijn',
  negative: 'Het veld ${path} moet een negatief getal zijn',
  integer: 'Het veld ${path} moet een geheel getal zijn',
}

export const date: DateLocale = {
  min: 'Het veld ${path} veld moet later zijn dan ${min}',
  max: 'Het veld ${path} veld moet eerder zijn dan ${max}',
}

export const boolean: BooleanLocale = {}

export const object: ObjectLocale = {
  noUnknown:
    'Het veld ${path} mag geen eigenschappen bevatten die niet eerder zijn gedefinieerd',
}

export const array: ArrayLocale = {
  min: 'Het veld ${path} moet uit ten minste ${min} items bestaan',
  max: 'Het veld ${path} moet uit maximaal ${max} items bestaan',
}
