// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { Api, Pagination } from '../../domain/models/types'
import { Facets } from '../../components/Filters/types'

export interface QueryParams {
  q: string
  organisatie: string[]
  type: string[]
  pagina: string
  aantalPerPagina: string
}

export interface APIQueryParams {
  q: string
  organization_ooid: string[]
  api_type: string[]
  page: string
  rowsPerPage: string
}

export interface ApiResult extends Pagination {
  apis: Api[]
  facets: Facets
}
