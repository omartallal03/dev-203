// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import userEvent from '@testing-library/user-event'
import { Helmet } from 'react-helmet'
import { waitFor } from '@testing-library/react'
import { UseQueryResult } from 'react-query/types/react/types'
import { fireEvent } from '@testing-library/dom'
import {
  act,
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../test-helpers'
import { apisMock } from '../../../.jest/mocks/apis.mock'
import ApiRepository from '../../domain/api-repository'
import { IncomingApi, IncomingApiResponse } from '../../domain/models/types'
import APIRepository from '../../domain/api-repository'
import * as hooks from '../../hooks/useApis'
import APIOverview from './index'

jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as Record<string, string>),
  useNavigate: () => mockedUsedNavigate,
}))

const mockedUsedNavigate = jest.fn()

const mockBackendSuccess = () => {
  const result = {
    ...apisMock,
  } as unknown as IncomingApiResponse<IncomingApi[]>

  jest.spyOn(global, 'fetch').mockImplementation(
    () =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(result),
      }) as Promise<Response>,
  )
  jest.spyOn(ApiRepository, 'getAll').mockReturnValue(Promise.resolve(result))
  jest.spyOn(hooks, 'useApis').mockReturnValueOnce({
    error: false,
    isLoading: false,
    isFetching: false,
    data: result,
  } as UseQueryResult<IncomingApiResponse<IncomingApi[]>>)
}

const mockBackendError = () => {
  jest.spyOn(ApiRepository, 'getAll').mockRejectedValue(new Error('rejected'))
  jest.spyOn(hooks, 'useApis').mockReturnValueOnce({
    error: new Error('rejected'),
    isLoading: false,
    isFetching: false,
    data: [],
  } as UseQueryResult<IncomingApiResponse<IncomingApi[]>>)
}

const mockBackendEmptyState = () => {
  const result = {
    ...apisMock,
    results: [],
  } as unknown as IncomingApiResponse<IncomingApi[]>

  jest.spyOn(global, 'fetch').mockImplementation(
    () =>
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve(result),
      }) as Promise<Response>,
  )
  jest.spyOn(APIRepository, 'getAll').mockReturnValue(Promise.resolve(result))
  jest.spyOn(hooks, 'useApis').mockReturnValueOnce({
    error: false,
    isLoading: false,
    isFetching: false,
    data: [],
  } as UseQueryResult<IncomingApiResponse<IncomingApi[]>>)
}

describe('APIOverview', () => {
  it('renders a heading', async () => {
    mockBackendSuccess()
    renderWithProviders(<APIOverview />)

    await waitFor(() => {
      expect(
        screen.getByRole('heading', {
          level: 1,
          name: `API's binnen de Nederlandse overheid`,
        }),
      ).toBeInTheDocument()
    })
  })

  it('handles backend errors gracefully', async () => {
    mockBackendError()
    renderWithProviders(<APIOverview />)

    await waitFor(() => {
      expect(
        screen.getByText(/Er ging iets fout tijdens het ophalen van de API's./),
      ).toBeInTheDocument()
    })
  })

  it('shows a message when there are no APIs available', async () => {
    mockBackendEmptyState()
    renderWithProviders(<APIOverview />)

    await waitFor(() => {
      expect(
        screen.getByText(/Er zijn \(nog\) geen API's beschikbaar./),
      ).toBeInTheDocument()
    })
  })

  it('has a search input field', async () => {
    mockBackendSuccess()
    renderWithProviders(<APIOverview />)

    await waitFor(() => {
      expect(screen.getByLabelText('Vind APIs')).toBeInTheDocument()
    })
  })

  it('has an add API button', async () => {
    mockBackendSuccess()
    renderWithProviders(<APIOverview />)

    await waitFor(() => {
      expect(
        screen.getAllByRole('link', {
          name: /api toevoegen/i,
        }),
      ).toHaveLength(2)
    })
  })

  describe('API List', () => {
    it('should show a filter for the API type', async () => {
      mockBackendSuccess()
      renderWithProviders(<APIOverview />)

      await waitFor(() => {
        expect(
          screen.getByRole('checkbox', { name: /REST\/JSON/ }),
        ).toBeInTheDocument()
        expect(
          screen.getByRole('checkbox', { name: /REST\/XML/ }),
        ).toBeInTheDocument()
      })
    })

    it('should show a filter for the organization', async () => {
      mockBackendSuccess()
      renderWithProviders(<APIOverview />)

      await waitFor(() => {
        expect(
          screen.getByRole('checkbox', {
            name: /Centraal Bureau voor de Statistiek/i,
          }),
        ).toBeInTheDocument()
        expect(
          screen.getByRole('checkbox', { name: /Gemeente Amsterdam/i }),
        ).toBeInTheDocument()
      })
    })

    it('should show the total number of APIs', async () => {
      mockBackendSuccess()
      renderWithProviders(<APIOverview />)

      await waitFor(() => {
        expect(screen.getByText(/49 API's/)).toBeInTheDocument()
      })
    })

    it('should show a list of APIs', async () => {
      mockBackendSuccess()
      renderWithProviders(<APIOverview />)

      await waitFor(() => {
        expect(
          screen.getByRole('link', { name: /CBS OData/i }),
        ).toBeInTheDocument()
      })
    })

    describe('Pagination', () => {
      const scrollTo = window.scrollTo
      beforeEach(() => {
        window.scrollTo = jest.fn()
      })
      afterEach(() => {
        window.scrollTo = scrollTo
      })

      it('should show pagination', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        await waitFor(() => {
          expect(screen.getByRole('button', { name: /1/ })).toBeInTheDocument()
          expect(screen.getByRole('button', { name: /2/ })).toBeInTheDocument()
        })
      })

      it('should handle pagination', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        const pageTwo = await screen.findByRole('button', { name: /2/ })
        await userEvent.click(pageTwo)

        await waitFor(() => {
          expect(mockedUsedNavigate).toHaveBeenCalledWith('?pagina=2')
        })
      })

      it('should handle results per page', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        const resultsPerPage = await screen.findByLabelText(
          'Aantal resultaten per pagina',
        )
        await userEvent.selectOptions(resultsPerPage, ['25'])

        await waitFor(() => {
          expect(mockedUsedNavigate).toHaveBeenCalledWith(
            expect.stringContaining('?aantalPerPagina=25'),
          )
        })
      })
    })

    describe('Filter', () => {
      it('should filter by text', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        const input = await screen.findByLabelText('Vind APIs')
        act(() => {
          fireEvent.change(input, { target: { value: 'o' } })
        })
        await userEvent.type(input, 'o')

        await waitFor(() => {
          expect(mockedUsedNavigate).toHaveBeenLastCalledWith(
            expect.stringContaining('/apis?q=o'),
          )
        })
      })

      it('should filter by API type', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        const checkbox = await screen.findByRole('checkbox', {
          name: /REST\/XML/,
        })
        await userEvent.click(checkbox)

        await waitFor(() => {
          expect(checkbox).toBeChecked()
          expect(mockedUsedNavigate).toHaveBeenCalledWith('?type=rest_xml')
        })
      })

      it('should filter by organization', async () => {
        mockBackendSuccess()
        renderWithProviders(<APIOverview />)

        const checkbox = await screen.findByRole('checkbox', {
          name: /Centraal Bureau voor de Statistiek/i,
        })
        await userEvent.click(checkbox)

        await waitFor(() => {
          expect(checkbox).toBeChecked()
          expect(mockedUsedNavigate).toHaveBeenCalledWith(
            '?organisatie=00000001812483297000',
          )
        })
      })
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', async () => {
      mockBackendSuccess()
      mountWithProviders(<APIOverview />)

      await waitFor(() => {
        expect(Helmet.peek().title).toEqual(
          'Developer Overheid: APIs binnen de Nederlandse overheid',
        )
      })
    })
  })
})
