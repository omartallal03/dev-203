// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { shallow } from 'enzyme/build'
import { APIType } from '../../../domain/models/enums'
import { StyledLink } from './styles'
import APISummary from './index'

describe('APISummary', () => {
  let wrapper

  beforeEach(() => {
    const organization = {
      name: 'Organization',
      ooid: 1,
    }
    wrapper = shallow(
      <APISummary
        id="test-api.json"
        organization={organization}
        serviceName="Service"
        apiType={APIType.REST_JSON}
        scores={{
          hasDocumentation: true,
          hasSpecification: true,
          hasContactDetails: false,
          providesSla: false,
        }}
        totalScore={{ points: 10, maxPoints: 10 }}
      />,
    )
  })

  it('should show a Link', () => {
    expect(wrapper.type()).toBe(StyledLink)
  })

  it('should link to the API detail page', () => {
    expect(wrapper.props().to).toBe('test-api.json')
  })

  it('should display the service and organization name', () => {
    expect(
      wrapper.containsAllMatchingElements(['Service', 'Organization']),
    ).toBeTruthy()
  })
})
