// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import PillBadge from '../../../components/PillBadge'
import { Api } from '../../../domain/models/types'
import {
  StyledLink,
  StyledServiceName,
  StyledOrganizationName,
  PillContainer,
  StyledGrade,
} from './styles'

type Props = Pick<
  Api,
  'id' | 'serviceName' | 'organization' | 'apiType' | 'totalScore'
>

const APISummary: React.FunctionComponent<Props> = ({
  id,
  serviceName,
  organization,
  apiType,
  totalScore,
  ...props
}) => (
  <StyledLink to={id} data-test="link" {...props}>
    <StyledServiceName>{serviceName}</StyledServiceName>
    <StyledOrganizationName>{organization.name}</StyledOrganizationName>
    <PillContainer>
      <PillBadge>{apiType.label}</PillBadge>
    </PillContainer>
    <StyledGrade totalScore={totalScore} />
  </StyledLink>
)

export default APISummary
