// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import mq from '../../../theme/mediaQueries'
import Grade from '../ApiDetail/APIDetails/GradeBanner/Grade'

export const StyledLink = styled(Link)`
  align-items: center;
  display: grid;
  grid-template-areas:
    'service service service'
    'organisation organisation organisation'
    'pill pill grade';
  grid-template-columns: 33.333% 33.333% 33.333%;
  grid-template-rows: auto;
  padding: ${(p) => p.theme.tokens.spacing05};
  text-decoration: none;

  &:hover {
    background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  }

  ${mq.smUp`
    grid-template-rows: auto auto;
    grid-template-columns: auto 150px 50px;
    grid-template-areas:
      "service pill grade"
      "organisation pill grade";
  `}
`

export const StyledServiceName = styled.h3`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  grid-area: service;
  margin: 2px 0 6px 0;
`

export const StyledOrganizationName = styled.small`
  grid-area: organisation;

  ${mq.smUp`
    margin-bottom: 0;
  `}
`

export const PillContainer = styled.div`
  grid-area: pill;

  ${mq.smUp`
    place-self: center;
  `}
`

export const StyledGrade = styled(Grade)`
  grid-area: grade;
  justify-self: end;

  ${mq.smUp`
    place-self: center;
  `}
`
