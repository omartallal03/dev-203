// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import APISummary from '../ApiSummary'
import { Api } from '../../../domain/models/types'
import { StyledCard, StyledList, StyledListItem } from './styles'

interface Props {
  apis: Pick<
    Api,
    'id' | 'serviceName' | 'organization' | 'apiType' | 'totalScore'
  >[]
}

const APIList: React.FunctionComponent<Props> = ({ apis }) => (
  <StyledCard>
    <StyledList id="apis">
      {apis.map((api) => (
        <StyledListItem key={api.id}>
          <APISummary
            id={api.id}
            serviceName={api.serviceName}
            organization={api.organization}
            apiType={api.apiType}
            totalScore={api.totalScore}
          />
        </StyledListItem>
      ))}
    </StyledList>
  </StyledCard>
)

export default APIList
