// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { shallow } from 'enzyme/build'
import React from 'react'
import { APIType } from '../../../domain/models/enums'
import { Api } from '../../../domain/models/types'
import APIList from './index'

const apis: Pick<
  Api,
  'id' | 'serviceName' | 'organizationName' | 'apiType' | 'totalScore'
>[] = [
  {
    id: 'test-api.yaml',
    organizationName: 'Organization Name',
    serviceName: 'Service Name',
    apiType: APIType.REST_JSON,
    totalScore: { points: 10, maxPoints: 10 },
  },
]

describe('APIList', () => {
  it('should show all available APIs', () => {
    const component = shallow(<APIList apis={apis} />)
    expect(component.find('APISummary')).toHaveLength(1)
  })

  it('should link to the API', () => {
    const component = shallow(<APIList apis={apis} />)
    expect(component.find('APISummary').find('APISummary').exists()).toBe(true)
  })

  describe('a11y', () => {
    it('has an internal anchor link for skip links to refer to', () => {
      const component = shallow(<APIList apis={apis} />)
      expect(component.find('#apis').exists()).toBe(true)
    })
  })
})
