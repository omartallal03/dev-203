// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import {
  DEFAULT_PAGE,
  DEFAULT_ROWS_PER_PAGE,
} from '../../components/Pagination/constants'
import { APIQueryParams } from './types'

export const getQueryParams = (
  searchParams: URLSearchParams,
): APIQueryParams => {
  return {
    q: searchParams.get('q') || '',
    organization_ooid: searchParams.getAll('organisatie'),
    api_type: searchParams.getAll('type'),
    page: searchParams.get('pagina') || DEFAULT_PAGE,
    rowsPerPage: searchParams.get('aantalPerPagina') || DEFAULT_ROWS_PER_PAGE,
  }
}
