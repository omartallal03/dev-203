// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { Helmet } from 'react-helmet'
import { waitFor } from '@testing-library/react'
import { UseQueryResult } from 'react-query/types/react/types'
import {
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../../test-helpers'
import * as hooks from '../../../hooks/useMetrics'
import { Metrics } from '../../../domain/models/types'
import APIStatistics from './index'

const mockBackendEmptyState = () => {
  jest.spyOn(hooks, 'useMetrics').mockReturnValue({
    data: {
      api: {
        totalCounts: [],
      },
      organization: {
        totalCounts: [],
      },
      repository: {
        totalCounts: [],
      },
    },
    isFetching: false,
    error: null,
  } as UseQueryResult<Metrics>)
}

const mockBackendLoadingState = () => {
  jest.spyOn(hooks, 'useMetrics').mockReturnValue({
    data: null,
    isFetching: true,
    error: null,
  } as UseQueryResult<Metrics>)
}

const mockBackendErrorState = () => {
  jest.spyOn(hooks, 'useMetrics').mockReturnValue({
    data: null,
    isFetching: false,
    error: true,
  } as UseQueryResult<Metrics>)
}

describe('<APIStatistics />', () => {
  it('renders a back button correctly', () => {
    mockBackendEmptyState()
    renderWithProviders(<APIStatistics />)

    expect(
      screen.getByRole('button', { name: /Terug naar API overzicht/ }),
    ).toBeInTheDocument()
  })

  it('renders the header correctly', () => {
    mockBackendEmptyState()
    renderWithProviders(<APIStatistics />)

    expect(
      screen.getByRole('heading', { level: 1, name: /Statistieken/ }),
    ).toBeInTheDocument()
  })

  it('renders the subheading correctly', () => {
    mockBackendEmptyState()
    renderWithProviders(<APIStatistics />)

    expect(
      screen.getByText(
        /Het verloop van de API's en repositories uit het Developer Overheid overzicht./,
      ),
    ).toBeInTheDocument()
  })

  it('renders the selectors correctly', () => {
    mockBackendEmptyState()
    renderWithProviders(<APIStatistics />)

    expect(screen.getByText(/Laatste maand/)).toBeInTheDocument()
    expect(screen.getByText(/Laatste jaar/)).toBeInTheDocument()
  })

  it('renders a spinner when loading', async () => {
    mockBackendLoadingState()
    renderWithProviders(<APIStatistics />)

    await waitFor(() => {
      expect(screen.getByTestId('loading')).toBeInTheDocument()
    })
  })

  it('renders an error message when needed', async () => {
    mockBackendErrorState()
    renderWithProviders(<APIStatistics />)

    await waitFor(() => {
      expect(
        screen.getByText(
          'Er ging iets fout tijdens het ophalen van de statistieken.',
        ),
      ).toBeInTheDocument()
    })
  })

  describe('API', () => {
    it('renders a title', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(screen.getAllByText("Aantal API's")).toHaveLength(2)
      })
    })

    it('renders a subtitle', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(
          screen.getAllByText(
            "Het aantal API's in het Developer Overheid overzicht.",
          ),
        ).toHaveLength(2)
      })
    })
  })

  describe('Organisation', () => {
    it('renders a title', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(screen.getAllByText('Aantal organisaties')).toHaveLength(2)
      })
    })

    it('renders a subtitle', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(
          screen.getAllByText("Het aantal organisaties dat API's aanbiedt."),
        ).toHaveLength(2)
      })
    })
  })

  describe('Repository', () => {
    it('renders a title', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(screen.getAllByText('Aantal repositories')).toHaveLength(2)
      })
    })

    it('renders a subtitle', async () => {
      mockBackendEmptyState()
      renderWithProviders(<APIStatistics />)

      await waitFor(() => {
        expect(
          screen.getAllByText(
            'Het aantal repositories in het Developer Overheid overzicht.',
          ),
        ).toHaveLength(2)
      })
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mockBackendEmptyState()
      mountWithProviders(<APIStatistics />)

      expect(Helmet.peek().title).toEqual('Developer Overheid: Statistieken')
    })
  })
})
