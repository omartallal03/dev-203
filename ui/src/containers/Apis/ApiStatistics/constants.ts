// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
export enum TimeRange {
  LastMonth = 'last-month',
  LastYear = 'last-year',
}

export const TIME_RANGE_KEY = 'time-range'
