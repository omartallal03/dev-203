// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import ArrowLeftIcon from '../../../assets/icons/ArrowLeft'
import mq from '../../../theme/mediaQueries'

export const StyledStatisticsPage = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  max-width: ${(p) => p.theme.containerWidth};
  padding: 0 ${(p) => p.theme.containerPadding};
`

export const StyledStatisticsHeader = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
`

export const StyledSubtitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`
export const StyledArrowIcon = styled(ArrowLeftIcon)`
  margin-right: 3px;
`

export const BackButton = styled.button`
  align-items: center;
  align-self: start;
  background: transparent;
  border: none;
  color: ${(p) => p.theme.colorTextLink};
  cursor: pointer;
  display: flex;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
  padding: 0;
`

export const StyledStatisticsBody = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${(p) => p.theme.tokens.spacing06};
  width: 100%;

  ${mq.smDown`
    flex-direction: column;
  `}
`

export const Label = styled.label`
  color: ${(p) => p.theme.colorTextInputLabel};
  display: block;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
`

export const Row = styled.div`
  display: flex;
  gap: ${(p) => p.theme.tokens.spacing06};

  ${mq.smDown`
    flex-direction: column;
  `}
`

export const Column = styled.div`
  display: flex;
  width: 50%;
`
