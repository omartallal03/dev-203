// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import Button from '@commonground/design-system/dist/components/Button'
import mq from '../../../../theme/mediaQueries'

export const StyledOptionButton = styled(Button)`
  ${(props) => {
    if (props.active) {
      return `
        background: #0B71A1;
        color: white;
        &:hover {
          background: #005282;
          color: white;
        };
    `
    }
  }}

  ${mq.xs`
    width: 100%;
  `}
`

export const StyledButtonGroup = styled.div`
  display: flex;
  gap: ${(p) => p.theme.tokens.spacing02};
  margin-bottom: ${(p) => p.theme.tokens.spacing05};

  ${StyledOptionButton}:first-child {
    margin-left: auto;
  }

  ${mq.xs`
    flex-direction: column;
  `}
`
