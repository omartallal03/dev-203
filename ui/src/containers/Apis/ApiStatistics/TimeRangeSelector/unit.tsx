// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { TimeRange } from '../constants'
import { StyledButtonGroup, StyledOptionButton } from './styles'

interface Props {
  onSelect: (period: TimeRange) => void
  value: TimeRange
}

const i18 = {
  [TimeRange.LastYear]: 'Laatste jaar',
  [TimeRange.LastMonth]: 'Laatste maand',
}

const TimeRangeSelector: React.FunctionComponent<Props> = ({
  onSelect,
  value,
}) => {
  return (
    <StyledButtonGroup>
      {Object.values(TimeRange)
        .reverse()
        .map((period) => {
          return (
            <StyledOptionButton
              active={period === value}
              key={period}
              onClick={() => onSelect(period)}
              variant="secondary"
            >
              {i18[period.toString()]}
            </StyledOptionButton>
          )
        })}
    </StyledButtonGroup>
  )
}

export default TimeRangeSelector
