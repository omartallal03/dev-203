// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import moment from 'moment'

export interface FilterOption {
  label: string
  value: null | moment.Moment
}
