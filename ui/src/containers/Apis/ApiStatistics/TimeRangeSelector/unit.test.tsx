// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import userEvent from '@testing-library/user-event'
import { renderWithProviders, screen } from '../../../../test-helpers'
import { TimeRange } from '../constants'
import TimeRangeSelector from './unit'

describe('<TimeRangeSelector />', () => {
  it('has a button for the last month and for the last year', () => {
    const onSelect = jest.fn()
    renderWithProviders(
      <TimeRangeSelector onSelect={onSelect} value={TimeRange.LastMonth} />,
    )

    expect(
      screen.getByRole('button', { name: 'Laatste maand' }),
    ).toBeInTheDocument()
    expect(
      screen.getByRole('button', { name: 'Laatste jaar' }),
    ).toBeInTheDocument()
  })

  it.each([
    {
      value: TimeRange.LastMonth,
      name: 'Laatste jaar',
      expected: 'last-year',
    },
    {
      value: TimeRange.LastMonth,
      name: 'Laatste maand',
      expected: 'last-month',
    },
  ])('renders two buttons', async ({ value, name, expected }) => {
    const onSelect = jest.fn()
    renderWithProviders(<TimeRangeSelector onSelect={onSelect} value={value} />)

    await userEvent.click(screen.getByRole('button', { name }))
    expect(onSelect).toHaveBeenCalledWith(expected)
  })
})
