// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import { Helmet } from 'react-helmet'
import { useNavigate } from 'react-router-dom'
import Spinner from '@commonground/design-system/dist/components/Spinner'
import { StyledSubtitle } from '../styles'
import AreaChart from '../../../components/AreaChart'
import { useMetrics } from '../../../hooks/useMetrics'
import { DONSmall } from '../../../components/CustomDON'
import BarChart from '../../../components/BarChart'
import ColumnChart from '../../../components/ColumnChart'
import { useStateParams } from '../../../hooks/useStateParams'
import {
  BackButton,
  StyledArrowIcon,
  StyledStatisticsHeader,
  StyledStatisticsPage,
  StyledStatisticsBody,
  Row,
} from './styles'
import TimeRangeSelector from './TimeRangeSelector/unit'
import { TIME_RANGE_KEY, TimeRange } from './constants'

const APIStatistics: React.FunctionComponent = () => {
  const [params, setParams] = useStateParams({
    'time-range': TimeRange.LastMonth,
  })
  const navigate = useNavigate()
  const timeRangeValue = params[TIME_RANGE_KEY.toString()]
  const { data: metrics, isFetching, error } = useMetrics(timeRangeValue)

  const handleSelectPeriod = (timeRange: TimeRange) => {
    setParams({ [TIME_RANGE_KEY]: timeRange })
  }

  const renderContent = () => {
    if (isFetching) {
      return <Spinner data-testid="loading" />
    }
    if (error) {
      return (
        <DONSmall>
          Er ging iets fout tijdens het ophalen van de statistieken.
        </DONSmall>
      )
    }
    return (
      <React.Fragment>
        <AreaChart
          title="Aantal API's"
          subtitle="Het aantal API's in het Developer Overheid overzicht."
          data={metrics?.api}
          timeRange={timeRangeValue}
        />
        <AreaChart
          title="Aantal organisaties"
          subtitle="Het aantal organisaties dat API's aanbiedt."
          data={metrics?.organization}
          timeRange={timeRangeValue}
        />
        <AreaChart
          title="Aantal repositories"
          subtitle="Het aantal repositories in het Developer Overheid overzicht."
          data={metrics?.repository}
          timeRange={timeRangeValue}
        />
        <Row>
          <BarChart
            title="Implementatie API rules"
            subtitle="Het percentage van de API's die aan een API rule voldoet."
            data={metrics?.designRulesLast}
          />
          <ColumnChart
            title="Verdeling API score"
            subtitle="Het aantal API's per API score."
            data={metrics?.designRulesApiSuccessesLast}
          />
        </Row>
      </React.Fragment>
    )
  }

  return (
    <StyledStatisticsPage>
      <Helmet>
        <title>Developer Overheid: Statistieken</title>
        <meta
          name="description"
          content="Het verloop van de API's en repositories uit het overzicht."
        />
      </Helmet>
      <StyledStatisticsHeader>
        <div>
          <BackButton onClick={() => navigate('/apis')}>
            <StyledArrowIcon /> Terug naar API overzicht
          </BackButton>
          <h1>Statistieken</h1>
          <StyledSubtitle>
            Het verloop van de API&apos;s en repositories uit het Developer
            Overheid overzicht.
          </StyledSubtitle>
          <TimeRangeSelector
            onSelect={handleSelectPeriod}
            value={timeRangeValue}
          />
        </div>
      </StyledStatisticsHeader>
      <StyledStatisticsBody>{renderContent()}</StyledStatisticsBody>
    </StyledStatisticsPage>
  )
}

export default APIStatistics
