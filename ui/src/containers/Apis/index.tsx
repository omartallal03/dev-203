// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Link, useNavigate, useSearchParams } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import Button from '@commonground/design-system/dist/components/Button'
import Spinner from '@commonground/design-system/dist/components/Spinner'
import Pagination from '../../components/Pagination'
import { ResultsHeader } from '../../components/ResultsHeader'
import { DONSmall } from '../../components/CustomDON'
import { modelFromAPIResponse } from '../../domain/models/api'
import { useDebounce } from '../../utils/hooks/useDebounce'
import { generateQueryParams } from '../../utils/uriHelpers'
import {
  DEFAULT_PAGE,
  DEFAULT_ROWS_PER_PAGE,
} from '../../components/Pagination/constants'
import { ReactComponent as LineChart } from '../../assets/icons/line-chart-line.svg'
import { useApis } from '../../hooks/useApis'
import APIList from './ApiList'
import {
  StyledOverviewPage,
  StyledOverviewHeader,
  StyledOverviewBody,
  StyledResultsContainer,
  StyledSubtitle,
  StyledSearch,
  StyledAddLinkDesktop,
  StyledAddIcon,
  Label,
  StyledFilters,
  StyledButton,
  StyledIcon,
} from './styles'
import { getQueryParams } from './helpers'
import { APIQueryParams, QueryParams } from './types'

const APIOverview: React.FunctionComponent = () => {
  const [searchParams] = useSearchParams()
  const navigate = useNavigate()

  const queryParams = getQueryParams(searchParams)
  const {
    data: response,
    error,
    isLoading,
    isFetching,
  } = useApis(queryParams) || {}
  const result = Object.assign({}, response, {
    apis: response?.results?.map((api) => modelFromAPIResponse(api)),
  })

  const debounced = useDebounce(searchParams.get('q'), 300)

  const isSearching = debounced !== searchParams.get('q')

  const handleSearchHandler = (query: string) => {
    searchParams.delete('pagina')
    searchParams.set('q', query)

    navigate(`/apis?${searchParams}`)
  }

  const formSubmitHandler = (event) => {
    event.preventDefault()

    const input = event.target.query
    handleSearchHandler(input.value)
  }

  const formChangeHandler = (event) => {
    event.preventDefault()

    const input = event.target.value
    handleSearchHandler(input)
  }

  const handleFilterChange = (params: APIQueryParams) => {
    // Reset facets when starting a new text search
    if (params.q !== queryParams.q) {
      params.organization_ooid = []
      params.api_type = []
    }

    const i18nParams: Partial<QueryParams> = {
      q: params.q,
      organisatie: params.organization_ooid || [],
      type: params.api_type || [],
    }

    if (params.page !== DEFAULT_PAGE) {
      i18nParams.pagina = params.page
    }

    if (params.rowsPerPage !== DEFAULT_ROWS_PER_PAGE) {
      i18nParams.aantalPerPagina = params.rowsPerPage
    }

    navigate(`?${generateQueryParams(i18nParams)}`)
  }

  const handlePageChange = (page: string) => {
    searchParams.set('pagina', page.toString())

    navigate(`?${searchParams}`)
  }

  const handleResultsPerPageChange = (resultsPerPage: string) => {
    searchParams.set('aantalPerPagina', resultsPerPage)
    searchParams.set('pagina', '1')

    navigate(`?${searchParams}`)
  }

  const { page } = queryParams
  const totalResults =
    !isLoading && !error && result ? result.totalResults : null

  const renderApiList = () => {
    if (isLoading || isFetching) {
      return <Spinner data-testid="loading" />
    }

    if (error) {
      return (
        <DONSmall>Er ging iets fout tijdens het ophalen van de API's.</DONSmall>
      )
    }

    if (!result.apis?.length) {
      return <DONSmall>Er zijn (nog) geen API's beschikbaar.</DONSmall>
    }

    return <APIList apis={result.apis} />
  }

  return (
    <StyledOverviewPage>
      <Helmet>
        <title>Developer Overheid: APIs binnen de Nederlandse overheid</title>
        <meta
          name="description"
          content="Een overzicht van APIs binnen de Nederlandse overheid."
        />
      </Helmet>
      <StyledOverviewHeader>
        <div>
          <h1>API's binnen de Nederlandse overheid</h1>
          <StyledSubtitle>
            Een wegwijzer naar de API’s die (semi-)overheidsorganisaties in
            Nederland aanbieden.
          </StyledSubtitle>
          <form onSubmit={(event) => formSubmitHandler(event)}>
            <div onChange={(event) => formChangeHandler(event)}>
              <Label htmlFor="search-api">Vind APIs</Label>
              <StyledSearch
                inputProps={{
                  placeholder: 'Voer een zoekterm in',
                  name: 'query',
                  id: 'search-api',
                  value: queryParams.q,
                }}
                searching={isSearching}
              />
            </div>
          </form>
        </div>
        <StyledButton variant="link" as={Link} to="/statistics">
          <StyledIcon as={LineChart} />
          Bekijk API statistieken
        </StyledButton>
      </StyledOverviewHeader>

      <StyledOverviewBody>
        <StyledFilters
          initialValues={queryParams}
          facets={result.facets}
          onSubmit={handleFilterChange}
        />
        <StyledResultsContainer>
          <ResultsHeader
            totalResults={totalResults}
            objectName="API"
            objectNamePlural="API's"
            addLinkTarget="add"
          >
            <Button as={StyledAddLinkDesktop} to="add/form" variant="secondary">
              <StyledAddIcon />
              API toevoegen
            </Button>
          </ResultsHeader>
          {renderApiList()}
          <Pagination
            currentPage={page}
            totalRows={result.totalResults}
            rowsPerPage={result.rowsPerPage}
            onPageChangedHandler={handlePageChange}
            onResultsPerPageChange={handleResultsPerPageChange}
          />
        </StyledResultsContainer>
      </StyledOverviewBody>
    </StyledOverviewPage>
  )
}

export default APIOverview
