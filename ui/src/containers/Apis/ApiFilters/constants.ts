// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

import { Filter } from '../../../components/Filters/types'
import { APIType } from '../../../domain/models/enums'

export const FILTERS: Filter[] = [
  {
    key: 'api_type',
    label: 'API type',
    getLabel: (term: string) => APIType.valueOf(term).label,
    skipTo: 'organization',
  },
  {
    key: 'organization_ooid',
    label: 'Organisatie',
    getLabel: (term: string) => term,
    skipTo: 'apis',
  },
]
