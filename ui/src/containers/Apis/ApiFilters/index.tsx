// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React, { useState } from 'react'
import Filters from '../../../components/Filters'
import { APIQueryParams } from '../types'
import { FILTERS } from './constants'

interface Props
  extends Omit<
    React.ComponentProps<typeof Filters>,
    'initialValues' | 'filters'
  > {
  initialValues: APIQueryParams
}

const APIFilters: React.FunctionComponent<Props> = ({ ...props }) => {
  const [sortOnCount, setSortOnCount] = useState(true)

  const handleExpand = (isExpanded: boolean) => {
    setSortOnCount(!isExpanded)
  }

  return (
    <Filters
      {...props}
      filters={FILTERS}
      onExpand={handleExpand}
      sortOnCount={sortOnCount}
      shouldExpand={true}
    />
  )
}

export default APIFilters
