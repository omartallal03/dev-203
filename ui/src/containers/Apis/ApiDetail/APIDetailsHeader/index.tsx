// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useNavigate } from 'react-router-dom'

// Components
import { H1, H2 } from '../../../../components/Headings'
import PillBadge from '../../../../components/PillBadge'

// Helpers
import { withRouter } from '../../../../utils/hooks/withRouter'
import { getExternalSpecificationDescription } from './helpers'

// Styles
import {
  StyledArrowIcon,
  StyledExternalIcon,
  BackButton,
  SpecLink,
  HeaderContainer,
} from './styles'

interface Props {
  previousName: string
  serviceName: string
  organizationName: string
  apiType?: { label: string }
  externalSpecificationUrl?: string
}
export const APIDetailsHeader: React.FunctionComponent<Props> = ({
  previousName,
  serviceName,
  organizationName,
  apiType,
  externalSpecificationUrl,
}) => {
  const navigate = useNavigate()
  return (
    <>
      <BackButton onClick={() => navigate(-1)}>
        <StyledArrowIcon /> Terug naar {previousName}
      </BackButton>

      {externalSpecificationUrl ? (
        <HeaderContainer>
          <H1>{serviceName}</H1>
          <SpecLink href={externalSpecificationUrl}>
            <span>
              {getExternalSpecificationDescription(externalSpecificationUrl)}
            </span>
            <StyledExternalIcon />
          </SpecLink>
        </HeaderContainer>
      ) : (
        <H1>{serviceName}</H1>
      )}
      <H2>{organizationName}</H2>
      {apiType && <PillBadge>{apiType.label}</PillBadge>}
    </>
  )
}

export default withRouter(APIDetailsHeader)
