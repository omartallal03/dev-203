// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { renderWithProviders, screen } from '../../../../test-helpers'
import { getExternalSpecificationDescription } from './helpers'
import { APIDetailsHeader } from './index'

const details: React.ComponentProps<typeof APIDetailsHeader> = {
  previousName: 'vorige',
  organizationName: 'Organization Name',
  serviceName: 'Service Name',
  externalSpecificationUrl: 'https://example.com/spec.yml',
}

describe('APIDetailsHeader', () => {
  beforeEach(() => {
    renderWithProviders(<APIDetailsHeader {...details} />)
  })

  it('should show the back button', () => {
    expect(
      screen.getByText(`Terug naar ${details.previousName}`),
    ).toBeInTheDocument()
  })

  it('should show the service as title', () => {
    expect(
      screen.getByRole('heading', {
        level: 1,
        name: 'Service Name',
      }),
    ).toBeInTheDocument()
  })

  it('should show the organization name as subtitle', () => {
    expect(
      screen.getByRole('heading', {
        level: 2,
        name: 'Organization Name',
      }),
    ).toBeInTheDocument()
  })

  it('should show a link to the external specification file', () => {
    const link = screen.getByRole('link')
    expect(link).toBeInTheDocument()
    expect(link.textContent).toContain('Originele specificatie (YAML)')
    expect(link.textContent).toContain('Externe link')
    expect(link).toHaveAttribute('href', 'https://example.com/spec.yml')
  })

  it('renders a badge when apiType is passed', () => {
    renderWithProviders(
      <APIDetailsHeader {...details} apiType={{ label: 'ACME' }} />,
    )
    expect(screen.getByText('ACME')).toBeInTheDocument()
  })
})

describe('getExternalSpecifictionDescription()', () => {
  it('should return YAML for a .yml file', () => {
    const actual = getExternalSpecificationDescription(
      'https://example.com/spec.yml',
    )
    expect(actual).toBe('Originele specificatie (YAML)')
  })

  it('should return YAML for a .yaml file', () => {
    const actual = getExternalSpecificationDescription(
      'https://example.com/spec.yaml',
    )
    expect(actual).toBe('Originele specificatie (YAML)')
  })

  it('should return JSON for a .json file', () => {
    const actual = getExternalSpecificationDescription(
      'https://example.com/spec.json',
    )
    expect(actual).toBe('Originele specificatie (JSON)')
  })

  it('should not specify the type if it has no extension', () => {
    const actual = getExternalSpecificationDescription(
      'https://example.com/spec',
    )
    expect(actual).toBe('Originele specificatie')
  })

  it('should not specify the type if it has an unrecognized extension', () => {
    const actual = getExternalSpecificationDescription(
      'https://example.com/spec.html',
    )
    expect(actual).toBe('Originele specificatie')
  })
})
