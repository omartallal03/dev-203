// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const getExternalSpecificationDescription = (url: string): string => {
  const text = 'Originele specificatie'

  if (url.substring(url.length - 5).toLowerCase() === '.json') {
    return text + ' (JSON)'
  } else if (
    url.substring(url.length - 4).toLowerCase() === '.yml' ||
    url.substring(url.length - 5).toLowerCase() === '.yaml'
  ) {
    return text + ' (YAML)'
  }

  return text
}
