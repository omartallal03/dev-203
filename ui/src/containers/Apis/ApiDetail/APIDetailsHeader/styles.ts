// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled, { css } from 'styled-components'

import ArrowLeftIcon from '../../../../assets/icons/ArrowLeft'
import ExternalIcon from '../../../../assets/icons/External'

const ButtonStyling = css`
  align-items: center;
  background: transparent;
  border: none;
  color: ${(p) => p.theme.colorTextLink};
  cursor: pointer;
  display: flex;
  font-family: 'Source Sans Pro', sans-serif;
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
  padding: 0;
`

export const BackButton = styled.button`
  ${ButtonStyling}
`

export const StyledArrowIcon = styled(ArrowLeftIcon)`
  margin-right: 3px;
`

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const SpecLink = styled.a`
  align-items: center;
  display: inline-flex;

  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  margin: ${(p) => `${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing05}`};
  text-decoration: none;

  & span {
    line-height: 1rem;
  }
`

export const StyledExternalIcon = styled(ExternalIcon)`
  height: 1rem;
  margin-left: ${(p) => p.theme.tokens.spacing02};
`
