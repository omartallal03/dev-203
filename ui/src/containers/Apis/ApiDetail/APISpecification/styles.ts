// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Container } from '../../../../components/design-system-candidates/Grid'

export const StyledContainer = styled(Container)`
  .api-content {
    a {
      text-decoration: underline !important;
      cursor: pointer !important;
      font-weight: bold !important;
    }

    button > span:first-child {
      font-weight: bold;
      height: 2.25rem;
      display: flex;
      align-items: center;
      justify-content: center;
    }
  }

  .scrollbar-container {
    li > label > span:first-child {
      font-size: 1rem;
      height: 2.25rem;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    li > label > span {
      &:hover {
        text-decoration: underline !important;
      }
    }
  }
`
