// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState } from 'react'
import { RedocStandalone } from 'redoc'
import { useParams } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import { Spinner, ErrorMessage } from '@commonground/design-system'
import APIDetailsHeader from '../APIDetailsHeader'
import { useApiDetails } from '../../../../hooks/useApiDetails'
import { redocOptions } from './constants'
import { StyledContainer } from './styles'
import { getExternalSpecificationUrl, getSpecificationUrl } from './helpers'

const APISpecification: React.FunctionComponent = () => {
  const params = useParams()
  const {
    data: details,
    isLoading,
    isFetching,
    error,
  } = useApiDetails(params.id!)
  const [redocLoading, setRedocLoading] = useState(true)
  const [redocError, setRedocError] = useState<Error | null>(null)

  const renderRedoc = (specificationUrl: string) => {
    if (redocError) {
      return (
        <ErrorMessage>
          Er ging iets fout tijdens het verwerken van de API specificatie.
        </ErrorMessage>
      )
    }

    return (
      <React.Fragment>
        <RedocStandalone
          specUrl={specificationUrl}
          options={redocOptions}
          onLoaded={(error) => {
            if (typeof error !== 'undefined') {
              setRedocError(error)
            }

            setRedocLoading(false)
          }}
        />
        {redocLoading && <Spinner />}
      </React.Fragment>
    )
  }

  const renderContent = () => {
    const specificationUrl = getSpecificationUrl(params)
    const externalSpecUrl = getExternalSpecificationUrl(params, details)

    if (isLoading || isFetching) {
      return <Spinner />
    }

    if (typeof specificationUrl === 'undefined' || error) {
      return (
        <ErrorMessage>
          Er ging iets fout tijdens het ophalen van de API specificatie.
        </ErrorMessage>
      )
    }

    return (
      <>
        <APIDetailsHeader
          previousName="API details"
          serviceName={details?.serviceName}
          organizationName={details?.organization?.name}
          externalSpecificationUrl={externalSpecUrl}
        />
        {renderRedoc(specificationUrl)}
      </>
    )
  }

  return (
    <StyledContainer>
      <Helmet>
        <title>
          Developer Overheid: API specificatie:{' '}
          {`${details?.serviceName}/${details?.organization?.name}`}
        </title>
        <meta
          name="description"
          content={`${details?.serviceName}/${details?.organization?.name}`}
        />
      </Helmet>
      {renderContent()}
    </StyledContainer>
  )
}

export default APISpecification
