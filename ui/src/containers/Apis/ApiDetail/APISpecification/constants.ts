// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import { RedocRawOptions } from 'redoc'
import theme from '../../../../theme'

export const redocOptions: RedocRawOptions = {
  hideLoading: true,
  theme: {
    breakpoints: {
      small: '576px',
      medium: '1750px',
    },
    schema: {
      labelsTextSize: '1rem',
      typeNameColor: 'red',
    },
    typography: {
      fontFamily: 'Source Sans Pro, sans-serif',
      fontSize: '18px',
      optimizeSpeed: false,
      smoothing: 'antialiased',
      headings: {
        fontWeight: 'bold',
        lineHeight: theme.tokens.lineHeightHeading,
        fontFamily: 'Source Sans Pro, sans-serif',
      },
      lineHeight: theme.tokens.lineHeightText,
      code: {
        wrap: true,
        fontSize: '16px',
      },
      links: {
        color: '#0B71A1',
        hover: '#005282',
      },
    },
    fab: {
      color: '#ffff00',
      backgroundColor: '#0000ff',
    },
    sidebar: {
      width: '300px',
      activeTextColor: '#0D4F71',
      textColor: '#000000',
      arrow: {
        size: '24px',
      },
    },
    colors: {
      primary: {
        main: '#0D4F71',
        contrastText: '#000000',
      },
      http: {
        basic: '#575757',
        delete: '#A52C2D',
        get: '#276501',
        head: '#8A3493',
        link: '#05606B',
        options: '#70550F',
        patch: '#8E4116',
        post: '#0050C7',
      },
      responses: {
        error: {
          color: '#863232',
          tabTextColor: '#FF7070',
        },
        success: {
          color: '#215601',
          tabTextColor: '#215601',
        },
        info: {
          color: '#0046AD',
          tabTextColor: '#0046AD',
        },
        redirect: {
          color: '#4D4D4D',
          tabTextColor: '#4D4D4D',
        },
      },
    },
    rightPanel: {
      backgroundColor: '#083044',
      textColor: '#ffffff',
    },
  },
}
