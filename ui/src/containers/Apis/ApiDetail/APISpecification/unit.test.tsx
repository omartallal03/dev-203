// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { waitFor } from '@testing-library/react'
import { Helmet } from 'react-helmet'
import { UseQueryResult } from 'react-query/types/react/types'
import {
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../../../test-helpers'
import { EnvironmentType } from '../../../../domain/models/enums'
import ApiRepository from '../../../../domain/api-repository'
import * as hooks from '../../../../hooks/useApiDetails'
import { Api } from '../../../../domain/models/types'
import APISpecification from './index'

const apiResponseObject: Api = {
  description: 'Description',
  organization: {
    name: 'Organization Name',
    ooid: 1,
  },
  serviceName: 'Service Name',
  apiType: 'rest_json',
  environments: [
    {
      name: EnvironmentType.PRODUCTION,
      apiUrl: 'API URL',
      specificationUrl: 'Specification URL',
      documentationUrl: 'Documentation URL',
    },
  ],
} as unknown as Api

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    id: 'organization-service',
    environment: 'production',
  }),
}))

jest.mock('redoc', () => ({
  ...jest.requireActual('redoc'), // use actual for all non-hook parts
  RedocStandalone: () => <div />,
}))

describe('<APISpecification />', () => {
  it('shows the API specification', async () => {
    jest.spyOn(ApiRepository, 'getById').mockImplementation()
    jest.spyOn(hooks, 'useApiDetails').mockReturnValueOnce({
      error: false,
      isLoading: false,
      isFetching: false,
      data: apiResponseObject,
    } as UseQueryResult<Api>)

    renderWithProviders(<APISpecification />)

    await waitFor(() => {
      expect(screen.getByText('Service Name')).toBeInTheDocument()
    })
  })

  it('shows an error message when loading or processing fails', async () => {
    jest.spyOn(ApiRepository, 'getById').mockImplementation()
    jest.spyOn(hooks, 'useApiDetails').mockReturnValueOnce({
      error: true,
      isLoading: false,
      isFetching: false,
      data: {},
    } as UseQueryResult<Api>)

    renderWithProviders(<APISpecification />)

    await waitFor(() => {
      expect(
        screen.getByText(
          'Er ging iets fout tijdens het ophalen van de API specificatie.',
        ),
      ).toBeInTheDocument()
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', async () => {
      jest.spyOn(ApiRepository, 'getById').mockImplementation()
      jest.spyOn(hooks, 'useApiDetails').mockReturnValueOnce({
        error: false,
        isLoading: false,
        isFetching: false,
        data: apiResponseObject,
      } as UseQueryResult<Api>)

      mountWithProviders(<APISpecification />)

      await waitFor(() => {
        expect(Helmet.peek().title).toEqual(
          expect.arrayContaining([
            'Developer Overheid: API specificatie:',
            ' ',
            'Service Name/Organization Name',
          ]),
        )
      })
    })
  })
})
