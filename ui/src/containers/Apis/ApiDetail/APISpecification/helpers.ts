// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { Params } from 'react-router-dom'
import { Api } from '../../../../domain/models/types'

export const getSpecificationUrl = (params: Params): string | undefined => {
  if (!params.id || !params.environment) {
    return
  }
  return `/api/apis/${params.id}/${params.environment}/specification`
}

export const getExternalSpecificationUrl = (
  params: Params,
  details: Api | undefined,
): string | undefined => {
  if (!params.environment) {
    return
  }

  const environmentData = details?.environments?.find(
    (env) => env.name.value === params.environment,
  )
  if (!environmentData) {
    return
  }

  return environmentData.specificationUrl
}
