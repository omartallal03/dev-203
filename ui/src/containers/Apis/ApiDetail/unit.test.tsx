// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { waitFor } from '@testing-library/react'
import { Helmet } from 'react-helmet'
import {
  screen,
  renderWithProviders,
  mountWithProviders,
} from '../../../test-helpers'
import { Api } from '../../../domain/models/types'
import APIDetail from '.'

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    id: '42',
  }),
}))

const props: Api = {
  badges: undefined,
  organization: {
    name: 'Organization Name',
    ooid: 1,
  },
  apiAuthentication: {},
  forum: {
    url: '',
    vendor: '',
  },
  relations: {},
  designRuleScores: [],
  environments: [],
  isReferenceImplementation: false,
  relatedCode: [],
  scores: {
    hasContactDetails: false,
    hasDocumentation: false,
    hasSpecification: false,
    providesSla: false,
  },
  termsOfUse: {
    governmentOnly: false,
    payPerUse: false,
    supportResponseTime: 0,
    uptimeGuarantee: 0,
  },
  totalScore: { maxPoints: 0, points: 0 },
  id: 'organization-service',
  description: 'Description',
  serviceName: 'Service Name',
  apiType: 'rest_json',
} as unknown as Api

describe('APIDetail', () => {
  it('fetches API details', async () => {
    const apiPromise = Promise.resolve(props)
    const getApiDetailsByIdMock = jest.fn(() => apiPromise)

    renderWithProviders(<APIDetail getApiDetailsById={getApiDetailsByIdMock} />)

    await waitFor(() => {
      expect(getApiDetailsByIdMock).toHaveBeenCalledWith('42')
    })
  })

  it('shows the details', async () => {
    const getApiDetailsByIdMock = jest.fn(() => Promise.resolve(props))

    renderWithProviders(<APIDetail getApiDetailsById={getApiDetailsByIdMock} />)

    await waitFor(() => {
      expect(screen.getByText('Organization Name')).toBeInTheDocument()
    })
  })

  it('shows an error when thrown', async () => {
    const apiPromise = Promise.reject(new Error('error'))
    const getApiDetailsByIdMock = jest.fn(() => apiPromise)

    renderWithProviders(<APIDetail getApiDetailsById={getApiDetailsByIdMock} />)

    await waitFor(() => {
      expect(
        screen.getByText('Er ging iets fout tijdens het ophalen van de API.'),
      ).toBeInTheDocument()
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<APIDetail />)
      expect(Helmet.peek().title).toEqual([
        'Developer Overheid: API ',
        'undefined',
      ])
    })
  })
})
