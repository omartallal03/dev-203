// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import APIDetailsRepository from '../../../domain/api-details-repository'
import { Api } from '../../../domain/models/types'
import APIDetails from './APIDetails'

type Props = React.ComponentProps<'div'> & {
  getApiDetailsById?: (id) => Api
}

const APIDetail: React.FunctionComponent<Props> = ({
  getApiDetailsById = APIDetailsRepository.getById,
  ...props
}) => {
  const params = useParams()

  const [details, setDetails] = useState<Api>()
  const [error, setError] = useState(false)
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    ;(async () => {
      await loadDetailsForApi(params.id)
    })()
  }, [params.id]) // eslint-disable-line react-hooks/exhaustive-deps

  const loadDetailsForApi = async (id) => {
    if (!getApiDetailsById) {
      return
    }

    try {
      setDetails(await getApiDetailsById(id))
    } catch (error) {
      setError(true)
    } finally {
      setLoaded(true)
    }
  }

  return (
    <div className="APIDetail" {...props}>
      <Helmet>
        <title>Developer Overheid: API {`${details?.serviceName}`}</title>
        <meta name="description" content={details?.description} />
      </Helmet>
      {!loaded ? null : error ? (
        <p data-test="error-message">
          Er ging iets fout tijdens het ophalen van de API.
        </p>
      ) : details ? (
        <APIDetails {...details} />
      ) : null}
    </div>
  )
}

export default APIDetail
