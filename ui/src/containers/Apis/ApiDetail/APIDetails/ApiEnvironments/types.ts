// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export interface ApiEnvironment {
  specificationUrl: string
  name: {
    label: string
    value: string
  }
  apiUrl: string
  documentationUrl: string
}
