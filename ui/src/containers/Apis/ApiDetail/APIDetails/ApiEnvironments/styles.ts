// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../../../../theme/mediaQueries'
import CodeBlock from '../../../AddApi/SubmitAPIMergeRequest/CodeBlock'
import External from '../../../../../assets/icons/External'

export const Wrapper = styled.section`
  padding-top: ${(p) => p.theme.tokens.spacing05};
`

export const Environment = styled.div``

export const EnvData = styled.div`
  align-items: baseline;
  display: flex;
  flex-wrap: wrap;

  ${mq.mdUp`
    justify-content: space-between;
  `}
`

export const Buttons = styled.div`
  display: flex;
  flex-wrap: wrap;

  & > :first-child {
    margin-right: ${(p) => p.theme.tokens.spacing05};
  }
`

export const EnvUri = styled(CodeBlock)`
  flex: 1 0 100%;
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
  margin-top: 0;

  ${mq.mdUp`
    flex: 1 0 0;
    margin-right: ${(p) => p.theme.tokens.spacing05};
  `}
`

export const ExternalIcon = styled(External)`
  margin-left: ${(p) => p.theme.tokens.spacing03};
`
