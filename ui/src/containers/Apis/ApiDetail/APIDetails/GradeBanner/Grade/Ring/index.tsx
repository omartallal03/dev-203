// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { getCircleProps, scaleMeasurements } from '../helpers'
import SVGCircle from '../SVGCircle'

interface Props extends React.ComponentProps<'svg'> {
  size: number
  color: string
  percentage: number
}

const Ring: React.FunctionComponent<Props> = ({
  percentage,
  color,
  size,
  strokeWidth,
  ...props
}) => {
  const measurements = scaleMeasurements({
    size,
    strokeWidth,
  })

  return (
    <svg viewBox="0 0 1 1" {...props}>
      <SVGCircle
        circleProps={getCircleProps(percentage, measurements)}
        color={color}
      />
    </svg>
  )
}

export default Ring
