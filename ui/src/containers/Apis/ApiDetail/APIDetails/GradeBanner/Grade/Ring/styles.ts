// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import Ring from '.'

export const StyledRing = styled(Ring)`
  height: 100%;
  position: relative;
  width: 100%;
  z-index: 1;
`
