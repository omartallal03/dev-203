// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export interface ScaleMeasurements {
  size: number
}
