// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StyledCircle, StyledScoreSubscript } from './styles'
import {
  calculatePercentage,
  percentageToBackgroundColor,
  percentageToRingColor,
  percentageToTextColor,
} from './helpers'
import { StyledPercentageRing } from './PercentageRing/styles'

interface Props {
  totalScore: {
    points: number
    maxPoints: number
  }
  size?: number
  strokeWidth?: number
  $largeAtMediaQuery?: string
  $largeSize?: number
  withText?: boolean
  staticBackgroundColor?: string
}

const Grade: React.FunctionComponent<Props> = ({
  totalScore,
  size = 35,
  strokeWidth = 4,
  $largeSize = 80,
  $largeAtMediaQuery,
  staticBackgroundColor,
  withText = true,
  ...props
}) => {
  const { points, maxPoints } = totalScore
  const percentage = calculatePercentage(points, maxPoints)
  const backgroundColor =
    staticBackgroundColor || percentageToBackgroundColor(percentage)

  // Half of the stroke is drawn outside the radius, make sure the total size is correct
  const radius = 0.5 * size - 0.5 * strokeWidth
  // Draw the circle so that it overlaps the inner half of the ring
  const circleOffset = 0.5 * strokeWidth

  const scaleFactor = $largeSize / size
  const largeRadius = scaleFactor * radius
  const largeCircleOffset = scaleFactor * circleOffset

  return (
    <StyledPercentageRing
      percentage={percentage}
      size={size}
      strokeWidth={strokeWidth}
      color={percentageToRingColor(percentage)}
      $largeAtMediaQuery={$largeAtMediaQuery}
      $largeSize={$largeSize}
      {...props}
    >
      <StyledCircle
        radius={radius}
        offset={circleOffset}
        largeAtMediaQuery={$largeAtMediaQuery}
        largeRadius={largeRadius}
        largeOffset={largeCircleOffset}
        backgroundColor={backgroundColor}
        textColor={percentageToTextColor(percentage)}
      >
        {withText && (
          <>
            {points}
            <StyledScoreSubscript largeAtMediaQuery={$largeAtMediaQuery}>
              /{maxPoints}
            </StyledScoreSubscript>
          </>
        )}
      </StyledCircle>
    </StyledPercentageRing>
  )
}

export default Grade
