// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../../../../../theme/mediaQueries'

interface Props {
  radius: number
  offset: number
  backgroundColor: string
  textColor: string
  largeAtMediaQuery?: string
  largeRadius: number
  largeOffset: number
}

export const StyledCircle = styled.div<Props>`
  align-items: center;
  background-color: ${(p) => p.backgroundColor};
  border-radius: 50%;
  color: ${(p) => p.textColor};
  display: flex;
  font-size: 13px;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  height: ${(p) => p.radius * 2}px;
  justify-content: center;
  left: ${(p) => p.offset}px;

  position: absolute;
  top: ${(p) => p.offset}px;
  width: ${(p) => p.radius * 2}px;

  ${(p) =>
    p.largeAtMediaQuery
      ? mq[p.largeAtMediaQuery]`
        width: ${(p) => p.largeRadius * 2}px;
        height: ${(p) => p.largeRadius * 2}px;
        top: ${(p) => p.largeOffset}px;
        left: ${(p) => p.largeOffset}px;
        font-size: 26px;
      `
      : ''}
`

export const StyledScoreSubscript = styled.span<{
  largeAtMediaQuery?: string
}>`
  font-size: 9px;
  padding-top: 4px;

  ${(p) =>
    p.largeAtMediaQuery
      ? mq[p.largeAtMediaQuery]`
      font-size: 18px;
      `
      : ''}
`
