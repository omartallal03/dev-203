// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import mq from '../../../../../../../theme/mediaQueries'
import PercentageRing from '.'

export const StyledPercentageRing = styled(PercentageRing)`
  height: ${(p) => p.size}px;
  position: relative;
  width: ${(p) => p.size}px;

  ${(p) =>
    p.$largeAtMediaQuery
      ? mq[p.$largeAtMediaQuery]`
        width: ${(p) => p.$largeSize}px;
        height: ${(p) => p.$largeSize}px;
      `
      : ''}
`
