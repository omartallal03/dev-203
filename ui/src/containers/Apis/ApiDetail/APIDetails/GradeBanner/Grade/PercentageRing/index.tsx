// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { node, string } from 'prop-types'
import { StyledRing } from '../Ring/styles'

const PercentageRing: React.FunctionComponent<
  React.ComponentProps<typeof StyledRing>
> = ({ className, children, ...props }) => {
  return (
    <div className={className}>
      <StyledRing {...props} />
      {children}
    </div>
  )
}

PercentageRing.propTypes = {
  className: string,
  children: node,
}

export default PercentageRing
