// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

interface Props extends React.ComponentProps<'circle'> {
  circleProps: {
    strokeWidth: number
    radius: number
    circumference: number
    dashOffset: number
  }
}

const SVGCircle: React.FunctionComponent<Props> = ({
  circleProps,
  color,
  className,
  ...props
}) => (
  <circle
    cx={0.5}
    cy={0.5}
    r={circleProps.radius}
    stroke={color}
    strokeWidth={circleProps.strokeWidth}
    strokeDasharray={`${circleProps.circumference} ${circleProps.circumference}`}
    strokeDashoffset={circleProps.dashOffset || 0}
    fillOpacity={0}
    transform="rotate(-90 0.5 0.5)"
    className={className}
    {...props}
  />
)

export default SVGCircle
