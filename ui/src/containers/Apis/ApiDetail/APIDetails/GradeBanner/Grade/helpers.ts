// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import theme from '../../../../../../theme'
import { ScaleMeasurements } from './types'

export const scaleMeasurements = ({
  size,
  ...otherMeasurements
}: Record<string, number | string | undefined>): ScaleMeasurements => {
  const scaledMeasurements: ScaleMeasurements = {
    size: 1,
  }

  for (const k of Object.keys(otherMeasurements)) {
    if (typeof otherMeasurements[k.toString()] === 'number') {
      const other = otherMeasurements[k.toString()]

      if (typeof other === 'number' && typeof size === 'number') {
        scaledMeasurements[k.toString()] = other / size
      }
    }
  }

  return scaledMeasurements
}

export const getCircleProps = (
  percentage: number,
  measurements: { size: number; strokeWidth?: number },
): {
  strokeWidth: number
  radius: number
  circumference: number
  dashOffset: number
} => {
  const { size, strokeWidth = 1 } = measurements

  // Half of the stroke is drawn outside the radius, make sure the total size is correct
  const radius = 0.5 * size - 0.5 * strokeWidth
  const circumference = 2 * Math.PI * radius
  const dashOffset = (1 - percentage) * circumference

  return {
    strokeWidth,
    radius,
    circumference,
    dashOffset,
  }
}

export const calculatePercentage = (
  points: number,
  maxPoints: number,
): number => {
  return points / maxPoints
}

export const percentageToRingColor = (percentage: number): string =>
  percentage < 0.3
    ? '#F02B41'
    : percentage < 0.7
    ? theme.tokens.colorPaletteGray500
    : '#63D19E'

export const percentageToBackgroundColor = (percentage: number): string =>
  percentage === 0
    ? '#FEEBED'
    : percentage < 1
    ? theme.tokens.colorPaletteGray100
    : '#CBFFE7'

export const percentageToTextColor = (percentage: number): string =>
  percentage === 0 ? '#F02B41' : theme.colorText
