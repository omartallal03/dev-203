// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import Grade from './Grade'
import {
  percentageToBackgroundColor,
  percentageToTextColor,
} from './Grade/helpers'

interface ContainerProps {
  percentage: number
}

export const Container = styled.div<ContainerProps>`
  align-items: center;
  background-color: ${(p) => percentageToBackgroundColor(p.percentage)};
  border-radius: 13px;
  color: ${(p) => percentageToTextColor(p.percentage)};
  display: flex;
  height: 138px;
  justify-content: center;
  margin: ${(p) => p.theme.tokens.spacing07} 0px;
  width: 100%;
`

export const StyledGrade = styled(Grade)`
  margin-right: ${(p) => p.theme.tokens.spacing05};
`

export const PointsText = styled.span`
  font-size: 38px;
`

export const MaxPointsText = styled.span`
  font-size: 22px;
  padding-top: 12px;
`
