// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { calculatePercentage } from './Grade/helpers'
import { Container, PointsText, MaxPointsText, StyledGrade } from './styles'

interface Props {
  totalScore: {
    points: number
    maxPoints: number
  }
}

const GradeBanner: React.FunctionComponent<Props> = ({ totalScore }) => {
  const { points, maxPoints } = totalScore
  const percentage = calculatePercentage(points, maxPoints)

  return (
    <Container percentage={percentage}>
      {points === 0 ? null : (
        <StyledGrade
          totalScore={totalScore}
          size={48}
          strokeWidth={8}
          staticBackgroundColor="#FFFFFF"
          withText={false}
        />
      )}

      <PointsText>{points}</PointsText>
      <MaxPointsText>/{maxPoints}</MaxPointsText>
    </Container>
  )
}

export default GradeBanner
