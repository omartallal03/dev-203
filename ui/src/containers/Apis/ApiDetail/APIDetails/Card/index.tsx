// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StyledCard, Body, Footer, Title, Header } from './styles'

const Card: React.FunctionComponent & Record<string, React.FunctionComponent> =
  StyledCard
Card.Body = Body
Card.Footer = Footer
Card.Title = Title
Card.Header = Header

export default Card
