// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const supportResponseTimeString = (
  supportResponseTime?: number,
): string => {
  if (typeof supportResponseTime !== 'number') {
    return 'Geen'
  }
  if (supportResponseTime <= 1) {
    return `${supportResponseTime} werkdag`
  }
  return `${supportResponseTime} werkdagen`
}
