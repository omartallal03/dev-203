// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../../../../theme/mediaQueries'
import { DONSmall } from '../../../../../components/CustomDON'

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  ${mq.smUp`
    display: flex;
    flex-direction: row;
  `}
`

export const Term = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;

  ${mq.smUp`
    display: flex;
    flex-direction: column;
    justify-content: start;
  `}
`

export const Key = styled(DONSmall)`
  margin-bottom: 4px;
  width: 50%;

  ${mq.smUp`
    width: 100%;
  `}
`

export const Value = styled.div`
  margin-bottom: 4px;
  width: 50%;

  ${mq.smUp`
    width: 100%;
  `}
`
