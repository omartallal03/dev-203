// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { TermsOfUse } from '../../../../../domain/models/types'
import { Wrapper, Term, Key, Value } from './styles'
import { supportResponseTimeString } from './helpers'

interface Props {
  apiAuthentication: {
    label: string
  }
  termsOfUse: TermsOfUse
}

const APITerms: React.FunctionComponent<Props> = ({
  apiAuthentication,
  termsOfUse,
}) => (
  <Wrapper>
    <Term>
      <Key>API Authenticatie</Key>
      <Value data-test="api-authentication">{apiAuthentication.label}</Value>
    </Term>

    <Term>
      <Key>Gebruik</Key>
      <Value>
        {termsOfUse.governmentOnly ? 'Alleen overheid' : 'Iedereen'}
      </Value>
    </Term>

    <Term>
      <Key>Kosten</Key>
      <Value>{termsOfUse.payPerUse ? 'Kosten voor gebruik' : 'Gratis'}</Value>
    </Term>

    <Term>
      <Key>Uptime garantie</Key>
      <Value>
        {termsOfUse.uptimeGuarantee ? `${termsOfUse.uptimeGuarantee}%` : 'Geen'}
      </Value>
    </Term>

    <Term>
      <Key>Helpdesk respons</Key>
      <Value>{supportResponseTimeString(termsOfUse.supportResponseTime)}</Value>
    </Term>
  </Wrapper>
)

export default APITerms
