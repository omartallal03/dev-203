// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export interface Post {
  id: string
  lastPostedAt: string
  lastPosterUsername: string
  postsCount: number
  slug: string
  title: string
}

export interface Forum {
  url: string
  vendor: string
}
