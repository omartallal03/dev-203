// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router'

// Components
import ErrorMessage from '../../../../../components/ErrorMessage'
import { H1 } from '../../../../../components/Headings'
import { fetchForumPosts } from './helpers'
import ForumPostsView from './ForumPostsView'

// Styles
import { ForumPostsContainer } from './styles'

// Types
import { Forum, Post } from './types'

interface Props {
  forum: Forum
}

const ForumPosts: React.FunctionComponent<Props> = ({ forum }) => {
  const { vendor, url } = forum
  const params = useParams()
  const [error, setError] = useState<Error | null>(null)
  const [posts, setPosts] = useState<Post[]>([])

  useEffect(() => {
    ;(async () => {
      const result = await fetchForumPosts(vendor, params.id || 'unknown')
      if (result) {
        if (!result.error) {
          setError(null)
          if (Array.isArray(result.json)) {
            setPosts(result.json)
          }
        } else {
          setError(result.error)
        }
      }
    })()
  }, [vendor, url, params.id, params])

  return (
    <ForumPostsContainer>
      <H1>Forum</H1>
      {error && (
        <ErrorMessage level="warning">
          Fout bij het ophalen van forum content
        </ErrorMessage>
      )}
      {!error && posts && posts.length > 0 && (
        <ForumPostsView url={url} posts={posts} />
      )}
    </ForumPostsContainer>
  )
}

export default ForumPosts
