// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import objectKeysToCamelCase from '../../../../../utils/objectKeysToCamelCase'
import { Post } from './types'

interface Result {
  error: null | Error
  json: null | Post[]
}

export const fetchForumPosts = async (
  vendor: string,
  apiId: string,
): Promise<Result> => {
  if (vendor !== 'discourse') {
    console.error('Forum vendor is not discourse.')
    return {
      error: new Error('Forum vendor is not discourse.'),
      json: null,
    }
  }

  try {
    const response = await fetch(`/api/apis/${apiId}/forum-posts`)
    const json = await response.json()
    return {
      error: null,
      json: objectKeysToCamelCase<Post[]>(json.topic_list.topics.slice(0, 5)),
    }
  } catch (error) {
    return {
      json: null,
      error: error as Error,
    }
  }
}
