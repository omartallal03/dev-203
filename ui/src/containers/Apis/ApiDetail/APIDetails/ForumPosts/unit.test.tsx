// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { waitFor } from '@testing-library/react'
import { renderWithRouter } from '../../../../../test-helpers'
import * as forumFunctions from './helpers'
import { Post } from './types'
import ForumPosts from './index'

jest.mock('react-router-dom', () => {
  return {
    useParams: () => ({
      id: '123',
    }),
  }
})

describe('ForumPosts', () => {
  it('should fetch forum posts and pass them to child', async () => {
    const json: Post[] = [
      {
        id: '1',
        title: 'my test post',
        url: 'https://link.post',
        lastPostedAt: 'this morning',
        lastPosterUsername: 'John Doe',
        postsCount: 1,
        slug: '/slug',
      },
    ]
    const forum = {
      vendor: 'discourse',
      url: 'http://link.api',
    }

    jest.spyOn(forumFunctions, 'fetchForumPosts').mockResolvedValueOnce({
      error: null,
      json,
    })

    renderWithRouter(<ForumPosts forum={forum} />)

    await waitFor(() => {
      expect(forumFunctions.fetchForumPosts).toHaveBeenCalledWith(
        'discourse',
        'unknown',
      )
    })
  })
})
