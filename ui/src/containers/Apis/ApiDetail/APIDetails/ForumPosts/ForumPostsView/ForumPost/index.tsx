// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'

import { showDate } from '../../../../../../../helpers'
import {
  Post,
  PostInfo,
  StyledExternalIcon,
  PostTitle,
  PostBody,
  StyledMessageIcon,
  PostsCount,
  LatestMessage,
  PostImage,
} from '../../styles'

interface Props {
  url: string
  title: string
  lastPosterUsername: string
  lastPostedAt: string
  postsCount: number
  slug: string
}

const ForumPost: React.FunctionComponent<Props> = ({
  url,
  title,
  lastPostedAt,
  postsCount,
  slug,
  lastPosterUsername,
}) => {
  const baseUrl = url.substring(0, url.indexOf('/c/'))
  return (
    <Post
      href={`${baseUrl}/t/${slug}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <PostImage title={lastPosterUsername}>
        {lastPosterUsername?.substring(0, 1).toUpperCase()}
      </PostImage>

      <PostBody>
        <PostTitle>{title}</PostTitle>
        <PostInfo>
          <StyledMessageIcon />
          <PostsCount>{postsCount}</PostsCount>
          <LatestMessage>
            Laatste bericht: {showDate(lastPostedAt)}
          </LatestMessage>
        </PostInfo>
      </PostBody>
      <StyledExternalIcon />
    </Post>
  )
}

export default ForumPost
