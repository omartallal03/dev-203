// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Button } from '@commonground/design-system'
import {
  PostList,
  ForumLinkSection,
  PostListItem,
  StyledChevronRight,
} from '../styles'
import { Post } from '../types'
import ForumPost from './ForumPost'

interface Props {
  url: string
  posts: Post[]
}

const ForumPostsView: React.FunctionComponent<Props> = ({ url, posts }) => (
  <>
    <PostList data-testid="postList">
      {posts.map((post) => (
        <PostListItem key={post.id}>
          <ForumPost {...post} url={url} />
        </PostListItem>
      ))}
    </PostList>

    <ForumLinkSection>
      <Button
        variant="link"
        as="a"
        href={url}
        target="_blank"
        rel="noopener noreferrer"
      >
        Ga naar volledige forum <StyledChevronRight />
      </Button>
    </ForumLinkSection>
  </>
)

export default ForumPostsView
