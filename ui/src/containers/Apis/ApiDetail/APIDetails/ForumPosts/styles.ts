// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import { ReactComponent as ChevronRight } from '../../../../../assets/svg/chevron-right.svg'
import { ReactComponent as MessageIcon } from '../../../../../assets/svg/message-icon.svg'
import ExternalIcon from '../../../../../assets/icons/External'

export const ForumPostsContainer = styled.div``

export const PostList = styled.ul`
  list-style: none;
  padding: 0;
`

export const PostListItem = styled.li`
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
`

export const Post = styled.a`
  align-items: center;
  color: ${(p) => p.theme.colorText};
  display: flex;
  padding: ${(p) => p.theme.tokens.spacing06} ${(p) => p.theme.tokens.spacing03};
  text-decoration: none;

  &:hover {
    background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  }

  &:hover,
  &:active {
    color: ${(p) => p.theme.colorText};
  }
`

export const PostBody = styled.div`
  flex-grow: 1;
  /* Trick to allow "text-overflow: ellipsis" in its children,
  see: https://css-tricks.com/flexbox-truncated-text */
  min-width: 0;
`

export const PostTitle = styled.div`
  color: ${(p) => p.theme.colorTextLink};
  font-weight: 600;
`

export const PostInfo = styled.small`
  align-items: center;
  display: flex;
`

export const PostImage = styled.div`
  align-items: center;
  background: #e0e0e0;
  border-radius: 50%;
  color: #616161;
  display: flex;
  justify-content: center;
  margin-right: ${(p) => p.theme.tokens.spacing05};
  min-height: ${(p) => p.theme.tokens.spacing07};
  min-width: ${(p) => p.theme.tokens.spacing07};
  text-align: center;
`

export const StyledMessageIcon = styled(MessageIcon)`
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
  flex-shrink: 0;
  height: ${(p) => p.theme.tokens.spacing05};
  margin-right: ${(p) => p.theme.tokens.spacing03};
  width: ${(p) => p.theme.tokens.spacing05};
`

export const PostsCount = styled.small`
  flex-shrink: 0;
  margin-right: ${(p) => p.theme.tokens.spacing05};
`

export const LatestMessage = styled.small`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

export const StyledExternalIcon = styled(ExternalIcon)`
  flex-shrink: 0;
  margin-left: ${(p) => p.theme.tokens.spacing05};
`

export const ForumLinkSection = styled.section`
  display: flex;
  justify-content: flex-end;
`

export const StyledChevronRight = styled(ChevronRight)`
  fill: ${(p) => p.theme.colorTextLink};
  margin-left: ${(p) => p.theme.tokens.spacing01};
`
