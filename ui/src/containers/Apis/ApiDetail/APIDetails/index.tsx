// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Route, Routes } from 'react-router-dom'

import { Row, Col } from '../../../../components/design-system-candidates/Grid'
import APIDetailsHeader from '../APIDetailsHeader'
import { Api } from '../../../../domain/models/types'
import ForumPosts from './ForumPosts'
import CodeExamples from './CodeExamples'
import Card from './Card'
import ImplementedByListContainer from './ImplementedByListContainer'
import APIDesignRulesPane from './APIDesignRulesPane'
import APIEnvironments from './ApiEnvironments'
import APITerms from './ApiTerms'
import GradeBox from './GradeBox'
import APIScoresPane from './ApiScoresPane'

import {
  Description,
  IntegrationContainer,
  StyledCardBody,
  StyledContainer,
} from './styles'
import { referenceImplementationsFromRelations } from './helpers'
import LinkToAPIContainer from './LinkToAPIContainer'

type Props = Api

const APIDetails: React.FunctionComponent<Props> = ({
  id,
  serviceName,
  organization,
  description,
  apiType,
  apiAuthentication,
  environments,
  forum,
  isReferenceImplementation,
  relatedCode,
  relations,
  termsOfUse,
  scores,
  designRuleScores,
  totalScore,
}) => {
  const showDesignRuleScores = !!designRuleScores
  const hasForum = !!forum?.url
  const hasCode = !!relatedCode?.length

  const renderScorePane = () => {
    if (showDesignRuleScores) {
      return (
        <APIDesignRulesPane
          parentUrl={`/apis/${id}`}
          designRuleScores={designRuleScores}
          totalScore={totalScore}
        />
      )
    }

    return (
      <APIScoresPane
        parentUrl={`/apis/${id}`}
        scores={scores}
        totalScore={totalScore}
      />
    )
  }

  return (
    <StyledContainer>
      <APIDetailsHeader
        previousName="API overzicht"
        serviceName={serviceName}
        organizationName={organization.name}
        apiType={apiType}
      />

      <Row>
        <Col width="75%">
          <Description>{description}</Description>
        </Col>
        <Col width="25%">
          <GradeBox
            apiId={id}
            totalScore={totalScore}
            isDesignRulesScore={showDesignRuleScores}
          />
        </Col>
      </Row>

      <Row>
        <Col width="75%">
          <APITerms
            apiAuthentication={apiAuthentication}
            termsOfUse={termsOfUse}
          />
        </Col>
      </Row>

      {environments && environments.length > 0 ? (
        <Card data-testid="environments">
          <Card.Body>
            <APIEnvironments environments={environments} apiId={id} />
          </Card.Body>
        </Card>
      ) : null}

      <IntegrationContainer twoColumn={hasForum && hasCode}>
        {hasForum && (
          <Card>
            <Card.Body>
              <ForumPosts forum={forum} />
            </Card.Body>
          </Card>
        )}

        {hasCode && (
          <Card>
            <Card.Body>
              <CodeExamples relatedCode={relatedCode} />
            </Card.Body>
          </Card>
        )}
      </IntegrationContainer>

      {isReferenceImplementation ? (
        <Card data-testid="is-reference">
          <StyledCardBody>
            <Card.Title>Geïmplementeerd door</Card.Title>
            <ImplementedByListContainer id={id} />
          </StyledCardBody>
        </Card>
      ) : null}

      {!isReferenceImplementation &&
      referenceImplementationsFromRelations(relations).length > 0 ? (
        <Card data-testid="uses-reference">
          <Card.Body>
            <Card.Title>Referentie implementatie</Card.Title>
            {referenceImplementationsFromRelations(relations).map((apiId) => (
              <LinkToAPIContainer id={apiId} key={apiId} />
            ))}
          </Card.Body>
        </Card>
      ) : null}

      <Routes>
        <Route path="/" element={null} />
        <Route
          path="/score-detail"
          element={<React.Fragment>{renderScorePane()}</React.Fragment>}
        />
      </Routes>
    </StyledContainer>
  )
}

export default APIDetails
