// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import {
  APIType,
  APIAuthentication,
  EnvironmentType,
} from '../../../../domain/models/enums'
import { renderWithRouter, screen } from '../../../../test-helpers'
import { designRuleScoresMock } from '../../../../domain/models/api.mock'
import { referenceImplementationsFromRelations } from './helpers'
import APIDetails from './index'

const details = {
  id: 'organization-service',
  description: 'Description',
  organization: {
    name: 'Organization Name',
    ooid: 1,
  },
  serviceName: 'Service Name',
  apiType: APIType.SOAP_XML,
  apiAuthentication: APIAuthentication.MUTUAL_TLS,
  badges: ['Golden API', 'Well-written docs'],
  environments: [
    {
      name: EnvironmentType.PRODUCTION,
      apiUrl: 'API URL',
      specificationUrl: 'Specification URL',
      documentationUrl: 'Documentation URL',
    },
  ],
  termsOfUse: {
    governmentOnly: true,
    payPerUse: false,
    uptimeGuarantee: 99.9,
    supportResponseTime: 2,
  },
  scores: {
    hasDocumentation: true,
    hasSpecification: false,
    hasContactDetails: false,
    providesSla: false,
  },
  designRuleScores: designRuleScoresMock,
  totalScore: {
    points: 1,
    maxPoints: 4,
  },
}

describe('referenceImplementationsFromRelations', () => {
  it('should return the API ids for its reference implementations', () => {
    const result = referenceImplementationsFromRelations({
      'an-api': ['reference-implementation'],
      'another-api': [],
    })

    expect(result).toEqual(['an-api'])
  })
})

describe('APIDetails', () => {
  it('should render expected info', () => {
    renderWithRouter(<APIDetails {...details} />)
    expect(screen.getByText('Organization Name')).toBeInTheDocument()
    expect(screen.getByText('Description')).toBeInTheDocument()
    expect(screen.getByText('SOAP/XML')).toBeInTheDocument()
    expect(screen.getByText('Mutual TLS')).toBeInTheDocument()
  })

  describe('environments information', () => {
    it('should render', () => {
      renderWithRouter(<APIDetails {...details} />)
      expect(screen.getByTestId('environments')).toBeInTheDocument()
    })
  })

  describe('reference implementation', () => {
    it('renders reference info if it is a reference implementation', () => {
      const referenceDetails = {
        ...details,
        isReferenceImplementation: true,
      }

      renderWithRouter(<APIDetails {...referenceDetails} />)
      expect(screen.getByTestId('is-reference')).toBeInTheDocument()
    })

    it("does not render if it's not", () => {
      renderWithRouter(<APIDetails {...details} />)
      expect(screen.queryByTestId('is-reference')).not.toBeInTheDocument()
    })
  })
})

describe('APIDetails scores', () => {
  it('renders the Design Rule score if it is provided', () => {
    renderWithRouter(<APIDetails {...details} />)
    expect(screen.getByText('Design Rule score')).toBeInTheDocument()
    expect(screen.queryByText('API score')).toBeNull()
  })

  it('renders the API score otherwise', () => {
    const testDetails = {
      ...details,
      designRuleScores: null,
    }

    renderWithRouter(<APIDetails {...testDetails} />)

    expect(screen.getByText('API score')).toBeInTheDocument()
    expect(screen.queryByText('Design Rule score')).toBeNull()
  })

  describe('/score-detail route', () => {
    it('renders the API Design Rule Pane if the Design Rule scores are provided', () => {
      renderWithRouter(<APIDetails {...details} />)
      expect(screen.getByText('Design Rule score')).toBeInTheDocument()
      expect(screen.queryByText('API score')).toBeNull()
    })

    it('renders the API Scores Pane otherwise', () => {
      const testDetails = {
        ...details,
        designRuleScores: null,
      }

      renderWithRouter(<APIDetails {...testDetails} />)

      expect(screen.getByText('API score')).toBeInTheDocument()
      expect(screen.queryByText('Design Rule scor')).toBeNull()
    })
  })
})
