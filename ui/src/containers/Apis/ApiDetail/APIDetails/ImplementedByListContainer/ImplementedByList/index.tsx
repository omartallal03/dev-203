// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import APISummary from '../../../../ApiSummary'
import { StyledList, StyledListItem } from '../../../../ApiList/styles'
import { Api } from '../../../../../../domain/models/types'

interface Props {
  apis: Api[]
}

const ImplementedByList: React.FunctionComponent<Props> = ({ apis }) => (
  <div className="ImplementedByList">
    <StyledList>
      {apis.map((api, i) => (
        <StyledListItem key={i}>
          <APISummary {...api} />
        </StyledListItem>
      ))}
    </StyledList>
  </div>
)

export default ImplementedByList
