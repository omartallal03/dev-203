// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import APIRepository from '../../../../../domain/api-repository'
import { modelFromAPIResponse } from '../../../../../domain/models/api'
import { Api, IncomingApi } from '../../../../../domain/models/types'
import ImplementedByList from './ImplementedByList'

interface Props {
  id: string
}

interface State {
  apis: Api[]
  error: boolean
  loaded: boolean
}

class ImplementedByListContainer extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      apis: [],
      error: false,
      loaded: false,
    }
  }

  componentDidMount(): void {
    const { id } = this.props
    this.loadDetailsForAPI(id)
  }

  componentDidUpdate(nextProps: Props): void {
    const { id } = nextProps
    const { id: prevId } = this.props

    if (prevId === id || typeof prevId === 'undefined') {
      return
    }

    this.loadDetailsForAPI(id)
  }

  async fetchImplementedByInfo(id: string): Promise<IncomingApi[]> {
    return APIRepository.fetchImplementedByInfo(id)
  }

  async loadDetailsForAPI(id: string): Promise<void> {
    try {
      const response = await this.fetchImplementedByInfo(id)
      const apis = response.map((api) => modelFromAPIResponse(api))
      this.setState({ apis: apis, loaded: true })
    } catch (error) {
      this.setState({ error: true, loaded: true })
    }
  }

  render(): JSX.Element {
    const { apis, error, loaded } = this.state

    return (
      <div className="ImplementedByListContainer">
        {!loaded ? null : error ? (
          <p data-test="error-message">
            Er ging iets fout tijdens het ophalen van de gerelateerde API&#39;s.
          </p>
        ) : apis && apis.length ? (
          <ImplementedByList apis={apis} />
        ) : (
          <p data-test="no-consumers-message">
            Er zijn momenteel geen API’s die verwijzen naar deze
            referentie-implementatie.
          </p>
        )}
      </div>
    )
  }
}

export default ImplementedByListContainer
