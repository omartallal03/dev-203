// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React, { Component } from 'react'
import APISummary from '../../../ApiSummary'
import APIRepository from '../../../../../domain/api-repository'
import { modelFromAPIResponse } from '../../../../../domain/models/api'
import { Api, IncomingApi } from '../../../../../domain/models/types'

interface Props {
  id: string
}

interface State {
  details: Api
  error: boolean
  loaded: boolean
}

class LinkToAPIContainer extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      details: {} as Api,
      error: false,
      loaded: false,
    }
  }

  componentDidMount(): void {
    const { id } = this.props
    this.loadDetailsForApi(id)
  }

  componentDidUpdate(nextProps: Props): Promise<void> | undefined {
    const { id } = nextProps
    const { id: prevId } = this.props

    if (prevId === id && typeof prevId !== 'undefined') {
      return
    }

    this.loadDetailsForApi(id)
  }

  async fetchAPIDetails(id: string): Promise<IncomingApi> {
    return APIRepository.getById(id)
  }

  async loadDetailsForApi(id: string): Promise<void> {
    try {
      const response = await this.fetchAPIDetails(id)
      const details = modelFromAPIResponse(response)
      this.setState({ details, loaded: true })
    } catch (error) {
      this.setState({ error: true, loaded: true })
      console.error(error)
    }
  }

  render(): JSX.Element {
    const { details, error, loaded } = this.state

    return (
      <div className="LinkToAPIContainer">
        {!loaded ? null : error ? (
          <p data-test="error-message">
            Er ging iets fout tijdens het ophalen van de API.
          </p>
        ) : (
          <APISummary
            serviceName={details.serviceName}
            organization={details.organization}
            id={details.id}
            totalScore={details.totalScore}
            apiType={details.apiType}
          />
        )}
      </div>
    )
  }
}

export default LinkToAPIContainer
