// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shallow } from 'enzyme'
import { modelFromAPIResponse } from '../../../../../domain/models/api'
import { flushPromises } from '../../../../../test-helpers'
import { backendApiMock } from '../../../../../domain/models/api.mock'
import LinkToAPIContainer from './index'

const apiFromAPIResponse = {
  ...backendApiMock,
  id: '42',
  service_name: 'Service',
  organization_name: 'Organization',
}

describe('LinkToAPIContainer', () => {
  describe('displaying the API', () => {
    let wrapper
    beforeEach(() => {
      wrapper = shallow(<LinkToAPIContainer id="42" />)
      wrapper.setState({
        details: modelFromAPIResponse(apiFromAPIResponse),
        loaded: true,
      })
    })

    it('should show the LinkToAPI', () => {
      expect(wrapper.find('APISummary').exists()).toBe(true)
    })
  })

  describe('when an error occurred while fetching the API', () => {
    beforeAll(() => {
      jest
        .spyOn(global.console, 'error')
        .mockImplementationOnce(() => jest.fn())
    })
    it('should set the error state', async () => {
      jest
        .spyOn(LinkToAPIContainer.prototype, 'fetchAPIDetails')
        .mockRejectedValueOnce(
          new Error('arbitrary reject reason coming from tests'),
        )

      const wrapper = shallow<LinkToAPIContainer>(
        <LinkToAPIContainer id="42" />,
      )
      await flushPromises()

      expect(wrapper.state().error).toBe(true)
    })
  })

  describe('when the component is in the error state', () => {
    beforeAll(() => {
      jest
        .spyOn(global.console, 'error')
        .mockImplementationOnce(() => jest.fn())
    })
    it('an error message should be visible', () => {
      const wrapper = shallow(<LinkToAPIContainer id="42" />)
      wrapper.setState({ error: true, loaded: true })
      const noTagsMessageElement = wrapper.find('[data-test="error-message"]')
      expect(noTagsMessageElement.exists()).toBe(true)
    })
  })
})
