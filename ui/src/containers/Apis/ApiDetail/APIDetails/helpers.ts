// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { RELATION_TYPE_REFERENCE_IMPLEMENTATION } from '../../constants'

export const referenceImplementationsFromRelations = (
  relations: Record<string, string> = {},
): string[] => {
  return Object.keys(relations).filter((apiId) =>
    relations[apiId.toString()].includes(
      RELATION_TYPE_REFERENCE_IMPLEMENTATION,
    ),
  )
}
