// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
export const listItems = [
  {
    name: 'Documentatie',
    scoresProp: 'hasDocumentation',
  },
  {
    name: 'Specificatie',
    scoresProp: 'hasSpecification',
  },
  {
    name: 'Contactgegevens',
    scoresProp: 'hasContactDetails',
  },
  {
    name: 'SLA',
    scoresProp: 'providesSla',
  },
]
