// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Drawer } from '@commonground/design-system'

import { IconList, StyledDrawer } from '../styles'
import CheckmarkCircle from '../../../../../assets/icons/Circles/CheckmarkCircle'
import CrossCircle from '../../../../../assets/icons/Circles/CrossCircle'
import GradeBanner from '../GradeBanner'
import { ScoreExplanation } from './styles'
import { listItems } from './constants'

interface Props {
  scores: {
    hasDocumentation?: boolean
    hasSpecification?: boolean
    hasContactDetails?: boolean
    providesSla?: boolean
  }
  totalScore: {
    points: number
    maxPoints: number
  }
  parentUrl: string
}

const APIScoresPane: React.FunctionComponent<Props> = ({
  scores,
  totalScore,
  parentUrl,
}) => {
  const navigate = useNavigate()

  const close = () => navigate(parentUrl)

  return (
    <StyledDrawer closeHandler={close} data-testid="scores-pane">
      <Drawer.Header title="Opbouw API Score" closeButtonLabel="Sluit" />
      <Drawer.Content>
        <p>Deze score geeft de kwaliteit van de API weer.</p>

        <GradeBanner totalScore={totalScore} />

        <ScoreExplanation>
          De score is tot stand gekomen door de volgende onderdelen:
        </ScoreExplanation>

        <IconList>
          {listItems.map(({ name, scoresProp }) => (
            <IconList.ListItem key={scoresProp}>
              <IconList.ListItem.Icon>
                {scores[scoresProp.toString()] ? (
                  <CheckmarkCircle />
                ) : (
                  <CrossCircle />
                )}
              </IconList.ListItem.Icon>
              <IconList.ListItem.Content>
                {name} {!scores[scoresProp.toString()] ? 'niet' : ''} aanwezig
              </IconList.ListItem.Content>
            </IconList.ListItem>
          ))}
        </IconList>
      </Drawer.Content>
    </StyledDrawer>
  )
}

export default APIScoresPane
