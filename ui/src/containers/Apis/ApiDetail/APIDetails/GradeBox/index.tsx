// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import {
  GradeContainer,
  GradeHeader,
  StyledGrade,
  StyledLink,
  StyledLinkMobile,
} from './styles'

interface Props {
  apiId?: string
  totalScore: {
    points: number
    maxPoints: number
  }
  isDesignRulesScore?: boolean
}

const GradeBox: React.FunctionComponent<Props> = ({
  totalScore,
  isDesignRulesScore,
}) => {
  const scoreDescription = isDesignRulesScore
    ? 'Design Rule score'
    : 'API score'
  const linkTarget = 'score-detail'

  return (
    <GradeContainer>
      <GradeHeader>{scoreDescription}</GradeHeader>
      <StyledGrade totalScore={totalScore} $largeAtMediaQuery="smUp" />
      <StyledLinkMobile to={linkTarget}>
        Toon {scoreDescription} opbouw
      </StyledLinkMobile>
      <StyledLink to={linkTarget}>Toon opbouw</StyledLink>
    </GradeContainer>
  )
}

export default GradeBox
