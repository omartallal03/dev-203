// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { StyledErrorsTitle, StyledExclamationMark } from './styles'

interface Props {
  titleText: string
}

export const ErrorsTitle: React.FunctionComponent<Props> = ({ titleText }) => (
  <StyledErrorsTitle>
    <StyledExclamationMark />
    {titleText}
  </StyledErrorsTitle>
)
