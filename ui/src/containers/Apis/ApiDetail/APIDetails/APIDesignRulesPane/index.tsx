// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Drawer } from '@commonground/design-system'

import CheckmarkCircle from '../../../../../assets/icons/Circles/CheckmarkCircle'
import CrossCircle from '../../../../../assets/icons/Circles/CrossCircle'
import { IconList, StyledDrawer } from '../styles'
import GradeBanner from '../GradeBanner'
import {
  IntroSection,
  StyledLink,
  ExternalIcon,
  StyledIconListItem,
  DesignRuleTitle,
  DesignRuleDescription,
  CollapsibleContainer,
  ErrorsCollapsible,
  ErrorList,
  Error,
  DesignRuleLinkContainer,
} from './styles'
import { ErrorsTitle } from './helpers'

interface Props {
  designRuleScores: {
    results: {
      name: string
      description: string
      url: string
      success: boolean
      errors: string[]
    }[]
  } | null
  totalScore: {
    points: number
    maxPoints: number
  }
  parentUrl: string
}

const APIDesignRulesPane: React.FunctionComponent<Props> = ({
  designRuleScores,
  totalScore,
  parentUrl,
}) => {
  const navigate = useNavigate()

  const close = () => navigate(parentUrl)

  return (
    <StyledDrawer closeHandler={close} data-testid="design-rules-pane">
      <Drawer.Header title="Opbouw API Score" closeButtonLabel="Sluit" />
      <Drawer.Content>
        <p>Deze score geeft de kwaliteit van de API weer.</p>

        <GradeBanner totalScore={totalScore} />

        <IntroSection>
          De score geeft weer hoe compliant deze API is met de{' '}
          <StyledLink
            href="https://publicatie.centrumvoorstandaarden.nl/api/adr/1.0/"
            target="_blank"
            rel="noopener noreferrer"
          >
            API Design Rules&nbsp;1.0
          </StyledLink>
          . Dit zijn de afspraken die we gemaakt hebben over API design. Een
          aantal design rules controleren we automatisch:
        </IntroSection>

        <IconList>
          {designRuleScores?.results.map((rule, index) => {
            const hasErrors = rule.errors.length > 0
            const errorText = hasErrors
              ? `${rule.errors.length} bevinding${
                  rule.errors.length > 1 ? 'en' : ''
                } bij automatische test`
              : ''
            return (
              <StyledIconListItem key={index}>
                <IconList.ListItem.Icon>
                  {rule.success ? <CheckmarkCircle /> : <CrossCircle />}
                </IconList.ListItem.Icon>
                <IconList.ListItem.Content>
                  <DesignRuleTitle lang="en">{rule.name}</DesignRuleTitle>
                  <DesignRuleDescription lang="en">
                    {rule.description}
                  </DesignRuleDescription>
                  <StyledLink
                    href={rule.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    aria-label={`Ga naar Design Rule ${rule.name}`}
                  >
                    <DesignRuleLinkContainer>
                      <span>Ga naar Design Rule</span>
                      <ExternalIcon />
                    </DesignRuleLinkContainer>
                  </StyledLink>

                  {hasErrors ? (
                    <CollapsibleContainer>
                      <ErrorsCollapsible
                        title={<ErrorsTitle titleText={errorText} />}
                        ariaLabel={errorText}
                      >
                        <ErrorList>
                          {rule.errors.map((error, index) => (
                            <Error key={index}>{error}</Error>
                          ))}
                        </ErrorList>
                      </ErrorsCollapsible>
                    </CollapsibleContainer>
                  ) : null}
                </IconList.ListItem.Content>
              </StyledIconListItem>
            )
          })}
        </IconList>
      </Drawer.Content>
    </StyledDrawer>
  )
}

export default APIDesignRulesPane
