// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { Collapsible } from '@commonground/design-system'

import External from '../../../../../assets/icons/External'
import { ReactComponent as ExclamationMark } from '../../../../../assets/svg/exclamationmark.svg'
import { IconList } from '../styles'

export const IntroSection = styled.div``

export const StyledLink = styled.a``

export const ExternalIcon = styled(External)`
  margin-left: ${(p) => p.theme.tokens.spacing03};
`

export const StyledIconListItem = styled(IconList.ListItem)`
  margin-top: ${(p) => p.theme.tokens.spacing07};
`

export const DesignRuleTitle = styled.h3`
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  margin: 0;
`

export const DesignRuleDescription = styled.div`
  margin: ${(p) => p.theme.tokens.spacing03} 0px;
`

export const DesignRuleLinkContainer = styled.div`
  align-items: center;
  display: inline-flex;
`

export const CollapsibleContainer = styled.div`
  border: solid ${(p) => p.theme.tokens.colorPaletteGray400};
  border-width: 1px 0;
  margin-top: ${(p) => p.theme.tokens.spacing06};
  padding: ${(p) => p.theme.tokens.spacing04} 0px;
`

export const ErrorsCollapsible = styled(Collapsible)``

export const StyledErrorsTitle = styled.h3`
  align-items: center;
  display: flex;
  margin: 0;
`

export const StyledExclamationMark = styled(ExclamationMark)`
  margin-right: ${(p) => p.theme.tokens.spacing03};
`

export const ErrorList = styled.ul`
  margin-bottom: ${(p) => p.theme.tokens.spacing02};
  margin-top: ${(p) => p.theme.tokens.spacing05};
  padding-left: ${(p) => p.theme.tokens.spacing06};
`

export const Error = styled.li`
  color: ${(p) => p.theme.colorAlertError};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  list-style-type: none;

  &:not(:last-child) {
    margin-bottom: ${(p) => p.theme.tokens.spacing03};
  }
`
