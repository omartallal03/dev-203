// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import styled from 'styled-components'
import { Drawer } from '@commonground/design-system'
import { Container } from '../../../../components/design-system-candidates/Grid'
import mq from '../../../../theme/mediaQueries'
import Card from './Card'

export const Description = styled.p`
  margin: ${(p) => p.theme.tokens.spacing06} 0 0;

  ${mq.smUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing07};
  `}
`

type IconListComponent = React.FunctionComponent &
  Record<string, React.FunctionComponent> &
  Record<string, Record<string, React.FunctionComponent>>

export const IconList: IconListComponent = styled.ul`
  list-style-position: inside;
  list-style-type: none;
  padding-left: 0;
`

IconList.ListItem = styled.li`
  align-items: flex-start;
  display: flex;
  margin-top: ${(p) => p.theme.tokens.spacing03};
`

IconList.ListItem.Icon = styled.div`
  flex-shrink: 0;
  margin-right: ${(p) => p.theme.tokens.spacing03};
`

IconList.ListItem.Content = styled.div`
  padding-top: 3px;
`

// Quickfix the problem of other (relative/absolute) items overlapping the drawer,
// see https://gitlab.com/commonground/core/design-system/-/issues/33
export const StyledDrawer = styled(Drawer)`
  position: relative;
`

export const StyledContainer = styled(Container)`
  margin-bottom: ${(p) => p.theme.tokens.spacing05};
`

interface IntegrationContainerProps {
  twoColumn: boolean
}

export const IntegrationContainer = styled.div`
  column-gap: ${(p) => p.theme.tokens.spacing07};
  display: grid;
  grid-template-columns: ${({ twoColumn }: IntegrationContainerProps) =>
    twoColumn ? '50% 50%' : '100%'};
  margin-top: ${(p) => p.theme.tokens.spacing07};
  padding-right: ${(p) => p.theme.tokens.spacing07};

  ${mq.mdDown`
    grid-template-columns: 100%;
    padding-right: unset;
    row-gap: ${(p) => p.theme.tokens.spacing07};
  `}
`

export const StyledCardBody = styled(Card.Body)`
  padding-top: ${(p) => p.theme.tokens.spacing07};
  h3 {
    margin-bottom: ${(p) => p.theme.tokens.spacing04};
  }
`
