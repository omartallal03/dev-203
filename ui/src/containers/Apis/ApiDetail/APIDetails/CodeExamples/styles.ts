// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import { H1 } from '../../../../../components/Headings'

export const CodeExamplesContainer = styled.div``
export const StyledH1 = styled(H1)`
  margin-bottom: 1px;
`
export const StyledList = styled.div`
  padding: 0;
`
