// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { showDate } from '../../../../../helpers'
import { IncomingCode, ApiCode } from './types'

const whichRepo = (source: string): 'GitLab' | 'GitHub' => {
  return source.toLowerCase().includes('gitlab') ? 'GitLab' : 'GitHub'
}

export const mapApiCode = (code: IncomingCode): ApiCode => ({
  id: code.id,
  lastChange: showDate(code.last_change),
  name: code.name,
  ownerName: code.owner_name,
  programmingLanguages: code.programming_languages,
  source: whichRepo(code.source),
  stars: code.stars,
  url: code.url,
})
