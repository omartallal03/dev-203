// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { GitLabIcon } from '../../../../../../assets/icons/GitLabIcon'
import { GitHubIcon } from '../../../../../../assets/icons/GitHubIcon'
import StarIcon from '../../../../../../assets/icons/StarIcon'
import PillBadge from '../../../../../../components/PillBadge'
import {
  HistoryIcon,
  IconContainer,
  StyledDescription,
  StyledExternalIcon,
  StyledLastCommitMessage,
  StyledListItem,
  StyledStarsCount,
  StyledTags,
  StyledTitle,
} from './styles'

interface Props {
  repo: string
  title: string
  stars: number
  lastCommit: string
  tags: string[]
  url: string
}

const CodeExample: React.FunctionComponent<Props> = ({
  repo,
  title,
  stars,
  lastCommit,
  tags,
  url,
  ...rest
}) => {
  return (
    <StyledListItem
      href={url}
      target="_blank"
      rel="noopener noreferrer"
      {...rest}
    >
      <StyledTitle>{title}</StyledTitle>
      <IconContainer>
        {repo === 'GitLab' ? (
          <GitLabIcon color="#0B71A1" title="GitLab" />
        ) : (
          <GitHubIcon color="#0B71A1" title="GitHub" />
        )}
      </IconContainer>
      <StyledDescription>
        <StarIcon />
        <StyledStarsCount>{stars}</StyledStarsCount>
        <HistoryIcon />
        <StyledLastCommitMessage>{lastCommit}</StyledLastCommitMessage>
      </StyledDescription>
      <StyledTags>
        {tags.map((tag) => (
          <PillBadge key={tag}>{tag}</PillBadge>
        ))}
      </StyledTags>
      <StyledExternalIcon />
    </StyledListItem>
  )
}

export default CodeExample
