// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import ExternalIcon from '../../../../../../assets/icons/External'
import { ReactComponent as History } from '../../../../../../assets/svg/history.svg'
import mq from '../../../../../../theme/mediaQueries'

export const HistoryIcon = styled(History)`
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
  height: 16px;
  margin-right: 4px;
  width: 16px;
`
export const IconContainer = styled.div`
  align-self: center;
  grid-area: icon;
  height: ${(p) => p.theme.tokens.spacing07};
  justify-self: center;
  width: ${(p) => p.theme.tokens.spacing07};
  ${mq.xs`
    width: ${(p) => p.theme.tokens.spacing06};
    height: ${(p) => p.theme.tokens.spacing06};
  `}
`
export const StyledDescription = styled.small`
  align-items: center;
  align-self: center;
  display: flex;
  grid-area: description;
  > * {
    &:nth-child(2) {
      margin-left: ${(p) => p.theme.tokens.spacing02};
    }
    &:nth-child(3) {
      margin-left: ${(p) => p.theme.tokens.spacing05};
    }
  }
  ${mq.xs`
    > * {
      &:nth-child(2) {
        margin-left: ${(p) => p.theme.tokens.spacing01};
      }
      &:nth-child(3) {
        margin-left: 0;
      }
    }
  `}
`
export const StyledExternalIcon = styled(ExternalIcon)`
  align-self: center;
  grid-area: link;
  justify-items: end;
  margin-left: auto;
`
export const StyledLastCommitMessage = styled.span``
export const StyledListItem = styled.a`
  border-bottom: 1px solid ${(p) => p.theme.tokens.colorPaletteGray300};
  color: ${(p) => p.theme.colorText};
  display: grid;

  &:hover {
    background-color: ${(p) => p.theme.tokens.colorPaletteGray100};
  }

  &:hover,
  &:active {
    color: ${(p) => p.theme.colorText};
  }

  grid-template-areas:
    'icon title .'
    'icon description link'
    '. tags tags';
  grid-template-columns: 50px auto 50px;
  grid-template-rows: 1fr 2fr auto;
  min-height: 153px;

  padding: ${(p) => p.theme.tokens.spacing06} ${(p) => p.theme.tokens.spacing03};
  text-decoration: none;
  ${mq.xs`
    grid-template-columns: 30px auto 30px;
  `}
`
export const StyledStarsCount = styled.span`
  min-width: 1rem;
`
export const StyledTags = styled.div`
  display: grid;
  grid-area: tags;
  grid-column-gap: 8px;
  grid-row-gap: 8px;
  grid-template-columns: auto auto auto auto auto;
  grid-template-rows: auto;
  > * {
    padding: 0;
  }
  ${mq.md`
    grid-template-columns: minmax(0,100px) minmax(0,100px) minmax(0,100px) minmax(0,100px) minmax(0,100px);
  `}
  ${mq.xs`
    grid-template-columns: minmax(0,100px) minmax(0,100px) minmax(0,100px);
    grid-column-gap: 4px;
    grid-row-gap: 4px;
  `}
`
export const StyledTitle = styled.h3`
  align-self: center;
  color: #0b71a1;
  font-weight: 600;
  grid-area: title;
  margin: 0;
`
