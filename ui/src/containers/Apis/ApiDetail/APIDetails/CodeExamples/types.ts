// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import { IncomingProgrammingLanguages } from '../../../../../domain/models/types'

export interface IncomingCode {
  id: string
  last_change: string
  name: string
  owner_name: string
  programming_languages: IncomingProgrammingLanguages
  source: string
  stars: number
  url: string
}

export interface ApiCode {
  id: string
  lastChange: string
  name: string
  ownerName: string
  programmingLanguages: IncomingProgrammingLanguages
  source: string
  stars: number
  url: string
}
