// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { IncomingRelatedCode } from '../../../../../domain/models/types'
import { CodeExamplesContainer, StyledH1, StyledList } from './styles'
import CodeExample from './CodeExample'
import { mapApiCode } from './helpers'

interface Props {
  relatedCode: IncomingRelatedCode[]
}

const CodeExamples: React.FunctionComponent<Props> = ({ relatedCode }) => {
  const apiUsedByText =
    relatedCode?.length === 1
      ? `1 open source project gebruikt deze API`
      : `${relatedCode?.length || 0} open source projecten gebruiken deze API`

  return (
    <CodeExamplesContainer data-testid="code-examples">
      <StyledH1>Code</StyledH1>
      <small>{apiUsedByText}</small>

      <StyledList>
        {relatedCode?.map((relatedCode) => {
          const code = mapApiCode(relatedCode)
          return (
            <CodeExample
              data-testid="code-example"
              key={code.name}
              lastCommit={code.lastChange}
              repo={code.source}
              stars={code.stars}
              tags={Object.entries(code.programmingLanguages).map(
                ([programmingLanguage]) => programmingLanguage,
              )}
              title={code.name}
              url={code.url}
            />
          )
        })}
      </StyledList>
    </CodeExamplesContainer>
  )
}

export default CodeExamples
