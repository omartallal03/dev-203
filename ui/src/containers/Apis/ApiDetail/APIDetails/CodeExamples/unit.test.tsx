// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import {
  renderWithProviders,
  cleanup,
  screen,
} from '../../../../../test-helpers'
import CodeExamples from './index'

const relatedCode = [
  {
    id: 9,
    owner_name: 'PDOK',
    name: 'data.labs.pdok.nl',
    url: 'https://github.com/PDOK/data.labs.pdok.nl',
    last_change: '2019-12-13T09:56:21+01:00',
    stars: 11,
    source: 'github',
    programming_languages: {
      Python: 0.1,
      HTML: 0.1,
      CSS: 0.1,
      JavaScript: 0.1,
      Shell: 0.1,
      Ruby: 0.1,
    },
  },
  {
    id: 3,
    owner_name: 'GemeenteUtrecht',
    name: 'BInG',
    url: 'https://gitlab.com/GemeenteUtrecht/BInG',
    last_change: '2020-03-13T11:28:20+01:00',
    stars: 0,
    source: 'gitlab',
    programming_languages: {
      Python: 0.1,
      HTML: 0.1,
      CSS: 0.1,
      JavaScript: 0.1,
      Shell: 0.1,
      Dockerfile: 0.1,
      TSQL: 0.1,
    },
  },
]

describe('CodeExamples', () => {
  afterEach(() => {
    cleanup()
    jest.useRealTimers()
  })

  it('renders without crashing', () => {
    renderWithProviders(<CodeExamples relatedCode={[]} />)

    expect(screen.getByTestId('code-examples')).toHaveTextContent('Code')
  })

  describe('Projects using this API', () => {
    it('should should the correct text with a single project', () => {
      renderWithProviders(<CodeExamples relatedCode={[relatedCode[0]]} />)

      expect(screen.getByTestId('code-examples')).toHaveTextContent(
        '1 open source project gebruikt deze API',
      )
    })

    it('should should the correct text with multiple projects', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getByTestId('code-examples')).toHaveTextContent(
        '2 open source projecten gebruiken deze API',
      )
    })
  })

  describe('List', () => {
    it('should show a list of code examples', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.queryAllByTestId('code-example')).toHaveLength(2)
    })
  })

  describe('CodeExample', () => {
    it('should have the title', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getAllByTestId('code-example').at(0)).toHaveTextContent(
        'data.labs.pdok.nl',
      )
    })

    it('should have the number of stars', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getAllByTestId('code-example').at(0)).toHaveTextContent(
        '11',
      )
    })

    it('should have the last commit date', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getAllByTestId('code-example').at(0)).toHaveTextContent(
        'jaar geleden',
      )
    })

    it('should have the used code languages', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getAllByTestId('code-example').at(0)).toHaveTextContent(
        'PythonHTMLCSSJavaScriptShellRuby',
      )
    })

    it('should link to the project', () => {
      renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

      expect(screen.getAllByTestId('code-example').at(0)).toHaveAttribute(
        'href',
        'https://github.com/PDOK/data.labs.pdok.nl',
      )
    })

    describe('Repo icon', () => {
      it('should show a GitHub icon when on GH', () => {
        renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

        expect(screen.getAllByTestId('code-example').at(0)).toHaveTextContent(
          'GitHub',
        )
      })

      it('should show a GitLab icon when on GL', () => {
        renderWithProviders(<CodeExamples relatedCode={relatedCode} />)

        expect(screen.getAllByTestId('code-example').at(1)).toHaveTextContent(
          'GitLab',
        )
      })
    })
  })
})
