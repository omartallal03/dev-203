// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'

export const COLLAPSIBLE_CONTENT = [
  {
    title: 'Grondslag',
    content: (
      <p>
        VNG Realisatie BV verwerkt deze persoonsgegevens op basis van
        gerechtvaardigd belang.
      </p>
    ),
  },
  {
    title: 'Categorieën Persoonsgegevens',
    content: (
      <ul>
        <li>Github-account Naam</li>
        <li>Gebruikersnaam/namen</li>
        <li>Profielafbeelding</li>
        <li>IP-adres</li>
        <li>Gekoppelde apparaten t.b.v. tweefactorauthenticatie</li>
        <li>E-mailadres</li>
      </ul>
    ),
  },
  {
    title:
      'Geen ontvangers, geen doorgifte, geen profielen geen geautomatiseerdebesluitvorming',
    content: (
      <p>
        Bij Developer.overheid.nl is geen sprake van ontvangers van
        persoonsgegevens. De persoonsgegevens van Developer.overheid.nl worden
        niet buiten de Europese Economische Ruimte (EER) verwerkt. Er worden
        geen profielen opgesteld. Er vindt geen geautomatiseerde besluitvorming
        plaats.
      </p>
    ),
  },
  {
    title: 'Bewaartermijnen',
    content: (
      <p>
        De gegevens worden bewaard zolang noodzakelijk voor het doel van de
        verwerking.
      </p>
    ),
  },
]
