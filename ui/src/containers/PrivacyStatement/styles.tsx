// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import PageContentCard from '../Apis/AddApi/PageContentCard'

export const StyledPageTitle = styled.h1`
  color: #212121;
  font-size: 2rem;
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: 0;
`

export const StyledPageDescription = styled.h2`
  color: #212121;
  font-size: 1.5rem;
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  margin-top: 10px;
`

export const StyledPageContentCard = styled(PageContentCard)`
  margin-top: 3rem;
  padding-top: 0;

  abbr {
    border-bottom: 1px dotted #a3aabf;
    text-decoration: none;
  }
`

export const StyledList = styled.ul`
  list-style-type: '- ';
`

export const StyledH2 = styled.h2`
  &:first-of-type {
    margin: ${(p) =>
      `${p.theme.tokens.spacing09} 0 ${p.theme.tokens.spacing05}`};
  }
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeXLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: ${(p) => `${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing05}`};
`

export const StyledH3 = styled.h3`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightRegular};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: ${(p) => p.theme.tokens.spacing05} 0;
`

export const CollapsibleWrapper = styled.div`
  border-bottom: 1px solid ${(p) => p.theme.colorCollapsibleBorder};
  margin-top: ${(p) => p.theme.tokens.spacing06};

  > * {
    border-top: 1px solid ${(p) => p.theme.colorCollapsibleBorder};
  }
`

export const CollapsibleTitle = styled.h3`
  font-size: 1rem;
  font-weight: normal;
  margin: ${(p) => `${p.theme.tokens.spacing05} 0`};
`

export const CollapsibleBody = styled.p`
  margin: 0;
  padding-top: ${(p) => p.theme.tokens.spacing05};
`
