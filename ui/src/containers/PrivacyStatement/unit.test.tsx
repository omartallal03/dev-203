// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { Helmet } from 'react-helmet'
import {
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../test-helpers'
import PrivacyStatement from './'

describe('<PrivacyStatement />', () => {
  it('should render correctly', () => {
    renderWithProviders(<PrivacyStatement />)
    expect(screen.getByText('Privacyverklaring')).toBeInTheDocument()
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<PrivacyStatement />)
      expect(Helmet.peek().title).toEqual(
        'Developer Overheid: privacyverklaring',
      )
    })
  })
})
