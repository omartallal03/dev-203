// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

import mq from '../../theme/mediaQueries'
import { Container } from '../../components/design-system-candidates/Grid'
import Card from '../Apis/ApiDetail/APIDetails/Card'
import { ReactComponent as ExternalIcon } from '../../assets/svg/chevron-right.svg'
import ArrowRight from '../../assets/icons/ArrowRight'

export const StyledHomePage = styled(Container)`
  flex-direction: column;
  margin-bottom: ${(p) => p.theme.tokens.spacing09};

  h2 {
    margin-bottom: ${(p) => p.theme.tokens.spacing04};
  }

  ${mq.smUp`
    margin-bottom: ${(p) => p.theme.tokens.spacing11};
  `}

  ${mq.smDown`
    h2 {
      margin-bottom: ${(p) => p.theme.tokens.spacing02};
    }
  `}
`

export const StyledHeading = styled.div`
  margin-bottom: ${(p) => p.theme.tokens.spacing08};
  max-width: 728px;
`

export const StyledCard = styled(Card)`
  max-width: 471px;
`

export const StyledExternalIcon = styled(ExternalIcon)`
  margin-left: ${(p) => p.theme.tokens.spacing04};
`

export const StyledInternalIcon = styled(ArrowRight)`
  margin-left: ${(p) => p.theme.tokens.spacing04};
`

export const Links = styled.div`
  column-gap: ${(p) => p.theme.tokens.spacing06};
  display: grid;
  grid-template-columns: 50% 50%;
  margin-bottom: ${(p) => p.theme.tokens.spacing10};
  padding-right: ${(p) => p.theme.tokens.spacing06};

  ${mq.smDown`
    ${StyledCard}:nth-child(2) {
      margin-top: ${(p) => p.theme.tokens.spacing05};
    }
    grid-template-columns: 100%;
    padding-right: 0;
  `}
`

export const StyledErrorMessage = styled.p`
  margin-top: ${(p) => p.theme.tokens.spacing05};
`
