// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import React from 'react'
import RepositoryPlaceholder from '../../../assets/svg/repository-placeholder.svg'

interface Props extends React.ComponentProps<'img'> {
  alt: string
}

export const Image: React.FunctionComponent<Props> = ({
  alt,
  src = RepositoryPlaceholder,
  ...props
}) => {
  const handleError = (
    event: React.SyntheticEvent<HTMLImageElement, Event>,
  ) => {
    event.currentTarget.src = RepositoryPlaceholder
  }

  return (
    <img
      alt={alt}
      src={src}
      {...props}
      crossOrigin="anonymous"
      onError={handleError}
    />
  )
}
