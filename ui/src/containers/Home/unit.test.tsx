// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import * as React from 'react'
import { screen } from '@testing-library/react'
import { Helmet } from 'react-helmet'
import { mountWithProviders, renderWithProviders } from '../../test-helpers'
import Home from './index'

describe('<Home />', () => {
  describe('on initialization', () => {
    it('contains the page title', () => {
      renderWithProviders(<Home />)

      expect(
        screen.getByText(
          /Eén centrale plek voor de developer die voor of met de overheid ontwikkelt/,
        ),
      ).toBeInTheDocument()
    })
  })

  describe('when rendered', () => {
    it('should contain a link to the API overview', async () => {
      renderWithProviders(<Home />)

      expect(screen.getByText(/Bekijk API’s/)).toBeInTheDocument()
    })

    it('should contain a link to developer forum', async () => {
      renderWithProviders(<Home />)

      expect(screen.getByText(/Ga naar forum/)).toBeInTheDocument()
    })
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<Home />)

      expect(Helmet.peek().title).toEqual('Developer Overheid')
    })

    it('has the correct header for important pages', () => {
      renderWithProviders(<Home />)

      expect(
        screen.getByText(/\(direct naar\) veelbezochte pagina's/),
      ).toBeInTheDocument()
    })
  })
})
