// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Helmet } from 'react-helmet'
import { Container } from '../../components/design-system-candidates/Grid'
import { GITLAB_REPO_URL } from '../../constants'
import { StyledPageTitle, StyledPageContentCard } from './styles'

const About: React.FunctionComponent = () => (
  <Container>
    <Helmet>
      <title>Developer Overheid: over Developer Overheid</title>
      <meta
        name="description"
        content="De website developer.overheid.nl is een wegwijzer naar de API’s die (semi-)overheidsorganisaties in Nederland aanbieden."
      />
    </Helmet>

    <StyledPageTitle>Over Developer Overheid</StyledPageTitle>

    <StyledPageContentCard>
      <StyledPageContentCard.Body>
        <p>
          De website developer.overheid.nl is een portaal voor developers die
          voor of met de overheid software ontwikkelen.
        </p>
        <p>
          Deze website is ‘permanent beta’, en zal worden aangepast naar de
          behoeften van gebruikers. Wensen en opmerkingen kunnen via een{' '}
          <a
            href={`${GITLAB_REPO_URL}/issues`}
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Open een issue"
          >
            melding op GitLab
          </a>{' '}
          doorgegeven worden.
        </p>
        <p>
          Op de overzichtpagina's met{' '}
          <abbr title="Application Programming Interfaces">API’s</abbr> en Open
          Source repositories wordt software getoond welke gepubliceerd is onder
          verantwoordelijkheid van organisaties die opgenomen zijn in het{' '}
          <a
            href="src/containers/About/About"
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Bekijk overheidsorganisaties"
          >
            Register van Overheidsorganisaties
          </a>
          . Dit is een (voorlopig) objectief en eenvoudig filter om te bepalen
          of content al dan niet een plek krijgt.
        </p>
        <p>
          Dit portaal is een initiatief van het ministerie van Binnenlandse
          Zaken en Koninkrijksrelaties en de Vereniging van Nederlandse
          Gemeenten.
        </p>

        <h2>Verantwoordelijkheid</h2>
        <p>
          De informatie die op deze site wordt aangeboden is afkomstig van
          andere overheidsorganisaties. Deze organisaties zijn zelf
          verantwoordelijk voor hun diensten en gegevens.
        </p>

        <h2>Persoonsgegevens en cookies</h2>
        <p>
          Deze website verzamelt geen persoonsgegevens, zet geen cookies en
          verzamelt geen persoonsgebonden analytische informatie.
        </p>

        <h2>Toegankelijkheid</h2>
        <p>
          Deze site is ontworpen om te voldoen aan het Besluit digitale
          toegankelijkheid overheid: <i>Status: A - voldoet volledig</i>. <br />
          De toegankelijkheidsverklaring is in te zien via het{' '}
          <a
            href="https://www.toegankelijkheidsverklaring.nl/register/7029"
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Bekijk toegankelijkheidsverklaring"
          >
            Register van Toegankelijkheidsverklaringen
          </a>
          .
        </p>

        <h2>Open Source</h2>
        <p>
          De broncode van deze website is te vinden op{' '}
          <a
            href={GITLAB_REPO_URL}
            target="_blank"
            rel="noopener noreferrer"
            aria-label="Bekijk broncode"
          >
            GitLab
          </a>
          .
        </p>
      </StyledPageContentCard.Body>
    </StyledPageContentCard>
  </Container>
)

export default About
