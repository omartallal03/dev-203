// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'
import PageContentCard from '../Apis/AddApi/PageContentCard'

export const StyledPageTitle = styled.h1`
  color: ${(p) => p.theme.colorText};
  font-size: ${(p) => p.theme.tokens.fontSizeXXLarge};
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
`

export const StyledPageContentCard = styled(PageContentCard)`
  margin-top: ${(p) => p.theme.tokens.spacing07};

  abbr {
    border-bottom: 1px dotted #a3aabf;
    text-decoration: none;
  }
`
