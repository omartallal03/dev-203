// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { shallow } from 'enzyme'
import { Helmet } from 'react-helmet'
import { mountWithProviders } from '../../test-helpers'
import { StyledPageTitle } from './styles'
import About from './index'

describe('<About />', () => {
  it('contains the page title', () => {
    const wrapper = shallow(<About />)
    expect(wrapper.find(StyledPageTitle).exists()).toBe(true)
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<About />)
      expect(Helmet.peek().title).toEqual(
        'Developer Overheid: over Developer Overheid',
      )
    })
  })
})
