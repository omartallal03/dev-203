// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import styled from 'styled-components'

// Components
import PageContentCard from '../Apis/AddApi/PageContentCard'

// Theme
import mq from '../../theme/mediaQueries'

export const StyledPageTitle = styled.h1`
  color: #212121;
  font-size: 2rem;
  font-weight: ${(p) => p.theme.tokens.fontWeightBold};
  line-height: ${(p) => p.theme.tokens.lineHeightHeading};
  margin: 0;
`

export const StyledSubtitle = styled.p`
  font-size: ${(p) => p.theme.tokens.fontSizeMedium};
  margin-bottom: ${(p) => p.theme.tokens.spacing07};
`

export const StyledPageContentCard = styled(PageContentCard)`
  margin-top: 3rem;
  padding-top: 0;

  abbr {
    border-bottom: 1px dotted #a3aabf;
    text-decoration: none;
  }
`

export const StyledColumnLayout = styled.div`
  display: flex;
  gap: ${(p) => p.theme.tokens.spacing14};

  p {
    display: flex;
    flex-direction: column;

    ${mq.smDown`
      a {
        font-size: 0.875rem;
      }
    `}
  }

  ${mq.smDown`
    flex-direction: column;
    gap: 0;
  `}
`

export const StyledTable = styled.table`
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  table-layout: fixed;
  width: 100%;

  thead {
    tr {
      text-align: left;
      th:nth-child(1) {
        width: ${(p) => p.theme.tokens.spacing08};
      }
      th:nth-child(2) {
        width: auto;
      }
      th:nth-child(3) {
        width: 8rem;
      }
    }
  }

  ${mq.smDown`
    border: 0;
    
    thead {
      border: none;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }
    
    tr {
      display: block;
      margin-bottom: .625em;
    }
    
    td {
      display: block;
      text-align: left;
      a {
        font-size: 0.875rem;
      }
    }
    
    td::before {
      content: attr(data-label);
      float: left;
      font-weight: bold;
      margin-right: ${(p) => p.theme.tokens.spacing02};
    }
    
    td:last-child {
      border-bottom: 0;
    }
  `}
`
