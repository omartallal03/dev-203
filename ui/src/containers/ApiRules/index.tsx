// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
import React from 'react'
import { Helmet } from 'react-helmet'

// Components
import { Container } from '../../components/design-system-candidates/Grid'
import ExternalLink from '../../components/ExternalLink'

// Styles
import {
  StyledColumnLayout,
  StyledPageContentCard,
  StyledSubtitle,
  StyledTable,
} from './styles'

const APIRules: React.FunctionComponent = () => {
  return (
    <Container>
      <Helmet>
        <title>Developer Overheid: API Rules</title>
        <meta
          name="description"
          content="Geonovum is samen met Bureau Forum Standaardisatie, Kamer van Koophandel, VNG Realisatie, Logius en het Kadaster het Kennisplatform API's gestart. Gezien de ontwikkeling naar een digitale samenleving waar veel digitale diensten eenvoudig met elkaar moeten kunnen samenwerken, geloven wij dat de Nederlandse overheid baat heeft bij een Kennisplatform API’s, waarin we gezamenlijk kijken naar strategische en tactische vraagstukken rond het ontwikkelen van API’s door de overheid en gebruik van deze APIs buiten en binnen de overheid."
        />
      </Helmet>

      <h1>API Rules</h1>
      <StyledSubtitle>
        Geonovum is samen met Bureau Forum Standaardisatie, Kamer van
        Koophandel, VNG Realisatie, Logius en het Kadaster het Kennisplatform
        API's gestart. Gezien de ontwikkeling naar een digitale samenleving waar
        veel digitale diensten eenvoudig met elkaar moeten kunnen samenwerken,
        geloven wij dat de Nederlandse overheid baat heeft bij een
        Kennisplatform API’s, waarin we gezamenlijk kijken naar strategische en
        tactische vraagstukken rond het ontwikkelen van API’s door de overheid
        en gebruik van deze APIs buiten en binnen de overheid.
      </StyledSubtitle>
      <StyledPageContentCard>
        <StyledPageContentCard.Body>
          <div>
            <h3>Wat is een API?</h3>
            <p>
              <abbr title="Application Programming Interfaces">API’s</abbr>{' '}
              (Application Programming Interfaces) spelen een belangrijke rol op
              het Internet. Met behulp van een API kan je informatie makkelijker
              herbruikbaar maken voor programmeurs. Een API dient als interface
              tussen verschillende softwareprogramma's. Het zorgt ervoor dat een
              applicatie automatisch toegang krijgt tot bepaalde informatie
              en/of functionaliteiten. Een aantal aanbieders van open data bij
              de overheid, ontsluiten hun gegevens al met behulp van een API.
              Deze API’s leveren in veel gevallen een rechtstreekse “kopie” van
              de data, met alle complexiteit van dien.
            </p>
          </div>
          <div>
            <h3>Hoe testen we API’s?</h3>
            <p>
              De API’s worden getest aan de hand van 7 regels zoals omschreven
              in het document{' '}
              <ExternalLink
                href="https://publicatie.centrumvoorstandaarden.nl/api/adr/1.0/"
                title="de REST-API Design Rules (Nederlandse API
              Strategie IIa) 1.0"
              />
              . Op basis van de uitkomst van de testen wordt een score
              toegewezen aan een API. Hoe hoger de score, hoe beter de kwaliteit
              van de API!
              <br /> <br />
              Vanaf dinsdag 6 december is onze nieuwe API-tester live. Deze{' '}
              <abbr title="API Design Rules">ADR</abbr>-validator is geschreven
              in de programmeertaal Go. Met de ADR-validator worden de testen
              nauwkeuriger uitgevoerd. Onder andere design rule{' '}
              <abbr title="Publish OAS document at a standard location in JSON-format">
                API-51
              </abbr>{' '}
              wordt nu grondiger getest: het{' '}
              <abbr title="OpenAPI Specification">OAS</abbr>
              -document moet op een standaard locatie gepubliceerd zijn, anders
              wordt hij niet meegenomen in de validatie. Dit heeft als gevolg
              dat de scores van de meeste al-geregistreerde en
              nieuw-geregistreerde API’s naar beneden gaan. Een bijkomend
              voordeel is dat de ADR -validator het mogelijk maakt om tijdens
              het ontwikkelproces, zowel lokaal als in{' '}
              <abbr title="Continuous Integration">CI</abbr>, de API te testen.
              Ook kan je verbeteringen in de tester aanbrengen door een merge
              request te openen. Zo kan je de ADR-validator in je eigen omgeving
              afstemmen op je eigen behoefte!
              <br /> <br />
              Benieuwd naar de invloed van de nieuwe API-tester op jouw API?
              Test hem{' '}
              <ExternalLink
                href="https://gitlab.com/commonground/don/adr-validator"
                title="hier"
              />
              .
            </p>
          </div>
          <div>
            <h3>Waarom een kennisplatform API's?</h3>
            <p>
              Het Kennisplatform API's wil API's beter bij de vraag aan laten
              sluiten, kennis over het toepassen van API's uitwisselen en de
              aanpak bij verschillende organisaties op elkaar afstemmen en waar
              nodig standaardiseren. Deze wens hebben we vastgelegd in een
              manifest die door diverse organisaties is ondertekend waaronder
              het Ministerie van Binnenlandse Zaken en Koninkrijksrelaties,
              PinkRoccade, Vicrea, VNG Realisatie, Forum Standaardisatie,
              Logius, Geonovum, Centric, Digicosmos, APIwise en SWIS.{' '}
              <ExternalLink
                href="https://github.com/Geonovum/KP-APIs-Stuurgroep/blob/master/Overige%20stukken/Manifest%20Kennisplatform%20API's.pdf"
                title="Bekijk het manifest"
              />
              <br />
              <br />
              Een voorbeeld van waar binnen de overheid actief wordt gewerkt aan
              het mogelijk maken van interactie op basis van API’s is de
              Omgevingswet. Ambitie van de Omgevingswet is dat burgers of
              bedrijven bij een aanvraag van een omgevingsvergunning over
              dezelfde informatie beschikken als de overheid die de vergunning
              beoordeelt. In het bij elkaar brengen van deze informatie spelen
              API’s een rol. Bovendien moeten burgers en bedrijven niet alleen
              via een overheidswebsite kunnen communiceren, maar ook via apps op
              telefoons en tablets. Het moet voor iedereen mogelijk zijn om voor
              de Omgevingswet nieuwe applicaties te ontwikkelen. Om dit mogelijk
              te maken, moeten API’s met omgevingswetinformatie en
              -functionaliteit (niet enkel data) voor iedereen op een
              gebruiksvriendelijke manier beschikbaar komen. Er zijn dus twee
              soorten gebruikers om rekening mee te houden: de eindgebruiker en
              de softwareontwikkelaar. Het resultaat tot nu toe kan je bekijken
              en beproeven op het{' '}
              <ExternalLink
                href="https://aandeslagmetdeomgevingswet.nl/ontwikkelaarsportaal/api-register/"
                title="pre-omgevingswet ontwikkelaarsportaal."
              />
              <br />
              <br />
              In het Kennisplatform verkennen we in werkgroepen de verschillende
              aspecten rond het gebruik en toepassing van API's door de
              overheid.
            </p>
            <div>
              <h3>API Strategie</h3>
              <p>
                Het Kennisplatform API werkt aan de Nederlandse API Strategie.
                De volledige Nederlandse API Strategie bestaat uit:
              </p>
              <StyledTable>
                <thead>
                  <tr>
                    <th scope="col">Part</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">Link</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td data-label="Part">I</td>
                    <td data-label="Description">
                      General description of the API Strategy
                    </td>
                    <td data-label="Status">Informative</td>
                    <td data-label="Link">
                      <ExternalLink href="https://docs.geostandaarden.nl/api/API-Strategie/" />
                    </td>
                  </tr>
                  <tr>
                    <td data-label="Part">IIa</td>
                    <td data-label="Description">
                      Standard for designing APIs
                    </td>
                    <td data-label="Status">Normative</td>
                    <td data-label="Link">
                      <ExternalLink href="https://publicatie.centrumvoorstandaarden.nl/api/adr/" />
                    </td>
                  </tr>
                  <tr>
                    <td data-label="Part">IIb</td>
                    <td data-label="Description">
                      Extension on the Standard for designing APIs
                    </td>
                    <td data-label="Status">Informative</td>
                    <td data-label="Link">
                      <ExternalLink href="https://docs.geostandaarden.nl/api/API-Strategie-ext/" />
                    </td>
                  </tr>
                </tbody>
              </StyledTable>
              <br />
              <p>
                Begin 2022 hebben verschillende overheidsorganisaties een
                intentie overeenkomst ondertekend waarmee zij zich ervoor
                uitspreken om API’s te structureren conform de afspraken in de
                API Strategie.
              </p>
            </div>
          </div>
          <div>
            <h3>
              REST API Design rules en OAUTH profiel op Pas-toe-leg-uit lijst
            </h3>
            <p>
              Op 9 juli 2020 zijn twee standaarden goedgekeurd die zijn
              voorbereid door werkgroepen van het Kennisplatform API: de REST
              API Design Rules en NL GOV Assurance profile for OAuth 2.0.
              <br />
              <br />
              Beide standaarden staan op de lijst met{' '}
              <ExternalLink
                href="https://forumstandaardisatie.nl/open-standaarden"
                title="verplichte open standaarden van het Forum Standaardisatie."
              />{' '}
              Dit betekent dat de standaard{' '}
              <ExternalLink
                href="https://forumstandaardisatie.nl/open-standaarden/rest-api-design-rules"
                title="REST-API Design Rules"
              />{' '}
              moet worden toegepast bij het aanbieden van REST-API’s voor het
              ontsluiten van overheidsinformatie of functionaliteit. Het{' '}
              <ExternalLink
                href="https://forumstandaardisatie.nl/open-standaarden/nl-gov-assurance-profile-oauth-20"
                title="NL GOV OAuth-profiel"
              />{' '}
              moet worden toegepast wanneer rechthebbende gebruikers of
              'resource owners' van een applicatie via impliciete of expliciete
              toestemming een dienst van een derde toegang geven tot die
              applicatie om gegevens via een REST-API te gebruiken.
            </p>
          </div>
          <StyledColumnLayout>
            <div>
              <h2>API Standaarden</h2>
              <p>Logius beheert de volgende API-standaarden:</p>
              <p>
                <ExternalLink
                  href="https://publicatie.centrumvoorstandaarden.nl/api/oauth/"
                  title="NL GOV Assurance profile for OAuth 2.0"
                />
                <ExternalLink
                  href="https://publicatie.centrumvoorstandaarden.nl/api/adr/"
                  title="API Design Rules"
                />
                <ExternalLink
                  href="https://logius.gitlab.io/oidc/"
                  title="NL GOV Assurance profile for OpenID Connect (in ontwikkeling)"
                />
              </p>
            </div>
            <div>
              <h2>Digikoppeling</h2>
              <p>
                Logius beheert Digikoppeling. Onderstaande documenten vormen
                samen de Digikoppeling standaard:
              </p>
              <h3>Normatieve documenten</h3>
              <p>
                <ExternalLink
                  href="https://publicatie.centrumvoorstandaarden.nl/dk/restapi/"
                  title="Digikoppeling Koppelvlakstandaard REST API"
                />
              </p>
              <h3>Overige informatie</h3>
              <p>
                <ExternalLink
                  href="https://github.com/Logius-standaarden/Openbare-Consultaties"
                  title="Openbare Consultaties Digikoppeling en API standaarden (Github)"
                />
              </p>
            </div>
          </StyledColumnLayout>
        </StyledPageContentCard.Body>
      </StyledPageContentCard>
    </Container>
  )
}

export default APIRules
