// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//
import * as React from 'react'
import { Helmet } from 'react-helmet'
import {
  mountWithProviders,
  renderWithProviders,
  screen,
} from '../../test-helpers'
import APIRules from './index'

describe('<APIRules />', () => {
  it('has a page description', () => {
    renderWithProviders(<APIRules />)
    expect(
      screen.getByText(
        "Geonovum is samen met Bureau Forum Standaardisatie, Kamer van Koophandel, VNG Realisatie, Logius en het Kadaster het Kennisplatform API's gestart. Gezien de ontwikkeling naar een digitale samenleving waar veel digitale diensten eenvoudig met elkaar moeten kunnen samenwerken, geloven wij dat de Nederlandse overheid baat heeft bij een Kennisplatform API’s, waarin we gezamenlijk kijken naar strategische en tactische vraagstukken rond het ontwikkelen van API’s door de overheid en gebruik van deze APIs buiten en binnen de overheid.",
      ),
    ).toBeInTheDocument()
  })

  it.each([
    {
      level: 1,
      name: 'API Rules',
    },
    {
      level: 3,
      name: 'Wat is een API?',
    },
    {
      level: 3,
      name: 'Hoe testen we API’s?',
    },
    {
      level: 3,
      name: "Waarom een kennisplatform API's?",
    },
    {
      level: 3,
      name: 'API Strategie',
    },
    {
      level: 3,
      name: 'REST API Design rules en OAUTH profiel op Pas-toe-leg-uit lijst',
    },
    {
      level: 2,
      name: 'API Standaarden',
    },
    {
      level: 2,
      name: 'Digikoppeling',
    },
    {
      level: 3,
      name: 'Normatieve documenten',
    },
    {
      level: 3,
      name: 'Overige informatie',
    },
  ])('has a heading $level: $name', ({ level, name }) => {
    renderWithProviders(<APIRules />)

    expect(screen.getByRole('heading', { level, name })).toBeInTheDocument()
  })

  it.each([
    {
      name: 'de REST-API Design Rules (Nederlandse API Strategie IIa) 1.0',
      href: 'https://publicatie.centrumvoorstandaarden.nl/api/adr/1.0/',
    },
    {
      name: 'hier',
      href: 'https://gitlab.com/commonground/don/adr-validator',
    },
    {
      name: 'Bekijk het manifest',
      href: "https://github.com/Geonovum/KP-APIs-Stuurgroep/blob/master/Overige%20stukken/Manifest%20Kennisplatform%20API's.pdf",
    },
    {
      name: 'pre-omgevingswet ontwikkelaarsportaal.',
      href: 'https://aandeslagmetdeomgevingswet.nl/ontwikkelaarsportaal/api-register/',
    },
    {
      name: 'https://docs.geostandaarden.nl/api/API-Strategie/',
      href: 'https://docs.geostandaarden.nl/api/API-Strategie/',
    },
    {
      name: 'https://publicatie.centrumvoorstandaarden.nl/api/adr/',
      href: 'https://publicatie.centrumvoorstandaarden.nl/api/adr/',
    },
    {
      name: 'https://docs.geostandaarden.nl/api/API-Strategie-ext/',
      href: 'https://docs.geostandaarden.nl/api/API-Strategie-ext/',
    },
    {
      name: 'verplichte open standaarden van het Forum Standaardisatie.',
      href: 'https://forumstandaardisatie.nl/open-standaarden',
    },
    {
      name: 'REST-API Design Rules',
      href: 'https://forumstandaardisatie.nl/open-standaarden/rest-api-design-rules',
    },
    {
      name: 'NL GOV OAuth-profiel',
      href: 'https://forumstandaardisatie.nl/open-standaarden/nl-gov-assurance-profile-oauth-20',
    },
    {
      name: 'NL GOV Assurance profile for OAuth 2.0',
      href: 'https://publicatie.centrumvoorstandaarden.nl/api/oauth/',
    },
    {
      name: 'API Design Rules',
      href: 'https://publicatie.centrumvoorstandaarden.nl/api/adr/',
    },
    {
      name: 'NL GOV Assurance profile for OpenID Connect (in ontwikkeling)',
      href: 'https://logius.gitlab.io/oidc/',
    },
    {
      name: 'Digikoppeling Koppelvlakstandaard REST API',
      href: 'https://publicatie.centrumvoorstandaarden.nl/dk/restapi/',
    },
    {
      name: 'Openbare Consultaties Digikoppeling en API standaarden (Github)',
      href: 'https://github.com/Logius-standaarden/Openbare-Consultaties',
    },
  ])('has a link [$name]($href)', ({ name, href }) => {
    renderWithProviders(<APIRules />)

    const link = screen.getByRole('link', { name: `${name} Externe link` })

    expect(link).toHaveAttribute('href', href)
  })

  describe('a11y', () => {
    it('correctly sets the page title', () => {
      mountWithProviders(<APIRules />)
      expect(Helmet.peek().title).toEqual('Developer Overheid: API Rules')
      expect(Helmet.peek().metaTags).toEqual([
        {
          content:
            "Geonovum is samen met Bureau Forum Standaardisatie, Kamer van Koophandel, VNG Realisatie, Logius en het Kadaster het Kennisplatform API's gestart. Gezien de ontwikkeling naar een digitale samenleving waar veel digitale diensten eenvoudig met elkaar moeten kunnen samenwerken, geloven wij dat de Nederlandse overheid baat heeft bij een Kennisplatform API’s, waarin we gezamenlijk kijken naar strategische en tactische vraagstukken rond het ontwikkelen van API’s door de overheid en gebruik van deze APIs buiten en binnen de overheid.",
          name: 'description',
        },
      ])
    })
  })
})
