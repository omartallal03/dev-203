// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//
declare module '@commonground/design-system'
declare module '@commonground/design-system/dist/components/*'
declare global {
  interface Window {
    goatcounter?: any
  }
}
export default global
