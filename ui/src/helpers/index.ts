// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
//

import { formatAsTimeAgo } from '../utils/timeAgo'

export const showDate = (timestamp: string): string => {
  return formatAsTimeAgo(new Date(timestamp))
}

export const getRulesetsFromData = <Data>(data?: Data) => {
  return Object.keys(data || []).map((ruleset) => ({
    label: ruleset,
    value: ruleset,
  }))
}
