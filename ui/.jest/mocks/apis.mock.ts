export const apisMock = {
    page: 1,
    rowsPerPage: 10,
    totalResults: 49,
    results: [
        {
            id: 'cbs-odata',
            service_name: 'CBS OData',
            description:
                'Met behulp van de CBS OData API kan je Articles, Pages, Figures, Events en Flash data van de CBS website ophalen.',
            organization: {
                name: 'Directeur-generaal van de Statistiek',
                ooid: 28212129,
            },
            api_type: 'rest_json',
            api_authentication: 'unknown',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://www.cbs.nl/odata/v1/',
                    specification_url: '',
                    documentation_url:
                        'https://www.cbs.nl/nl-nl/onze-diensten/open-data/website-open-data',
                },
            ],
            contact: {
                email: '',
                phone: '',
                url: 'https://www.cbs.nl/nl-nl/over-ons/contact',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [
                {
                    id: 1,
                    owner_name: 'statistiekcbs',
                    name: 'CBS-Open-Data-v4',
                    url: 'https://github.com/statistiekcbs/CBS-Open-Data-v4',
                    avatar_url: null,
                    last_change: '2020-10-11T20:06:19+02:00',
                    stars: 13,
                    fork_count: 0,
                    issue_open_count: null,
                    merge_request_open_count: null,
                    source: 'GitHub repository',
                    programming_languages: ['R', 'Python'],
                    topics: [],
                },
                {
                    id: 2,
                    owner_name: 'statistiekcbs',
                    name: 'CBS-Open-Data-v3',
                    url: 'https://github.com/statistiekcbs/CBS-Open-Data-v3',
                    avatar_url: null,
                    last_change: '2020-08-15T19:08:49+02:00',
                    stars: 12,
                    fork_count: 0,
                    issue_open_count: null,
                    merge_request_open_count: null,
                    source: 'GitHub repository',
                    programming_languages: ['R', 'Python', 'C#', 'HTML'],
                    topics: [],
                },
            ],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '99.500000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: false,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:31.918000+01:00',
                percentage_score: '28.57',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-afval',
            service_name: 'Afvalcontainers en putten',
            description: 'Afvalcontainers en putten in Amsterdam',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'unknown',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/afval/',
                    specification_url:
                        'https://api.data.amsterdam.nl/afval/redoc/?format=openapi',
                    documentation_url: 'https://api.data.amsterdam.nl/afval/',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/afvalcontainers/issues',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [
                {
                    id: 4,
                    owner_name: 'pmoreira1',
                    name: 'amsterdam-project',
                    url: 'https://github.com/pmoreira1/amsterdam-project',
                    avatar_url: null,
                    last_change: '2020-04-21T17:26:17+02:00',
                    stars: 0,
                    fork_count: 0,
                    issue_open_count: null,
                    merge_request_open_count: null,
                    source: 'GitHub repository',
                    programming_languages: ['Python', 'Jupyter Notebook'],
                    topics: [],
                },
            ],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: null,
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:21:16.547000+01:00',
                percentage_score: '0.00',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-afvalophaalgebieden',
            service_name: 'Afvalregels bij locatie x, y',
            description:
                'Vind de afval ophaaldagen voor de opgegeven locatie (x/y of lat/lon) in Amsterdam.',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/afvalophaalgebieden/search/',
                    specification_url:
                        'https://api.data.amsterdam.nl/afvalophaalgebieden/openapi.yaml',
                    documentation_url: 'https://github.com/amsterdam/afvalophaalgebieden',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/amsterdam/afvalophaalgebieden/issues',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:21:21.359000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-bag',
            service_name: 'Basisregistratie Adressen en Gebouwen (BAG)',
            description:
                'De Basisregistratie Adressen en Gebouwen (BAG) bevat gegevens over panden, verblijfsobjecten, standplaatsen en ligplaatsen en de bijbehorende adressen en de benoeming van woonplaatsen en openbare ruimten.',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/bag/v1.1/',
                    specification_url:
                        'https://api.data.amsterdam.nl/bag/v1.1/docs/api-docs/bag/?format=openapi',
                    documentation_url: 'https://www.amsterdam.nl/stelselpedia/bag-index/',
                },
            ],
            contact: { email: 'datapunt@amsterdam.nl', phone: '', url: '' },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:15.158000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-bag-search',
            service_name: 'BAG Search',
            description: 'Zoekservice voor BAG, BRK en Amsterdamse Gebieden',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/atlas/search/',
                    specification_url:
                        'https://api.data.amsterdam.nl/bag/docs/api-docs/search/?format=openapi',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/bag/docs/api-docs/search%3Fformat%3Dopenapi',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/atlas',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:21:14.557000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-bbga',
            service_name: 'Basisbestand Gebieden Amsterdam (BBGA)',
            description: 'Specifieke functionaliteit voor de BBGA API.',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/bbga/',
                    specification_url:
                        'https://api.data.amsterdam.nl/bbga/docs/api-docs/?format=openapi',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/bbga/docs/api-docs/%3Fformat%3Dopenapi',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://data.amsterdam.nl/',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:34.562000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-brk',
            service_name: 'Basisregistratie Kadaster (BRK)',
            description:
                'De Basisregistratie kadaster (BRK) bevat informatie over percelen, eigendom, hypotheken, beperkte rechten (zoals recht van erfpacht, opstal en vruchtgebruik) en leidingnetwerken. Daarnaast staan er kadastrale kaarten in met perceel, perceelnummer, oppervlakte, kadastrale grens en de grenzen van het rijk, de provincies en gemeenten.',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'unknown',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/brk/',
                    specification_url:
                        'https://api.data.amsterdam.nl/bag/docs/api-docs/brk/?format=openapi',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/bag/docs/api-docs/brk%3Fformat%3Dopenapi',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/bag_services',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:15.500000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-dcatd',
            service_name: 'Datasetcatalogus',
            description: '-',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/dcatd/',
                    specification_url: 'https://api.data.amsterdam.nl/dcatd/openapi',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/dcatd/openapi',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/bag_services',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:46.563000+01:00',
                percentage_score: '57.14',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-gebieden',
            service_name: 'Basisregistratie Amsterdamse Gebieden',
            description:
                'In de Registratie gebieden worden de Amsterdamse stadsdelen, wijk (voorheen) buurtcombinaties, buurten en bouwblokken vastgelegd. Verder bevat de registratie gegevens van de grootstedelijke gebieden binnen de gemeente, de UNESCO werelderfgoedgrens en de gebieden van gebiedsgericht werken.',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/gebieden/',
                    specification_url:
                        'https://api.data.amsterdam.nl/bag/docs/api-docs/gebieden/?format=openapi',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/bag/docs/api-docs/gebieden%3Fformat%3Dopenapi',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/bag_services',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:12.734000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
        {
            id: 'gemeente-amsterdam-geosearch',
            service_name: 'Geosearch',
            description:
                'Een API om objecten uit verschillende andere datasets van de Gemeente Amsterdam mee op te zoeken op basis van een coördinaat en een straal',
            organization: {
                name: 'Gemeente Amsterdam',
                ooid: 25698,
            },
            api_type: 'rest_json',
            api_authentication: 'none',
            badges: [],
            environments: [
                {
                    name: 'production',
                    api_url: 'https://api.data.amsterdam.nl/geosearch/',
                    specification_url:
                        'https://api.data.amsterdam.nl/geosearch/docs/geosearch.yml',
                    documentation_url:
                        'https://api.data.amsterdam.nl/api/swagger/?url=/geosearch/docs/geosearch.yml',
                },
            ],
            contact: {
                email: 'datapunt@amsterdam.nl',
                phone: '',
                url: 'https://github.com/Amsterdam/geosearch',
            },
            is_reference_implementation: false,
            referenced_apis: [],
            related_repositories: [],
            terms_of_use: {
                government_only: false,
                pay_per_use: false,
                uptime_guarantee: '0.000000',
                support_response_time: null,
            },
            scores: {
                has_documentation: true,
                has_specification: true,
                has_contact_details: true,
                provides_sla: false,
            },
            design_rule_scores: {
                started_at: '2021-02-16T14:20:42.790000+01:00',
                percentage_score: '14.29',
                test_version: '09 Juli 2020',
                results: [],
            },
        },
    ],
    facets: {
        api_type: {
            terms: [
                { term: 'rest_json', count: 45 },
                { term: 'rest_xml', count: 1 },
                { term: 'unknown', count: 2 },
                { term: 'wfs', count: 1 },
            ],
        },
        organization_ooid: {
            terms: [
                {
                    term: '00000001812483297000',
                    display_name: 'Centraal Bureau voor de Statistiek',
                    count: 1,
                },
                {
                    term: '00000001802327497000',
                    display_name: 'Dienst voor het kadaster en de openbare registers',
                    count: 4,
                },
                {
                    term: '00000001817301409000',
                    display_name: 'Gemeenschappelijke Regeling Drechtsteden',
                    count: 1,
                },
                {
                    term: 25698,
                    display_name: 'Gemeente Amsterdam',
                    count: 21,
                },
                {
                    term: '00000001006033404000',
                    display_name: 'Kamer van Koophandel Nederland',
                    count: 2,
                },
                { term: '00000001822477348000', display_name: 'Logius', count: 2 },
                {
                    term: '00000001003214345000',
                    display_name:
                        'Ministerie van Binnenlandse Zaken en Koninkrijksrelaties',
                    count: 2,
                },
                {
                    term: '00000001821699180000',
                    display_name: 'Ministerie van IenW-Rijkswaterstaat',
                    count: 1,
                },
                {
                    term: '00000001002306608000',
                    display_name: 'Provincie Zuid-Holland',
                    count: 1,
                },
                { term: '00000001804770694000', display_name: 'RDW', count: 1 },
                {
                    term: '00000004000000062000',
                    display_name: 'Rijksinstituut voor Volksgezondheid en Milieu',
                    count: 2,
                },
                {
                    term: '00000004000000059000',
                    display_name: 'Uitvoeringsorganisatie Bedrijfsvoering Rijk (UBR)',
                    count: 2,
                },
                {
                    term: '00000001821002193000',
                    display_name: 'VNG Realisatie',
                    count: 9,
                },
            ],
        },
    },
}
