import { IncomingApiResponse, IncomingEvent } from '../../src/domain/models/types';

const results: Record<string, number | string>[] = []
for (let i = 1; i <= 20; i++) {
    const startDate =
        i < 10 ? `2021-02-0${i}T13:30:00+01:00` : `2021-02-22T${i}:30:00+01:00`

    results.push({
        id: i,
        title: `Event ${i}`,
        start_date: startDate,
        location: 'Den Bosch',
        registration_url: 'https://www.meetup.com',
    })
}

export const eventsMock = {
    page: 1,
    rowsPerPage: 10,
    totalResults: 20,
    results,
} as unknown as IncomingApiResponse<IncomingEvent[]>
