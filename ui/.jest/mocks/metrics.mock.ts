import { IncomingMetrics } from '../../src/domain/models/types';

export const metricsMock: IncomingMetrics =
    {
        "organization": {
            "total_counts": {
                "2022-08-01": 18,
                "2022-08-02": 18
            }
        },
        "api": {
            "total_counts": {
                "2022-08-01": 101,
                "2022-08-02": 101
            }
        },
        "repository": {
            "total_counts": {
                "2022-08-01": 114,
                "2022-08-02": 114
            }
        },
        "design_rules_last": {
            "09_juli_2020": {
                "API-03": {
                    "success_count": 2,
                    "total_count": 10
                },
                "API-16": {
                    "success_count": 2,
                    "total_count": 10
                },
                "API-20": {
                    "success_count": 6,
                    "total_count": 10
                },
                "API-48": {
                    "success_count": 2,
                    "total_count": 10
                },
                "API-51": {
                    "success_count": 1,
                    "total_count": 10
                },
                "API-56": {
                    "success_count": 2,
                    "total_count": 10
                },
                "API-57": {
                    "success_count": 2,
                    "total_count": 10
                }
            }
        },
        "design_rules_api_successes_last": {
            "09_juli_2020": {
                "0/7": 4,
                "1/7": 4,
                "2/7": 0,
                "3/7": 0,
                "4/7": 0,
                "5/7": 0,
                "6/7": 1,
                "7/7": 1
            }
        }
    }
