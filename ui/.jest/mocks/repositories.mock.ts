export const repositoriesMock = {
    page: 1,
    rowsPerPage: 10,
    totalResults: 11,
    results: [
        {
            id: 1,
            owner_name: 'statistiekcbs',
            name: 'CBS-Open-Data-v4',
            url: 'https://github.com/statistiekcbs/CBS-Open-Data-v4',
            avatar_url: null,
            last_change: '2020-10-11T20:06:19+02:00',
            stars: 13,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['R', 'Python'],
            topics: [],
            related_apis: [
                {
                    service_name: 'CBS OData',
                    organization_name: 'Centraal Bureau voor de Statistiek',
                    api_id: 'cbs-odata',
                },
            ],
        },
        {
            id: 2,
            owner_name: 'statistiekcbs',
            name: 'CBS-Open-Data-v3',
            url: 'https://github.com/statistiekcbs/CBS-Open-Data-v3',
            avatar_url: null,
            last_change: '2020-08-15T19:08:49+02:00',
            stars: 12,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['R', 'Python', 'C#', 'HTML'],
            topics: [],
            related_apis: [
                {
                    service_name: 'CBS OData',
                    organization_name: 'Centraal Bureau voor de Statistiek',
                    api_id: 'cbs-odata',
                },
            ],
        },
        {
            id: 9,
            owner_name: 'PDOK',
            name: 'data.labs.pdok.nl',
            url: 'https://github.com/PDOK/data.labs.pdok.nl',
            avatar_url: null,
            last_change: '2019-12-13T09:56:21+01:00',
            stars: 11,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: [
                'Python',
                'HTML',
                'CSS',
                'JavaScript',
                'Shell',
                'Ruby',
            ],
            topics: [],
            related_apis: [
                {
                    service_name: 'Basisregistratie Adressen en Gebouwen (BAG)',
                    organization_name:
                        'Dienst voor het kadaster en de openbare registers',
                    api_id: 'kadaster-bag',
                },
                {
                    service_name: 'Basisregistratie Topografie (BRT)',
                    organization_name:
                        'Dienst voor het kadaster en de openbare registers',
                    api_id: 'kadaster-brt',
                },
                {
                    service_name: 'Basisregistratie Kadaster (BRK)',
                    organization_name:
                        'Dienst voor het kadaster en de openbare registers',
                    api_id: 'kadaster-brk',
                },
            ],
        },
        {
            id: 10,
            owner_name: 'commonground',
            name: 'developer.overheid.nl',
            url: 'https://gitlab.com/commonground/developer.overheid.nl',
            avatar_url: null,
            last_change: '2020-11-03T13:12:17.051000+01:00',
            stars: 9,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'gitlab',
            programming_languages: [
                'Python',
                'HTML',
                'JavaScript',
                'Dockerfile',
                'Go',
            ],
            topics: [],
            related_apis: [],
        },
        {
            id: 11,
            owner_name: 'AERIUS',
            name: 'AERIUS',
            url: 'https://gitlab.com/AERIUS/AERIUS',
            avatar_url: null,
            last_change: '2020-10-21T12:24:51.454000+02:00',
            stars: 5,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'gitlab',
            programming_languages: [
                'JavaScript',
                'TSQL',
                'Java',
                'PLpgSQL',
                'Gherkin',
            ],
            topics: [],
            related_apis: [
                {
                    service_name: 'AERIUS Connect',
                    organization_name: 'Rijksinstituut voor Volksgezondheid en Milieu',
                    api_id: 'rivm-aerius-connect',
                },
            ],
        },
        {
            id: 12,
            owner_name: 'Signalen',
            name: 'backend',
            url: 'https://github.com/Signalen/backend',
            avatar_url: null,
            last_change: '2020-11-03T09:22:40+01:00',
            stars: 2,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['Dockerfile', 'Python', 'HTML', 'Shell'],
            topics: [],
            related_apis: [
                {
                    service_name: 'Basisregistratie Topografie (BRT)',
                    organization_name:
                        'Dienst voor het kadaster en de openbare registers',
                    api_id: 'kadaster-brt',
                },
            ],
        },
        {
            id: 7,
            owner_name: 'ProvZH',
            name: 'stikstof',
            url: 'https://github.com/ProvZH/stikstof',
            avatar_url: null,
            last_change: '2020-03-12T12:58:54+01:00',
            stars: 0,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['Python', 'Jupyter Notebook'],
            topics: [],
            related_apis: [
                {
                    service_name: 'AERIUS Connect',
                    organization_name: 'Rijksinstituut voor Volksgezondheid en Milieu',
                    api_id: 'rivm-aerius-connect',
                },
            ],
        },
        {
            id: 6,
            owner_name: 'r5atom',
            name: 'Saturday-Datascience',
            url: 'https://github.com/r5atom/Saturday-Datascience',
            avatar_url: null,
            last_change: '2020-02-07T21:15:50+01:00',
            stars: 0,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['Jupyter Notebook'],
            topics: [],
            related_apis: [
                {
                    service_name: 'Gekentekende voertuigen',
                    organization_name: 'RDW',
                    api_id: 'rdw-brv',
                },
            ],
        },
        {
            id: 5,
            owner_name: 'dmitrychebayewski',
            name: 'airrotterdam-api',
            url: 'https://github.com/dmitrychebayewski/airrotterdam-api',
            avatar_url: null,
            last_change: '2020-10-26T21:17:48+01:00',
            stars: 0,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['Kotlin'],
            topics: [],
            related_apis: [
                {
                    service_name: 'Luchtmeetnet',
                    organization_name: 'Rijksinstituut voor Volksgezondheid en Milieu',
                    api_id: 'luchtmeetnet-luchtmeetnet',
                },
            ],
        },
        {
            id: 4,
            owner_name: 'pmoreira1',
            name: 'amsterdam-project',
            url: 'https://github.com/pmoreira1/amsterdam-project',
            avatar_url: null,
            last_change: '2020-04-21T17:26:17+02:00',
            stars: 0,
            fork_count: 0,
            issue_open_count: null,
            merge_request_open_count: null,
            source: 'github',
            programming_languages: ['Python', 'Jupyter Notebook'],
            topics: [],
            related_apis: [
                {
                    service_name: 'Afvalcontainers en putten',
                    organization_name: 'Gemeente Amsterdam',
                    api_id: 'gemeente-amsterdam-afval',
                },
            ],
        },
    ],
    facets: {
        programming_languages: {
            terms: [
                { term: 3, display_name: 'C#', count: 1 },
                { term: 5, display_name: 'CSS', count: 2 },
                { term: 8, display_name: 'Dockerfile', count: 3 },
                { term: 16, display_name: 'Gherkin', count: 1 },
                { term: 13, display_name: 'Go', count: 1 },
                { term: 4, display_name: 'HTML', count: 5 },
                { term: 14, display_name: 'Java', count: 1 },
                { term: 6, display_name: 'JavaScript', count: 4 },
                { term: 10, display_name: 'Jupyter Notebook', count: 3 },
                { term: 11, display_name: 'Kotlin', count: 1 },
                { term: 15, display_name: 'PLpgSQL', count: 1 },
                { term: 2, display_name: 'Python', count: 8 },
                { term: 1, display_name: 'R', count: 2 },
                { term: 12, display_name: 'Ruby', count: 1 },
                { term: 7, display_name: 'Shell', count: 3 },
                { term: 9, display_name: 'TSQL', count: 2 },
            ],
        },
        topics: { terms: [] },
    },
    programmingLanguages: [
        { id: 3, name: 'C#' },
        { id: 5, name: 'CSS' },
        { id: 8, name: 'Dockerfile' },
        { id: 16, name: 'Gherkin' },
        { id: 13, name: 'Go' },
        { id: 4, name: 'HTML' },
        { id: 14, name: 'Java' },
        { id: 6, name: 'JavaScript' },
        { id: 10, name: 'Jupyter Notebook' },
        { id: 11, name: 'Kotlin' },
        { id: 15, name: 'PLpgSQL' },
        { id: 2, name: 'Python' },
        { id: 1, name: 'R' },
        { id: 12, name: 'Ruby' },
        { id: 7, name: 'Shell' },
        { id: 9, name: 'TSQL' },
    ],
}
