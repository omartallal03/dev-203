// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
//

const isDev = process.env?.NODE_ENV === 'development'

module.exports = {
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: [
    'header',
    'jest',
    'import',
    'prettier',
    'jsx-a11y',
    'better-styled-components',
  ],
  extends: [
    'react-app',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'plugin:security/recommended',
    'plugin:jsx-a11y/strict',
  ],
  rules: {
    // generic
    'no-console': isDev
      ? ['warn', { allow: ['error', 'warn'] }]
      : ['error', { allow: ['error', 'warn'] }],
    // prettier
    'prettier/prettier': [
      'error',
      {
        tabWidth: 2,
        useTabs: false,
        semi: false,
        singleQuote: true,
        trailingComma: 'all',
        arrowParens: 'always',
      },
    ],
    // header
    'header/header': [
      2,
      'line',
      [
        {
          pattern: ' Copyright © VNG Realisatie \\d{4}$',
          template: ' Copyright © VNG Realisatie 2022',
        },
        ' Licensed under the EUPL',
        '',
      ],
    ],
    // react
    'react/no-unsafe': 'warn',
    'react/forbid-prop-types': ['error', { forbid: ['any'] }],
    'react/jsx-handler-names': 'error',
    'react/jsx-pascal-case': ['error', { allowAllCaps: true }],
    'react/sort-comp': 'error',
    'react/no-unescaped-entities': 0,
    // import
    'import/order': 'error',
    'import/first': 'error',
    'import/no-amd': 'error',
    'import/no-webpack-loader-syntax': 'error',
    // jest
    'jest/consistent-test-it': [
      'error',
      {
        fn: 'test',
        withinDescribe: 'it',
      },
    ],
    'jest/expect-expect': [
      'error',
      {
        assertFunctionNames: ['expect'],
      },
    ],
    'jest/no-jasmine-globals': 'error',
    'jest/no-done-callback': 'error',
    'jest/prefer-to-contain': 'error',
    'jest/prefer-to-have-length': 'error',
    'jest/valid-describe-callback': 'error',
    'jest/valid-expect-in-promise': 'error',
    // security
    'security/detect-object-injection': 'off', // https://github.com/nodesecurity/eslint-plugin-security/issues/21
    // DON specific
    '@typescript-eslint/no-unused-vars': isDev ? 'warn' : 'error',
    'react/prop-types': 'off',
    'better-styled-components/sort-declarations-alphabetically': 2,
  },
  overrides: [
    {
      files: ['*.test.js', ',*.test.jsx', '*.test.ts', ',*.test.tsx'],
      rules: {
        'react/display-name': 0,
      },
    },
  ],
}
