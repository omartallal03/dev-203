const { terminalLog, sizes, config } = require("../support/e2e")

describe('API Overview', () => {

  beforeEach(() => {
    cy.viewport(sizes[0][0], sizes[0][1])
    cy.visit('/apis')
  })

  it('should show the page title', () => {
    cy.get('h1').contains("API's binnen de Nederlandse overheid")
  })

  it('should have an add API button', () => {
    const button = cy.contains('API toevoegen')
    button.click({force: true})
    cy.url().should('include', '/apis/add/form')
  })

  it('should have a search API field', () => {
    const input = cy.get('input')
    input.type("xxx")
    cy.contains("Er zijn (nog) geen API's beschikbaar.")
  })

  it('should have filters for API type', () => {
    cy.get('[type="checkbox"]').first().click({force: true})
    cy.url().should('include', '/apis?type=')
  })

  it('should have filters for organisation', () => {
    const checkbox = cy.get('[id="organization_ooid.0"]')
    checkbox.click()

    checkbox.parent().then(function (elem) {
      const regExNum = /^.*?\([^\d]*(\d+)[^\d]*\).*$/
      const number = regExNum.exec(elem.text())[1]

      cy.contains(`${number} API`)

      cy.url().should('include', `organisatie=`)
    })
  })

  it(`should have a list of API's`, () => {
    cy.get('[data-test="link"]').first().as("link")
    cy.get("@link").screenshot()
  })

  context('a11y', async () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/apis')
        cy.screenshot().toMatchImageSnapshot();

        cy.get('[data-test="link"]')
        cy.screenshot()

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
