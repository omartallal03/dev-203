const { terminalLog, sizes, config } = require("../support/e2e")

describe('Submit API', () => {
  beforeEach(() => {
    cy.visit('/apis/add')
  })

  it('should show the page title', () => {
    cy.get('h1').contains('API toevoegen')
  })

  it('should have add by form', () => {
    cy.intercept('POST', '/api/submit-api', { fixture: 'submit-api.json' })

    cy.contains('Toevoegen via formulier').as('form')
    cy.get('@form').screenshot()
    cy.get('#serviceName').type("API naam")
    cy.get('#description').type("API description")
    cy.get('#organization').click()
    cy.get('#react-select-2-option-0').click();
    cy.get('#apiType').select('REST/JSON').should('have.value', 'rest_json')
    cy.get('#apiAuthentication-none').click()
    cy.get('#productionApiUrl').type("https://productionApiUrl.don.url")
    cy.get('#productionSpecificationUrl').type("https://productionSpecificationUrl.don.url")
    cy.get('#productionDocumentationUrl').type("https://productionDocumentationUrl.don.url")
    cy.get('#hasAcceptanceEnvironment').click()
    cy.get('#acceptanceApiUrl').type("https://acceptanceApiUrl.don.url")
    cy.get('#acceptanceSpecificationUrl').type("https://acceptanceSpecificationUrl.don.url")
    cy.get('#acceptanceDocumentationUrl').type("https://acceptanceDocumentationUrl.don.url")
    cy.get('#hasDemoEnvironment').click()
    cy.get('#demoApiUrl').type("https://demoApiUrl.don.url")
    cy.get('#demoSpecificationUrl').type("https://demoSpecificationUrl.don.url")
    cy.get('#demoDocumentationUrl').type("https://demoDocumentationUrl.don.url")
    cy.get('#contactEmail').type("john.doe@test.com")
    cy.get('#contactPhone').type("+11123456789")
    cy.get('#contactUrl').type("https://contact.me")
    cy.get('#isBasedOnReferenceImplementation').click()
    cy.get('#referenceImplementation').select('Zaaktypecatalogus (ZTC) Vereniging van Nederlandse Gemeenten').should('have.value', 'vng-ztc')
    cy.get('#termsOfUseGovernmentOnly').click()
    cy.get('#termsOfUsePayPerUse').click()
    cy.get('#termsOfUseUptimeGuarantee').type("{backspace}{backspace}99.99")
    cy.get('#termsOfUseSupportResponseTime').type("1")
    cy.get('button').parent().screenshot()
    cy.get('button').parent().toMatchImageSnapshot();
    cy.get('button').contains('API toevoegen').click()
    cy.screenshot('submit-api-filled-form')
    cy.contains('De API is toegevoegd. Wij zullen deze zo snel mogelijk nakijken.').screenshot()
  })

  it('should have add by MR', () => {
    cy.contains('Via Merge Request').as('MR')
    cy.get('@MR').screenshot()
    cy.get('@MR').toMatchImageSnapshot();
    cy.get('@MR').click()
    cy.contains('data/{organization}-{api}')
    cy.contains('content/api/{organization}-{api}.yaml')
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/apis/add')
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })

})
