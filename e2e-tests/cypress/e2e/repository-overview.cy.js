const { terminalLog, sizes, config } = require("../support/e2e")

describe('Repository Overview', () => {
  beforeEach(() => {
    cy.visit('/repositories')
  })

  it('should show the page title', () => {
    cy.get('h1').contains('Open source repositories')
  })

  it('should have a search project field', () => {
    const input = cy.get('input')
    input.type("xxx")

    cy.url().should('include', '/repositories?q=xxx')
  })

  it('should have filters for technology', () => {
    cy.get('[type="checkbox"]').first().click()
    cy.url().should('include', '/repositories?technologie=')
  })

  it('should have a list of projects', () => {
    cy.get('[data-test="link"]').first().as("link")
    cy.get("@link").screenshot()
  })

  context('a11y', () => {
    sizes.forEach(size => {
      it(`${size.toString().replace(",", "x")}: has no detectable a11y violations on load`, () => {
        if (Cypress._.isArray(size)) {
          cy.viewport(size[0], size[1])
        } else {
          cy.viewport(size)
        }
        cy.visit('/repositories')
        cy.screenshot().toMatchImageSnapshot();

        cy.injectAxe()
        // Test the page at initial load
        cy.checkA11y(null, config, terminalLog)
      })
    })
  })
})
